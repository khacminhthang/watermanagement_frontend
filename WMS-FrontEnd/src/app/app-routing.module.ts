import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AdminRoutingName } from "src/app/routes/routes-name";
import {LoginComponent} from "src/app/features/public/layout/login/login.component";
import {AuthGuard} from "src/app/auth/auth.guard";
import { CoreRoutingName } from 'src/app/routes/core-routes-name';

const appRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "",
        loadChildren: () =>
        import("src/app/features/public/public.module").then(
          (mod) => mod.PublicModule
        )
      },
      {
        path: "login",
        component: LoginComponent
      }
    ]
  },
  {
    path: AdminRoutingName.adminUri,
    canActivate: [AuthGuard],
    children: [
      {
        path: "",
        loadChildren: () =>
        import("src/app/features/admin/admin.module").then(
          (mod) => mod.AdminModule
        )
      }
    ]
  },
  {
    path: "**",
    redirectTo: `${CoreRoutingName.ErrorUri}/${CoreRoutingName.NotFoundUri}`,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
