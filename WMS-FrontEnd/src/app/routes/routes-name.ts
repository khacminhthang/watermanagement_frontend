export class AdminRoutingName {
  // Các routes cấp 1
  public static danhmucUri = "danhmuc";
  public static wrUri = "tnnuoc";
  public static adminUri = "admin";
  public static publicUri = "public";
  public static baocaoUri = "baocao";
  public static thuvienUri = "thuvien";
  public static hethongUri = "hethong";
  // Các routes cấp 2
  // 1.1. Routes phần danh mục
  public static canhanUri = "canhan";
  public static nhomthamsoUri = "nhomthamso";
  public static dvhcUri = "dvhc";
  public static congtyUri = "congty";
  public static coquanUri = "coquan";
  public static tangchuanuocUri = "tangchuanuoc";
  public static tieuchuanUri = "tieuchuan";
  public static thamsoUri = "thamso";
  public static tcclUri = "tccl";
  public static duanUri = "duan";
  public static loaisolieuUri = "loaisolieu";
  public static donvidoUri = "donvido";

  // 1.2. Routes phần tài nguyên nước
  public static swUri = "nuocmat";
  public static gwUri = "nuocduoidat";
  public static wellsUri = "giengkhoan";
  public static gwexUri = "khaithacndd";
  public static wellexUri = "giengkhaithac";
  public static swexUri = "khaithacnm";
  public static swTramBomUri = "trambom"
  public static diUri = "xathai";
  public static pdiUri = "diemxathai";
  public static gwStationUri = "stations";
  public static swDiemkhaithac = "diemkhaithacnm";
  public static swTrambom = "dstrambom";



  // 1.3 Routes báo cáo
  public static chartsUri = "charts";

  // 1.4 Routes hệ thống
  public static quanlyRole = "quanlyrole";
  public static quanlyUser = "quanlyuser";
}
