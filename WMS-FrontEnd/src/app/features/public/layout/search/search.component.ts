import {Component, Inject, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {CommonFacadeService} from "src/app/service/admin/common/common-facade.service";
import {MatdialogService} from "src/app/core/services/utilities/matdialog.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  searchForm: FormGroup;
  public allDataSearch: any;
  errorForm = '';
  constructor(private formBuilder: FormBuilder,
              public dialogRef: MatDialogRef<SearchComponent>,
              @Inject(MAT_DIALOG_DATA) public dataGetIO: any,
              public commonFacadeService: CommonFacadeService,
              public imDialogService: MatdialogService,
              private modalDialog: MatDialog) { }

  ngOnInit() {
    this.formInit();
  }

  // init FormControl
  formInit() {
    this.searchForm = this.formBuilder.group({
      loaicongtrinh: ["", Validators.required],
      tukhoa: ["", Validators.required],
    });
  }

  async search() {
    if (this.searchForm.valid) {
      this.errorForm = '';
      let key: any = this.searchForm.value.tukhoa;
      console.log("SearchComponent -> search -> key", key)
      let ObjKey: any = this.searchForm.value.loaicongtrinh;
      console.log("SearchComponent -> search -> ObjKey", ObjKey)
      

      this.allDataSearch = await this.commonFacadeService
        .getSearchService()
        .getFetchAll({query: key, objKey: ObjKey});
    } else {
      this.errorForm = 'Bạn phải nhập đầy đủ thông tin';
    }
  }

  openToaDo(data: any) {
    this.imDialogService.dataResult = data;
    this.dialogRef.close(`Oke`);
  }

  closeDialog() {
    this.dialogRef.close(`Oke`);
  }
}
