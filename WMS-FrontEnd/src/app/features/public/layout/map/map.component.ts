import {
  Component,
  OnInit,
  ComponentFactoryResolver,
  ViewChild,
  ViewContainerRef,
} from "@angular/core";
import { MapService } from "src/app/core/services/utilities/map.service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { MatDialog, MatSidenav } from "@angular/material";
import { StationDetailComponent } from "src/app/features/public/station-detail/station-detail.component";
import { MatdialogService } from "src/app/core/services/utilities/matdialog.service";
import { SearchComponent } from "src/app/features/public/layout/search/search.component";
import { CallFunctionService } from "src/app/core/services/utilities/call-function.service";

@Component({
  selector: "app-map",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.scss"],
})
export class MapComponent implements OnInit {
  // @ts-ignore
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  mDialog: any;
  objTram: any;

  @ViewChild("compIo", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  constructor(
    private mapSv: MapService,
    public matSidenavService: MatsidenavService,
    public callFunctionService: CallFunctionService,
    public cfr: ComponentFactoryResolver,
    private imDialog: MatDialog,
    imDialogService: MatdialogService
  ) {
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
  }

  ngOnInit() {
    this.callFunctionService.setComp(this);
    // khởi tạo matSidenav service
    this.matSidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    // // gán giá trị cho function trong mapservice để mở sidenav từ map component

    this.mapSv.mapComp = this;
    this.mapSv.mapInit("viewMap");

    // add các layer từ geoserver để hiển thị
    this.mapSv.changeDataWms(
      "http://localhost:8080/geoserver/hanhchinhvn/wms",
      "diemkhaithac",
      21
    );
    this.mapSv.getFeatureInfo();
  }

  // đóng sidenav
  public closeSidenav() {
    this.matSidenavService.close();
  }

  // mở và bind thông tin trạm vào io
  public openSidenav(item) {
    if (item.numberReturned > 0) {
      this.matSidenavService.setTitle("Thông tin chi tiết công trình");
      this.matSidenavService.setContentComp(StationDetailComponent, "", item);
      this.matSidenavService.open();
    }
  }

  /**
   * Hàm đóng mat dialog
   */
  closeMatDialog() {
    this.imDialog.closeAll();
  }

  /**
   * Hàm mở thư viện dialog
   */
  public showMatDialog() {
    this.mDialog.setDialog(
      this,
      SearchComponent,
      "showToaDoTram",
      "closeMatDialog",
      "",
      "50%",
      "60vh"
    );
    this.mDialog.open();
  }

  /**
   * Hiển thị những file select trong thư viện
   */
  showToaDoTram() {
    this.objTram = this.mDialog.dataResult;
    if (this.objTram) {
      this.mapSv.zoomToPoint(
        this.objTram,
        this.objTram.tendiem,
        this.objTram.vitri
      );
    }
  }

  doFunction(methodName) {
    this[methodName]();
  }
}
