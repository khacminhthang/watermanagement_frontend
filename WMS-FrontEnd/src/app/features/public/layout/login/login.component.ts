import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {validationAllErrorMessagesService} from "src/app/core/services/utilities/validatorService";
import {HethongFacadeService} from "src/app/service/admin/hethong/hethong-facade.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginIOForm: FormGroup;
  errorLogin: string;

  // error message
  validationErrorMessages = {
    userName: { required: "Tên tài khoản không được để trống!" },
    passWord: { pattern: "Mật khẩu không được để trống!" }
  };

  // form errors
  formErrors = {
    userName: "",
    passWord: ""
  };
  constructor(
    private formBuilder: FormBuilder,
    public hethongFacadeService: HethongFacadeService,
  ) { }

  ngOnInit() {
    this.formInit();
  }

  formInit() {
    this.loginIOForm = this.formBuilder.group({
      userName: ["", Validators.required],
      passWord: ["", Validators.required]
    });
  }

  // Sự kiện đăng nhập
  public loginAdmin() {
    this.logAllValidationErrorMessages();
    if (this.loginIOForm.valid === true) {
      const inputLogin = {userName: this.loginIOForm.value.userName, password: this.loginIOForm.value.passWord};
      this.hethongFacadeService.getUserService().loginAdmin(inputLogin).subscribe(
        res => {
          localStorage.setItem('token', res.token); 
          window.location.href = 'http://localhost:4200/admin';
        },
        error => {this.errorLogin = "Tên đăng nhập hoặc mật khẩu không chính xác"; }
      );
    }
  }
  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.loginIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }
}
