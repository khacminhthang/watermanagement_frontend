import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {MatSidenav} from "@angular/material";
import {SidenavService} from "src/app/core/services/utilities/sidenav.service";

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss']
})
export class AsideComponent implements OnInit, OnDestroy {

  private comp: any;
  @ViewChild('aside', {static: true}) public sidenav: MatSidenav;
  @ViewChild('compcontainer', { read: ViewContainerRef, static: true }) public content: ViewContainerRef;
  private componentRef: ComponentRef<any>;
  constructor(public sidenavService: SidenavService, public cfr: ComponentFactoryResolver) {
  }

  ngOnInit() {
    this.sidenavService.setSidenav(this.sidenav, this.content, this.cfr);
  }

  openCurrentSidenav() {
    this.sidenavService.toggle();
  }
  ngOnDestroy(): void {
    if (this.componentRef) {
      this.componentRef.destroy();
      this.componentRef = null;
    }
  }
}
