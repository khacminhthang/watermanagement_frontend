import { Component, OnInit } from '@angular/core';
import {SidenavService} from "src/app/core/services/utilities/sidenav.service";
import {Router} from "@angular/router";
import {CallFunctionService} from "src/app/core/services/utilities/call-function.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private sidenav: SidenavService, private router: Router,
              public callFunctionService: CallFunctionService) { }

  ngOnInit() {
  }

  openCurrentSidenav() {
    this.sidenav.toggle();
  }

  login() {
    this.router.navigate(['/login']);
  }
  showSearch() {
    this.callFunctionService.callFunctionComp('showMatDialog');
  }
}
