import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {PublicComponent} from "./public.component";

const publicRoutes: Routes = [
  {
    path: "",
    component: PublicComponent,
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(publicRoutes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
