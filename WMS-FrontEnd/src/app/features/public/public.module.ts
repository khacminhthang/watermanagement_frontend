import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicComponent } from "src/app/features/public/public.component";
import { PublicRoutingModule } from "src/app/features/public/public-routing.module";
import { MatSidenavModule } from "@angular/material";
import { AsideComponent } from 'src/app/features/public/layout/aside/aside.component';
import { HeaderComponent } from 'src/app/features/public/layout/header/header.component';
import { MapComponent } from 'src/app/features/public/layout/map/map.component';
import { SidenavService } from "src/app/core/services/utilities/sidenav.service";
import { StationDetailComponent } from 'src/app/features/public/station-detail/station-detail.component';
import {AdminSharedModule} from "src/app/features/admin/admin-shared.module";
import {OwlDateTimeModule} from "ng-pick-datetime";
import { LoginComponent } from 'src/app/features/public/layout/login/login.component';
import { SearchComponent } from 'src/app/features/public/layout/search/search.component';



@NgModule({
  declarations: [PublicComponent, AsideComponent, HeaderComponent, MapComponent, StationDetailComponent, LoginComponent, SearchComponent],
  imports: [
    CommonModule,
    PublicRoutingModule,
    MatSidenavModule,
    AdminSharedModule,
    OwlDateTimeModule
  ],
  providers: [SidenavService],
  entryComponents: [StationDetailComponent, SearchComponent, HeaderComponent]
})
export class PublicModule { }
