import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { DetailRowService } from "@syncfusion/ej2-angular-grids";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CommonFacadeService} from "src/app/service/admin/common/common-facade.service";
import {HighchartService} from "src/app/service/admin/common/highchart.service";
import {Observable} from "rxjs";
import {DataStateChangeEventArgs, GridComponent} from "@syncfusion/ej2-angular-grids";
import {DatePipe} from "@angular/common";
import {VanHanh} from "src/app/shared/constants/vanhanh-constants";
import {STATUS} from "src/app/shared/constants/status-constants";
import {DmFacadeService} from "src/app/service/admin/danhmuc/danhmuc-facade.service";

@Component({
  selector: 'app-station-detail',
  templateUrl: './station-detail.component.html',
  styleUrls: ['./station-detail.component.scss'],
  providers: [DetailRowService],
  encapsulation: ViewEncapsulation.None
})
export class StationDetailComponent implements OnInit {
  public obj: any;
  public dataTram: any;
  public tenTram = '';
  public id: number;
  public objKey: number;
  public checkShowChitiet = false;
  public listThamso: any;
  public dataBieuDo: any;
  public thongsoFilters: any;
  public checkget = true;
  firstdate: string;
  lastdate: string;
  public dvqlData: any;
  public caNhanData: any;
  public dvql: any;
  public caNhan: any;
  public dataTinh: any;
  public dataHuyen: any;
  public dataXa: any;
  public tinh: any;
  public huyen: any;
  public xa: any;
  public vanHanh = VanHanh;
  public status = STATUS;
  public listData: any;
  errorForm = "";
  // khai báo cho list phân trang
  public state: DataStateChangeEventArgs;
  public listdatathongso: Observable<DataStateChangeEventArgs>;
  public dlquantractsService: any;
  DateForm: FormGroup;
  settingsCommon = new SettingsCommon();
  constructor(public commonFacadeService: CommonFacadeService,
              public dmFacadeService: DmFacadeService,
              private chartSv: HighchartService,
              private formBuilder: FormBuilder,
              private datePipe: DatePipe,
              ) {
    // Get new service
    this.dlquantractsService = this.commonFacadeService.getDlquantractsService();
    this.listdatathongso = this.dlquantractsService;
  }
  item: any;
  ngOnInit() {
    this.bindingConfigValidation();
    this.dataTram = this.obj.features;
    this.showDVQL(this.dataTram[0].properties.id_donviquanly);
    this.showCaNhan(this.dataTram[0].properties.id_chusudung);
    this.showTinh(this.dataTram[0].properties.matinh);
    this.showHuyen(this.dataTram[0].properties.matinh, this.dataTram[0].properties.mahuyen);
    this.showXa(this.dataTram[0].properties.mahuyen, this.dataTram[0].properties.maxa);
  }

  // config input validation form
  bindingConfigValidation() {
    this.DateForm = this.formBuilder.group({
      firstdate: ["", Validators.required],
      lastdate: ["", Validators.required],
      idthamso: [""]
    });
  }

  // Show kết quả quan trắc
  showKetQua() {
    if (this.DateForm.valid) {
      this.checkget = false;
      this.firstdate = this.datePipe.transform(this.DateForm.value.firstdate, "dd/MM/yyyy hh:mm:ss");
      this.lastdate = this.datePipe.transform(this.DateForm.value.lastdate, "dd/MM/yyyy hh:mm:ss");
      this.getAllTimeFromTo(this.firstdate, this.lastdate);
    } else {
      this.errorForm = "Bạn phải nhập đầy đủ thông tin";
    }
  }

  // Get all kết quả quan trắc phân trang time from to
  async getAllTimeFromTo(firstDate, lastDate) {
    this.errorForm = "";
    const data: any = await this.commonFacadeService.getDlquantractsService().getAllKetQuaTimeFromTo(
      {
        timeFrom: firstDate,
        timeTo: lastDate,
        IdTramquantrac: this.id,
        IdThamsodo: this.DateForm.value.idthamso,
        ObjKey: this.objKey,
        PageSize: -1
      }
    );
    if (data) {
      data.items.map((item, index) => {
        item.serialNumber = index + 1;
      });
      this.listData = data.items;
      this.dataBieuDo = data.items;
      this.chartInit(this.dataBieuDo);
    } else {
      this.listData = [];
      this.dataBieuDo = [];
      this.chartInit(this.dataBieuDo);
    }
  }


  // Convert string thành dd/mm/yyyy
  public convertDateTimeString(data) {
    const [date, time]: string[] = data.fullTime.split(' ');
    const [year, month, day]: string[] = date.split('-');
    if (year.length === 4) {
      const awesomeDateTime = `${day}-${month}-${year}`;
      return awesomeDateTime;
    } else {
      const awesomeDateTime1 = `${year}-${month}-${day}`;
      return awesomeDateTime1;
    }
    ;
  }

  // Convert string thành dd/mm/yyyy
  public convertTimeString(data) {
    const [date, time]: string[] = data.fullTime.split(' ');
    if (time) {
      return time;
    } else {
      return null;
    }
  }
  // khởi tạo biểu đồ
  chartInit(data) {
    this.chartSv.setSource(data);
    this.chartSv.preDataChart(data, "gw");
    if (data.length === 0) {
      this.chartSv.setChartName("chưa có kết quả đo");
    } else {
      this.chartSv.setChartName("kết quả");
    }
    this.chartSv.setXTittle("Thời gian đo");
    this.chartSv.setYTittle("Kết quả đo");
    this.chartSv.chartInit1m("chartkq", "line");
  }

  // Hàm chạy khi ấn nút chi tiết
   async showChiTiet(datact) {
     console.log(datact, 'datact')
     this.errorForm = "";
     this.checkShowChitiet = true;
     this.tenTram = datact.properties.tendiem;
     this.objKey = datact.properties.obj_key;
     const [name, id]: string[] = datact.id.split('.');
     this.id = +id;
     this.listThamso = await this.commonFacadeService.getObjthamsoService().getFetchAll({ idObj: this.id, objKey: this.objKey });
     if (this.listThamso && this.listThamso.length > 0) {
       this.DateForm.controls.idthamso.setValue(this.listThamso[0].idthamso);
       const data: any = await this.commonFacadeService.getDlquantractsService().getAllKetQuaFirst(
         {
           Take: 100,
           IdTramquantrac: this.id,
           IdThamsodo: this.listThamso[0].idthamso,
           ObjKey: this.objKey,
           PageSize: -1
         }
       );
       if (data) {
         data.items.map((item, index) => {
           item.serialNumber = index + 1;
         });
         this.listData = data.items;
         this.dataBieuDo = data.items;
         this.chartInit(this.dataBieuDo);
       } else {
         this.listData = [];
         this.dataBieuDo = [];
         this.chartInit(this.dataBieuDo);
       }
     }
  }

  async showThongSo() {
    this.errorForm = "";
    this.checkget = true;
    const data: any = await this.commonFacadeService.getDlquantractsService().getAllKetQuaFirst(
      {
        Take: 100,
        IdTramquantrac: this.id,
        IdThamsodo: this.DateForm.value.idthamso,
        ObjKey: this.objKey,
        PageSize: -1
      }
    );
    if (data) {
      data.items.map((item, index) => {
        item.serialNumber = index + 1;
      });
      this.listData = data.items;
      this.dataBieuDo = data.items;
      this.chartInit(this.dataBieuDo);
    } else {
      this.listData = [];
      this.dataBieuDo = [];
      this.chartInit(this.dataBieuDo);
    }
  }

  /**
   * Hàm get tên Cá nhân theo id
   * @param id
   */
  async showCaNhan(id: number) {
    if (id) {
      this.caNhanData = await this.dmFacadeService
        .getDmCanhanService()
        .getByid(id).subscribe(res => {
          res ? this.caNhan = res.hovaten : this.caNhan = '';
        });
    }
  }


  async showTinh(id: number) {
    if (id) {
      this.dataTinh = await this.dmFacadeService
        .getProvinceService()
        .getFetchAll();
      if (this.dataTinh) {
        this.dataTinh.map(res => {
          if (res.matinh === id) {
            this.tinh = res.tendvhc;
          }
        });
      }
    }
  }

  async showHuyen(idtinh: number, idhuyen: number) {
    if (idtinh) {
      this.dataHuyen = await this.dmFacadeService
        .getDistrictService()
        .getByid(idtinh).subscribe( res => {
          if (res) {
            res.map(res1 => {
              if (res1.mahuyen === idhuyen) {
                this.huyen = res1.tendvhc;
              }
            });
          }
        });
    }
  }

  async showXa(idhuyen: number, idxa: number) {
    if (idxa) {
      await this.dmFacadeService
        .getWardService()
        .getByid(idhuyen).subscribe( res => {
          if (res) {
            res.map(res1 => {
              if (res1.maxa === idxa) {
                this.xa = res1.tendvhc;
              }
            });
          }
        });
    }
  }

  /**
   * Hàm get tên Vận hành bằng id
   * @param id
   */
  showTenVanHanh(id: number) {
    for (const v of this.vanHanh) {
      if (v.id === id) {
        return v.ten;
      }
      return "";
    }
  }

  /**@function showTrangThai
   * Hàm show trạng thái của điểm khai thác theo id truyền vào
   */
  showTrangThai(id: number) {
    for (const s of this.status) {
      if (s.id === id) {
        return s.name;
      }
      return "";
    }
  }

  /**
   * Hàm get tên Đơn vị quản lý bằng id
   * @param id
   */
  async showDVQL(id: number) {
    if (id) {
      this.dvqlData = await this.dmFacadeService
        .getDmCoquantochucService()
        .getByid(id).subscribe(res => {
          res ? this.dvql = res.tencoquan : this.dvql = '';
        });
    }
  }
}
