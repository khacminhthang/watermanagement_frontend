import { Component, OnInit } from '@angular/core';
import { LineChartjsService } from "src/app/core/services/utilities/line-chartjs.service";

@Component({
  selector: 'app-line-range',
  templateUrl: './line-range.component.html',
  styleUrls: ['./line-range.component.css']
})
export class LineRangeComponent implements OnInit {

  constructor(public chartService: LineChartjsService) { }
  public resetZoom(){
    this.chartService.resetZoom();
  }
  ngOnInit() {
    this.chartService.setElement("chartid");
    this.chartService.initChart();
  }

}
