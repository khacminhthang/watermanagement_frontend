import { LineRangeComponent } from 'src/app/features/admin/danhmuc/baocao/ngcharts/line-range/line-range.component';
import { LineComponent } from 'src/app/features/admin/danhmuc/baocao/ngcharts/line/line.component';
import { GaugeComponent } from 'src/app/features/admin/danhmuc/baocao/ngcharts/gauge/gauge.component';
import { BaocaoRoutingModule } from 'src/app/features/admin/danhmuc/baocao/baocao-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaocaoComponent } from 'src/app/features/admin/danhmuc/baocao/baocao.component';
import { NgchartsComponent } from 'src/app/features/admin/danhmuc/baocao/ngcharts/ngcharts.component';
import {ChartsModule} from "ng2-charts";
import { ScatterComponent } from 'src/app/features/admin/danhmuc/baocao/ngcharts/scatter/scatter.component';
import {AdminSharedModule} from "src/app/features/admin/admin-shared.module";
import {OwlDateTimeModule} from "ng-pick-datetime";




@NgModule({
  declarations: [
    BaocaoComponent,
    NgchartsComponent,
    GaugeComponent,
    LineComponent,
    LineRangeComponent,
    ScatterComponent,

  ],
  imports: [
    CommonModule,
    BaocaoRoutingModule,
    ChartsModule,
    AdminSharedModule,
    OwlDateTimeModule
  ]
})
export class BaocaoModule { }
