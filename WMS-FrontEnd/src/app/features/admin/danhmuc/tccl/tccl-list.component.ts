import {
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { MatDialog } from "@angular/material/dialog";
import { MatSidenav } from "@angular/material/sidenav";
import { MyAlertDialogComponent } from "src/app/shared/components/my-alert-dialog/my-alert-dialog.component";
import { OutputTieuchuanchatluongModel } from "src/app/models/admin/danhmuc/tccl.model";
import { DmTcclIoComponent } from "src/app/features/admin/danhmuc/tccl/tccl-io.component";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { QueryCellInfoEventArgs } from "@syncfusion/ej2-angular-grids";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import { HttpErrorResponse } from "@angular/common/http";
@Component({
  selector: "app-tccl-list",
  templateUrl: "./tccl-list.component.html",
  styleUrls: ["./tccl-list.component.scss"]
})
export class DmTcclListComponent implements OnInit {
  listTieuchuanchatluong: OutputTieuchuanchatluongModel[];
  selectedItem: OutputTieuchuanchatluongModel;
  listData: any;
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compTieuchuanchatluongio", {
    read: ViewContainerRef,
    static: true
  })
  public content: ViewContainerRef;
  settingsCommon = new SettingsCommon();
  navArray = [
    { title: "Quản trị", url: "/admin" },
    {
      title: "Danh mục",
      url: "/admin/danhmuc"
    },
    {
      title: "Tiêu chuẩn chất lượng",
      url: "//admin/danhmuc/tccl"
    }
  ];

  buttonArray = [];
  constructor(
    public matsidenavService: MatsidenavService,
    public progressService: ProgressService,
    public cfr: ComponentFactoryResolver,
    public dmFacadeService: DmFacadeService,
    public modalDialog: MatDialog,
    public commonService: CommonServiceShared,
    private subHeaderService: SubHeaderService
  ) { }

  ngOnInit() {
    this.subHeaderService.setParentComp(this);
    this.matsidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 10 };
    this.getAllTieuchuanchatluong();
    this.dataSubHeader();
  }

  /**
   * Truyền data vào header
   */
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }

  /**
   * get all Tieu chuan chat luong
   */
  async getAllTieuchuanchatluong() {
    this.listData = await this.dmFacadeService
      .getTieuchuanchatluongService()
      .getFetchAll();
    const listDataItems = this.listData;
    if (listDataItems) {
      listDataItems.map((tccl, index) => {
        tccl.serialNumber = index + 1;
      });
    }
    this.listTieuchuanchatluong = listDataItems;
  }

  /**
   * open sidebar execute insert
   */
  public openTieuchuanchatluongIOSidebar() {
    this.matsidenavService.setTitle("Thêm mới dữ liệu tiêu chuẩn chất lượng");
    this.matsidenavService.setContentComp(DmTcclIoComponent, "new");
    this.matsidenavService.open();
  }

  /**
   * edit open sidebar
   */
  public editItemTieuchuanchatluong(event) {
    this.getItemByEvent(event);
    this.matsidenavService.setTitle(
      "Sửa dữ liệu thông tin tiêu chuẩn chất lượng"
    );
    this.matsidenavService.setContentComp(
      DmTcclIoComponent,
      "edit",
      this.selectedItem
    );
    this.matsidenavService.open();
  }

  /**
   * delete
   */
  public deleteTieuchuanchatluong(event) {
    this.getItemByEvent(event);
    const canDelete: string = this.dmFacadeService
      .getTieuchuanchatluongService()
      .checkBeDeleted(this.selectedItem.id);
    this.canBeDeletedCheck(canDelete);
  }

  public canBeDeletedCheck(sMsg: string) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog();
    } else {
      this.canDeleteDialog(sMsg);
    }
  }

  confirmDeleteDiaLog() {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Chắc chắn xóa?";
    dialogRef.componentInstance.content =
      "Tiêu chuẩn chất lượng: <b>" + this.selectedItem.tentieuchuan + "</b>";
    dialogRef.componentInstance.okeButton = "Đồng ý";
    dialogRef.componentInstance.cancelButton = "Hủy bỏ";
    dialogRef.componentInstance.visibleOkButton = true;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.dmFacadeService
          .getTieuchuanchatluongService()
          .deleteItem(this.selectedItem.id).subscribe(
            () => this.getAllTieuchuanchatluong(),
            (error: HttpErrorResponse) => {
              this.commonService.showeNotiResult(error.message, 2000);
            },
            () => this.commonService.showeNotiResult("Đã xóa thành công: " + this.selectedItem.tentieuchuan, 2000)
          );
      }
    });
  }

  canDeleteDialog(sMsg: string) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Không thể xóa!";
    dialogRef.componentInstance.content = "<b>" + sMsg + "</b>";
    dialogRef.componentInstance.cancelButton = "Đóng";
    dialogRef.componentInstance.visibleOkButton = false;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.componentInstance.headerColor = "red";
  }

  /**
   * close
   */
  public closeTieuchuanchatluongIOSidebar() {
    this.matSidenav.close();
  }

  /**
   * set id in coloumn
   */
  customiseCell(args: QueryCellInfoEventArgs) {
    if (args.column.field === "id") {
      args.cell.id = args.data[args.column.field];
    }
  }

  public getItemByEvent(event) {
    const item = this.commonService.getByEvent(
      event,
      this.listTieuchuanchatluong
    );
    this.selectedItem = item;
  }

  doFunction(methodName) {
    this[methodName]();
  }
}
