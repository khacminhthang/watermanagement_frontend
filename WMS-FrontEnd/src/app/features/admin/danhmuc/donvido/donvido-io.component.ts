import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  displayFieldCssService, validationAllErrorMessagesService
} from "src/app/core/services/utilities/validatorService";
import { HttpErrorResponse } from "@angular/common/http";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import {InputDonvidoModel} from "src/app/models/admin/danhmuc/donvido.model";

@Component({
  selector: 'app-donvido-io',
  templateUrl: './donvido-io.component.html',
  styleUrls: ['./donvido-io.component.scss']
})
export class DmDonvidoIoComponent implements OnInit {
  donvidoIOForm: FormGroup;
  public inputModel: InputDonvidoModel;
  public editMode: boolean;
  public purpose: string;
  public obj: any;
  public nhomDonvidoFilters = [
    {id: 1, tenndvd: 'Nhóm đo chiều dài'},
    {id: 2, tenndvd: 'Nhóm đo thể tích'},
    {id: 3, tenndvd: 'Nhóm đo khối lượng'},
    {id: 4, tenndvd: 'Nhóm đo chất lượng không khí'}];
  public allNhomDonvido = [
    {id: 1, tenndvd: 'Nhóm đo chiều dài'},
    {id: 2, tenndvd: 'Nhóm đo thể tích'},
    {id: 3, tenndvd: 'Nhóm đo khối lượng'},
    {id: 4, tenndvd: 'Nhóm đo chất lượng không khí'}
  ];
  // error message
  validationErrorMessages = {
    tendonvido: { required: "Tên đơn vị đo không được để trống!" }
  };

  // form errors
  formErrors = {
    tendonvido: ""
  };

  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    public dmFacadeService: DmFacadeService
  ) { }

  ngOnInit() {
    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
  }

  /**
   * Hàm khởi tạo form thêm hoặc sửa
   */
  bindingConfigAddOrUpdate() {
    this.editMode = false;
    this.inputModel = new InputDonvidoModel();
    if (this.purpose === "new" || this.purpose === "edit") {
      this.editMode = true;
    }
    // check edit
    if (this.editMode === true && this.obj) {
      this.donvidoIOForm.setValue({
        tendonvido: this.obj.tendonvido,
        kyhieudonvido: this.obj.kyhieudonvido,
        idNhomdonvido: this.obj.idNhomdonvido
      });
    }
  }

  /**
   * Khởi tạo validate form
   */
  bindingConfigValidation() {
    this.donvidoIOForm = this.formBuilder.group({
      tendonvido: ["", Validators.required],
      kyhieudonvido: [""],
      idNhomdonvido: [""]
    });
  }

  /**
   * Hàm lưu
   * @param operMode trạng thái thêm hoặc sửa
   */
  public onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    if(this.donvidoIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.matSidenavService.close();
    }
  }

  /**
   * Hàm gọi hàm thêm hoặc sửa
   */
  public addOrUpdateContinue(operMode: string) {
    this.logAllValidationErrorMessages();
    if(this.donvidoIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.onFormReset();
      this.purpose = "new";
    }
  }

  /**
   * add or update
   * @param operMode trạng thái thêm hoặc sửa
   */
  private addOrUpdate(operMode: string) {
    this.inputModel = this.donvidoIOForm.value;
    if (operMode === "new") {
      this.dmFacadeService
        .getDonvidoService()
        .addItem(this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("refreshList"),
          (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Thêm mới đơn vị đo thành công!",
              2000
            )
        );
    } else if (operMode === "edit") {
      const id: number = this.obj.id;
      this.dmFacadeService
        .getDonvidoService()
        .updateItem(id, this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("refreshList"),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Cập nhập dữ liệu thành công",
              2000
            )
        );
    }
  }

  /**
   * on form reset
   */
  public onFormReset() {
    this.donvidoIOForm.reset();
  }

  /**
   * Validation click submit
   */
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.donvidoIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  /**
   * display fields css
   */
  public displayFieldCss(field: string) {
    displayFieldCssService(field);
  }

  /**
   * Hàm đóng sidenav
   */
  public closeDonvidoIOSidebar() {
    this.matSidenavService.close();
  }
}
