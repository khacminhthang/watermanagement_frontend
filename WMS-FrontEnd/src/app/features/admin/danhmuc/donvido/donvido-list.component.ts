import {
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { MatSidenav } from "@angular/material/sidenav";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { QueryCellInfoEventArgs } from "@syncfusion/ej2-grids";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import {OutputDonvidoModel} from "src/app/models/admin/danhmuc/donvido.model";
import {DmDonvidoIoComponent} from "src/app/features/admin/danhmuc/donvido/donvido-io.component";
import {HttpErrorResponse} from "@angular/common/http";
@Component({
  selector: 'app-donvido-list',
  templateUrl: './donvido-list.component.html',
  styleUrls: ['./donvido-list.component.scss']
})
export class DmDonvidoListComponent implements OnInit {
  listDonvido: OutputDonvidoModel[];
  selectedItem: OutputDonvidoModel;
  listData: any;
  navArray = [
    { title: "Quản trị", url: "/admin" },
    {
      title: "Danh mục",
      url: "/admin/danhmuc"
    },
    {
      title: "Đơn vị đo",
      url: "//admin/danhmuc/donvido"
    }
  ];

  buttonArray = [];
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compDonvidoio", { read: ViewContainerRef, static: true }) public content: ViewContainerRef;

  settingsCommon = new SettingsCommon();

  constructor(
    public matsidenavService: MatsidenavService,
    public progressService: ProgressService,
    public cfr: ComponentFactoryResolver,
    public dmFacadeService: DmFacadeService,
    public commonService: CommonServiceShared,
    private subHeaderService: SubHeaderService
  ) { }
  ngOnInit() {
    this.matsidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 10 };
    this.getAllDonvido();
    this.subHeaderService.setParentComp(this);
    this.dataSubHeader();
  }

  /**
   * Gán dữ liệu vào sub-header service
   */
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }

  /**
   * get all Loai so lieu
   */
  async getAllDonvido() {
    this.listData = await this.dmFacadeService
      .getDonvidoService()
      .getFetchAll();
    const listDataItems = this.listData;
    if (listDataItems) {
      listDataItems.map((donvido, index) => {
        donvido.serialNumber = index + 1;
      });
    }
    this.listDonvido = listDataItems;
  }

  /**
   * Mở sidenav thêm mới
   */
  public openDonvidoIOSidebar() {
    this.matsidenavService.setTitle("Thêm mới dữ liệu đơn vị đo");
    this.matsidenavService.setContentComp(DmDonvidoIoComponent, "new");
    this.matsidenavService.open();
  }

  /**
   * Mở sidenav sửa
   * @param event 
   */
  public openDonvidoIOSidebarEdit(event) {
    this.getItemByEvent(event);
    this.matsidenavService.setTitle("Sửa dữ liệu thông tin đơn vị đo");
    this.matsidenavService.setContentComp(
      DmDonvidoIoComponent,
      "edit",
      this.selectedItem
    );
    this.matsidenavService.open();
  }

  /**
   * Đóng sidenav
   */
  public closeDonvidoIOSidebar() {
    this.matsidenavService.close();
  }

  /**
   * Hàm get dữ liệu đơn vị đo đang được chọn
   * @param event 
   */
  private getItemByEvent(event) {
    const item = this.commonService.getByEvent(event, this.listDonvido);
    this.selectedItem = item;
  }

  /**
   * Hàm xóa đơn vị đo
   */
  public deleteDonvido(event) {
    this.getItemByEvent(event);
    const canDelete: string = this.dmFacadeService
      .getLoaisolieuService()
      .checkBeDeleted(this.selectedItem.id);
    this.canBeDeletedCheck(canDelete);
  }

  public canBeDeletedCheck(sMsg: string) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog();
    } else {
      this.canDeleteDialog(sMsg);
    }
  }

  /**
   * Hàm hiển thị form xác nhận xóa
   */
  confirmDeleteDiaLog() {
    const dialogRef = this.commonService.confirmDeleteDiaLogService(
      "Tên loại số liệu:",
      this.selectedItem.tendonvido
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.dmFacadeService
          .getDonvidoService()
          .deleteItem( this.selectedItem.id )
          .subscribe(
            () => this.refreshList(),
            (error: HttpErrorResponse) => {
              this.commonService.showeNotiResult(error.message, 2000);
            },
            () => this.commonService.showeNotiResult("Đã xóa thành công: " + this.selectedItem.tendonvido, 2000)
          );
      }
    });
  }

  canDeleteDialog(sMsg: string) {
    this.commonService.canDeleteDialogService(sMsg);
  }

  /**
   * Hàm lấy lại danh sách đơn vị đo
   */
  public refreshList() {
    this.getAllDonvido();
  }

  // set id in coloumn
  customiseCell(args: QueryCellInfoEventArgs) {
    if (args.column.field === "id") {
      args.cell.id = args.data[args.column.field];
    }
  }

  // call method
  doFunction(methodName) {
    this[methodName]();
  }
}
