import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatSidenav } from "@angular/material/sidenav";
import { QueryCellInfoEventArgs } from "@syncfusion/ej2-grids";

import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { OutputCanhanModel } from "src/app/models/admin/danhmuc/canhan.model";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { DmCanhanIoComponent } from "src/app/features/admin/danhmuc/canhan/canhan-io.component";
import {HttpErrorResponse} from "@angular/common/http";
import {CommonServiceShared} from "src/app/core/services/utilities/common-service";

@Component({
  selector: "app-canhan-list",
  templateUrl: "./canhan-list.component.html",
  styleUrls: ["./canhan-list.component.scss"]
})
export class DmCanhanListComponent implements OnDestroy, OnInit {
  // @ts-ignore
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compcanhanio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;

  public componentRef: ComponentRef<any>;
  public settingsCommon = new SettingsCommon();
  listCanhan: OutputCanhanModel[];
  selectedItem: OutputCanhanModel;
  listData: any;

  navArray = [
    { title: "Quản trị", url: "/admin" },
    {
      title: "Danh mục",
      url: "/admin/danhmuc"
    },
    {
      title: "Cá nhân",
      url: "//admin/danhmuc/canhan"
    }
  ];

  buttonArray = [];

  /**
   * Hàm constructor phải bắt buộc có hai biến là MatSidenavService và ComponentFactoryResolver để Init MatsidenavService
   */
  constructor(
    public matSidenavService: MatsidenavService,
    public cfr: ComponentFactoryResolver,
    public progressService: ProgressService,
    public dmFacadeService: DmFacadeService,
    private modalDialog: MatDialog,
    private subHeaderService: SubHeaderService,
    public commonService: CommonServiceShared,
  ) { }

  /**
   * Khi khởi tạo component cha phải gọi setSidenave để khỏi tạo Sidenav
   */
  ngOnInit() {
    this.matSidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.getAllCanhan();
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 8 };
    this.subHeaderService.setParentComp(this);
    this.dataSubHeader();
  }

  /**
   * Gán dữ liệu vào sub-header service
   */
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }

  /**
   * Hàm getAll cá nhân đẻ binding dữ liệu lên EJS grid
   */
  async getAllCanhan() {
    this.listData = await this.dmFacadeService
      .getDmCanhanService()
      .getFetchAll();
    const listDataItems = this.listData;
    if (listDataItems) {
      listDataItems.map((canhan, index) => {
        canhan.serialNumber = index + 1;
      });
    }
    this.listCanhan = listDataItems;
  }

  /**
   * Hàm xóa một bản ghi, được gọi khi nhấn nút xóa trên giao diện list
   */
  async deleteItemCanhan(event) {
    this.getItemByEvent(event);
    // Phải check xem dữ liệu muốn xóa có đang được dùng ko, đang dùng thì ko xóa
    // Trường hợp dữ liệu có thể xóa thì Phải hỏi người dùng xem có muốn xóa không
    // Nếu đồng ý xóa
    const canDelete: string = this.dmFacadeService
      .getDmCanhanService()
      .checkBeDeleted(this.selectedItem.id);
    this.canBeDeletedCheck(canDelete);
  }

  /**
   * check xem dữ liệu đang dùng có xóa được không
   * @param sMsg 
   */
  public canBeDeletedCheck(sMsg: string) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog();
    } else {
      this.cantDeleteDialog(sMsg);
    }
  }

  /**
   * Hiển thi form xác nhận xóa
   */
  confirmDeleteDiaLog() {
    const dialogRef = this.commonService.confirmDeleteDiaLogService(
      "Tên cá nhân:",
      this.selectedItem.hovaten
    );
    dialogRef.afterClosed().subscribe(async result => {
      if (result === "confirm") {
        await this.dmFacadeService
          .getDmCanhanService()
          .deleteItem(this.selectedItem.id).subscribe(
            () => this.getAllCanhan(),
            (error: HttpErrorResponse) => {
              this.commonService.showeNotiResult(error.message, 2000);
            },
            () => this.commonService.showeNotiResult("Đã xóa thành công: " + this.selectedItem.hovaten, 2000)
          );
      }
    });
  }

  /**
   * Hiển thị form không thể xóa
   * @param sMsg
   */
  cantDeleteDialog(sMsg: string) {
    this.commonService.canDeleteDialogService(sMsg);
  }


  /**
   * Hàm sửa thông tin chi tiết một bản ghi, được gọi khi nhấn nút xem chi tiết trên giao diện list
   * @param event 
   */
  public editItemCanhan(event) {
    this.getItemByEvent(event);
    this.matSidenavService.setTitle("Sửa dữ liệu cá nhân");
    this.matSidenavService.setContentComp(
      DmCanhanIoComponent,
      "edit",
      this.selectedItem
    );
    this.matSidenavService.open();
  }

  /**
   * Hàm đặt tiêu đề, Đặt content component và sau đó mở sidebar lên
   */
  public openCanhanIOSidebar() {
    this.matSidenavService.setTitle("Thêm mới cá nhân");
    this.matSidenavService.setContentComp(DmCanhanIoComponent, "new");
    this.matSidenavService.open();
  }

  /**
   * Hàm đóng sidenav
   */
  public closeCanhanIOSidebar() {
    this.matSidenavService.close();
  }

  /**
   * Hàm lấy ra đối tượng khi người dùng click vào một trong 3 nút xóa, chi tiết, sửa
   * @param event 
   */
  public getItemByEvent(event) {
    const target = event.currentTarget;
    const pElement = target.parentElement.parentElement;
    const pclassID = pElement.getAttribute("id");
    const item = this.listCanhan.find(x => x.id === +pclassID);
    this.selectedItem = item;
  }

  customiseCell(args: QueryCellInfoEventArgs) {
    if (args.column.field === "id") {
      args.cell.id = args.data[args.column.field];
    }
  }

  /**
   * Hàm dùng để gọi các hàm khác, truyền vào tên hàm cần thực thi
   */
  doFunction(methodName) {
    this[methodName]();
  }

  ngOnDestroy(): void {
    if (this.componentRef) {
      this.componentRef.destroy();
      this.componentRef = null;
    }
  }
}
