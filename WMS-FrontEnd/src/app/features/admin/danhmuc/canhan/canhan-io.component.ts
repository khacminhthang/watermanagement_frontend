import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { InputCanhanModel } from "src/app/models/admin/danhmuc/canhan.model";
import { HttpErrorResponse } from "@angular/common/http";
import { OutputDvhcModel } from "src/app/models/admin/danhmuc/dvhc.model";
import { loaigiayto } from "src/app/features/admin/consts/enum";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import {
  validationAllErrorMessagesService,
} from "src/app/core/services/utilities/validatorService";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { MatDialog } from "@angular/material";
import { MatdialogService } from "src/app/core/services/utilities/matdialog.service";
import { ThuvienComponent } from "src/app/features/admin/thuvien/thuvien.component";

@Component({
  selector: "app-canhan-io",
  templateUrl: "./canhan-io.component.html",
  styleUrls: ["./canhan-io.component.scss"]
})
export class DmCanhanIoComponent implements OnInit {
  canhanIOForm: FormGroup;
  submitted = false;
  public obj: any;
  public purpose: string;
  public editMode: boolean;
  public inputModel: InputCanhanModel;
  public allTinhData: any;
  public allTinh: any;
  public allHuyen: any;
  public allXa: any;
  public model: any;

  // Những biến dành cho phần file
  mDialog: any;
  srcanh = '';
  public fileArray: any;
  // filter Đơn vị hành chính
  public dvhcProvinceFilters: OutputDvhcModel[];
  public dvhcDistrictFilters: OutputDvhcModel[];
  public dvhcWardFilters: OutputDvhcModel[];
  // loại giấy tờ CMND/hộ chiếu
  public loaigiayto = loaigiayto;

  // error message
  validationErrorMessages = {
    hovaten: { required: "Tên cá nhân không được để trống!" },
    sodienthoai: { pattern: "Định dạng số điện thoại không đúng!" },
    email: { email: "Định dạng email không đúng!" }
  };

  // form errors
  formErrors = {
    hovaten: "",
    sodienthoai: "",
    email: ""
  };
  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    public dmFacadeService: DmFacadeService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    private imDialog: MatDialog, imDialogService: MatdialogService
  ) {
    this.matSidenavService.okCallBackFunction = null;
    this.matSidenavService.cancelCallBackFunction = null;
    this.matSidenavService.confirmStatus = null;
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
  }

  async ngOnInit() {
    await this.formInit();
    await this.bindingConfigAddOrUpdate();
  }

  /**
   * Hàm khởi tạo form khi sửa dữ liệu
   */
  bindingConfigAddOrUpdate() {
    this.showDvhcTinh();
    this.editMode = false;
    this.inputModel = new InputCanhanModel();
    // check edit
    this.formOnEdit();
  }
  

  /**
   * Hàm khởi tạo form
   */
  formInit() {
    this.canhanIOForm = this.formBuilder.group({
      hovaten: ["", Validators.required],
      diachi: [""],
      matinh: [""],
      mahuyen: [""],
      maxa: [""],
      sodienthoai: ["", Validators.pattern("^[0-9-+]+$")],
      loaigiayto: [""],
      socmthochieu: [""],
      email: ["", Validators.email],
      note: [""],
      imgLink: [""]
    });
  }
  
  
  /**
   * Hàm bind dữ liệu vào form sửa
   */
  formOnEdit() {
    if (this.obj) {
      this.canhanIOForm.setValue({
        hovaten: this.obj.hovaten,
        diachi: this.obj.diachi,
        matinh: this.obj.matinh,
        mahuyen: this.obj.mahuyen,
        maxa: this.obj.maxa,
        sodienthoai: this.obj.sodienthoai,
        loaigiayto: this.obj.loaigiayto,
        socmthochieu: this.obj.socmthochieu,
        email: this.obj.email,
        note: this.obj.note,
        imgLink: this.obj.imgLink
      });
      this.srcanh = this.obj.imgLink;
      this.showDvhcHuyen();
      this.showDvhcXa();
    }
    this.editMode = true;
  }

  /**
   * Hàm lấy đơn vị hành chính tỉnh
   */
  async showDvhcTinh() {
    this.allTinhData = await this.dmFacadeService
      .getProvinceService()
      .getFetchAll();
    this.allTinh = this.allTinhData;
    this.dvhcProvinceFilters = this.allTinhData;
  }

  /**
   * Hàm lấy đơn vị hành chính huyện theo tỉnh
   */
  async showDvhcHuyen() {
    if (!this.canhanIOForm.value.matinh === true) {
      this.allHuyen = [];
      this.dvhcDistrictFilters = [];
      this.allXa = [];
      this.dvhcWardFilters = [];
      if (this.editMode === true) this.canhanIOForm.controls["mahuyen"].setValue("");
    }
    if (!this.canhanIOForm.value.matinh === false) {
      if (this.editMode === true) this.canhanIOForm.controls["mahuyen"].setValue("");
      this.allXa = [];
      this.dvhcWardFilters = [];
      this.allHuyen = await this.dmFacadeService
        .getDistrictService()
        .getFetchAllId(this.canhanIOForm.value.matinh);
      this.dvhcDistrictFilters = this.allHuyen;
    }
  }

  /**
   * Hàm lấy đơn vị hành chính xã theo huyện
   */
  async showDvhcXa() {
    if (!this.canhanIOForm.value.mahuyen === true) {
      this.allXa = [];
      this.dvhcWardFilters = [];
      if (this.editMode === true) this.canhanIOForm.controls["maxa"].setValue("");
    }
    if (!this.canhanIOForm.value.matinh === false && !this.canhanIOForm.value.mahuyen === false) {
      if (this.editMode === true) this.canhanIOForm.controls["maxa"].setValue("");
      this.allXa = await this.dmFacadeService
        .getWardService()
        .getFetchAllId(this.canhanIOForm.value.mahuyen);
      this.dvhcWardFilters = this.allXa;
    }
  }

  /**
   * Hàm được gọi khi nhấn lưu
   * @param operMode  biểu thị là form sửa hay thêm mới
   */
  async onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.canhanIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.matSidenavService.close();
    }
  }

  /**
   * Hàm thêm mới hoặc sửa dữ liệu cá nhân
   */
  private addOrUpdate(operMode: string) {
    const dmFacadeService = this.dmFacadeService.getDmCanhanService();
    this.inputModel = this.canhanIOForm.value;
    this.inputModel.imgLink = this.srcanh;
    if (operMode === "new") {
      dmFacadeService.addItem(this.inputModel).subscribe(
        res => this.matSidenavService.doParentFunction("getAllCanhan"),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        },
        () =>
          this.commonService.showeNotiResult(
            "Thêm mới cá nhân thành công!",
            2000
          )
      );
    } else if (operMode === "edit") {
      const id: number = this.obj.id;
      dmFacadeService.updateItem(id, this.inputModel).subscribe(
        res => this.matSidenavService.doParentFunction("getAllCanhan"),
        (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
        () =>
          this.commonService.showeNotiResult(
            "Cập nhật cá nhân thành công!",
            2000
          )
      );
    }
  }

  

  /**
   * Hàm reset form, gọi khi nhấn nút reset dữ liệu
   */
  public onFormReset() {
    this.canhanIOForm.reset();
  }

  /**
   * Hàm lưu và reset form để tiếp tục nhập mới dữ liệu. Trường hợp này khi người dùng muốn nhập dữ liệu liên tục
   */
  async onContinueAdd(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.canhanIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.onFormReset();
      this.purpose = "new";
    }
  }
  
  /**
   * Validation click submit
   */
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.canhanIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }
  public closeCanhanIOSidebar() {
    this.matSidenavService.close();
  }

  /**
   * Hàm đóng mat dialog
   */
  closeMatDialog() {
    this.imDialog.closeAll();
  }

  /**
   * Hàm mở thư viện dialog
   */
  public showMatDialog() {
    this.mDialog.setDialog(this, ThuvienComponent, 'showFileSelect', 'closeMatDialog', 'simpleFileV1', '75%', '85vh');
    this.mDialog.open();
  }

  /**
   * Hiển thị những file select trong thư viện
   */
  showFileSelect() {
    this.fileArray = this.mDialog.dataResult;
    this.srcanh = this.fileArray[0].link;
  }

  /**
   * Xóa ảnh hiện có
   */
  deleteAnh() {
    this.srcanh = '';
  }

  /**
   *  Hàm gọi từ function con gọi vào chạy function cha
   * @param methodName
   */
  doFunction(methodName) {
    this[methodName]();
  }
}
