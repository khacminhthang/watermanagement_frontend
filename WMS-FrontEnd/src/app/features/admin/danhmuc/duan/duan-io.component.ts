import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { displayFieldCssService, validationAllErrorMessagesService } from "src/app/core/services/utilities/validatorService";
import { InputDuanModel } from "src/app/models/admin/danhmuc/duan.model";
import { HttpErrorResponse } from "@angular/common/http";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { DatePipe } from "@angular/common";

@Component({
  selector: "app-duan-io",
  templateUrl: "./duan-io.component.html",
  styleUrls: ["./duan-io.component.scss"]
})
export class DmDuanIoComponent implements OnInit {
  duanIOForm: FormGroup;
  public inputModel: InputDuanModel;
  public editMode: boolean;
  public purpose: string;
  public obj: any;

  // error message
  validationErrorMessages = {
    projectName: { required: "Tên dự án không được để trống!" }
  };

  // form errors
  formErrors = {
    projectName: ""
  };

  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    public dmFacadeService: DmFacadeService,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
  }

  // config Form use add or update
  bindingConfigAddOrUpdate() {
    this.editMode = false;
    this.inputModel = new InputDuanModel();
    if (this.purpose === "new" || this.purpose === "edit") {
      this.editMode = true;
    }
    // check edit
    if (this.editMode === true && this.obj) {
      this.duanIOForm.setValue({
        projectCode: this.obj.projectCode,
        projectName: this.obj.projectName,
        signedDate: this.obj.signedDate,
        signedAgency: this.obj.signedAgency,
        completeDate: this.obj.completeDate,
        projectLeader: this.obj.projectLeader
      });
    }
  }

  /**
   * Hàm khởi tạo form thêm hoặc sửa
   */
  bindingConfigValidation() {
    this.duanIOForm = this.formBuilder.group({
      projectName: ["", Validators.required],
      projectCode: [""],
      signedDate: [""],
      signedAgency: [""],
      completeDate: [""],
      projectLeader: [""]
    });
  }

  /**
  * Hàm lưu
  * @param operMode trạng thái thêm hoặc sửa
  */
  public onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.duanIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.matSidenavService.close();
    }
  }

  /**
   * Hàm gọi hàm thêm hoặc sửa
   */
  public addOrUpdateContinue(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.duanIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.onFormReset();
      this.purpose = "new";
    }
  }

  /**
   * add or update
   * @param operMode trạng thái thêm hoặc sửa
   */
  private addOrUpdate(operMode: string) {
    this.inputModel = this.duanIOForm.value;
    this.inputModel.signedDate = this.datePipe.transform(this.duanIOForm.value.signedDate, "yyyy-MM-dd");
    this.inputModel.completeDate = this.datePipe.transform(this.duanIOForm.value.completeDate, "yyyy-MM-dd");
    if (operMode === "new") {
      this.dmFacadeService
        .getDuanService()
        .addItem(this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("refreshList"),
          (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Thêm mới dự án thành công!",
              2000
            )
        );
    } else if (operMode === "edit") {
      const id: number = this.obj.idProject;
      this.dmFacadeService
        .getDuanService()
        .updateItem(id, this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("refreshList"),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Cập nhập dữ liệu thành công",
              2000
            )
        );
    }
  }

  /**
   * on form reset
   */
  public onFormReset() {
    this.duanIOForm.reset();
  }

  /**
   * Validation click submit
   */
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.duanIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  /**
   * display fields css
   */
  public displayFieldCss(field: string) {
    displayFieldCssService(field);
  }

  /**
   * Hàm đóng sidenav
   */
  public closeDuanIOSidebar() {
    this.matSidenavService.close();
  }
}
