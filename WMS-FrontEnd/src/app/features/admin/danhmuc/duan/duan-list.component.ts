import {
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { OutputDuanModel } from "src/app/models/admin/danhmuc/duan.model";
import { DmDuanIoComponent } from "src/app/features/admin/danhmuc/duan/duan-io.component";
import { MatSidenav } from "@angular/material/sidenav";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { QueryCellInfoEventArgs } from "@syncfusion/ej2-grids";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: "app-duan-list",
  templateUrl: "./duan-list.component.html",
  styleUrls: ["./duan-list.component.scss"]
})
export class DmDuanListComponent implements OnInit {
  listData: any;
  listDuan: OutputDuanModel[];
  selectedItem: OutputDuanModel;
  navArray = [
    { title: "Quản trị", url: "/admin" },
    {
      title: "Danh mục",
      url: "/admin/danhmuc"
    },
    {
      title: "Dự án",
      url: "//admin/danhmuc/duan"
    }
  ];

  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("duanio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;

  settingsCommon = new SettingsCommon();

  constructor(
    public matsidenavService: MatsidenavService,
    public progressService: ProgressService,
    public cfr: ComponentFactoryResolver,
    public dmFacadeService: DmFacadeService,
    public commonService: CommonServiceShared,
    private subHeaderService: SubHeaderService
  ) { }
  ngOnInit() {
    this.matsidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 10 };
    this.getAllDuan();
    this.subHeaderService.setParentComp(this);
    this.dataSubHeader();
  }

  /**
   * Gán dữ liệu vào sub-header service
   */
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
  }

  /**
   * get all Dự án
   */
  async getAllDuan() {
    this.listData = await this.dmFacadeService
      .getDuanService()
      .getFetchAll();
    const listDataItems = this.listData;
    if (listDataItems) {
      listDataItems.map((duan, index) => {
        duan.serialNumber = index + 1;
      });
    }
    this.listDuan = listDataItems;
  }
  
   /**
   * Mở sidenav thêm mới
   */
  public openDuanIOSidebar() {
    this.matsidenavService.setTitle("Thêm mới dữ liệu dự án");
    this.matsidenavService.setContentComp(DmDuanIoComponent, "new");
    this.matsidenavService.open();
  }

  /**
   * Mở sidenav sửa
   * @param event 
   */
  public openDuanIOSidebarEdit(event) {
    this.getItemByEvent(event);
    this.matsidenavService.setTitle("Sửa dữ liệu thông tin dự án");
    this.matsidenavService.setContentComp(
      DmDuanIoComponent,
      "edit",
      this.selectedItem
    );
    this.matsidenavService.open();
  }

  /**
   * Đóng sidenav
   */
  public closeDuanIOSidebar() {
    this.matsidenavService.close();
  }

  /**
   * Convert string thành dd/mm/yyyy
   */
  public convertTimeString(data) {
    if (data !== null) {
      const [year, month, day]: string[] = data.split('-');
      const awesomeDateTime = `${day}-${month}-${year}`;
      return awesomeDateTime;
    } else return null;
  }

  /**
   * Hàm get dữ liệu dự án đang được chọn
   * @param event 
   */
  public getItemByEvent(event) {
    const target = event.currentTarget;
    const pElement = target.parentElement.parentElement;
    const pclassID = pElement.getAttribute("id");
    const item = this.listDuan.find(x => x.idProject === +pclassID);
    this.selectedItem = item;
  }


  /**
   * Hàm xóa đơn vị đo
   */
  public deleteDuan(event) {
    this.getItemByEvent(event);
    const canDelete: string = this.dmFacadeService
      .getDuanService()
      .checkBeDeleted(this.selectedItem.idProject);
    this.canBeDeletedCheck(canDelete);
  }

  public canBeDeletedCheck(sMsg: string) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog();
    } else {
      this.canDeleteDialog(sMsg);
    }
  }

  /**
   * Hàm hiển thị form xác nhận xóa
   */
  confirmDeleteDiaLog() {
    const dialogRef = this.commonService.confirmDeleteDiaLogService(
      "Tên dự án:",
      this.selectedItem.projectName
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.dmFacadeService
          .getDuanService()
          .deleteItem(this.selectedItem.idProject).subscribe(
          () => this.refreshList(),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () => this.commonService.showeNotiResult("Đã xóa thành công: " + this.selectedItem.projectName, 2000)
        );
      }
    });
  }

  canDeleteDialog(sMsg: string) {
    this.commonService.canDeleteDialogService(sMsg);
  }

  /**
   * Hàm lấy lại danh sách dự án
   */
  public refreshList() {
    this.getAllDuan();
  }

  // set id in coloumn
  customiseCell(args: QueryCellInfoEventArgs) {
    if (args.column.field === "idProject") {
      args.cell.id = args.data[args.column.field];
    }
  }

  // call method
  doFunction(methodName) {
    this[methodName]();
  }
}
