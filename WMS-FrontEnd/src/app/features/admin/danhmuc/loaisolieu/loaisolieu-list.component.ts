import {
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { DmLoaisolieuIoComponent } from "src/app/features/admin/danhmuc/loaisolieu/loaisolieu-io.component";
import { OutputLoaisolieuModel } from "src/app/models/admin/danhmuc/loaisolieu.model";
import { MatSidenav } from "@angular/material/sidenav";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { QueryCellInfoEventArgs } from "@syncfusion/ej2-grids";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import {HttpErrorResponse} from "@angular/common/http";
@Component({
  selector: "app-loaisolieu-list",
  templateUrl: "./loaisolieu-list.component.html",
  styleUrls: ["./loaisolieu-list.component.scss"]
})
export class DmLoaisolieuListComponent implements OnInit {
  listLoaisolieu: OutputLoaisolieuModel[];
  selectedItem: OutputLoaisolieuModel;
  listData: any;
  navArray = [
    { title: "Quản trị", url: "/admin" },
    {
      title: "Danh mục",
      url: "/admin/danhmuc"
    },
    {
      title: "Loại số liệu",
      url: "//admin/danhmuc/loaisolieu"
    }
  ];

  buttonArray = [
    // {
    //   title: 'Thêm',
    //   icon: 'fal fa-plus-square',
    //   color: 'btn-primary',
    //   event: 'openLoaisolieuIOSidebar'
    // },
    // {
    //   title: 'Tải lại dữ liệu',
    //   icon: 'far fa-sync-alt',
    //   color: 'btn-outline-primary mr-3',
    //   event: 'getAllLoaisolieu'
    // }
  ];
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compLoaisolieuio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;

  settingsCommon = new SettingsCommon();

  constructor(
    public matsidenavService: MatsidenavService,
    public progressService: ProgressService,
    public cfr: ComponentFactoryResolver,
    public dmFacadeService: DmFacadeService,
    public commonService: CommonServiceShared,
    private subHeaderService: SubHeaderService
  ) { }
  ngOnInit() {
    this.matsidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 10 };
    this.getAllLoaisolieu();
    this.subHeaderService.setParentComp(this);
    this.dataSubHeader();
  }

  /**
   * Gán dữ liệu vào sub-header service
   */
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }

  /**
   * get all Loai so lieu
   */
  async getAllLoaisolieu() {
    this.listData = await this.dmFacadeService
      .getLoaisolieuService()
      .getFetchAll();
    const listDataItems = this.listData;
    if (listDataItems) {
      listDataItems.map((loaisolieu, index) => {
        loaisolieu.serialNumber = index + 1;
      });
    }
    this.listLoaisolieu = listDataItems;
  }
  /**
   * open sidebar excute insert
   */
  public openLoaisolieuIOSidebar() {
    this.matsidenavService.setTitle("Thêm mới dữ liệu loại số liệu");
    this.matsidenavService.setContentComp(DmLoaisolieuIoComponent, "new");
    this.matsidenavService.open();
  }

  /**
   *  open sidebar edit
   */
  public openLoaisolieuIOSidebarEdit(event) {
    this.getItemByEvent(event);
    this.matsidenavService.setTitle("Sửa dữ liệu thông tin loại số liệu");
    this.matsidenavService.setContentComp(
      DmLoaisolieuIoComponent,
      "edit",
      this.selectedItem
    );
    this.matsidenavService.open();
  }

  /**
   * close sidebar
   */
  public closeLoaisolieuIOSidebar() {
    this.matsidenavService.close();
  }

  /**
   *  getItemByEvent
   */
  private getItemByEvent(event) {
    const item = this.commonService.getByEvent(event, this.listLoaisolieu);
    this.selectedItem = item;
  }

  /**
   * delete
   */
  public deleteLoaisolieu(event) {
    this.getItemByEvent(event);
    const canDelete: string = this.dmFacadeService
      .getLoaisolieuService()
      .checkBeDeleted(this.selectedItem.id);
    this.canBeDeletedCheck(canDelete);
  }

  public canBeDeletedCheck(sMsg: string) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog();
    } else {
      this.canDeleteDialog(sMsg);
    }
  }

  confirmDeleteDiaLog() {
    const dialogRef = this.commonService.confirmDeleteDiaLogService(
      "Tên loại số liệu:",
      this.selectedItem.tenloaisolieu
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.dmFacadeService
          .getLoaisolieuService()
          .deleteItem(this.selectedItem.id).subscribe(
          () => this.refreshList(),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () => this.commonService.showeNotiResult("Đã xóa thành công: " + this.selectedItem.tenloaisolieu, 2000)
        );
      }
    });
  }

  canDeleteDialog(sMsg: string) {
    this.commonService.canDeleteDialogService(sMsg);
  }

  // refresh grid
  public refreshList() {
    this.getAllLoaisolieu();
  }

  // set id in coloumn
  customiseCell(args: QueryCellInfoEventArgs) {
    if (args.column.field === "id") {
      args.cell.id = args.data[args.column.field];
    }
  }

  // call method
  doFunction(methodName) {
    this[methodName]();
  }
}
