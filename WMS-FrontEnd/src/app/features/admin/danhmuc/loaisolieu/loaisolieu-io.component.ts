import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  displayFieldCssService, validationAllErrorMessagesService,
} from "src/app/core/services/utilities/validatorService";
import { HttpErrorResponse } from "@angular/common/http";
import { InputLoaisolieuModel } from "src/app/models/admin/danhmuc/loaisolieu.model";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";

@Component({
  selector: "app-loaisolieu-io",
  templateUrl: "./loaisolieu-io.component.html",
  styleUrls: ["./loaisolieu-io.component.scss"]
})
export class DmLoaisolieuIoComponent implements OnInit {
  loaisolieuIOForm: FormGroup;
  public inputModel: InputLoaisolieuModel;
  public editMode: boolean;
  public purpose: string;
  public obj: any;

  // error message
  validationErrorMessages = {
    tenloaisolieu: { required: "Tên loại số liệu không được để trống!" }
  };

  // form errors
  formErrors = {
    tenloaisolieu: ""
  };

  // ctor
  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    public dmFacadeService: DmFacadeService
  ) { }

  // onInit
  ngOnInit() {
    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
  }

  /**
   * config Form use add or update
   */
  bindingConfigAddOrUpdate() {
    this.editMode = false;
    this.inputModel = new InputLoaisolieuModel();
    if (this.purpose === "new" || this.purpose === "edit") {
      this.editMode = true;
    }
    // check edit
    if (this.editMode === true && this.obj) {
      this.loaisolieuIOForm.setValue({
        tenloaisolieu: this.obj.tenloaisolieu,
        tenviettat: this.obj.tenviettat
      });
    }
  }

  /**
   * config input validation form
   */
  bindingConfigValidation() {
    this.loaisolieuIOForm = this.formBuilder.group({
      tenloaisolieu: ["", Validators.required],
      tenviettat: [""]
    });
  }

  /**
   * Hàm lưu
   */
  public onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.loaisolieuIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.matSidenavService.close();
    }
  }

  /**
   * Hàm thêm mới và reset lại form
   */
  public addOrUpdateContinue(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.loaisolieuIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.onFormReset();
      this.purpose = "new";
    }
  }

  /**
   * Hàm thêm mới hoặc sửa
   */
  private addOrUpdate(operMode: string) {
    this.inputModel = this.loaisolieuIOForm.value;
    if (operMode === "new") {
      this.dmFacadeService
        .getLoaisolieuService()
        .addItem(this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("refreshList"),
          (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Thêm mới loại số liệu thành công!",
              2000
            )
        );
    } else if (operMode === "edit") {
      const id: number = this.obj.id;
      this.dmFacadeService
        .getLoaisolieuService()
        .updateItem(id, this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("refreshList"),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Cập nhập dữ liệu thành công",
              2000
            )
        );
    }
  }

  /**
   * reset lại form
   */
  public onFormReset() {
    this.loaisolieuIOForm.reset();
  }

  /**
   * Validation click submit
   */
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.loaisolieuIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  /**
   * display fields css
   */
  public displayFieldCss(field: string) {
    displayFieldCssService(field);
  }

  /**
   * close sidebar
   */
  public closeLoaisolieuIOSidebar() {
    this.matSidenavService.close();
  }
}
