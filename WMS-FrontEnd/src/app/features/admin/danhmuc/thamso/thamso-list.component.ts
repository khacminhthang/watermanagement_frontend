import {
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { OutputThamsoModel } from "src/app/models/admin/danhmuc/thamso.model";
import { MatSidenav } from "@angular/material/sidenav";
import { MatDialog } from "@angular/material/dialog";
import { DmThamsoIoComponent } from "src/app/features/admin/danhmuc/thamso/thamso-io.component";
import { MyAlertDialogComponent } from "src/app/shared/components/my-alert-dialog/my-alert-dialog.component";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { QueryCellInfoEventArgs } from "@syncfusion/ej2-angular-grids";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import {HttpErrorResponse} from "@angular/common/http";
@Component({
  selector: "app-thamso-list",
  templateUrl: "./thamso-list.component.html",
  styleUrls: ["./thamso-list.component.scss"]
})
export class DmThamsoListComponent implements OnInit {
  listThamso: OutputThamsoModel[];
  selectedItem: OutputThamsoModel;
  listDuLieu: any;
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compThamsoio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  settingsCommon = new SettingsCommon();
  navArray = [
    { title: "Quản trị", url: "/admin" },
    {
      title: "Danh mục",
      url: "/admin/danhmuc"
    },
    {
      title: "Tham số",
      url: "//admin/danhmuc/thamso"
    }
  ];

  buttonArray = [];
  constructor(
    public matsidenavService: MatsidenavService,
    public progressService: ProgressService,
    public cfr: ComponentFactoryResolver,
    public dmFacadeService: DmFacadeService,
    public modalDialog: MatDialog,
    public commonService: CommonServiceShared,
    private subHeaderService: SubHeaderService
  ) { }

  ngOnInit() {
    this.subHeaderService.setParentComp(this);
    this.matsidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 10 };
    this.getAllThamso();
    this.dataSubHeader();
  }

  /**
   *  Truyền data vào header
   */
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }

  /**
   * get all tham so
   */
  async getAllThamso() {
    this.listDuLieu = await this.dmFacadeService
      .getThamsoService()
      .getFetchAll();
    const listDataItems = this.listDuLieu;
    if (listDataItems) {
      listDataItems.map((thamso, index) => {
        thamso.serialNumber = index + 1;
      });
    }
    this.listThamso = listDataItems;
  }

  /**
   * open sidebar execute insert
   */
  public openThamsoIOSidebar() {
    this.matsidenavService.setTitle("Thêm mới dữ liệu tham số");
    this.matsidenavService.setContentComp(DmThamsoIoComponent, "new");
    this.matsidenavService.open();
  }

  /**
   * edit open sidebar
   */
  public editItemThamso(event) {
    this.getItemByEvent(event);
    this.matsidenavService.setTitle("Sửa dữ liệu thông tin tham số");
    this.matsidenavService.setContentComp(
      DmThamsoIoComponent,
      "edit",
      this.selectedItem
    );
    this.matsidenavService.open();
  }

  /**
   * delete
   */
  public deleteThamso(event) {
    this.getItemByEvent(event);
    const canDelete: string = this.dmFacadeService
      .getThamsoService()
      .checkBeDeleted(this.selectedItem.id);
    this.canBeDeletedCheck(canDelete);
  }

  public canBeDeletedCheck(sMsg: string) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog();
    } else {
      this.canDeleteDialog(sMsg);
    }
  }

  confirmDeleteDiaLog() {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Chắc chắn xóa?";
    dialogRef.componentInstance.content =
      "Tham số: <b>" + this.selectedItem.tenthamso + "</b>";
    dialogRef.componentInstance.okeButton = "Đồng ý";
    dialogRef.componentInstance.cancelButton = "Hủy bỏ";
    dialogRef.componentInstance.visibleOkButton = true;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.dmFacadeService
          .getThamsoService()
          .deleteItem(this.selectedItem.id).subscribe(
          () => this.getAllThamso(),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () => this.commonService.showeNotiResult("Đã xóa thành công: " + this.selectedItem.tenthamso, 2000)
        );
      }
    });
  }

  canDeleteDialog(sMsg: string) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Không thể xóa!";
    dialogRef.componentInstance.content = "<b>" + sMsg + "</b>";
    dialogRef.componentInstance.cancelButton = "Đóng";
    dialogRef.componentInstance.visibleOkButton = false;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.componentInstance.headerColor = "red";
  }

  /**
   * close
   */
  public closeThamsoIOSidebar() {
    this.matSidenav.close();
  }

  /**
   * set id in coloumn
   */
  customiseCell(args: QueryCellInfoEventArgs) {
    if (args.column.field === "id") {
      args.cell.id = args.data[args.column.field];
    }
  }
  /**
   * getItemByEvent
   */
  public getItemByEvent(event) {
    const target = event.currentTarget;
    const item = this.commonService.getByEvent(event, this.listThamso);
    this.selectedItem = item;
  }

  // call method
  doFunction(methodName) {
    this[methodName]();
  }
}
