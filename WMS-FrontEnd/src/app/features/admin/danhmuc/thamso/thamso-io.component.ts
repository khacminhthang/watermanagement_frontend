import { OutputNhomthamsoModel } from "src/app/models/admin/danhmuc/nhomthamso.model";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  validationAllErrorMessagesService,
} from "src/app/core/services/utilities/validatorService";
import { HttpErrorResponse } from "@angular/common/http";
import { InputThamsoModel } from "src/app/models/admin/danhmuc/thamso.model";
import { donvido } from "src/app/features/admin/consts/enum";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import {OutputDonvidoModel} from "src/app/models/admin/danhmuc/donvido.model";

@Component({
  selector: "app-thamso-io",
  templateUrl: "./thamso-io.component.html",
  styleUrls: ["./thamso-io.component.scss"]
})
export class DmThamsoIoComponent implements OnInit {
  thamsoIOForm: FormGroup;
  submitted = false;
  public editMode: boolean;
  public purpose: string;
  public inputModel: InputThamsoModel;
  public obj: any;
  public nhomthamsoList: OutputNhomthamsoModel[];
  public nhomthamsoListFilter: OutputNhomthamsoModel[];
  public donvidoListFilter: OutputDonvidoModel[];
  public donvido = donvido;
  public listDulieuNhomThamSo: any;
  listDonvido: any;
  listDulieuDonvido: any;

  // error message
  validationErrorMessages = {
    tenthamso: { required: "Tên tham số không được để trống!" },
    idNhomthamso: { required: "Nhóm tham số không được để trống!" }
  };

  // form errors
  formErrors = {
    tenthamso: "",
    idNhomthamso: ""
  };

  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    public dmFacadeService: DmFacadeService
  ) {
    this.matSidenavService.okCallBackFunction = null;
    this.matSidenavService.cancelCallBackFunction = null;
    this.matSidenavService.confirmStatus = null;
  }

  ngOnInit() {
    this.formInit();
    // Nếu editMode = true và purpose là edit (obj đã được set) thì sẽ set giá trị cho các input Textbox bằng đối tượng vừa được nhấn edit
    this.bindingConfigAddOrUpdate();
    this.getNhomthamso();
    this.getAllDonvido();
  }

  async getNhomthamso() {
    this.listDulieuNhomThamSo = await this.dmFacadeService
      .getNhomthamsoService()
      .getFetchAll();
    this.nhomthamsoList = this.listDulieuNhomThamSo;
  }

  /**
   * config Form use add or update
   */
  bindingConfigAddOrUpdate() {
    this.editMode = false;
    this.inputModel = new InputThamsoModel();
    if (this.purpose === "new" || this.purpose === "edit") {
      this.editMode = true;
    }
    // check edit
    this.formOnEdit();
  }

  /**
   * init FormControl
   */
  formInit() {
    this.thamsoIOForm = this.formBuilder.group({
      tenthamso: ["", Validators.required],
      kyhieuthamso: [""],
      idNhomthamso: ["", Validators.required],
      idDonvidomacdinh: [""]
    });
  }

  /**
   * init edit form
   */
  formOnEdit() {
    if (this.editMode === true && this.obj) {
      this.thamsoIOForm.setValue({
        tenthamso: this.obj.tenthamso,
        kyhieuthamso: this.obj.kyhieuthamso,
        idNhomthamso: this.obj.idNhomthamso,
        idDonvidomacdinh: this.obj.idDonvidomacdinh
      });
    }
  }

  /**
   * on submit
   */
  async onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    if(this.thamsoIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.matSidenavService.close();
    }
  }

  /**
   * get all tham so
   */
  async getAllDonvido() {
    this.listDulieuDonvido = await this.dmFacadeService
      .getDonvidoService()
      .getFetchAll();
    this.listDonvido = this.listDulieuDonvido;
    this.donvidoListFilter = this.listDulieuDonvido;
  }

  private addOrUpdate(operMode: string) {
    this.inputModel = this.thamsoIOForm.value;
    if (operMode === "new") {
      this.dmFacadeService
        .getThamsoService()
        .addItem(this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("getAllThamso"),
          (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Thêm mới tham số thành công!",
              2000
            )
        );
    } else if (operMode === "edit") {
      const id: number = this.obj.id;
      this.dmFacadeService
        .getThamsoService()
        .updateItem(id, this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("getAllThamso"),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Cập nhập dữ liệu thành công",
              2000
            )
        );
    }
  }

  /**
   * on save and reset form
   */
  async addContinue(operMode: string) {
    this.logAllValidationErrorMessages();
    if(this.thamsoIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.onFormReset();
      this.purpose = "new";
    }
  }

  /**
   * reset form
   */
  public onFormReset() {
    this.thamsoIOForm.reset();
  }

  /**
   * Validation click submit
   */
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.thamsoIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  // close sidebar
  public closeThamsoIOSidebar() {
    this.matSidenavService.close();
  }
}
