import {
  Component,
  OnInit,
} from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { InputDvhcModel } from "src/app/models/admin/danhmuc/dvhc.model";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { GlobalVar } from "src/app/shared/constants/global-var";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";

@Component({
  selector: "app-dvhc-io",
  templateUrl: "./dvhc-io.component.html",
  styleUrls: ["./dvhc-io.component.scss"]
})
export class DmDvhcIoComponent implements OnInit {
  dvhcIOForm: FormGroup;
  submitted = false;
  public editMode: boolean;
  public purpose: string;
  public inputDvhcModel: InputDvhcModel;
  hiddenProvineName = true;
  hiddenDistrictName = true;
  hiddenMatinh = false;
  hiddenMahuyen = true;
  hiddenMaxa = true;
  disabledMatinh = false;
  public obj: any;

  constructor(
    private formBuilder: FormBuilder,
    private matSidenavService: MatsidenavService,
    private dmFacadeSv: DmFacadeService,
    private commonService: CommonServiceShared
  ) {
    this.matSidenavService.okCallBackFunction = null;
    this.matSidenavService.cancelCallBackFunction = null;
    this.matSidenavService.confirmStatus = null;
  }

  ngOnInit() {
    this.configOnInit();
  }

  /**
   * config OnInit
   */
  configOnInit() {
    this.editMode = false;
    this.inputDvhcModel = new InputDvhcModel();
    // declare fields, validate
    this.dvhcIOForm = this.formBuilder.group({
      id: [""],
      tendvhc: ["", Validators.required],
      matinh: [""],
      mahuyen: [""],
      maxa: [""],
      tentinh: [{ value: "", disabled: true }],
      tenhuyen: [{ value: "", disabled: true }]
    });

    const sideBarModeProvince = ["editProvince", "newProvince"];
    const sideBarModeDistrict = ["newDistrict", "editDistrict"];
    const sideBarModeWard = ["newWard", "editWard"];
    if (sideBarModeProvince.indexOf(this.purpose) !== -1) {
      this.editMode = true;
    } else if (sideBarModeDistrict.indexOf(this.purpose) !== -1) {
      this.editMode = true;
      this.hiddenProvineName = false;
      this.hiddenMatinh = true;
      this.hiddenMahuyen = false;
      this.dvhcIOForm.controls.id.setValue(this.obj.id);
      this.dvhcIOForm.controls.tentinh.setValue(
        GlobalVar.provinceSelected
      );
      this.dvhcIOForm.controls.matinh.setValue(this.obj.matinh);
    } else if (sideBarModeWard.indexOf(this.purpose) !== -1) {
      this.editMode = true;
      this.hiddenMatinh = true;
      this.hiddenMahuyen = true;
      this.hiddenProvineName = false;
      this.hiddenDistrictName = false;
      this.hiddenMaxa = false;
      this.dvhcIOForm.controls.id.setValue(this.obj.id);
      this.dvhcIOForm.controls.matinh.setValue(this.obj.matinh);
      this.dvhcIOForm.controls.mahuyen.setValue(this.obj.mahuyen);
      this.dvhcIOForm.controls.tentinh.setValue(
        GlobalVar.provinceSelected
      );
      this.dvhcIOForm.controls.tenhuyen.setValue(
        GlobalVar.districtSelected
      );
    }
    // edit province
    if (this.purpose === "editProvince" && this.editMode === true && this.obj) {
      this.dvhcIOForm.setValue({
        id: this.obj.id,
        tendvhc: this.obj.tendvhc,
        matinh: this.obj.matinh,
        tentinh: this.obj.tendvhc,
        tenhuyen: "",
        mahuyen: "",
        maxa: ""
      });
    }
    // edit district
    if (this.purpose === "editDistrict" && this.editMode === true && this.obj) {
      this.hiddenProvineName = false;
      this.dvhcIOForm.setValue({
        id: this.obj.id,
        tendvhc: this.obj.tendvhc,
        mahuyen: this.obj.mahuyen,
        tenhuyen: this.obj.tendvhc,
        maxa: "",
        matinh: "",
        tentinh: GlobalVar.provinceSelected
      });
    }
    // edit wardCode
    if (this.purpose === "editWard" && this.editMode === true && this.obj) {
      (this.hiddenDistrictName = false),
        (this.hiddenProvineName = false),
        this.dvhcIOForm.setValue({
          id: this.obj.id,
          tendvhc: this.obj.tendvhc,
          maxa: this.obj.maxa,
          mahuyen: "",
          matinh: "",
          tentinh: GlobalVar.provinceSelected,
          tenhuyen: GlobalVar.districtSelected
        });
    }
  }

  /**
   * Hàm lưu
   */
  public onSubmit(operMode: string) {
    if (operMode === "newProvince" || operMode === "editProvince") {
      this.addOrUpdateProvince(operMode);
    } else if (operMode === "newDistrict" || operMode === "editDistrict") {
      this.addOrUpdateDistrict(operMode);
    } else if (operMode === "newWard" || operMode === "editWard") {
      this.addOrUpdateWard(operMode);
    }
  }

  /**
   * add or update province
   */
  private addOrUpdateProvince(operMode: string) {
    this.inputDvhcModel = this.dvhcIOForm.value;
    if (operMode === "newProvince") {
      this.dmFacadeSv
        .getProvinceService()
        .addItem(this.inputDvhcModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("getAllProvince"),
          (errorResponese: HttpErrorResponse) => {
            this.commonService.showError(errorResponese);
          },
          () =>
            this.commonService.showeNotiResult(
              "Tạo mới tỉnh/thành phố thành công!",
              2000
            )
        );
    } else if (operMode === "editProvince") {
      const id: number = this.obj.id;
      this.dmFacadeSv
        .getProvinceService()
        .updateItem(id, this.inputDvhcModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("getAllProvince"),
          (errorResponese: HttpErrorResponse) => {
            this.commonService.showError(errorResponese);
          },
          () =>
            this.commonService.showeNotiResult(
              "Cập nhập tỉnh/thành phố thành công!",
              2000
            )
        );
    }
    // close sidebar
    this.closeDvhcIOSidebar();
  }

  /**
   * add or update district
   */
  private addOrUpdateDistrict(operMode: string) {
    this.inputDvhcModel = this.dvhcIOForm.value;
    if (operMode === "newDistrict") {
      this.inputDvhcModel.id = 0;
      this.inputDvhcModel.parentid = this.obj.id;
      this.dmFacadeSv
        .getDistrictService()
        .addItem(this.inputDvhcModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("refreshGridDistrict"),
          (errorResponse: HttpErrorResponse) => {
            this.commonService.showError(errorResponse);
          },
          () =>
            this.commonService.showeNotiResult(
              "Tạo mới quận/huyện/thị xã thành công!",
              2000
            )
        );
    } else if (operMode === "editDistrict") {
      const id: number = this.obj.id;
      this.inputDvhcModel.matinh = this.obj.matinh;
      this.inputDvhcModel.parentid = this.obj.parentid;
      this.dmFacadeSv
        .getDistrictService()
        .updateItem(id, this.inputDvhcModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("refreshGridDistrict"),
          (errorResponse: HttpErrorResponse) => {
            this.commonService.showError(errorResponse);
          },
          () =>
            this.commonService.showeNotiResult(
              "Cập nhập quận/huyện/thị xã thành công!",
              2000
            )
        );
    }
    // close sidebar
    this.closeDvhcIOSidebar();
  }

  /**
   *  add or update ward
   */
  private addOrUpdateWard(operMode: string) {
    this.inputDvhcModel = this.dvhcIOForm.value;
    if (operMode === "newWard") {
      this.inputDvhcModel.id = 0;
      this.inputDvhcModel.parentid = this.obj.id;
      this.dmFacadeSv
        .getWardService()
        .addItem(this.inputDvhcModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("refreshGridWard"),
          (errorResponse: HttpErrorResponse) => {
            this.commonService.showError(errorResponse);
          },
          () =>
            this.commonService.showeNotiResult(
              "Tạo mới xã/phường/thị trấn thành công!",
              2000
            )
        );
    } else if (operMode === "editWard") {
      const id: number = this.obj.id;
      this.inputDvhcModel.matinh = this.obj.matinh;
      this.inputDvhcModel.mahuyen = this.obj.mahuyen;
      this.inputDvhcModel.parentid = this.obj.parentid;
      this.dmFacadeSv
        .getWardService()
        .updateItem(id, this.inputDvhcModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("refreshGridWard"),
          (errorResponse: HttpErrorResponse) => {
            this.commonService.showError(errorResponse);
          },
          () =>
            this.commonService.showeNotiResult(
              "Cập nhật xã/phường/thị trấn thành công!",
              2000
            )
        );
    }
    // close sidebar
    this.closeDvhcIOSidebar();
  }

  /**
   * reset form
   */
  public onFormReset() {
    this.dvhcIOForm.reset();
  }

  /**
   * close dvhc
   */
  public closeDvhcIOSidebar() {
    this.matSidenavService.close();
  }
}
