import {
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { OutputDvhcModel } from "src/app/models/admin/danhmuc/dvhc.model";
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { ServiceName } from "src/app/shared/constants/service-name";
import { MatSidenav } from "@angular/material/sidenav";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { QueryCellInfoEventArgs } from "@syncfusion/ej2-angular-grids";
import { DmDvhcIoComponent } from "./dvhc-io.component";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import { GlobalVar } from "src/app/shared/constants/global-var";
import {MyAlertDialogComponent} from "src/app/shared/components/my-alert-dialog/my-alert-dialog.component";
import {HttpErrorResponse} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
@Component({
  selector: "app-dvhc-list",
  templateUrl: "./dvhc-list.component.html",
  styleUrls: ["./dvhc-list.component.scss"]
})
export class DmDvhcListComponent implements OnInit {
  listDataDvhcProvince: OutputDvhcModel[];
  listDatadvhcDistrict: OutputDvhcModel[];
  listDatadvhcWard: OutputDvhcModel[];
  selectedItem: OutputDvhcModel;
  disabledDistrict = true;
  disabledWard = true;
  deleteName: string;
  navArray = [
    { title: "Quản trị", url: "/admin" },
    {
      title: "Danh mục",
      url: "/admin/danhmuc"
    },
    {
      title: "Đơn vị hành chính",
      url: "//admin/danhmuc/dvhc"
    }
  ];

  buttonArray = [];

  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("componentdvhcio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;

  settingsCommon = new SettingsCommon();

  constructor(
    public dmFacadeSv: DmFacadeService,
    public matsidenavService: MatsidenavService,
    public cfr: ComponentFactoryResolver,
    public progressService: ProgressService,
    public commonService: CommonServiceShared,
    private subHeaderService: SubHeaderService,
    private modalDialog: MatDialog,
  ) { }

  ngOnInit() {
    this.matsidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 10 };
    this.bindingConfig();
    this.subHeaderService.setParentComp(this);
    this.dataSubHeader();
  }

  /**
   * Gán dữ liệu vào sub-header service
   */
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }
  // binding config
  async bindingConfig() {
    await this.getAllProvince();
  }

  /**
   * get list province
   */
  async getAllProvince() {
    const listData: any = await this.dmFacadeSv.getProvinceService().getFetchAll();
    const listDataItems = listData;
    if (listDataItems) {
      listDataItems.map((tinh, index) => {
        tinh.serialNumber = index + 1;
      });
    }
    this.listDataDvhcProvince = listDataItems;
  }

  /**
   * get district
   */
  async getDistrict(matinh: string) {
    const listDataItems: any = await this.dmFacadeSv
      .getDistrictService()
      .getFetchAllId(matinh);
    if (listDataItems) {
      listDataItems.map((huyen, index) => {
        huyen.serialNumber = index + 1;
      });
    }
    this.listDatadvhcDistrict = listDataItems;
  }

  /**
   * get ward
   */
  async getWard(mahuyen: string) {
    const listDataItems: any = await this.dmFacadeSv
      .getWardService()
      .getFetchAllId(mahuyen);
    if (listDataItems) {
      listDataItems.map((xa, index) => {
        xa.serialNumber = index + 1;
      });
    }
    this.listDatadvhcWard = listDataItems;
  }

  /**
   * childTinh
   */
  public childProvince(event) {
    this.getChildProvinceByEvent(event);
    this.disabledDistrict = false;
    this.listDatadvhcWard = [];
    this.disabledWard = true;
  }

  /**
   * child Huyen
   */
  public childDistrict(event) {
    this.getChildDistrictByEvent(event);
    this.disabledWard = false;
  }

  /**
   * get Child Province by event
   */
  private getChildProvinceByEvent(event) {
    this.selectedItem = null;
    const data = this.commonService.getByEvent(
      event,
      this.listDataDvhcProvince
    );
    this.getDistrict(data.matinh);
    this.selectedItem = data;
    GlobalVar.provinceSelected = this.selectedItem.tendvhc;
  }

  /**
   * get child district by event
   */
  private getChildDistrictByEvent(event) {
    this.selectedItem = null;
    const data = this.commonService.getByEvent(
      event,
      this.listDatadvhcDistrict
    );
    this.getWard(data.mahuyen);
    this.selectedItem = data;
    GlobalVar.districtSelected = this.selectedItem.tendvhc;
  }

  /**
   * Open sidebar add Province
   */
  public openDvhcProvinceIOSidebar() {
    this.setValueSidebar(
      "Thêm mới đơn vị hành chính tỉnh/thành phố",
      DmDvhcIoComponent,
      "newProvince"
    );
    this.listDatadvhcDistrict = [];
    this.listDatadvhcWard = [];
    this.disabledDistrict = true;
    this.disabledWard = true;
  }

  /**
   * open sidebar add District
   */
  public openDvhcDistrictIOSidebar() {
    this.setValueSidebar(
      "Thêm mới đơn vị hành chính quận/huyện/thị xã",
      DmDvhcIoComponent,
      "newDistrict",
      this.selectedItem
    );
    this.listDatadvhcWard = [];
    this.disabledWard = true;
  }

  // open sidebar add ward
  public openDvhcWardIOSidebar() {
    this.setValueSidebar(
      "Thêm mới đơn vị hành chính xã/phường/thị trấn",
      DmDvhcIoComponent,
      "newWard",
      this.selectedItem
    );
  }

  /**
   * open sidebar edit Province
   */
  public openDvhcEditProvince(event: any) {
    this.getItemByEvent(event, this.listDataDvhcProvince);
    this.setValueSidebar(
      "Sửa thông tin tỉnh/thành phố",
      DmDvhcIoComponent,
      "editProvince",
      this.selectedItem
    );
  }

  /**
   * open sidebar edit district
   */
  public openDvhcEditDistrict(event: any) {
    this.getItemByEvent(event, this.listDatadvhcDistrict);
    this.setValueSidebar(
      "Sửa thông tin quận/huyện/thị xã",
      DmDvhcIoComponent,
      "editDistrict",
      this.selectedItem
    );
  }

  /**
   * open sidebar edit ward
   */
  public openDvhcEditWard(event: any) {
    this.getItemByEvent(event, this.listDatadvhcWard);
    this.setValueSidebar(
      "Sửa thông tin xã/phường/thị trấn",
      DmDvhcIoComponent,
      "editWard",
      this.selectedItem
    );
  }

  /**
   * get item by envent
   */
  public getItemByEvent(event: any, listData: any) {
    this.selectedItem = null;
    const data = this.commonService.getByEvent(event, listData);
    this.selectedItem = data;
  }

  /**
   * set value open/edit sidebar
   */
  public setValueSidebar(
    title: string,
    component: any,
    editMode: string,
    item?: any
  ) {
    this.matsidenavService.setTitle(title);
    this.matsidenavService.setContentComp(component, editMode, item);
    this.matsidenavService.open();
  }

  /**
   * refresh grid district
   */
  public refreshGridDistrict() {
    this.getDistrict(this.selectedItem.matinh);
  }

  /**
   * resfresh grid ward
   */
  public refreshGridWard() {
    this.getWard(this.selectedItem.mahuyen);
  }

  /**
   * close
   */
  public closeDvhcIOSidebar() {
    this.matsidenavService.close();
  }

  /**
   * set id in coloumn
   */
  customiseCell(args: QueryCellInfoEventArgs) {
    if (
      args.column.field === ServiceName.ID_DVHC ||
      args.column.field === ServiceName.TEN_DVHC
    ) {
      args.cell.id = args.data[ServiceName.ID_DVHC];
    }
  }

  doFunction(methodName) {
    this[methodName]();
  }

  /**
   * Hàm xóa một bản ghi, được gọi khi nhấn nút xóa trên giao diện list
   */
  async deleteItemProvince(event: any) {
    this.getItemByEvent(event, this.listDataDvhcProvince);
    this.deleteName = 'province';
    // Phải check xem dữ liệu muốn xóa có đang được dùng ko, đang dùng thì ko xóa
    // Trường hợp dữ liệu có thể xóa thì Phải hỏi người dùng xem có muốn xóa không
    // Nếu đồng ý xóa
    const canDelete: string = this.dmFacadeSv
      .getProvinceService()
      .checkBeDeleted(this.selectedItem.id);
    this.canBeDeletedCheck(canDelete);
  }
  // Hàm xóa một bản ghi, được gọi khi nhấn nút xóa trên giao diện list
  async deleteItemDistrict(event: any) {
    this.getItemByEvent(event, this.listDatadvhcDistrict);
    this.deleteName = 'District';
    // Phải check xem dữ liệu muốn xóa có đang được dùng ko, đang dùng thì ko xóa
    // Trường hợp dữ liệu có thể xóa thì Phải hỏi người dùng xem có muốn xóa không
    // Nếu đồng ý xóa
    const canDelete: string = this.dmFacadeSv
      .getDistrictService()
      .checkBeDeleted(this.selectedItem.id);
    this.canBeDeletedCheck(canDelete);
  }
  // Hàm xóa một bản ghi, được gọi khi nhấn nút xóa trên giao diện list
  async deleteItemWard(event: any) {
    this.getItemByEvent(event, this.listDatadvhcWard);
    this.deleteName = 'Ward'
    // Phải check xem dữ liệu muốn xóa có đang được dùng ko, đang dùng thì ko xóa
    // Trường hợp dữ liệu có thể xóa thì Phải hỏi người dùng xem có muốn xóa không
    // Nếu đồng ý xóa
    const canDelete: string = this.dmFacadeSv
      .getWardService()
      .checkBeDeleted(this.selectedItem.id);
    this.canBeDeletedCheck(canDelete);
  }

  public canBeDeletedCheck(sMsg: string) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog();
    } else {
      this.cantDeleteDialog(sMsg);
    }
  }

  confirmDeleteDiaLog() {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Chắc chắn xóa?";
    dialogRef.componentInstance.content =
      "Họ và tên: <b>" + this.selectedItem.tendvhc + "</b>";
    dialogRef.componentInstance.okeButton = "Đồng ý";
    dialogRef.componentInstance.cancelButton = "Hủy bỏ";
    dialogRef.componentInstance.visibleOkButton = true;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.afterClosed().subscribe(async result => {
      if (result === "confirm") {
        if (this.deleteName === 'province') {
          await this.dmFacadeSv
            .getProvinceService()
            .deleteItem(this.selectedItem.id).subscribe(
              () => this.getAllProvince(),
              (error: HttpErrorResponse) => {
                this.commonService.showeNotiResult(error.message, 2000);
              },
              () => this.commonService.showeNotiResult("Đã xóa thành công: " + this.selectedItem.tendvhc, 2000)
            );
        }
        if (this.deleteName === 'District') {
          await this.dmFacadeSv
            .getDistrictService()
            .deleteItem(this.selectedItem.id).subscribe(
              () => this.getDistrict(this.selectedItem.matinh),
              (error: HttpErrorResponse) => {
                this.commonService.showeNotiResult(error.message, 2000);
              },
              () => this.commonService.showeNotiResult("Đã xóa thành công: " + this.selectedItem.tendvhc, 2000)
            );
        }
        if (this.deleteName === 'Ward') {
          await this.dmFacadeSv
            .getWardService()
            .deleteItem(this.selectedItem.id).subscribe(
              () => this.getWard(this.selectedItem.mahuyen),
              (error: HttpErrorResponse) => {
                this.commonService.showeNotiResult(error.message, 2000);
              },
              () => this.commonService.showeNotiResult("Đã xóa thành công: " + this.selectedItem.tendvhc, 2000)
            );
        }
      }
    });
  }

  cantDeleteDialog(sMsg: string) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Không thể xóa!";
    dialogRef.componentInstance.content = "<b>" + sMsg + "</b>";
    dialogRef.componentInstance.cancelButton = "Đóng";
    dialogRef.componentInstance.visibleOkButton = false;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.componentInstance.headerColor = "red";
  }
}
