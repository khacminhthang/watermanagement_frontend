import { ThuvienComponent } from 'src/app/features/admin/thuvien/thuvien.component';
import { MatDialog } from '@angular/material/dialog';
import { MatdialogService } from 'src/app/core/services/utilities/matdialog.service';
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { OutputDvhcModel } from "src/app/models/admin/danhmuc/dvhc.model";
import {
  validationAllErrorMessagesService
} from "src/app/core/services/utilities/validatorService";
import {
  InputCoquantochucModel,
  OutputCoquantochucModel
} from "src/app/models/admin/danhmuc/coquantochuc.model";
import { HttpErrorResponse } from "@angular/common/http";
import * as ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";

@Component({
  selector: "app-coquan-io",
  templateUrl: "./coquan-io.component.html",
  styleUrls: ["./coquan-io.component.scss"]
})
export class DmCoquanIoComponent implements OnInit {
  coquantochucIOForm: FormGroup;
  public inputModel: InputCoquantochucModel;
  public editMode: boolean;
  public purpose: string;
  public obj: any;
  public coquangList: OutputCoquantochucModel[];
  public coquan: any;
  public coquangListFilter: OutputCoquantochucModel[];

  public allTinhData: any;
  public allTinh: any;
  public allHuyen: any;
  public allXa: any;
  public model: any;
  // Ck editor
  public Editor = ClassicEditor;

  // Những biến dành cho phần file
  mDialog: any;
  srcanh = '';
  public fileArray: any;

  // filter Đơn vị hành chính
  public dvhcProvinceFilters: OutputDvhcModel[];
  public dvhcDistrictFilters: OutputDvhcModel[];
  public dvhcWardFilters: OutputDvhcModel[];
  // error message
  validationErrorMessages = {
    tencoquan: { required: "Tên cơ quan không được để trống!" },
    email: { required: "Email không được để trống", email: "Định dạng email không đúng" }
  };

  // form errors
  formErrors = {
    tencoquan: "",
    email: ""
  };

  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    public dmFacadeService: DmFacadeService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    private imDialog: MatDialog, imDialogService: MatdialogService
  ) {
    this.matSidenavService.okCallBackFunction = null;
    this.matSidenavService.cancelCallBackFunction = null;
    this.matSidenavService.confirmStatus = null;
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
  }

  async ngOnInit() {
    await this.formInit();
    await this.bindingConfigAddOrUpdate();
  }

  /**
   * Hàm khởi tạo form khi sửa dữ liệu
   */
  bindingConfigAddOrUpdate() {
    this.showDvhcTinh();
    this.editMode = false;
    this.inputModel = new InputCoquantochucModel();
    // check edit
    this.formOnEdit();
  }

  /**
   * Hàm khởi tạo form
   */
  formInit() {
    this.coquantochucIOForm = this.formBuilder.group({
      tencoquan: ["", Validators.required],
      // idcha: [""],
      diachi: [""],
      sodienthoai: [""],
      email: ["", Validators.compose([Validators.required, Validators.email])],
      matinh: [""],
      mahuyen: [""],
      maxa: [""],
      note: [""],
      imgLink: [""]
    });
  }

  /**
     * Hàm bind dữ liệu vào form sửa
  */
  formOnEdit() {
    // check edit
    if (this.obj) {
      this.coquantochucIOForm.setValue({
        tencoquan: this.obj.tencoquan,
        // idcha: this.obj.idcha,
        diachi: this.obj.diachi,
        sodienthoai: this.obj.sodienthoai,
        email: this.obj.email,
        matinh: this.obj.matinh,
        mahuyen: this.obj.mahuyen,
        maxa: this.obj.maxa,
        note: this.obj.note,
        imgLink: this.obj.imgLink
      });
      this.srcanh = this.obj.imgLink;
      this.showDvhcHuyen();
      this.showDvhcXa();
    }
    this.editMode = true;
  }

  /**
   * Hàm lấy đơn vị hành chính tỉnh
   */
  async showDvhcTinh() {
    this.allTinhData = await this.dmFacadeService
      .getProvinceService()
      .getFetchAll();
    this.allTinh = this.allTinhData;
    this.dvhcProvinceFilters = this.allTinhData;
  }

  /**
   * Hàm lấy đơn vị hành chính huyện theo tỉnh
   */
  async showDvhcHuyen() {
    if (!this.coquantochucIOForm.value.matinh === true) {
      this.allHuyen = [];
      this.dvhcDistrictFilters = [];
      this.allXa = [];
      this.dvhcWardFilters = [];
      if (this.editMode === true) this.coquantochucIOForm.controls["mahuyen"].setValue("");
    }
    if (!this.coquantochucIOForm.value.matinh === false) {
      if (this.editMode === true) this.coquantochucIOForm.controls["mahuyen"].setValue("");
      this.allXa = [];
      this.dvhcWardFilters = [];
      this.allHuyen = await this.dmFacadeService
        .getDistrictService()
        .getFetchAllId(this.coquantochucIOForm.value.matinh);
      this.dvhcDistrictFilters = this.allHuyen;
    }
  }

  /**
   * Hàm lấy đơn vị hành chính xã theo huyện
   */
  async showDvhcXa() {
    if (!this.coquantochucIOForm.value.mahuyen === true) {
      this.allXa = [];
      this.dvhcWardFilters = [];
      if (this.editMode === true) this.coquantochucIOForm.controls["maxa"].setValue("");
    }
    if (!this.coquantochucIOForm.value.matinh === false && !this.coquantochucIOForm.value.mahuyen === false) {
      if (this.editMode === true) this.coquantochucIOForm.controls["maxa"].setValue("");
      this.allXa = await this.dmFacadeService
        .getWardService()
        .getFetchAllId(this.coquantochucIOForm.value.mahuyen);
      this.dvhcWardFilters = this.allXa;
    }
  }

  /**
   * Hàm được gọi khi nhấn lưu
   * @param operMode  biểu thị là form sửa hay thêm mới
   */
  async onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.coquantochucIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.matSidenavService.close();
    }
  }

  /**
   * Hàm thêm mới và reset lại form
   */
  public addOrUpdateContinue(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.coquantochucIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.onFormReset();
      this.purpose = "new";
    }
  }

   /**
   * Hàm thêm mới hoặc sửa dữ liệu cơ quan
   */
  private addOrUpdate(operMode: string) {
    this.inputModel = this.coquantochucIOForm.value;
    this.inputModel.imgLink = this.srcanh;
    if (operMode === "new") {
      this.dmFacadeService
        .getDmCoquantochucService()
        .addItem(this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("refreshList"),
          (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Thêm mới cơ quan tổ chức thành công!",
              2000
            )
        );
    } else if (operMode === "edit") {
      const id: number = this.obj.id;
      this.dmFacadeService
        .getDmCoquantochucService()
        .updateItem(id, this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("refreshList"),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Cập nhập dữ liệu thành công",
              2000
            )
        );
    }
  }

  /**
   * Hàm reset form, gọi khi nhấn nút reset dữ liệu
   */
  public onFormReset() {
    this.coquantochucIOForm.reset();
  }

  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.coquantochucIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  // close sidebar
  public closeCoquantochucIOSidebar() {
    this.matSidenavService.close();
  }

  /**
   * Hàm đóng mat dialog
   */
  closeMatDialog() {
    this.imDialog.closeAll();
  }

  /**
   * Hàm mở thư viện dialog
   */
  public showMatDialog() {
    this.mDialog.setDialog(this, ThuvienComponent, 'showFileSelect', 'closeMatDialog', 'simpleFileV1', '75%', '85vh');
    this.mDialog.open();
  }

  /**
   * Hiển thị những file select trong thư viện
   */
  showFileSelect() {
    this.fileArray = this.mDialog.dataResult;
    this.srcanh = this.fileArray[0].link;
  }

  /**
   * Xóa ảnh hiện có
   */
  deleteAnh() {
    this.srcanh = '';
  }

  /**
   *  Hàm gọi từ function con gọi vào chạy function cha
   * @param methodName
   */
  doFunction(methodName) {
    this[methodName]();
  }
}
