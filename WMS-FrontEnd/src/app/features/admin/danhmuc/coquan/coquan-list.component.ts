import {
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { QueryCellInfoEventArgs } from "@syncfusion/ej2-grids";
import { MatSidenav } from "@angular/material/sidenav";

import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { DmCoquanIoComponent } from "src/app/features/admin/danhmuc/coquan/coquan-io.component";
import { OutputCoquantochucModel } from "src/app/models/admin/danhmuc/coquantochuc.model";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: "app-coquan-list",
  templateUrl: "./coquan-list.component.html",
  styleUrls: ["./coquan-list.component.scss"]
})
export class DmCoquanListComponent implements OnInit {
  listCoquantochuc: OutputCoquantochucModel[];
  selectedItem: OutputCoquantochucModel;
  listData: any;
  navArray = [
    {
      title: "Quản trị",
      url: "/admin"
    },
    {
      title: "Danh mục",
      url: "/admin/danhmuc"
    },
    {
      title: "Cơ quan & Tổ chức",
      url: "//admin/danhmuc/coquan"
    }
  ];

  buttonArray = [
    // {
    //   title: 'Thêm',
    //   icon: 'fal fa-plus-square',
    //   color: 'btn-primary',
    //   event: 'openCoquantochucIOSidebar'
    // },
    // {
    //   title: 'Tải lại dữ liệu',
    //   icon: 'far fa-sync-alt',
    //   color: 'btn-outline-primary mr-3',
    //   event: 'getAllCoquan'
    // }
  ];
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compcoquantochucio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  settingsCommon = new SettingsCommon();

  constructor(
    public matsidenavService: MatsidenavService,
    public progressService: ProgressService,
    public cfr: ComponentFactoryResolver,
    public dmFacadeSv: DmFacadeService,
    public commonService: CommonServiceShared,
    private subHeaderService: SubHeaderService
  ) { }

  ngOnInit() {
    this.matsidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 10 };
    this.subHeaderService.setParentComp(this);
    this.getAllCoquan();
    this.dataSubHeader();
    this.refreshList();
  }

  /**
   * Gán dữ liệu vào sub-header service
   */
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }

  /**
   * Hàm getAll cơ quan
   */
  async getAllCoquan() {
    this.listData = await this.dmFacadeSv
      .getDmCoquantochucService()
      .getFetchAll();
    const listDataItems = this.listData;
    if (listDataItems) {
      listDataItems.map((coquan, index) => {
        coquan.serialNumber = index + 1;
      });
    }
    this.listCoquantochuc = listDataItems;
  }

  /**
   * Mở form thêm mới
   */
  public openCoquantochucIOSidebar() {
    this.matsidenavService.setTitle("Thêm mới cơ quan tổ chức");
    this.matsidenavService.setContentComp(DmCoquanIoComponent, "new");
    this.matsidenavService.open();
  }

  /**
   * Mở form sửa
   */
  public openCoquantochucIOSidebarEdit(event) {
    this.getItemByEvent(event);
    this.matsidenavService.setTitle("Sửa dữ liệu thông tin cơ quan tổ chức");
    this.matsidenavService.setContentComp(
      DmCoquanIoComponent,
      "edit",
      this.selectedItem
    );
    this.matsidenavService.open();
  }

  /**
   * Đóng sidenav
   */
  public closeCoquantochucIOSidebar() {
    this.matsidenavService.close();
  }

  /**
   * Lấy dữ liệu cơ quan được chọn
   */
  private getItemByEvent(event) {
    const target = event.currentTarget;
    const pElement = target.parentElement.parentElement;
    const pclassID = pElement.getAttribute("id");
    const item = this.listCoquantochuc.find(x => x.id === +pclassID);
    this.selectedItem = item;
  }


  /**
   * Hàm xóa cơ quan
   */
  public deleteCoquantochuc(event) {
    this.getItemByEvent(event);
    const canDelete: string = this.dmFacadeSv
      .getDmCoquantochucService()
      .checkBeDeleted(this.selectedItem.id);
    this.canBeDeletedCheck(canDelete);
  }

  /**
   * check xem dữ liệu đang dùng có xóa được không
   * @param sMsg 
   */
  public canBeDeletedCheck(sMsg: string) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog();
    } else {
      this.canDeleteDialog(sMsg);
    }
  }

  /**
   * Hiển thi form xác nhận xóa
   */
  confirmDeleteDiaLog() {
    const dialogRef = this.commonService.confirmDeleteDiaLogService(
      "Tên cơ quan tổ chức:",
      this.selectedItem.tencoquan
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.dmFacadeSv
          .getDmCoquantochucService()
          .deleteItem(this.selectedItem.id).subscribe(
            () => this.refreshList(),
            (error: HttpErrorResponse) => {
              this.commonService.showeNotiResult(error.message, 2000);
            },
            () => this.commonService.showeNotiResult("Đã xóa thành công: " + this.selectedItem.tencoquan, 2000)
          );
      }
    });
  }

  /**
   * Hiển thị form không thể xóa
   * @param sMsg
   */
  canDeleteDialog(sMsg: string) {
    this.commonService.canDeleteDialogService(sMsg);
  }

  /**
   * Lấy lại danh sách cơ quan
   */
  public refreshList() {
    this.getAllCoquan();
  }

  /**
   * set id cho cột
   */
  customiseCell(args: QueryCellInfoEventArgs) {
    if (args.column.field === "id") {
      args.cell.id = args.data[args.column.field];
    }
  }

  /**
   * Hàm dùng để gọi các hàm khác, truyền vào tên hàm cần thực thi
   */
  doFunction(methodName) {
    this[methodName]();
  }
}
