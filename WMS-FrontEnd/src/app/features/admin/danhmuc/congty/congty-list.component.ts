import {
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { OutputCongtyModel } from "src/app/models/admin/danhmuc/congty.model";
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { DmCongtyIoComponent } from "src/app/features/admin/danhmuc/congty/congty-io.component";
import { MatSidenav } from "@angular/material/sidenav";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { QueryCellInfoEventArgs } from "@syncfusion/ej2-angular-grids";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import {HttpErrorResponse} from "@angular/common/http";
@Component({
  selector: "app-congty-list",
  templateUrl: "./congty-list.component.html",
  styleUrls: ["./congty-list.component.scss"]
})
export class DmCongtyListComponent implements OnInit {
  
  listCompany: OutputCongtyModel[];
  selectedItem: OutputCongtyModel;
  listData: any;
  navArray = [
    { title: "Quản trị", url: "/admin" },
    {
      title: "Danh mục",
      url: "/admin/danhmuc"
    },
    {
      title: "Công ty",
      url: "//admin/danhmuc/congty"
    }
  ];

  buttonArray = [];
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compnCompanyio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  settingsCommon = new SettingsCommon();

  constructor(
    public matsidenavService: MatsidenavService,
    public progressService: ProgressService,
    public cfr: ComponentFactoryResolver,
    public dmFacadeSv: DmFacadeService,
    public commonService: CommonServiceShared,
    private subHeaderService: SubHeaderService
  ) { }

  ngOnInit() {
    this.matsidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.settingsCommon.toolbar = ["Search"];
    this.subHeaderService.setParentComp(this);
    this.settingsCommon.pageSettings = { pageSize: 10 };
    this.getAllCompany();
    this.dataSubHeader();
  }

  /**
   * Gán dữ liệu vào sub-header service
   */
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }

  /**
   * Lấy danh sách công ty
   */
  async getAllCompany() {
    this.listData = await this.dmFacadeSv
      .getDmCongtyService()
      .getFetchAll();
    const listDataItems = this.listData;
    if (listDataItems) {
      listDataItems.map((congty, index) => {
        congty.serialNumber = index + 1;
      });
    }
    this.listCompany = listDataItems;
  }

  /**
   * Mở form thêm mới
   */
  public openCompanyIOSidebar() {
    this.matsidenavService.setTitle("Thêm mới công ty");
    this.matsidenavService.setContentComp(DmCongtyIoComponent, "new");
    this.matsidenavService.open();
  }

  /**
   * Mở form edit
   */
  public editItemCompany(event) {
    this.getItemByEvent(event);
    this.matsidenavService.setTitle("Sửa dữ liệu công ty");
    this.matsidenavService.setContentComp(
      DmCongtyIoComponent,
      "edit",
      this.selectedItem
    );
    this.matsidenavService.open();
  }

  /**
   * Hàm xóa công ty
   */
  public deleteCompany(event) {
    this.getItemByEvent(event);
    const canDelete: string = this.dmFacadeSv
      .getDmCongtyService()
      .checkBeDeleted(this.selectedItem.id);
    this.canBeDeletedCheck(canDelete);
  }

  /**
   * check xem dữ liệu đang dùng có xóa được không
   * @param sMsg 
   */
  public canBeDeletedCheck(sMsg: string) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog();
    } else {
      this.canDeleteDialog(sMsg);
    }
  }

   /**
   * Hiển thi form xác nhận xóa
   */
  confirmDeleteDiaLog() {
    const dialogRef = this.commonService.confirmDeleteDiaLogService(
      "Tên công ty:",
      this.selectedItem.tencongty
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.dmFacadeSv
          .getDmCongtyService()
          .deleteItem(this.selectedItem.id).subscribe(
          () => this.getAllCompany(),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () => this.commonService.showeNotiResult("Đã xóa thành công: " + this.selectedItem.tencongty, 2000)
        );
      }
    });
  }

  /**
   * Hiển thị form không thể xóa
   * @param sMsg
   */
  canDeleteDialog(sMsg: string) {
    this.commonService.canDeleteDialogService(sMsg);
  }

  /**
   * Hàm đóng sidenav
   */
  public closeCompnayIOSidebar() {
    this.matSidenav.close();
  }

  /**
   * Hàm lấy dữ liệu công ty được chọn
   */
  private getItemByEvent(event) {
    const item = this.commonService.getByEvent(event, this.listCompany);
    this.selectedItem = item;
  }

  /**
   * Set id vào cột
   */
  customiseCell(args: QueryCellInfoEventArgs) {
    if (args.column.field === "id") {
      args.cell.id = args.data[args.column.field];
    }
  }

  /**
   * Hàm lấy lại list công ty
   */
  public refreshListCompany() {
    this.getAllCompany();
  }

  /**
   * Hàm dùng để gọi các hàm khác, truyền vào tên hàm cần thực thi
   */
  doFunction(methodName) {
    this[methodName]();
  }
}
