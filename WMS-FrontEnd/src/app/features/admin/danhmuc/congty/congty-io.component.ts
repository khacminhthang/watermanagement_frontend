import { ThuvienComponent } from 'src/app/features/admin/thuvien/thuvien.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { OutputDvhcModel } from "src/app/models/admin/danhmuc/dvhc.model";
import {
  validationAllErrorMessagesService,
} from "src/app/core/services/utilities/validatorService";
import { InputCongtyModel } from "src/app/models/admin/danhmuc/congty.model";
import { HttpErrorResponse } from "@angular/common/http";
import * as ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { MatdialogService } from 'src/app/core/services/utilities/matdialog.service';
@Component({
  selector: "app-congty-io",
  templateUrl: "./congty-io.component.html",
  styleUrls: ["./congty-io.component.scss"]
})
export class DmCongtyIoComponent implements OnInit {
  companyIOForm: FormGroup;
  submitted = false;
  public editMode: boolean;
  public purpose: string;
  public inputModel: InputCongtyModel;
  public obj: any;
  public Editor = ClassicEditor;
  public allTinh: any;
  public allTinhData: any;
  public allHuyen: any;
  public allXa: any;
  dvhcProvinceFilters: OutputDvhcModel[];
  dvhcDistrictFilters: OutputDvhcModel[];
  dvhcWardFilters: OutputDvhcModel[];

  // Những biến dành cho phần file
  mDialog: any;
  srcanh = '';
  public fileArray: any;

  // error message
  validationErrorMessages = {
    tencongty: { required: "Tên công ty không được để trống!" },
    sodienthoai: { required: "Số điện thoại không được để trống", pattern: "Định dạng số điện thoại không đúng!" },
    email: { required: "Email không được để trống", email: "Định dạng email không đúng" }
  };
  // form errors
  formErrors = {
    tencongty: "",
    sodienthoai: "",
    email: ""
  };
  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    public dmFacadeService: DmFacadeService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    private imDialog: MatDialog, imDialogService: MatdialogService
  ) {
    this.matSidenavService.okCallBackFunction = null;
    this.matSidenavService.cancelCallBackFunction = null;
    this.matSidenavService.confirmStatus = null;
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
  }

  ngOnInit() {
    this.formInit();
    this.bindingConfigAddOrUpdate();
  }

  // binding Config
  bindingConfigAddOrUpdate() {
    this.showDvhcTinh();
    this.editMode = false;
    this.inputModel = new InputCongtyModel();

    // Nếu editMode = true và purpose là edit (obj đã được set) thì sẽ set giá trị cho các input Textbox bằng đối tượng vừa được nhấn edit
    this.formOnEdit();
  }

  formInit() {
    this.companyIOForm = this.formBuilder.group({
      tencongty: ["", Validators.required],
      diachi: [""],
      sodienthoai: ["", Validators.compose([Validators.pattern("^[0-9-+]+$"),Validators.required])],
      email: ["", Validators.compose([Validators.required, Validators.email])],
      note: [""],
      imgLink: [""],
      matinh: [""],
      mahuyen: [""],
      maxa: [""]
    });
  }

  formOnEdit() {
    if (this.obj) {
      this.companyIOForm.setValue({
        tencongty: this.obj.tencongty,
        diachi: this.obj.diachi,
        matinh: this.obj.matinh,
        mahuyen: this.obj.mahuyen,
        maxa: this.obj.maxa,
        sodienthoai: this.obj.sodienthoai,
        email: this.obj.email,
        note: this.obj.note,
        imgLink: this.obj.imgLink
      });
      this.srcanh = this.obj.imgLink;
      this.showDvhcHuyen();
      this.showDvhcXa();
    }
    this.editMode = true;
  }

  // on Submit
  async onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.companyIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.matSidenavService.close();
    }
  }

  // save and reset form
  public addOrUpdateContinue(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.companyIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.onFormReset();
      this.purpose = "new";
    }
  }

  // add or update
  private addOrUpdate(operMode: string) {
    this.inputModel = this.companyIOForm.value;
    this.inputModel.imgLink = this.srcanh;
    if (operMode === "new") {
      this.dmFacadeService
        .getDmCongtyService()
        .addItem(this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("refreshListCompany"),
          (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () => this.commonService.showeNotiResult("Tạo mới thành công!", 2000)
        );
    } else if (operMode === "edit") {
      const id: number = this.obj.id;
      this.dmFacadeService
        .getDmCongtyService()
        .updateItem(id, this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("refreshListCompany"),
          (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Cập nhật dữ liệu thành công!",
              2000
            )
        );
    }
  }

  // Các hàm get đơn vị hành chính
  async showDvhcTinh() {
    this.allTinhData = await this.dmFacadeService
      .getProvinceService()
      .getFetchAll();
    this.allTinh = this.allTinhData;
    this.dvhcProvinceFilters = this.allTinhData;
  }

  async showDvhcHuyen() {
    if (!this.companyIOForm.value.matinh === true) {
      this.allHuyen = [];
      this.dvhcDistrictFilters = [];
      this.allXa = [];
      this.dvhcWardFilters = [];
      if (this.editMode === true) this.companyIOForm.controls["mahuyen"].setValue("");
    }
    if (!this.companyIOForm.value.matinh === false) {
      if (this.editMode === true) this.companyIOForm.controls["mahuyen"].setValue("");
      this.allXa = [];
      this.dvhcWardFilters = [];
      this.allHuyen = await this.dmFacadeService
        .getDistrictService()
        .getFetchAllId(this.companyIOForm.value.matinh);
      this.dvhcDistrictFilters = this.allHuyen;
    }
  }

  async showDvhcXa() {
    if (!this.companyIOForm.value.mahuyen === true) {
      this.allXa = [];
      this.dvhcWardFilters = [];
      if (this.editMode === true) this.companyIOForm.controls["maxa"].setValue("");
    }
    if (!this.companyIOForm.value.matinh === false && !this.companyIOForm.value.mahuyen === false) {
      if (this.editMode === true) this.companyIOForm.controls["maxa"].setValue("");
      this.allXa = await this.dmFacadeService
        .getWardService()
        .getFetchAllId(this.companyIOForm.value.mahuyen);
      this.dvhcWardFilters = this.allXa;
    }
  }

  /**
   * Hàm đóng mat dialog
   */
  closeMatDialog() {
    this.imDialog.closeAll();
  }

  /**
   * Hàm mở thư viện dialog
   */
  public showMatDialog() {
    this.mDialog.setDialog(this, ThuvienComponent, 'showFileSelect', 'closeMatDialog', 'simpleFileV1', '75%', '85vh');
    this.mDialog.open();
  }

  /**
   * Hiển thị những file select trong thư viện
   */
  showFileSelect() {
    this.fileArray = this.mDialog.dataResult;
    this.srcanh = this.fileArray[0].link;
    console.log("DmCanhanIoComponent -> showFileSelect -> this.srcanh", this.srcanh)
  }

  /**
   * Xóa ảnh hiện có
   */
  deleteAnh() {
    this.srcanh = '';
  }

  /**
   *  Hàm gọi từ function con gọi vào chạy function cha
   * @param methodName
   */
  doFunction(methodName) {
    this[methodName]();
  }


  // reset form
  public onFormReset() {
    this.companyIOForm.reset();
  }
  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.companyIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  // close sidebar
  public closeCompanyIOSidebar() {
    this.matSidenavService.close();
  }
}
