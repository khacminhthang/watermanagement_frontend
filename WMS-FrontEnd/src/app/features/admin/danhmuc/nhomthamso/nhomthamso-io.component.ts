import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { InputNhomthamsoModel } from "src/app/models/admin/danhmuc/nhomthamso.model";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import {validationAllErrorMessagesService} from "src/app/core/services/utilities/validatorService";

@Component({
  selector: "app-nhomthamso-io",
  templateUrl: "./nhomthamso-io.component.html",
  styleUrls: ["./nhomthamso-io.component.scss"]
})
export class DmNhomthamsoIoComponent implements OnInit {
  nhomthamsoIOForm: FormGroup;
  submitted = false;
  public obj: any;
  public purpose: string;
  public editMode: boolean;
  public inputNtsModel: InputNhomthamsoModel;
  // error message
  validationErrorMessages = {
    tennhom: { required: "Tên nhóm tham số không được để trống!" },
  };

  // form errors
  formErrors = {
    tennhom: "",
  };

  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    public dmFacadeService: DmFacadeService,
    private formBuilder: FormBuilder,
    private commonService: CommonServiceShared
  ) {
    this.matSidenavService.okCallBackFunction = null;
    this.matSidenavService.cancelCallBackFunction = null;
    this.matSidenavService.confirmStatus = null;
  }

  ngOnInit() {
    this.editMode = false;
    this.inputNtsModel = new InputNhomthamsoModel();
    // Nếu purpose là edit hay new thì đặt editMode thành true để hiển thị form nhập dữ liệu
    if (this.purpose === "edit" || this.purpose === "new") {
      this.editMode = true;
    }
    this.nhomthamsoIOForm = this.formBuilder.group({
      tennhom: ["", Validators.required],
      kyhieunhom: [""]
    });
    // Nếu editMode = true và purpose là edit (obj đã được set) thì sẽ set giá trị cho các input Textbox bằng đối tượng vừa được nhấn edit
    if (this.editMode === true && this.obj) {
      this.nhomthamsoIOForm.setValue({
        tennhom: this.obj.tennhom,
        kyhieunhom: this.obj.kyhieunhom
      });
    }
  }

  /**
   * Hàm được gọi khi nhấn nút Lưu, Truyền vào operMode để biết là Edit hay tạo mới
   */
  async onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.nhomthamsoIOForm.valid === true) {
      try {
        this.inputNtsModel = this.nhomthamsoIOForm.value;
        if (operMode === "new") {
          await this.dmFacadeService
            .getNhomthamsoService()
            .addItem(this.inputNtsModel)
            .subscribe(res =>
              this.matSidenavService.doParentFunction("getAllNhomthamso")
            );
          this.commonService.showeNotiResult("Tạo mới thành công!", 2000);
        } else if (operMode === "edit") {
          const idEdit: number = this.obj.id;
          await this.dmFacadeService
            .getNhomthamsoService()
            .updateItem(idEdit, this.inputNtsModel)
            .subscribe(res =>
              this.matSidenavService.doParentFunction("getAllNhomthamso")
            );
          this.commonService.showeNotiResult(
            "Cập nhật dữ liệu thành công!",
            2000
          );
        }
        this.matSidenavService.close();
      } catch (error) {
        this.commonService.showeNotiResult(error, 2000);
      }
    }
  }

  /**
   *  Hàm reset form, gọi khi nhấn nút reset dữ liệu
   */
  public onFormReset() {
    // Hàm .reset sẽ xóa trắng mọi control trên form
    this.nhomthamsoIOForm.reset();
    // Trong trường hợp mong muốn reset một số trường về giá trị mặc định thì dùng .patchValue
    // https://angular.io/guide/reactive-forms#patching-the-model-value
  }

  /**
   * Hàm lưu và reset form để tiếp tục nhập mới dữ liệu. Trường hợp này khi người dùng muốn nhập dữ liệu liên tục
   */
  async onContinueAdd(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.nhomthamsoIOForm.valid === true) {
      try {
        this.inputNtsModel = this.nhomthamsoIOForm.value;
        if (operMode === "new") {
          await this.dmFacadeService
            .getNhomthamsoService()
            .addItem(this.inputNtsModel)
            .subscribe(res =>
              this.matSidenavService.doParentFunction("getAllNhomthamso")
            );
          this.commonService.showeNotiResult("Tạo mới thành công!", 2000);
        } else if (operMode === "edit") {
          const idEdit: number = this.obj.id;
          await this.dmFacadeService
            .getNhomthamsoService()
            .updateItem(idEdit, this.inputNtsModel)
            .subscribe(res =>
              this.matSidenavService.doParentFunction("getAllNhomthamso")
            );
          this.commonService.showeNotiResult(
            "Cập nhật dữ liệu thành công!",
            2000
          );
        }
        this.onFormReset();
        // Sau khi reset form thì chuyển chế độ sang nhập mới (dù trước đó là edit hay nhập mới)
        this.purpose = "new";
      } catch (error) {
        this.commonService.showeNotiResult(error, 2000);
      }
    }
  }

  /**
   *  Validation click submit
   */
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.nhomthamsoIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  /**
   * Hàm đóng sidenav
   */
  public closeNhomthamsoIOSidebar() {
    this.matSidenavService.close();
  }
}
