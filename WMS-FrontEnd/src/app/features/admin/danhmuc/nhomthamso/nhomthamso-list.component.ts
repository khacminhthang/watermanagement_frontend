import {
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { OutputNhomthamsoModel } from "src/app/models/admin/danhmuc/nhomthamso.model";
import { DmNhomthamsoIoComponent } from "src/app/features/admin/danhmuc/nhomthamso/nhomthamso-io.component";
import { MatDialog } from "@angular/material/dialog";
import { MatSidenav } from "@angular/material/sidenav";
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { MyAlertDialogComponent } from "src/app/shared/components/my-alert-dialog/my-alert-dialog.component";
import { QueryCellInfoEventArgs } from "@syncfusion/ej2-angular-grids";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import {HttpErrorResponse} from "@angular/common/http";
import {CommonServiceShared} from "src/app/core/services/utilities/common-service";
@Component({
  selector: "app-nhomthamso-list",
  templateUrl: "./nhomthamso-list.component.html",
  styleUrls: ["./nhomthamso-list.component.scss"]
})
export class DmNhomthamsoListComponent implements OnInit {
  listNhomthamso: OutputNhomthamsoModel[];
  selectedItem: OutputNhomthamsoModel;
  listDuLieu: any;
  navArray = [
    { title: "Quản trị", url: "/admin" },
    {
      title: "Danh mục",
      url: "/admin/danhmuc"
    },
    {
      title: "Nhóm tham số",
      url: "/admin/danhmuc/nhomthamso"
    }
  ];

  buttonArray = [];
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compnhomthamsoio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  settingsCommon = new SettingsCommon();

  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    public cfr: ComponentFactoryResolver,
    private modalDialog: MatDialog,
    public dmFacadeService: DmFacadeService,
    private subHeaderService: SubHeaderService,
    public commonService: CommonServiceShared
  ) { }

  ngOnInit() {
    this.matSidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 8 };
    this.getAllNhomthamso();
    this.subHeaderService.setParentComp(this);
    this.dataSubHeader();
  }

  /**
   * Gán dữ liệu vào sub-header service
   */
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }

  /**
   * Hàm getAll Nhóm tham số đẻ binding dữ liệu lên EJS grid
   */
  async getAllNhomthamso() {
    this.listDuLieu = await this.dmFacadeService
      .getNhomthamsoService()
      .getFetchAll();
    const listDataItems = this.listDuLieu;
    if (listDataItems) {
      listDataItems.map((nhomthamso, index) => {
        nhomthamso.serialNumber = index + 1;
      });
    }
    this.listNhomthamso = listDataItems;
  }

  /**
   * Hàm đặt tiêu đề, Đặt content component và sau đó mở sidebar lên
   */
  public openNhomThamsoIOSidebar() {
    this.matSidenavService.setTitle("Thêm mới Nhóm tham số");
    this.matSidenavService.setContentComp(DmNhomthamsoIoComponent, "new");
    this.matSidenavService.open();
  }

  /**
   * hàm dóng sidebar
   */
  public closeNhomthamsoIOSidebar() {
    this.matSidenavService.close();
  }

  /**
   * Hàm xóa một bản ghi, được gọi khi nhấn nút xóa trên giao diện list
   */
  async deleteItemNhomthamso(event) {
    this.getItemByEvent(event);
    // Phải check xem dữ liệu muốn xóa có đang được dùng ko, đang dùng thì ko xóa
    // Trường hợp dữ liệu có thể xóa thì Phải hỏi người dùng xem có muốn xóa không
    // Nếu đồng ý xóa
    const canDelete: string = this.dmFacadeService
      .getNhomthamsoService()
      .checkBeDeleted(this.selectedItem.id);
    this.canBeDeletedCheck(canDelete);
  }

  public canBeDeletedCheck(sMsg: string) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog();
    } else {
      this.cantDeleteDialog(sMsg);
    }
  }

  confirmDeleteDiaLog() {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Chắc chắn xóa?";
    dialogRef.componentInstance.content =
      "Nhóm tham số: <b>" + this.selectedItem.tennhom + "</b>";
    dialogRef.componentInstance.okeButton = "Đồng ý";
    dialogRef.componentInstance.cancelButton = "Hủy bỏ";
    dialogRef.componentInstance.visibleOkButton = true;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.afterClosed().subscribe(async result => {
      if (result === "confirm") {
        await this.dmFacadeService
          .getNhomthamsoService()
          .deleteItem(this.selectedItem.id).subscribe(
            () => this.getAllNhomthamso(),
            (error: HttpErrorResponse) => {
              this.commonService.showeNotiResult(error.message, 2000);
            },
            () => this.commonService.showeNotiResult("Đã xóa thành công: " + this.selectedItem.tennhom, 2000)
          );
      }
    });
  }

  cantDeleteDialog(sMsg: string) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Không thể xóa!";
    dialogRef.componentInstance.content = "<b>" + sMsg + "</b>";
    dialogRef.componentInstance.cancelButton = "Đóng";
    dialogRef.componentInstance.visibleOkButton = false;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.componentInstance.headerColor = "red";
  }

  /**
   * Hàm sửa thông tin chi tiết một bản ghi, được gọi khi nhấn nút xem chi tiết trên giao diện list
   */
  public editItemNhomthamso(event) {
    this.getItemByEvent(event);
    this.matSidenavService.setTitle("Sửa dữ liệu nhóm tham số");
    this.matSidenavService.setContentComp(
      DmNhomthamsoIoComponent,
      "edit",
      this.selectedItem
    );
    this.matSidenavService.open();
  }

  /**
   * Hàm lấy ra đối tượng khi người dùng click vào một trong 3 nút xóa, chi tiết, sửa
   */
  public getItemByEvent(event) {
    const target = event.currentTarget;
    const pElement = target.parentElement.parentElement;
    const pclassID = pElement.getAttribute("id");
    this.selectedItem = this.listNhomthamso.find(x => x.id === +pclassID);
  }

  /**
   * Hàm dùng để set id vào cột thao tác
   */
  customiseCell(args: QueryCellInfoEventArgs) {
    if (args.column.field === "id") {
      args.cell.id = args.data[args.column.field];
    }
  }

  /**
   * Hàm dùng để gọi các hàm khác, truyền vào tên hàm cần thực thi
   */
  doFunction(methodName) {
    this[methodName]();
  }

}
