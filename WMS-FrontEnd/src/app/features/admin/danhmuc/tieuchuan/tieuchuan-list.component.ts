import {
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { OutputTieuchuanModel } from "src/app/models/admin/danhmuc/tieuchuan.model";
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { DmTieuchuanIoComponent } from "src/app/features/admin/danhmuc/tieuchuan/tieuchuan-io.component";
import { MatDialog } from "@angular/material/dialog";
import { MatSidenav } from "@angular/material/sidenav";
import { MyAlertDialogComponent } from "src/app/shared/components/my-alert-dialog/my-alert-dialog.component";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { QueryCellInfoEventArgs } from "@syncfusion/ej2-angular-grids";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import {HttpErrorResponse} from "@angular/common/http";
@Component({
  selector: "app-tieuchuan-list",
  templateUrl: "./tieuchuan-list.component.html",
  styleUrls: ["./tieuchuan-list.component.scss"]
})
export class DmTieuchuanListComponent implements OnInit {
  listTieuchuan: OutputTieuchuanModel[];
  selectedItem: OutputTieuchuanModel;
  listData: any;
  data: any;
  navArray = [
    { title: "Quản trị", url: "/admin" },
    {
      title: "Danh mục",
      url: "/admin/danhmuc"
    },
    {
      title: "Tiêu chuẩn",
      url: "//admin/danhmuc/tieuchuan"
    }
  ];

  buttonArray = [];
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compTieuchuanio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  settingsCommon = new SettingsCommon();
  constructor(
    public matsidenavService: MatsidenavService,
    public progressService: ProgressService,
    public cfr: ComponentFactoryResolver,
    public dmFacadeService: DmFacadeService,
    public modalDialog: MatDialog,
    public commonService: CommonServiceShared,
    private subHeaderService: SubHeaderService
  ) { }

  ngOnInit() {
    this.matsidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 10 };
    this.getAllTieuchuan();
    this.subHeaderService.setParentComp(this);
    this.dataSubHeader();
  }

  /**
   * Gán dữ liệu vào sub-header service
   */
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }

  /**
   * get all Tieu chuan
   */
  async getAllTieuchuan() {
    this.listData = await this.dmFacadeService
      .getTieuchuanService()
      .getFetchAll();
    const listDataItems = this.listData;
    if (listDataItems) {
      listDataItems.map((tieuchuan, index) => {
        tieuchuan.serialNumber = index + 1;
      });
    }
    this.listTieuchuan = listDataItems;
  }

  /**
   * open sidebar execute insert
   */
  public openTieuchuanIOSidebar() {
    this.matsidenavService.setTitle("Thêm mới dữ liệu tiêu chuẩn");
    this.matsidenavService.setContentComp(DmTieuchuanIoComponent, "new");
    this.matsidenavService.open();
  }

  /**
   * edit open sidebar
   */
  public editItemTieuchuan(event) {
    this.getItemByEvent(event);
    this.matsidenavService.setTitle("Sửa dữ liệu thông tin tiêu chuẩn");
    this.matsidenavService.setContentComp(
      DmTieuchuanIoComponent,
      "edit",
      this.selectedItem
    );
    this.matsidenavService.open();
  }

  /**
   * delete
   */
  public deleteTieuchuan(event) {
    this.getItemByEvent(event);
    const canDelete: string = this.dmFacadeService
      .getTieuchuanService()
      .checkBeDeleted(this.selectedItem.id);
    this.canBeDeletedCheck(canDelete);
  }

  public canBeDeletedCheck(sMsg: string) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog();
    } else {
      this.canDeleteDialog(sMsg);
    }
  }

  confirmDeleteDiaLog() {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Chắc chắn xóa?";
    dialogRef.componentInstance.content =
      "Tiêu chuẩn: <b>" + this.selectedItem.tentieuchuan + "</b>";
    dialogRef.componentInstance.okeButton = "Đồng ý";
    dialogRef.componentInstance.cancelButton = "Hủy bỏ";
    dialogRef.componentInstance.visibleOkButton = true;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.dmFacadeService
          .getTieuchuanService()
          .deleteItem(this.selectedItem.id).subscribe(
          () => this.getAllTieuchuan(),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () => this.commonService.showeNotiResult("Đã xóa thành công: " + this.selectedItem.tentieuchuan, 2000)
        );
      }
    });
  }

  /**
   * Convert string thành dd/mm/yyyy
   */
  public convertTimeString(data) {
    if (data.thoigianbanhanh !== null) {
      const [year, month, day]: string[] = data.thoigianbanhanh.split('-');
      const awesomeDateTime = `${day}-${month}-${year}`;
      return awesomeDateTime;
    } else return null;
  }

  canDeleteDialog(sMsg: string) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Không thể xóa!";
    dialogRef.componentInstance.content = "<b>" + sMsg + "</b>";
    dialogRef.componentInstance.cancelButton = "Đóng";
    dialogRef.componentInstance.visibleOkButton = false;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.componentInstance.headerColor = "red";
  }

  public closeTieuchuanIOSidebar() {
    this.matSidenav.close();
  }

  customiseCell(args: QueryCellInfoEventArgs) {
    if (args.column.field === "id") {
      args.cell.id = args.data[args.column.field];
    }
  }

  public getItemByEvent(event) {
    const target = event.currentTarget;
    const item = this.commonService.getByEvent(event, this.listTieuchuan);
    this.selectedItem = item;
  }

  doFunction(methodName) {
    this[methodName]();
  }
}
