import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { InputTieuchuanModel } from "src/app/models/admin/danhmuc/tieuchuan.model";
import {
  validationAllErrorMessagesService,
} from "src/app/core/services/utilities/validatorService";
import { HttpErrorResponse } from "@angular/common/http";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { DatePipe } from "@angular/common";

@Component({
  selector: "app-tieuchuan-io",
  templateUrl: "./tieuchuan-io.component.html",
  styleUrls: ["./tieuchuan-io.component.scss"]
})
export class DmTieuchuanIoComponent implements OnInit {
  tieuchuanIOForm: FormGroup;
  submitted = false;
  public editMode: boolean;
  public purpose: string;
  public inputModel: InputTieuchuanModel;
  public obj: any;
  public doccument: any;
  docFilters: any;


  // error message
  validationErrorMessages = {
    tentieuchuan: { required: "Tên tiêu chuẩn không được để trống!" },
    sohieutieuchuan: { required: "Số hiệu tiêu chuẩn không được để trống!" }
  };

  // form errors
  formErrors = {
    tentieuchuan: "",
    sohieutieuchuan: ""
  };

  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    public dmFacadeService: DmFacadeService,
    private datePipe: DatePipe
  ) {
    this.matSidenavService.okCallBackFunction = null;
    this.matSidenavService.cancelCallBackFunction = null;
    this.matSidenavService.confirmStatus = null;
  }

  ngOnInit() {
    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
  }


  /**
   * config Form use add or update
   */
  bindingConfigAddOrUpdate() {
    this.editMode = false;
    this.inputModel = new InputTieuchuanModel();
    if (this.purpose === "new" || this.purpose === "edit") {
      this.editMode = true;
    }
    // check edit
    this.formOnEdit();
  }

  /**
   * init FormControl
   */
  bindingConfigValidation() {
    this.tieuchuanIOForm = this.formBuilder.group({
      tentieuchuan: ["", Validators.required],
      sohieutieuchuan: ["", Validators.required],
      thoigianbanhanh: [""],
      coquanbanhanh: [""],
      hientrang: [""],
      // idTailieu: [""]
    });
  }

  /**
   * init edit form
   */
  formOnEdit() {
    if (this.editMode === true && this.obj) {
      this.tieuchuanIOForm.setValue({
        tentieuchuan: this.obj.tentieuchuan,
        sohieutieuchuan: this.obj.sohieutieuchuan,
        thoigianbanhanh: this.obj.thoigianbanhanh,
        coquanbanhanh: this.obj.coquanbanhanh,
        hientrang: this.obj.hientrang,
        // idTailieu: this.obj.idTailieu
      });
    }
  }

  /**
   * add or update form
   */
  private addOrUpdate(operMode: string) {
    this.inputModel = this.tieuchuanIOForm.value;
    this.inputModel.thoigianbanhanh = this.datePipe.transform(this.tieuchuanIOForm.value.thoigianbanhanh, "yyyy-MM-dd");
    if (operMode === "new") {
      this.dmFacadeService
        .getTieuchuanService()
        .addItem(this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("getAllTieuchuan"),
          (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Thêm mới tiêu chuẩn thành công!",
              2000
            )
        );
    } else if (operMode === "edit") {
      const id: number = this.obj.id;
      this.dmFacadeService
        .getTieuchuanService()
        .updateItem(id, this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("getAllTieuchuan"),
          (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Cập nhật tiêu chuẩn thành công!",
              2000
            )
        );
    }
  }

  async onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    if(this.tieuchuanIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.matSidenavService.close();
    }
  }

  async addContinue(operMode: string) {
    this.logAllValidationErrorMessages();
    if(this.tieuchuanIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.onFormReset();
      this.purpose = "new";
    }
  }

  public onFormReset() {
    this.tieuchuanIOForm.reset();
  }

  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.tieuchuanIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  public closeTieuchuanIOSidebar() {
    this.matSidenavService.close();
  }
}
