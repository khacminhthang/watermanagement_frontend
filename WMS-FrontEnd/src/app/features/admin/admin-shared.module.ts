import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "src/app/shared/shared.module";

import {
  FilterService,
  GridModule,
  GroupService,
  PageService,
  SearchService,
  SortService,
  ToolbarService,
  ResizeService
} from "@syncfusion/ej2-angular-grids";
import { TreeGridModule } from "@syncfusion/ej2-angular-treegrid";

// Các module Material UI
import { MatDialogModule } from "@angular/material/dialog";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatSelectModule } from "@angular/material/select";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { MatSelectFilterModule } from "mat-select-filter";
import { SubHeaderComponent } from "src/app/core/comps/sub-header/sub-header.component";
import { SubSidenavComponent } from "../../core/layout/sub-sidenav/sub-sidenav.component";

// Các services core
import { MatdialogService } from "src/app/core/services/utilities/matdialog.service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";

// Các components từ Shared dùng chung
import { ThongsoquantracIoComponent } from "src/app/shared/components/thongsoquantrac-io/thongsoquantrac-io.component";
import { ThongsoquantracListComponent } from "src/app/shared/components/thongsoquantrac-list/thongsoquantrac-list.component";
import { KetquaquantracListComponent } from "src/app/shared/components/ketquaquantrac-list/ketquaquantrac-list.component";
import { KetquaquantracIoComponent } from "src/app/shared/components/ketquaquantrac-io/ketquaquantrac-io.component";


import {
  OwlDateTimeModule,
  OwlNativeDateTimeModule,
  DateTimeAdapter,
  OWL_DATE_TIME_LOCALE,
  OWL_DATE_TIME_FORMATS
} from 'ng-pick-datetime';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';
// Format custom
export const MY_CUSTOM_FORMATS = {
  parseInput: 'DD-MM-YYYY HH:mm:ss',
  fullPickerInput: 'DD-MM-YYYY HH:mm:ss',
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: ' HH:mm:ss',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};
@NgModule({
  imports: [
    CommonModule,
    GridModule,
    MatSidenavModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    TreeGridModule,
    MatSelectModule,
    CKEditorModule,
    MatSelectFilterModule,
    SharedModule,
    RouterModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatCheckboxModule
  ],
  exports: [
    MatDialogModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatSelectModule,
    MatCheckboxModule,
    CKEditorModule,
    MatSelectFilterModule,
    TreeGridModule,
    FormsModule,
    ReactiveFormsModule,
    GridModule,
    SharedModule,
    SubHeaderComponent,
    SubSidenavComponent,
    KetquaquantracListComponent,
    KetquaquantracIoComponent,
    ThongsoquantracListComponent,
    ThongsoquantracIoComponent,
  ],
  declarations: [
    SubHeaderComponent,
    SubSidenavComponent,
    KetquaquantracListComponent,
    KetquaquantracIoComponent,
    ThongsoquantracListComponent,
    ThongsoquantracIoComponent,
  ],
  entryComponents: [
    SubHeaderComponent,
    SubSidenavComponent,
    ThongsoquantracIoComponent,
    KetquaquantracIoComponent,
  ],
  providers: [
    MatdialogService,
    MatsidenavService,
    PageService,
    FilterService,
    SortService,
    SearchService,
    GroupService,
    ToolbarService,
    ResizeService,
    { provide: OWL_DATE_TIME_LOCALE, useValue: 'vi' },
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS },
    DatePipe
  ]
})
export class AdminSharedModule { }
