import { ComponentFactoryResolver, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ExComponentFactoryResolverService } from "src/app/core/services/utilities/ex-component-factory-resolver.service";
// Các module features thành phần của phần quản trị dữ liệu
import { QuantriRoutingModule } from "src/app/features/admin/admin-routing.module";
import { AdminSharedModule } from "src/app/features/admin/admin-shared.module";
import { AdminComponent } from "src/app/features/admin/admin.component";
import { CoreModule } from "src/app/core/core.module";
import { SidenavComponent } from "src/app/features/admin/layout/sidenav/sidenav.component";
import { SimplebarAngularModule } from "simplebar-angular";
import { ThuvienModule } from "src/app/features/admin/thuvien/thuvien.module";
// import {HethongModule} from "./hethong/hethong.module";

@NgModule({
  declarations: [AdminComponent, SidenavComponent],
  imports: [
    CommonModule,
    QuantriRoutingModule,
    AdminSharedModule,
    CoreModule,
    SimplebarAngularModule,
    ThuvienModule,
  ],
  exports: [SidenavComponent],
  providers: [],
  entryComponents: []
})
export class AdminModule {
  constructor(
    exResolver: ExComponentFactoryResolverService,
    localResolver: ComponentFactoryResolver
  ) {
    exResolver.registerResolver(localResolver);
  }
}
