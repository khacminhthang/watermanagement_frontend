import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {AdminRoutingName} from "src/app/routes/routes-name";
import {HethongComponent} from "src/app/features/admin/hethong/hethong.component";
import {QuanlyusersListComponent} from "src/app/features/admin/hethong/quanlyusers/quanlyusers-list/quanlyusers-list.component";
import {QuanlyroleListComponent} from "src/app/features/admin/hethong/quanlyrole/quanlyrole-list/quanlyrole-list.component";

const hethongRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "",
        component: HethongComponent
      },
      {
        path: AdminRoutingName.quanlyUser,
        component: QuanlyusersListComponent
      },
      {
        path: AdminRoutingName.quanlyRole,
        component: QuanlyroleListComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(hethongRoutes)],
  exports: [RouterModule]
})
export class HethongRoutingModule {
}
