import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanlyroleListComponent } from './quanlyrole-list.component';

describe('QuanlyroleListComponent', () => {
  let component: QuanlyroleListComponent;
  let fixture: ComponentFixture<QuanlyroleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuanlyroleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanlyroleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
