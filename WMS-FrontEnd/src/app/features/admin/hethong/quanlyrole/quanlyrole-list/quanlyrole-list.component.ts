import {Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {MatDialog, MatSidenav} from "@angular/material";
import {SettingsCommon} from "src/app/shared/constants/setting-common";
import {MatsidenavService} from "src/app/core/services/utilities/matsidenav.service";
import {ProgressService} from "src/app/core/services/utilities/progress.service";
import {CommonServiceShared} from "src/app/core/services/utilities/common-service";
import {SubHeaderService} from "src/app/core/services/utilities/subheader.service";
import {MyAlertDialogComponent} from "src/app/shared/components/my-alert-dialog/my-alert-dialog.component";
import {HttpErrorResponse} from "@angular/common/http";
import {QuanlyroleIoComponent} from "src/app/features/admin/hethong/quanlyrole/quanlyrole-io/quanlyrole-io.component";
import {HethongFacadeService} from "src/app/service/admin/hethong/hethong-facade.service";
import {OutputRoleModel} from "src/app/models/admin/hethong/role.model";

@Component({
  selector: 'app-quanlyrole-list',
  templateUrl: './quanlyrole-list.component.html',
  styleUrls: ['./quanlyrole-list.component.scss']
})
export class QuanlyroleListComponent implements OnInit {
  listRole: OutputRoleModel[];
  listDuLieu: any;
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compRoleio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  settingsCommon = new SettingsCommon();
  navArray = [
    { title: "Quản trị", url: "/admin" },
    {
      title: "Hệ thống",
      url: "/admin/hethong"
    },
    {
      title: "Quản lý role",
      url: "//admin/hethong/quanlyrole"
    }
  ];

  buttonArray = [];
  constructor(
    public matsidenavService: MatsidenavService,
    public progressService: ProgressService,
    public cfr: ComponentFactoryResolver,
    public hethongFacadeService: HethongFacadeService,
    public modalDialog: MatDialog,
    public commonService: CommonServiceShared,
    private subHeaderService: SubHeaderService
  ) { }

  ngOnInit() {
    this.subHeaderService.setParentComp(this);
    this.matsidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 10 };
    this.getAllRole();
    this.dataSubHeader();
  }

  // Truyền data vào header
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }
  // get all role
  async getAllRole() {
    this.listDuLieu = await this.hethongFacadeService
      .getRoleService()
      .getFetchAll();
    const listDataItems = this.listDuLieu;
    if (listDataItems) {
      listDataItems.map((role, index) => {
        role.serialNumber = index + 1;
      });
    }
    this.listRole = listDataItems;
  }

  // open sidebar execute insert
  public openRoleIOSidebar() {
    this.matsidenavService.setTitle("Thêm mới dữ liệu role");
    this.matsidenavService.setContentComp(QuanlyroleIoComponent, "new");
    this.matsidenavService.open();
  }

  // delete
  public deleteRole(data) {
    const canDelete: string = this.hethongFacadeService
      .getRoleService()
      .checkBeDeleted(data.id);
    this.canBeDeletedCheck(canDelete, data);
  }

  public canBeDeletedCheck(sMsg: string, data) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog(data);
    } else {
      this.canDeleteDialog(sMsg);
    }
  }

  confirmDeleteDiaLog(data) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Chắc chắn xóa?";
    dialogRef.componentInstance.content =
      "Role: <b>" + data.name + "</b>";
    dialogRef.componentInstance.okeButton = "Đồng ý";
    dialogRef.componentInstance.cancelButton = "Hủy bỏ";
    dialogRef.componentInstance.visibleOkButton = true;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.hethongFacadeService
          .getRoleService()
          .deleteRequestItem({Id: data.id}).subscribe(
          () => this.getAllRole(),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () => this.commonService.showeNotiResult("Đã xóa thành công: " + data.name, 2000)
        );
      }
    });
  }

  canDeleteDialog(sMsg: string) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Không thể xóa!";
    dialogRef.componentInstance.content = "<b>" + sMsg + "</b>";
    dialogRef.componentInstance.cancelButton = "Đóng";
    dialogRef.componentInstance.visibleOkButton = false;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.componentInstance.headerColor = "red";
  }

  // close
  public closeRoleIOSidebar() {
    this.matSidenav.close();
  }

  // call method
  doFunction(methodName) {
    this[methodName]();
  }
}
