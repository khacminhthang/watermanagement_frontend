import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatsidenavService} from "src/app/core/services/utilities/matsidenav.service";
import {ProgressService} from "src/app/core/services/utilities/progress.service";
import {CommonServiceShared} from "src/app/core/services/utilities/common-service";
import {HttpErrorResponse} from "@angular/common/http";
import {
  displayFieldCssService,
  validationAllErrorMessagesService
} from "src/app/core/services/utilities/validatorService";
import {HethongFacadeService} from "src/app/service/admin/hethong/hethong-facade.service";

@Component({
  selector: 'app-quanlyrole-io',
  templateUrl: './quanlyrole-io.component.html',
  styleUrls: ['./quanlyrole-io.component.scss']
})
export class QuanlyroleIoComponent implements OnInit {
  roleIOForm: FormGroup;
  public editMode: boolean;
  public purpose: string;
  public obj: any;
  // error message
  validationErrorMessages = {
    roleName: { required: "Tên role không được để trống!" }
  };

  // form errors
  formErrors = {
    roleName: ""
  };

  // ctor
  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    public hethongFacadeService: HethongFacadeService
  ) { }

  // onInit
  ngOnInit() {
    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
  }

  // config Form use add or update
  bindingConfigAddOrUpdate() {
    this.editMode = false;
    if (this.purpose === "new" || this.purpose === "edit") {
      this.editMode = true;
    }
    // check edit
    if (this.editMode === true && this.obj) {
      this.roleIOForm.setValue({
        roleName: this.obj.name,
      });
    }
  }

  // config input validation form
  bindingConfigValidation() {
    this.roleIOForm = this.formBuilder.group({
      roleName: ["", Validators.required],
    });
  }

  // on Submit
  public onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    if(this.roleIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.matSidenavService.close();
    }
  }

  // add or update continue
  public addOrUpdateContinue(operMode: string) {
    this.logAllValidationErrorMessages();
    if(this.roleIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.onFormReset();
      this.purpose = "new";
    }
  }

  // add or update
  private addOrUpdate(operMode: string) {
    if (operMode === "new") {
      this.hethongFacadeService
        .getRoleService()
        .addRequestItem({role: this.roleIOForm.value.roleName})
        .subscribe(
          res => this.matSidenavService.doParentFunction("getAllRole"),
          (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Thêm mới role thành công!",
              2000
            )
        );
    }
  }

  // on form reset
  public onFormReset() {
    this.roleIOForm.reset();
  }

  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.roleIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  // display fields css
  public displayFieldCss(field: string) {
    displayFieldCssService(field);
  }

  // close sidebar
  public closeRoleIOSidebar() {
    this.matSidenavService.close();
  }
}
