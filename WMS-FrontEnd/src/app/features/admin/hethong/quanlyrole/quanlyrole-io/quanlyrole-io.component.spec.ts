import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanlyroleIoComponent } from './quanlyrole-io.component';

describe('QuanlyroleIoComponent', () => {
  let component: QuanlyroleIoComponent;
  let fixture: ComponentFixture<QuanlyroleIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuanlyroleIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanlyroleIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
