import {NgModule} from "@angular/core";
import {HethongComponent} from "src/app/features/admin/hethong/hethong.component";
import {HethongRoutingModule} from "src/app/features/admin/hethong/hethong-routing.module";
import {QuanlyroleListComponent} from "src/app/features/admin/hethong/quanlyrole/quanlyrole-list/quanlyrole-list.component";
import {QuanlyusersListComponent} from "src/app/features/admin/hethong/quanlyusers/quanlyusers-list/quanlyusers-list.component";
import {QuanlyroleIoComponent} from "src/app/features/admin/hethong/quanlyrole/quanlyrole-io/quanlyrole-io.component";
import {QuanlyusersIoComponent} from "src/app/features/admin/hethong/quanlyusers/quanlyusers-io/quanlyusers-io.component";
import {AdminSharedModule} from "src/app/features/admin/admin-shared.module";
import {CommonModule} from "@angular/common";
import { ChangePasswordIoComponent } from './quanlyusers/change-password-io/change-password-io.component';
import { ChangeRoleIoComponent } from './quanlyusers/change-role-io/change-role-io.component';

@NgModule({
  declarations: [
    HethongComponent,
    QuanlyroleListComponent,
    QuanlyusersListComponent,
    QuanlyroleIoComponent,
    QuanlyusersIoComponent,
    ChangePasswordIoComponent,
    ChangeRoleIoComponent],
  imports: [
    HethongRoutingModule,
    AdminSharedModule,
    CommonModule,
  ],
  providers: [],
  entryComponents: [
    QuanlyusersIoComponent,
    QuanlyroleIoComponent,
    ChangePasswordIoComponent,
    ChangeRoleIoComponent
  ]
})
export class HethongModule {}
