import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeRoleIoComponent } from './change-role-io.component';

describe('ChangeRoleIoComponent', () => {
  let component: ChangeRoleIoComponent;
  let fixture: ComponentFixture<ChangeRoleIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeRoleIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeRoleIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
