import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { HttpErrorResponse } from "@angular/common/http";
import {
  displayFieldCssService,
  validationAllErrorMessagesService
} from "src/app/core/services/utilities/validatorService";
import { HethongFacadeService } from "src/app/service/admin/hethong/hethong-facade.service";

@Component({
  selector: 'app-change-role-io',
  templateUrl: './change-role-io.component.html',
  styleUrls: ['./change-role-io.component.scss']
})
export class ChangeRoleIoComponent implements OnInit {

  public changeRoleForm: FormGroup;
  public obj: any;

  public roleFilters: any;
  public allRole: any;

  // error message
  validationErrorMessages = {
  };

  // form errors
  formErrors = {
  };

  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    public hethongFacadeService: HethongFacadeService
  ) { }

  ngOnInit() {
    this.bindingConfigValidation();
    this.getAllRole();
    console.log(this.obj, 'obj');
  }

  bindingConfigValidation() {
    this.changeRoleForm = this.formBuilder.group({
      // password: ["", Validators.required],
      role: [""],
    });
  }

   // on Submit
   public onSubmit() {
    // this.logAllValidationErrorMessages();
    // if (this.changeRoleForm.valid === true) {
    //   let inputModel: any = this.changeRoleForm.value;
    //   this.hethongFacadeService.getUserService()
    //   .changePass(this.obj.id, inputModel.newPass).
    //   subscribe(
    //     res => this.matSidenavService.doParentFunction("getAllUser"),
    //     (error: HttpErrorResponse) => {
    //       this.commonService.showError(error);
    //     },
    //     () =>
    //       this.commonService.showeNotiResult(
    //         "Đổi thành công!",
    //         2000
    //       )
    //   );
    //   this.matSidenavService.close();
    // }
  }

  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.changeRoleForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  // display fields css
  public displayFieldCss(field: string) {
    displayFieldCssService(field);
  }


  // close sidebar
  public closeUserIOSidebar() {
    this.matSidenavService.close();
  }
  hashString(){

  }

  async getAllRole() {
    this.allRole = await this.hethongFacadeService.getRoleService().getFetchAll();
    this.roleFilters = this.allRole;
    console.log(this.allRole, 'allRole')
  }

}
