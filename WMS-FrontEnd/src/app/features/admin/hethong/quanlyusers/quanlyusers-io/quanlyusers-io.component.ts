import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { HttpErrorResponse } from "@angular/common/http";
import {
  displayFieldCssService,
  validationAllErrorMessagesService
} from "src/app/core/services/utilities/validatorService";
import { InputAddUserModel, InputUpdateUserModel } from "src/app/models/admin/hethong/user.model";
import { HethongFacadeService } from "src/app/service/admin/hethong/hethong-facade.service";

@Component({
  selector: 'app-quanlyusers-io',
  templateUrl: './quanlyusers-io.component.html',
  styleUrls: ['./quanlyusers-io.component.scss']
})
export class QuanlyusersIoComponent implements OnInit {
  userIOForm: FormGroup;
  public inputAddModel: InputAddUserModel;
  public inputUpdateModel: InputUpdateUserModel;
  public editMode: boolean;
  public purpose: string;
  public obj: any;

  public roleFilters: any;
  public allRole: any;
  // error message
  validationErrorMessages = {
    userName: { required: "Tên tài khoản không được để trống!" }
  };

  // form errors
  formErrors = {
    userName: ""
  };

  // ctor
  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    public hethongFacadeService: HethongFacadeService
  ) { }

  // onInit
  ngOnInit() {
    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
    this.getAllRole();
  }

  // config Form use add or update
  bindingConfigAddOrUpdate() {
    this.editMode = false;
    this.inputAddModel = new InputAddUserModel();
    this.inputUpdateModel = new InputUpdateUserModel();
    if (this.purpose === "new" || this.purpose === "edit") {
      this.editMode = true;
    }
    // check edit
    this.formOnEdit();
  }

  // config input validation form
  bindingConfigValidation() {
    this.userIOForm = this.formBuilder.group({
      userName: ["", Validators.required],
      email: [""],
      password: [""],
      fullName: [""],
      role: [""]
    });
  }

  formOnEdit() {
    if (this.obj) {
      this.userIOForm.removeControl('password');
      this.userIOForm.removeControl('role');
      this.userIOForm.setValue({
        userName: this.obj.userName,
        email: this.obj.email,
        fullName: this.obj.fullName
      });
    }
    this.editMode = true;
  }

  async getAllRole() {
    this.allRole = await this.hethongFacadeService.getRoleService().getFetchAll();
    this.roleFilters = this.allRole;
  }
  // on Submit
  public onSubmit(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.userIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.matSidenavService.close();
    }
  }

  // add or update continue
  public addOrUpdateContinue(operMode: string) {
    this.logAllValidationErrorMessages();
    if (this.userIOForm.valid === true) {
      this.addOrUpdate(operMode);
      this.onFormReset();
      this.purpose = "new";
    }
  }

  // add or update
  private addOrUpdate(operMode: string) {
    if (operMode === "new") {
      this.inputAddModel = this.userIOForm.value;
      this.hethongFacadeService
        .getUserService()
        .addUser(this.inputAddModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("getAllUser"),
          (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Thêm mới user thành công!",
              2000
            )
        );
    } else if (operMode === "edit") {
      this.inputUpdateModel = this.userIOForm.value;
      this.inputUpdateModel.id = this.obj.id;
      this.hethongFacadeService
        .getUserService()
        .updateBodyItem(this.inputUpdateModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("getAllUser"),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Cập nhập dữ liệu user thành công",
              2000
            )
        );
    }
  }

  // on form reset
  public onFormReset() {
    this.userIOForm.reset();
  }

  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.userIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  // display fields css
  public displayFieldCss(field: string) {
    displayFieldCssService(field);
  }

  // close sidebar
  public closeUserIOSidebar() {
    this.matSidenavService.close();
  }
}
