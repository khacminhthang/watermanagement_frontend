import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanlyusersIoComponent } from './quanlyusers-io.component';

describe('QuanlyusersIoComponent', () => {
  let component: QuanlyusersIoComponent;
  let fixture: ComponentFixture<QuanlyusersIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuanlyusersIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanlyusersIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
