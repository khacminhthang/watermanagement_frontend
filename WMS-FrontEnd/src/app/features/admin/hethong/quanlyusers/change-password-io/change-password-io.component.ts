import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { HttpErrorResponse } from "@angular/common/http";
import {
  displayFieldCssService,
  validationAllErrorMessagesService
} from "src/app/core/services/utilities/validatorService";
import { HethongFacadeService } from "src/app/service/admin/hethong/hethong-facade.service";

@Component({
  selector: 'app-change-password-io',
  templateUrl: './change-password-io.component.html',
  styleUrls: ['./change-password-io.component.scss']
})
export class ChangePasswordIoComponent implements OnInit {

  public changePasswordForm: FormGroup;
  public obj: any;

  public isCheckPass = false;

  // error message
  validationErrorMessages = {
    newPass: { required: "Mật khẩu không được để trống!" }
  };

  // form errors
  formErrors = {
    newPass: ""
  };

  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    public hethongFacadeService: HethongFacadeService
  ) { }

  ngOnInit() {
    this.bindingConfigValidation();
  }

  // config input validation form
  bindingConfigValidation() {
    this.changePasswordForm = this.formBuilder.group({
      // password: ["", Validators.required],
      newPass: [""],
      newPassRepeat: [""],
    });
  }

  // on Submit
  public onSubmit() {
    this.logAllValidationErrorMessages();
    if (this.changePasswordForm.value.newPass != (this.changePasswordForm.value.newPassRepeat)) {
      this.isCheckPass = true;
      return;
    }
    if (this.changePasswordForm.valid === true) {
      let inputModel: any = this.changePasswordForm.value;
      this.hethongFacadeService.getUserService()
        .changePass(this.obj.id, inputModel.newPass).
        subscribe(
          res => this.matSidenavService.doParentFunction("getAllUser"),
          (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Đổi thành công!",
              2000
            )
        );
      this.matSidenavService.close();
    }

  }



  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.changePasswordForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  // display fields css
  public displayFieldCss(field: string) {
    displayFieldCssService(field);
  }


  // close sidebar
  public closeUserIOSidebar() {
    this.matSidenavService.close();
  }

  hashString() {

  }

}
