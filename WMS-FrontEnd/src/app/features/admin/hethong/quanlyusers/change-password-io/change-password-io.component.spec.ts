import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePasswordIoComponent } from './change-password-io.component';

describe('ChangePasswordIoComponent', () => {
  let component: ChangePasswordIoComponent;
  let fixture: ComponentFixture<ChangePasswordIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangePasswordIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePasswordIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
