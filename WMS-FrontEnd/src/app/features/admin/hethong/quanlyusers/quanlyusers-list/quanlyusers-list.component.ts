import {Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {MatDialog, MatSidenav} from "@angular/material";
import {SettingsCommon} from "src/app/shared/constants/setting-common";
import {MatsidenavService} from "src/app/core/services/utilities/matsidenav.service";
import {ProgressService} from "src/app/core/services/utilities/progress.service";
import {HethongFacadeService} from "src/app/service/admin/hethong/hethong-facade.service";
import {CommonServiceShared} from "src/app/core/services/utilities/common-service";
import {SubHeaderService} from "src/app/core/services/utilities/subheader.service";
import {MyAlertDialogComponent} from "src/app/shared/components/my-alert-dialog/my-alert-dialog.component";
import {HttpErrorResponse} from "@angular/common/http";
import {OutputUserModel} from "src/app/models/admin/hethong/user.model";
import { QuanlyusersIoComponent } from 'src/app/features/admin/hethong/quanlyusers/quanlyusers-io/quanlyusers-io.component';
import { ChangePasswordIoComponent } from '../change-password-io/change-password-io.component';
import { ChangeRoleIoComponent } from '../change-role-io/change-role-io.component';

@Component({
  selector: 'app-quanlyusers-list',
  templateUrl: './quanlyusers-list.component.html',
  styleUrls: ['./quanlyusers-list.component.scss']
})
export class QuanlyusersListComponent implements OnInit {
  listUser: OutputUserModel[];
  listDuLieu: any;
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compUserio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  settingsCommon = new SettingsCommon();
  navArray = [
    { title: "Quản trị", url: "/admin" },
    {
      title: "Hệ thống",
      url: "/admin/hethong"
    },
    {
      title: "Quản lý user",
      url: "//admin/hethong/quanlyuser"
    }
  ];

  buttonArray = [];
  constructor(
    public matsidenavService: MatsidenavService,
    public progressService: ProgressService,
    public cfr: ComponentFactoryResolver,
    public hethongFacadeService: HethongFacadeService,
    public modalDialog: MatDialog,
    public commonService: CommonServiceShared,
    private subHeaderService: SubHeaderService
  ) { }

  ngOnInit() {
    this.subHeaderService.setParentComp(this);
    this.matsidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 10 };
    this.getAllUser();
    this.dataSubHeader();
  }

  // Truyền data vào header
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }
  // get all role
  async getAllUser() {
    this.listDuLieu = await this.hethongFacadeService
      .getUserService()
      .getFetchAll();
    const listDataItems = this.listDuLieu;
    if (listDataItems) {
      listDataItems.map((user, index) => {
        user.serialNumber = index + 1;
      });
    }
    this.listUser = listDataItems;
  }

  // open sidebar execute insert
  public openUserIOSidebar() {
    this.matsidenavService.setTitle("Thêm mới dữ liệu user");
    this.matsidenavService.setContentComp(QuanlyusersIoComponent, "new");
    this.matsidenavService.open();
  }

  // Hàm sửa thông tin chi tiết một bản ghi, được gọi khi nhấn nút xem chi tiết trên giao diện list
  public openRoleIOSidebarEdit(data) {
    this.matsidenavService.setTitle("Sửa dữ liệu người dùng");
    this.matsidenavService.setContentComp(
      QuanlyusersIoComponent,
      "edit",
      data
    );
    this.matsidenavService.open();
  }
  // delete
  public deleteUser(data) {
    const canDelete: string = this.hethongFacadeService
      .getUserService()
      .checkBeDeleted(data.id);
    this.canBeDeletedCheck(canDelete, data);
  }

  public canBeDeletedCheck(sMsg: string, data) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog(data);
    } else {
      this.canDeleteDialog(sMsg);
    }
  }

  confirmDeleteDiaLog(data) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Chắc chắn xóa?";
    dialogRef.componentInstance.content =
      "Username: <b>" + data.userName + "</b>";
    dialogRef.componentInstance.okeButton = "Đồng ý";
    dialogRef.componentInstance.cancelButton = "Hủy bỏ";
    dialogRef.componentInstance.visibleOkButton = true;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.hethongFacadeService
          .getUserService()
          .deleteRequestItem({Id: data.id}).subscribe(
          () => this.getAllUser(),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () => this.commonService.showeNotiResult("Đã xóa thành công: " + data.userName, 2000)
        );
      }
    });
  }

  canDeleteDialog(sMsg: string) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Không thể xóa!";
    dialogRef.componentInstance.content = "<b>" + sMsg + "</b>";
    dialogRef.componentInstance.cancelButton = "Đóng";
    dialogRef.componentInstance.visibleOkButton = false;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.componentInstance.headerColor = "red";
  }

  // close
  public closeUserIOSidebar() {
    this.matSidenav.close();
  }

  public changePassword(data){
    this.matsidenavService.setTitle("Đổi password");
    this.matsidenavService.setContentComp(ChangePasswordIoComponent, 'changepass', data);
    this.matsidenavService.open();
  }

  changeRole(data){
    this.matsidenavService.setTitle("Đổi quyền");
    this.matsidenavService.setContentComp(ChangeRoleIoComponent, 'changepass', data);
    this.matsidenavService.open();
  }

  // call method
  doFunction(methodName) {
    this[methodName]();
  }
}
