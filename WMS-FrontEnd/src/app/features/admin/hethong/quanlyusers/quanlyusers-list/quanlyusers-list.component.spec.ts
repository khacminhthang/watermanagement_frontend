import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuanlyusersListComponent } from './quanlyusers-list.component';

describe('QuanlyusersListComponent', () => {
  let component: QuanlyusersListComponent;
  let fixture: ComponentFixture<QuanlyusersListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuanlyusersListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuanlyusersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
