import { Routes, RouterModule } from '@angular/router';
import { AdminRoutingName } from 'src/app/routes/routes-name';
import { BaocaoComponent } from 'src/app/features/admin/danhmuc/baocao/baocao.component';
import { NgModule } from '@angular/core';
import {NgchartsComponent} from "src/app/features/admin/danhmuc/baocao/ngcharts/ngcharts.component";

const baocaoRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "",
        children: [
          { path: "", component: BaocaoComponent },
          { path: AdminRoutingName.chartsUri, component: NgchartsComponent }
        ]
      }
    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(baocaoRoutes)],
  exports: [RouterModule]
})
export class BaocaoRoutingModule { }
