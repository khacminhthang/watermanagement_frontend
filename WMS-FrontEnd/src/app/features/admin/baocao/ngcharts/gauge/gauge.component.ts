import { Component, OnInit } from '@angular/core';
import { ChartType, ChartOptions, ChartDataSets } from "chart.js";
import { MultiDataSet, Label } from "ng2-charts";

@Component({
  selector: 'app-gauge',
  templateUrl: './gauge.component.html',
  styleUrls: ['./gauge.component.css']
})
export class GaugeComponent implements OnInit {
  public doughnutChartLabels: Label[] = ["0-50: Tốt", "51-100: Trung Bình", "101-150: Không tốt với nhóm nhạy cảm", "151-200: Xấu", "201-300: Rất xấu"];
  public doughnutChartData: MultiDataSet = [
    [50, 50, 50, 50, 100]
  ];
  public doughnutChartType: ChartType = "doughnut";
  public chartOptions: ChartOptions = {
    legend: {
      display: true,
      labels: {
        fontColor: "#000000"
      }
    },
    circumference: Math.PI,
    rotation: -Math.PI,
    cutoutPercentage: 60
  };
  public chartDataSets: ChartDataSets[] = [
    {
      borderWidth: 1,
      backgroundColor: ["Green", "Yellow", "Orange", "Red", "Purple"],
      hoverBackgroundColor: ["Green", "Yellow", "Orange", "Red", "Purple"]
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
