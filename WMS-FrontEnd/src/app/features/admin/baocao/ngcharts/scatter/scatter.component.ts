import {Component, OnInit} from '@angular/core';
import {ScatterChartjsService} from "src/app/core/services/utilities/scatter-chartjs.service";

@Component({
  selector: 'app-scatter',
  templateUrl: './scatter.component.html',
  styleUrls: ['./scatter.component.scss']
})
export class ScatterComponent implements OnInit {

  constructor(public scatterService: ScatterChartjsService) {
  }

  public resetZoom() {
    this.scatterService.resetZoom();
  }

  ngOnInit() {
    this.scatterService.setElement("scatterid");
    this.scatterService.initChart();
  }

}
