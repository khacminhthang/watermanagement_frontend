import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ViewChild,
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  DateTimeAdapter,
  OWL_DATE_TIME_FORMATS,
  OWL_DATE_TIME_LOCALE,
  OwlDateTimeComponent,
  OwlDateTimeFormats,
} from "ng-pick-datetime";
import { MomentDateTimeAdapter } from "ng-pick-datetime-moment";
import { DatePipe } from "@angular/common";
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import {
  GridComponent,
  ExcelExportService,
} from "@syncfusion/ej2-angular-grids";
import { CommonFacadeService } from "src/app/service/admin/common/common-facade.service";
import { ExcelExportProperties } from "@syncfusion/ej2-angular-grids";
import { validationAllErrorMessagesService } from "src/app/core/services/utilities/validatorService";
import { TnnuocFacadeService } from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
export const MY_MOMENT_DATE_TIME_FORMATS: OwlDateTimeFormats = {
  parseInput: "YYYY",
  fullPickerInput: "l LT",
  datePickerInput: "YYYY",
  timePickerInput: "LT",
  monthYearLabel: "YYYY",
  dateA11yLabel: "LL",
  monthYearA11yLabel: "YYYY",
};
@Component({
  selector: "app-baocao",
  templateUrl: "./baocao.component.html",
  styleUrls: ["./baocao.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    ExcelExportService,
    {
      provide: DateTimeAdapter,
      useClass: MomentDateTimeAdapter,
      deps: [OWL_DATE_TIME_LOCALE],
    },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_DATE_TIME_FORMATS },
  ],
})
export class BaocaoComponent implements OnInit {
  formTienIch: FormGroup;
  public year;
  public trungBinhNam = 0;
  public dulieuCaoNhat = { dulieu: 0, ngay: "", thang: 0 };
  public dulieuThapNhat = { dulieu: 0, ngay: "", thang: 0 };
  public tenThongSo;
  public tenCongTrinh;
  public song;
  public toadoX;
  public toadoY;
  public tenDonViDo;
  public ghiChu;
  public fileName;
  public dataThongKe: any;
  @ViewChild("grid", { static: false }) public grid: GridComponent;
  public settingsCommon = new SettingsCommon();
  public dataGet: any;
  public dataTrungBinh: any;

  public congtrinhFilters: any;
  public allCongTrinh: any;
  public thamsoFilters: any;
  public allThamSo: any;
  // Error message
  validationErrorMessages = {
    dateTime: { required: "Năm không được để trống" },
  };

  // Form errors
  formErrors = {
    dateTime: "",
  };
  constructor(
    private datePipe: DatePipe,
    private formBuilder: FormBuilder,
    public commonFacadeService: CommonFacadeService,
    public tnnFacadeService: TnnuocFacadeService
  ) {}

  ngOnInit() {
    this.formTienIch = this.formBuilder.group({
      loaicongtrinh: [""],
      idcongtrinh: [""],
      idthamso: [""],
      dateTime: ["", Validators.required],
    });
  }

  /**
   * Hàm láy thông tin liên quan sử dụng cho phiếu
   */
  getInfo() {
    this.allCongTrinh.map((ct) => {
      if (ct.id === this.formTienIch.value.idcongtrinh) {
        this.tenCongTrinh = ct.tendiem;
        this.toadoX = ct.toadox;
        this.toadoY = ct.toadoy;
        this.ghiChu = ct.note;
      }
    });
    this.allThamSo.map((ts) => {
      if (ts.idthamso === this.formTienIch.value.idthamso) {
        this.tenThongSo = ts.tenthamso;
        this.tenDonViDo = ts.kyhieudonvido;
      }
    });
  }

  chosenYearHandler(
    normalizedYear: any,
    datepicker: OwlDateTimeComponent<any>
  ) {
    this.formTienIch.setValue({ congtrinh: "", dateTime: normalizedYear });
    datepicker.close();
  }

  exportFileExcel(): void {
    this.getInfo();
    this.fileName = `${this.tenCongTrinh}_${this.tenThongSo}_${this.year}.xlsx`;
    const file = this.fileName.toLowerCase();
    const excelExportProperties: ExcelExportProperties = {
      fileName: file,
      theme: {
        header: {
          fontName: "Times New Roman",
          fontSize: 16,
          hAlign: "Center",
          borders: { color: "#1b1b2f" },
        },
        record: {
          fontName: "Times New Roman",
          hAlign: "Center",
          borders: { color: "#1b1b2f" },
        },
      },
      header: {
        headerRows: 3,
        rows: [
          {
            cells: [
              {
                index: 1,
                colSpan: 4,
                value: "Trạm: " + this.tenCongTrinh,
                style: {
                  fontName: "Times New Roman",
                  fontSize: 16,
                  hAlign: "Left",
                },
              },
              {
                index: 11,
                colSpan: 3,
                value: "Tọa độ x: " + this.toadoX,
                style: {
                  fontName: "Times New Roman",
                  fontSize: 16,
                  hAlign: "Right",
                },
              },
            ],
          },
          {
            cells: [
              {
                index: 1,
                colSpan: 3,
                value: "Sông: ",
                style: {
                  fontName: "Times New Roman",
                  fontSize: 16,
                  hAlign: "Left",
                },
              },
              {
                index: 4,
                colSpan: 6,
                value: this.tenThongSo,
                style: {
                  fontName: "Times New Roman",
                  fontSize: 18,
                  hAlign: "Center",
                  bold: true,
                },
              },
              {
                index: 11,
                colSpan: 3,
                value: "Tọa độ y: " + this.toadoY,
                style: {
                  fontName: "Times New Roman",
                  fontSize: 16,
                  hAlign: "Right",
                },
              },
            ],
          },
          {
            cells: [
              {
                index: 1,
                colSpan: 4,
                value: "Ghi chú: " + this.ghiChu,
                style: {
                  fontName: "Times New Roman",
                  fontSize: 16,
                  hAlign: "Left",
                },
              },
              {
                index: 6,
                colSpan: 2,
                value: "Năm: " + this.year,
                style: {
                  fontName: "Times New Roman",
                  fontSize: 16,
                  hAlign: "Center",
                },
              },
              {
                index: 11,
                colSpan: 3,
                value: "Đơn vị đo: ",
                style: {
                  fontName: "Times New Roman",
                  fontSize: 16,
                  hAlign: "Right",
                },
              },
            ],
          },
        ],
      },
      footer: {
        footerRows: 3,
        rows: [
          {
            cells: [
              {
                index: 1,
                rowSpan: 3,
                value: "Tổng trung bình",
                style: {
                  hAlign: "Center",
                  vAlign: "Center",
                  wrapText: true,
                  borders: { color: "#1b1b2f" },
                },
              },
              {
                index: 2,
                colSpan: 5,
                value: "Trung bình năm",
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 7,
                value: this.trungBinhNam,
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 8,
                value: this.tenDonViDo,
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 9,
                colSpan: 5,
                value: "",
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
            ],
          },
          {
            cells: [
              {
                index: 2,
                colSpan: 5,
                value: "Lớn nhất",
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 7,
                value: this.dulieuCaoNhat.dulieu,
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 8,
                value: this.tenDonViDo,
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 9,
                value: "Ngày",
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 10,
                value: this.dulieuCaoNhat.ngay,
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 11,
                value: "Tháng",
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 12,
                value: this.dulieuCaoNhat.thang,
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 13,
                value: "",
                style: { borders: { color: "#1b1b2f" } },
              },
            ],
          },
          {
            cells: [
              {
                index: 2,
                colSpan: 5,
                value: "Nhỏ nhất",
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 7,
                value: this.dulieuThapNhat.dulieu,
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 8,
                value: this.tenDonViDo,
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 9,
                value: "Ngày",
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 10,
                value: this.dulieuThapNhat.ngay,
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 11,
                value: "Tháng",
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 12,
                value: this.dulieuThapNhat.thang,
                style: { hAlign: "Center", borders: { color: "#1b1b2f" } },
              },
              {
                index: 13,
                value: "",
                style: { borders: { color: "#1b1b2f" } },
              },
            ],
          },
        ],
      },
    };
    this.grid.excelExport(excelExportProperties);
  }

  /**
   * Hàm để định dạng dữ liệu cho đúng kiểu để xuất file theo ngày
   */
  formatDefaultData() {
    this.dataTrungBinh = [
      {
        ngay: 1,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 2,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 3,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 4,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 5,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 6,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 7,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 8,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 9,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 10,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 11,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 12,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 13,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 14,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 15,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 16,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 17,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 18,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 19,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 20,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 21,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 22,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 23,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 24,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 25,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 26,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 27,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 28,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 29,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 30,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: 31,
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: "Tổng",
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: "Trung bình",
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: "Max",
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: "Ngày",
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: "Min",
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
      {
        ngay: "Ngày",
        thang1: null,
        thang2: null,
        thang3: null,
        thang4: null,
        thang5: null,
        thang6: null,
        thang7: null,
        thang8: null,
        thang9: null,
        thang10: null,
        thang11: null,
        thang12: null,
      },
    ];
  }
  /**
   * Lấy dữ liệu data api theo năm
   */
  async getDataYear(year) {
    this.dataGet = await this.commonFacadeService
      .getDlquantractsService()
      .getKetquaYear({
        Year: year,
        IdTramquantrac: this.formTienIch.value.idcongtrinh,
        PageNumber: 1,
        PageSize: -1,
        Objkey: this.formTienIch.value.loaicongtrinh,
        IdThamsodo: this.formTienIch.value.idthamso,
      });
    await this.convertDataDefaultToGrid(this.dataGet.items);
  }

  /**
   * Hàm tính toán ra hàng
   * @param data
   */
  async getCalculateData(data) {
    const dem = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    // Gán dữ liệu khởi đầu vào dòng max tháng
    data[33].thang1 = 0;
    data[33].thang2 = 0;
    data[33].thang3 = 0;
    data[33].thang4 = 0;
    data[33].thang5 = 0;
    data[33].thang6 = 0;
    data[33].thang7 = 0;
    data[33].thang8 = 0;
    data[33].thang9 = 0;
    data[33].thang10 = 0;
    data[33].thang11 = 0;
    data[33].thang12 = 0;

    // Gán dữ liệu khởi đầu vào dòng min tháng
    data[35].thang1 = data[0].thang1;
    data[35].thang2 = data[0].thang2;
    data[35].thang3 = data[0].thang3;
    data[35].thang4 = data[0].thang4;
    data[35].thang5 = data[0].thang5;
    data[35].thang6 = data[0].thang6;
    data[35].thang7 = data[0].thang7;
    data[35].thang8 = data[0].thang8;
    data[35].thang9 = data[0].thang9;
    data[35].thang10 = data[0].thang10;
    data[35].thang11 = data[0].thang11;
    data[35].thang12 = data[0].thang12;
    for (let i = 0; i < 31; i++) {
      if (data[35].thang1 === null) {
        data[35].thang1 = data[i].thang1;
      }
      if (data[35].thang2 === null) {
        data[35].thang2 = data[i].thang2;
      }
      if (data[35].thang3 === null) {
        data[35].thang3 = data[i].thang3;
      }
      if (data[35].thang4 === null) {
        data[35].thang4 = data[i].thang4;
      }
      if (data[35].thang5 === null) {
        data[35].thang5 = data[i].thang5;
      }
      if (data[35].thang6 === null) {
        data[35].thang6 = data[i].thang6;
      }
      if (data[35].thang7 === null) {
        data[35].thang7 = data[i].thang7;
      }
      if (data[35].thang8 === null) {
        data[35].thang8 = data[i].thang8;
      }
      if (data[35].thang9 === null) {
        data[35].thang9 = data[i].thang9;
      }
      if (data[35].thang10 === null) {
        data[35].thang10 = data[i].thang10;
      }
      if (data[35].thang11 === null) {
        data[35].thang11 = data[i].thang11;
      }
      if (data[35].thang12 === null) {
        data[35].thang12 = data[i].thang12;
      }
      // Set hàng min max cho mảng data
      if (!data[i].thang1 === false) {
        if (data[i].thang1 > data[33].thang1) {
          data[33].thang1 = data[i].thang1;
        }
        if (data[i].thang1 < data[35].thang1) {
          data[35].thang1 = data[i].thang1;
        }
      }
      if (!data[i].thang2 === false) {
        if (data[i].thang2 > data[33].thang2) {
          data[33].thang2 = data[i].thang2;
        }
        if (data[i].thang2 < data[35].thang2) {
          data[35].thang2 = data[i].thang2;
        }
      }
      if (!data[i].thang3 === false) {
        if (data[i].thang3 > data[33].thang3) {
          data[33].thang3 = data[i].thang3;
        }
        if (data[i].thang3 < data[35].thang3) {
          data[35].thang3 = data[i].thang3;
        }
      }
      if (!data[i].thang4 === false) {
        if (data[i].thang4 > data[33].thang4) {
          data[33].thang4 = data[i].thang4;
        }
        if (data[i].thang4 < data[35].thang4) {
          data[35].thang4 = data[i].thang4;
        }
      }
      if (!data[i].thang5 === false) {
        if (data[i].thang5 > data[33].thang5) {
          data[33].thang5 = data[i].thang5;
        }
        if (data[i].thang5 < data[35].thang5) {
          data[35].thang5 = data[i].thang5;
        }
      }
      if (!data[i].thang6 === false) {
        if (data[i].thang6 > data[33].thang6) {
          data[33].thang6 = data[i].thang6;
        }
        if (data[i].thang6 < data[35].thang6) {
          data[35].thang6 = data[i].thang6;
        }
      }
      if (!data[i].thang7 === false) {
        if (data[i].thang7 > data[33].thang7) {
          data[33].thang7 = data[i].thang7;
        }
        if (data[i].thang7 < data[35].thang7) {
          data[35].thang7 = data[i].thang7;
        }
      }
      if (!data[i].thang8 === false) {
        if (data[i].thang8 > data[33].thang8) {
          data[33].thang8 = data[i].thang8;
        }
        if (data[i].thang8 < data[35].thang8) {
          data[35].thang8 = data[i].thang8;
        }
      }
      if (!data[i].thang9 === false) {
        if (data[i].thang9 > data[33].thang9) {
          data[33].thang9 = data[i].thang9;
        }
        if (data[i].thang9 < data[35].thang9) {
          data[35].thang9 = data[i].thang9;
        }
      }
      if (!data[i].thang10 === false) {
        if (data[i].thang10 > data[33].thang10) {
          data[33].thang10 = data[i].thang10;
        }
        if (data[i].thang10 < data[35].thang10) {
          data[35].thang10 = data[i].thang10;
        }
      }
      if (!data[i].thang11 === false) {
        if (data[i].thang11 > data[33].thang11) {
          data[33].thang11 = data[i].thang11;
        }
        if (data[i].thang11 < data[35].thang11) {
          data[35].thang11 = data[i].thang11;
        }
      }
      if (!data[i].thang12 === false) {
        if (data[i].thang12 > data[33].thang12) {
          data[33].thang12 = data[i].thang12;
        }
        if (data[i].thang12 < data[35].thang12) {
          data[35].thang12 = data[i].thang12;
        }
      }

      // tăng biến đếm tồn tại dữ liệu
      if (data[i].thang1 !== null) {
        dem[0]++;
      }
      if (data[i].thang2 !== null) {
        dem[1]++;
      }
      if (data[i].thang3 !== null) {
        dem[2]++;
      }
      if (data[i].thang4 !== null) {
        dem[3]++;
      }
      if (data[i].thang5 !== null) {
        dem[4]++;
      }
      if (data[i].thang6 !== null) {
        dem[5]++;
      }
      if (data[i].thang7 !== null) {
        dem[6]++;
      }
      if (data[i].thang8 !== null) {
        dem[7]++;
      }
      if (data[i].thang9 !== null) {
        dem[8]++;
      }
      if (data[i].thang10 !== null) {
        dem[9]++;
      }
      if (data[i].thang11 !== null) {
        dem[10]++;
      }
      if (data[i].thang12 !== null) {
        dem[11]++;
      }

      // Tính tổng các ngày trong tháng
      data[31].thang1 = data[31].thang1 + data[i].thang1;
      data[31].thang2 = data[31].thang2 + data[i].thang2;
      data[31].thang3 = data[31].thang3 + data[i].thang3;
      data[31].thang4 = data[31].thang4 + data[i].thang4;
      data[31].thang5 = data[31].thang5 + data[i].thang5;
      data[31].thang6 = data[31].thang6 + data[i].thang6;
      data[31].thang7 = data[31].thang7 + data[i].thang7;
      data[31].thang8 = data[31].thang8 + data[i].thang8;
      data[31].thang9 = data[31].thang9 + data[i].thang9;
      data[31].thang10 = data[31].thang10 + data[i].thang10;
      data[31].thang11 = data[31].thang11 + data[i].thang11;
      data[31].thang12 = data[31].thang12 + data[i].thang12;
    }

    // Tìm những tháng giá trị bằng max và min
    for (let j = 0; j < 31; j++) {
      // Tìm những tháng max
      if (data[j].thang1 === data[33].thang1) {
        if (data[34].thang1 !== null) {
          data[34].thang1 = data[34].thang1 + ", " + `${j + 1}`;
        } else {
          data[34].thang1 = `${j + 1}`;
        }
      }
      if (data[j].thang2 === data[33].thang2) {
        if (data[34].thang2 !== null) {
          data[34].thang2 = data[34].thang2 + ", " + `${j + 1}`;
        } else {
          data[34].thang2 = `${j + 1}`;
        }
      }
      if (data[j].thang3 === data[33].thang3) {
        if (data[34].thang3 !== null) {
          data[34].thang3 = data[34].thang3 + ", " + `${j + 1}`;
        } else {
          data[34].thang3 = `${j + 1}`;
        }
      }
      if (data[j].thang4 === data[33].thang4) {
        if (data[34].thang4 !== null) {
          data[34].thang4 = data[34].thang4 + ", " + `${j + 1}`;
        } else {
          data[34].thang4 = `${j + 1}`;
        }
      }
      if (data[j].thang5 === data[33].thang5) {
        if (data[34].thang5 !== null) {
          data[34].thang5 = data[34].thang5 + ", " + `${j + 1}`;
        } else {
          data[34].thang5 = `${j + 1}`;
        }
      }
      if (data[j].thang6 === data[33].thang6) {
        if (data[34].thang6 !== null) {
          data[34].thang6 = data[34].thang6 + ", " + `${j + 1}`;
        } else {
          data[34].thang6 = `${j + 1}`;
        }
      }
      if (data[j].thang7 === data[33].thang7) {
        if (data[34].thang7 !== null) {
          data[34].thang7 = data[34].thang7 + ", " + `${j + 1}`;
        } else {
          data[34].thang7 = `${j + 1}`;
        }
      }
      if (data[j].thang8 === data[33].thang8) {
        if (data[34].thang8 !== null) {
          data[34].thang8 = data[34].thang8 + ", " + `${j + 1}`;
        } else {
          data[34].thang8 = `${j + 1}`;
        }
      }
      if (data[j].thang9 === data[33].thang9) {
        if (data[34].thang9 !== null) {
          data[34].thang9 = data[34].thang9 + ", " + `${j + 1}`;
        } else {
          data[34].thang9 = `${j + 1}`;
        }
      }
      if (data[j].thang10 === data[33].thang10) {
        if (data[34].thang10 !== null) {
          data[34].thang10 = data[34].thang10 + ", " + `${j + 1}`;
        } else {
          data[34].thang10 = `${j + 1}`;
        }
      }
      if (data[j].thang11 === data[33].thang11) {
        if (data[34].thang11 !== null) {
          data[34].thang11 = data[34].thang11 + ", " + `${j + 1}`;
        } else {
          data[34].thang11 = `${j + 1}`;
        }
      }
      if (data[j].thang12 === data[33].thang12) {
        if (data[34].thang12 !== null) {
          data[34].thang12 = data[34].thang12 + ", " + `${j + 1}`;
        } else {
          data[34].thang12 = `${j + 1}`;
        }
      }

      // Tìm những tháng min
      if (data[j].thang1 === data[35].thang1) {
        if (data[36].thang1 !== null) {
          data[36].thang1 = data[36].thang1 + ", " + `${j + 1}`;
        } else {
          data[36].thang1 = `${j + 1}`;
        }
      }
      if (data[j].thang2 === data[35].thang2) {
        if (data[36].thang2 !== null) {
          data[36].thang2 = data[36].thang2 + ", " + `${j + 1}`;
        } else {
          data[36].thang2 = `${j + 1}`;
        }
      }
      if (data[j].thang3 === data[35].thang3) {
        if (data[36].thang3 !== null) {
          data[36].thang3 = data[36].thang3 + ", " + `${j + 1}`;
        } else {
          data[36].thang3 = `${j + 1}`;
        }
      }
      if (data[j].thang4 === data[35].thang4) {
        if (data[36].thang4 !== null) {
          data[36].thang4 = data[36].thang4 + ", " + `${j + 1}`;
        } else {
          data[36].thang4 = `${j + 1}`;
        }
      }
      if (data[j].thang5 === data[35].thang5) {
        if (data[36].thang5 !== null) {
          data[36].thang5 = data[36].thang5 + ", " + `${j + 1}`;
        } else {
          data[36].thang5 = `${j + 1}`;
        }
      }
      if (data[j].thang6 === data[35].thang6) {
        if (data[36].thang6 !== null) {
          data[36].thang6 = data[36].thang6 + ", " + `${j + 1}`;
        } else {
          data[36].thang6 = `${j + 1}`;
        }
      }
      if (data[j].thang7 === data[35].thang7) {
        if (data[36].thang7 !== null) {
          data[36].thang7 = data[36].thang7 + ", " + `${j + 1}`;
        } else {
          data[36].thang7 = `${j + 1}`;
        }
      }
      if (data[j].thang8 === data[35].thang8) {
        if (data[36].thang8 !== null) {
          data[36].thang8 = data[36].thang8 + ", " + `${j + 1}`;
        } else {
          data[36].thang8 = `${j + 1}`;
        }
      }
      if (data[j].thang9 === data[35].thang9) {
        if (data[36].thang9 !== null) {
          data[36].thang9 = data[36].thang9 + ", " + `${j + 1}`;
        } else {
          data[36].thang9 = `${j + 1}`;
        }
      }
      if (data[j].thang10 === data[35].thang10) {
        if (data[36].thang10 !== null) {
          data[36].thang10 = data[36].thang10 + ", " + `${j + 1}`;
        } else {
          data[36].thang10 = `${j + 1}`;
        }
      }
      if (data[j].thang11 === data[35].thang11) {
        if (data[36].thang11 !== null) {
          data[36].thang11 = data[36].thang11 + ", " + `${j + 1}`;
        } else {
          data[36].thang11 = `${j + 1}`;
        }
      }
      if (data[j].thang12 === data[35].thang12) {
        if (data[36].thang12 !== null) {
          data[36].thang12 = data[36].thang12 + ", " + `${j + 1}`;
        } else {
          data[36].thang12 = `${j + 1}`;
        }
      }
    }
    // Tính trung bình dữ liệu của các tháng các ngày trong tháng
    if (dem[0] !== 0) {
      data[32].thang1 = data[31].thang1 / dem[0];
    }
    if (dem[1] !== 0) {
      data[32].thang2 = data[31].thang2 / dem[1];
    }
    if (dem[2] !== 0) {
      data[32].thang3 = data[31].thang3 / dem[2];
    }
    if (dem[3] !== 0) {
      data[32].thang4 = data[31].thang4 / dem[3];
    }
    if (dem[4] !== 0) {
      data[32].thang5 = data[31].thang5 / dem[4];
    }
    if (dem[5] !== 0) {
      data[32].thang6 = data[31].thang6 / dem[5];
    }
    if (dem[6] !== 0) {
      data[32].thang7 = data[31].thang7 / dem[6];
    }
    if (dem[7] !== 0) {
      data[32].thang8 = data[31].thang8 / dem[7];
    }
    if (dem[8] !== 0) {
      data[32].thang9 = data[31].thang9 / dem[8];
    }
    if (dem[9] !== 0) {
      data[32].thang10 = data[31].thang10 / dem[9];
    }
    if (dem[10] !== 0) {
      data[32].thang11 = data[31].thang11 / dem[10];
    }
    if (dem[11] !== 0) {
      data[32].thang12 = data[31].thang12 / dem[11];
    }
    // Tính trung bình dữ liệu theo năm
    let d = 0;
    let array = [
      data[32].thang1,
      data[32].thang2,
      data[32].thang3,
      data[32].thang4,
      data[32].thang5,
      data[32].thang6,
      data[32].thang7,
      data[32].thang8,
      data[32].thang9,
      data[32].thang10,
      data[32].thang11,
      data[32].thang12,
    ];
    array.map((tb) => {
      if (tb !== null) {
        this.trungBinhNam = this.trungBinhNam + tb;
        d++;
      }
    });
    if (d !== 0) {
      this.trungBinhNam = this.trungBinhNam / d;
    }
    // Lấy dữ liệu max của một năm
    array = [
      data[33].thang1,
      data[33].thang2,
      data[33].thang3,
      data[33].thang4,
      data[33].thang5,
      data[33].thang6,
      data[33].thang7,
      data[33].thang8,
      data[33].thang9,
      data[33].thang10,
      data[33].thang11,
      data[33].thang12,
    ];
    array.map((dt, index) => {
      if (dt >= this.dulieuCaoNhat.dulieu) {
        this.dulieuCaoNhat.dulieu = dt;
        this.dulieuCaoNhat.thang = index + 1;
      }
    });
    array = [
      data[34].thang1,
      data[34].thang2,
      data[34].thang3,
      data[34].thang4,
      data[34].thang5,
      data[34].thang6,
      data[34].thang7,
      data[34].thang8,
      data[34].thang9,
      data[34].thang10,
      data[34].thang11,
      data[34].thang12,
    ];
    array.map((ngay, index) => {
      if (index === this.dulieuCaoNhat.thang - 1) {
        this.dulieuCaoNhat.ngay = ngay;
      }
    });

    // Lấy dữ liệu min của một năm
    this.dulieuThapNhat.dulieu = data[35].thang1;
    array = [
      data[35].thang1,
      data[35].thang2,
      data[35].thang3,
      data[35].thang4,
      data[35].thang5,
      data[35].thang6,
      data[35].thang7,
      data[35].thang8,
      data[35].thang9,
      data[35].thang10,
      data[35].thang11,
      data[35].thang12,
    ];
    array.map((dt, index) => {
      if (dt <= this.dulieuThapNhat.dulieu) {
        this.dulieuThapNhat.dulieu = dt;
        this.dulieuThapNhat.thang = index + 1;
      }
    });
    array = [
      data[36].thang1,
      data[36].thang2,
      data[36].thang3,
      data[36].thang4,
      data[36].thang5,
      data[36].thang6,
      data[36].thang7,
      data[36].thang8,
      data[36].thang9,
      data[36].thang10,
      data[36].thang11,
      data[36].thang12,
    ];
    array.map((ngay, index) => {
      if (index === this.dulieuThapNhat.thang - 1) {
        this.dulieuThapNhat.ngay = ngay;
      }
    });

    // format 2 chữ số sau dấu phảy của trung bình ngày và năm
    this.trungBinhNam = +this.trungBinhNam.toFixed(2);
    const thang = [
      "thang1",
      "thang2",
      "thang3",
      "thang4",
      "thang5",
      "thang6",
      "thang7",
      "thang8",
      "thang9",
      "thang10",
      "thang11",
      "thang12",
    ];
    thang.map((value) => {
      if (!data[32][value] === false) {
        data[32][value] = +data[32][value].toFixed(2);
        data[31][value] = +data[31][value].toFixed(2);
      }
    });
    this.dataTrungBinh = data;
    await (this.dataThongKe = this.dataTrungBinh);
    console.log(this.dataTrungBinh);
  }
  /**
   * Chuyển dữ liệu dạng default lấy được từ api vào dữ liệu mẫu để nhập vào bảng kiểu trung bình
   * @param data
   */
  async convertDataDefaultToGrid(data) {
    data.map((dt) => {
      switch (dt.thangdo) {
        case 1:
          this.dataTrungBinh[dt.ngaydo - 1].thang1 = dt.avgKetquado;
          break;
        case 2:
          this.dataTrungBinh[dt.ngaydo - 1].thang2 = dt.avgKetquado;
          break;
        case 3:
          this.dataTrungBinh[dt.ngaydo - 1].thang3 = dt.avgKetquado;
          break;
        case 4:
          this.dataTrungBinh[dt.ngaydo - 1].thang4 = dt.avgKetquado;
          break;
        case 5:
          this.dataTrungBinh[dt.ngaydo - 1].thang5 = dt.avgKetquado;
          break;
        case 6:
          this.dataTrungBinh[dt.ngaydo - 1].thang6 = dt.avgKetquado;
          break;
        case 7:
          this.dataTrungBinh[dt.ngaydo - 1].thang7 = dt.avgKetquado;
          break;
        case 8:
          this.dataTrungBinh[dt.ngaydo - 1].thang8 = dt.avgKetquado;
          break;
        case 9:
          this.dataTrungBinh[dt.ngaydo - 1].thang9 = dt.avgKetquado;
          break;
        case 10:
          this.dataTrungBinh[dt.ngaydo - 1].thang10 = dt.avgKetquado;
          break;
        case 11:
          this.dataTrungBinh[dt.ngaydo - 1].thang11 = dt.avgKetquado;
          break;
        case 12:
          this.dataTrungBinh[dt.ngaydo - 1].thang12 = dt.avgKetquado;
          break;
      }
    });
    this.getCalculateData(this.dataTrungBinh);
    this.grid.refresh();
  }

  /**
   * Show kết quả theo năm của thông số
   */
  showKetQua() {
    this.getInfo();
    this.logAllValidationErrorMessages();
    if (this.formTienIch.valid) {
      this.year = this.datePipe.transform(
        this.formTienIch.value.dateTime,
        "yyyy"
      );
      this.formatDefaultData();
      this.getDataYear(this.year);
    }
  }

  async showLoaiCongTrinh() {
    switch (this.formTienIch.value.loaicongtrinh) {
      case 5:
        this.allCongTrinh = await this.tnnFacadeService
          .getSwKhaithacnuocmatService()
          .getFetchAll({ pageNumber: 1, pageSize: -1 });
        this.allCongTrinh = this.allCongTrinh.items;
        this.congtrinhFilters = this.allCongTrinh;
        break;
      case 6:
        this.allCongTrinh = await this.tnnFacadeService
          .getSwXaNuocThaiService()
          .getFetchAll({ pageNumber: 1, pageSize: -1 });
        this.allCongTrinh = this.allCongTrinh.items;
        this.congtrinhFilters = this.allCongTrinh;
        break;
      case 7:
        this.allCongTrinh = await this.tnnFacadeService
          .getSwTrambomService()
          .getFetchAll({ pageNumber: 1, pageSize: -1 });
        this.allCongTrinh = this.allCongTrinh.items;
        this.congtrinhFilters = this.allCongTrinh;
        break;
    }
  }

  async showCongTrinh() {
    this.allThamSo = await this.commonFacadeService
      .getObjthamsoService()
      .getFetchAll({
        idObj: this.formTienIch.value.idcongtrinh,
        ObjKey: this.formTienIch.value.loaicongtrinh,
      });
    this.thamsoFilters = this.allThamSo;
  }
  /**
   * Hàm check validation khi submit
   */
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.formTienIch,
      this.validationErrorMessages,
      this.formErrors
    );
  }
}
