import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdminRoutingName } from "src/app/routes/routes-name";
import { AdminComponent } from "./admin.component";
import {AuthGuard} from "../../auth/auth.guard";

const adminRoutes: Routes = [
  {
    path: "",
    component: AdminComponent,
    children: [
      {
        path: AdminRoutingName.danhmucUri,
        //canActivate: [AuthGuard],
        loadChildren: () =>
          import("src/app/features/admin/danhmuc/danhmuc.module").then(
            mod => mod.DanhmucModule
          )
      },
      {
        path: AdminRoutingName.wrUri,
        // canActivate: [AuthGuard],
        loadChildren: () =>
          import("src/app/features/admin/tnnuoc/tnnuoc.module").then(
            mod => mod.TnnuocModule
          )
      },
      {
        path: AdminRoutingName.baocaoUri,
        // canActivate: [AuthGuard],
        loadChildren: () =>
          import("src/app/features/admin/baocao/baocao.module").then(
            mod => mod.BaocaoModule
          )
      },
      {
        path: AdminRoutingName.hethongUri,
        // canActivate: [AuthGuard],
        loadChildren: () =>
          import("src/app/features/admin/hethong/hethong.module").then(
            mod => mod.HethongModule
          )
      },
      {
        path: AdminRoutingName.thuvienUri,
        //canActivate: [AuthGuard],
        loadChildren: () =>
          import("src/app/features/admin/thuvien/thuvien.module").then(
            mod => mod.ThuvienModule
          )
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class QuantriRoutingModule { }
