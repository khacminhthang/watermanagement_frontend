import { Component, Inject, OnInit, HostListener, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from "@angular/material/dialog";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  displayFieldCssService,
  validationAllErrorMessagesService, ValidatorToaDoService
} from "src/app/core/services/utilities/validatorService";
import { InputGmediaModel, OutputGmediaModel } from "src/app/models/admin/common/gmedia.model";
import { DatePipe } from "@angular/common";
import { HttpErrorResponse } from "@angular/common/http";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatdialogService } from "src/app/core/services/utilities/matdialog.service";
import { MyAlertDialogComponent } from "src/app/shared/components/my-alert-dialog/my-alert-dialog.component";
import { SplitAreaDirective, SplitComponent } from "angular-split";
import { CommonFacadeService } from "src/app/service/admin/common/common-facade.service";
import { MatMenuTrigger } from "@angular/material/menu";
import { ObjKeyArray } from "src/app/shared/constants/objkey-constants";
@Component({
  selector: 'app-thuvien',
  templateUrl: './thuvien.component.html',
  styleUrls: ['./thuvien.component.scss']
})
export class ThuvienComponent implements OnInit {
  // @ts-ignore
  @ViewChild(MatMenuTrigger) contextMenu: MatMenuTrigger;
  ObjKeyArray = ObjKeyArray.ArrayObj;
  mediaIOForm: FormGroup;
  arrayIndex: number[] = [];
  activeTabCheck = 0;
  setSearch = '';
  changeClass = 'overflow-hidden';
  pageNumber: number;
  pageSize: number;
  listData: any;
  listDataItems: OutputGmediaModel[] = [];
  loaiGiaoDien: number;
  maxHeight = '55vh';
  type = '';
  navArray = [
    { title: "Quản trị", url: "/admin" },
    {
      title: "Thư viện",
      url: "/admin/thuvien"
    }
  ];
  @ViewChild('split', { static: false }) split: SplitComponent;
  @ViewChild('area1', { static: false }) area1: SplitAreaDirective;
  @ViewChild('area2', { static: false }) area2: SplitAreaDirective;
  @HostListener('scroll', ['$event'])
  errorX = '';
  errorY = '';
  errorSrid = '';
  errorKieuToaDo = '';
  validationErrorMessages = {
    ten: { required: "Tên điểm không được để trống!" },
    matinh: { required: "Hãy chọn tỉnh!" },
    mahuyen: { required: "Hãy chọn huyện!" },
    luuluongxatrungbinh: { min: "Lưu lượng xả trung bình phải lớn hơn 0", pattern: "Lưu lượng xả trung bình phải là kiểu số" },
    luuluonglonnhatchophep: { min: "Lưu lượng lớn nhất cho phép phải lớn hơn 0", pattern: "Lưu lượng lớn nhất cho phép phải là kiểu số" },
    toadox: { pattern: "Tọa độ x phải là kiểu số" },
    toadoy: { pattern: "Tọa độ y phải là kiểu số" },
    caodoz: { pattern: "Cao độ z phải là kiểu số" },
  };
  inputModel: InputGmediaModel;
  // form errors
  formErrors = {
    toadox: "",
    toadoy: "",
    caodoz: "",
    srid: "",
    vitri: "",
    objKey: "",
    tieude: "",
    idLoaitailieu: "",
    tacgia: "",
    nguoiky: "",
    note: "",
    kieutoado: ""
  };
  buttonArray = [];

  constructor(
    public dialogRef: MatDialogRef<ThuvienComponent>,
    @Inject(MAT_DIALOG_DATA) public dataGetIO: any,
    private formBuilder: FormBuilder,
    public cmFacadeService: CommonFacadeService,
    public validatorToaDoService: ValidatorToaDoService,
    private datePipe: DatePipe,
    public commonService: CommonServiceShared,
    public imDialogService: MatdialogService,
    private modalDialog: MatDialog) { }

  async ngOnInit() {
    await this.bindingConfigValidation();
    this.getPagesize();
    this.getLoaiGiaoDien();
  }

  /**
   * Lấy pageSize trong bảng setting theo mediaPageSize
   */
  async getPagesize() {
    // Get settings
    const pageSize: any = 50;
    this.pageSize = pageSize;
    this.getPageMedia();
  }
  /**
   * Set loại giao diện nào của thư viện
   */
  getLoaiGiaoDien() {
    this.loaiGiaoDien = 0;
    if (this.dataGetIO.model === 'simpleFileV1') {
      this.loaiGiaoDien = 1;
      this.maxHeight = '400px';
      this.type = 'img';
    } else {
      if (this.dataGetIO.model === 'simpleFileV2') {
        this.loaiGiaoDien = 2;
        this.maxHeight = '400px';
      }
    }
  }

  /**
   * Get all phân trang Media
   */
  getPageMedia() {
    this.arrayIndex = [];
    this.pageNumber = 1;
    this.cmFacadeService
      .getGmediaService()
      .getAll({ PageNumber: 1, PageSize: this.pageSize, Type: this.type }).subscribe(data => {
        this.listData = data;
        this.listDataItems = this.listData.items;
        this.setIconShow();
      });
  }
  // config input validation form
  bindingConfigValidation() {
    this.mediaIOForm = this.formBuilder.group({
      objKey: [""],
      tieude: [""],
      idLoaitailieu: [""],
      tacgia: [""],
      nguoiky: [""],
      note: [""]
    });
  }

  setValueToForm(data) {
    this.mediaIOForm.setValue({
      objKey: data.objKey,
      tieude: data.tieude,
      idLoaitailieu: data.idLoaitailieu,
      tacgia: data.tacgia,
      nguoiky: data.nguoiky,
      note: data.note
    });
  }
  /**
   * Thêm File vào  side bar khi select và nếu chỉ chọn 1 file sẽ lưu file đó
   */
  addFiles() {
    this.imDialogService.dataResult = null;
    let arrayFileChecked: any[] = [];
    this.listDataItems.map((value) => {
      if (value.checked === true) {
        arrayFileChecked.push(value);
      }
    });
    this.errorX = '';
    this.errorY = '';
    this.errorSrid = '';
    this.errorKieuToaDo = '';
    // Gọi service validator Tọa độ x, y. srid
    this.validatorToaDoService.validatorToaDo(this.mediaIOForm);
    this.errorX = this.validatorToaDoService.errorX;
    this.errorY = this.validatorToaDoService.errorY;
    this.errorSrid = this.validatorToaDoService.errorSrid;
    if (this.validatorToaDoService.submit === true) {
      this.inputModel = this.mediaIOForm.value;
      this.inputModel.id = arrayFileChecked[0].id;
      this.inputModel.link = arrayFileChecked[0].link;
      console.log("addFiles -> this.loaiGiaoDien", this.loaiGiaoDien)
      this.cmFacadeService
        .getGmediaService()
        .updateItem(this.inputModel.id, this.inputModel)
        .subscribe(
          res => {
            if (this.dataGetIO.model) {
              this.imDialogService.dataResult = arrayFileChecked;
              this.dialogRef.close(`Oke`);
            } else {
              this.getPageMedia();
            }
          },
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () => {
            if (this.loaiGiaoDien === 0) {
              this.commonService.showeNotiResult(
                "Cập nhật file thành công",
                2000
              );
            }
           
          }
        );
    }
  }

  /**
   * function button hủy bỏ
   */
  onNoClick() {
    this.dialogRef.close();
  }

  downloadFile() {
    this.listDataItems.map((data) => {
      if (data.checked === true) {
        window.open(data.link, "_blank");
      }
    });
  }
  /**
   * Delete file hoặc multifile
   */
  deleteFile() {
    let arrayIdFileChecked: number[] = [];
    this.listDataItems.map((data) => {
      if (data.checked === true) {
        arrayIdFileChecked.push(data.id);
      }
    });
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Chắc chắn xóa?";
    if (arrayIdFileChecked.length > 1) {
      dialogRef.componentInstance.content =
        "<b>" + "Bạn có muốn xóa multiple file ?" + "</b>";
    } else {
      dialogRef.componentInstance.content =
        "<b>" + "Bạn có muốn xóa file ?" + "</b>";
    }
    dialogRef.componentInstance.okeButton = "Đồng ý";
    dialogRef.componentInstance.cancelButton = "Hủy bỏ";
    dialogRef.componentInstance.visibleOkButton = true;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.afterClosed().subscribe(async result => {
      if (result === "confirm") {
        await this.cmFacadeService
          .getGmediaService()
          .deleteArrayItem(arrayIdFileChecked).subscribe(
            () => this.getPageMedia(),
            (error: HttpErrorResponse) => {
              this.commonService.showeNotiResult(error.message, 2000);
            },
            () => this.commonService.showeNotiResult("Đã xóa thành công file", 2000)
          );
      }
    });
  }

  /**
   * Validate khi click nút submit
   */
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.mediaIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  /**
   * Reset lại validator tọa độ
   */
  public resetValidator() {
    this.errorX = '';
    this.errorY = '';
    this.errorSrid = '';
  }
  /**
   * display fields css
   * @param field
   */
  public displayFieldCss(field: string) {
    displayFieldCssService(field);
  }

  /**
   * Hàm khi checked 1 file
   * @param checked
   * @param i
   */
  showOptions(checked, i) {
    this.listDataItems[i].checked = checked;
    if (this.loaiGiaoDien === 0) {
      if (this.listDataItems[i].checked === true) {
        this.arrayIndex.push(i);
        this.setValueToForm(this.listDataItems[this.arrayIndex[0]]);
      } else {
        this.arrayIndex.splice(this.arrayIndex.indexOf(i), 1);
        if (this.arrayIndex.length === 0) {
        } else { this.setValueToForm(this.listDataItems[this.arrayIndex[0]]); }
      }
    } else {
      if ((this.loaiGiaoDien === 1) || (this.loaiGiaoDien === 2)) {
        if (this.listDataItems[i].checked === true) {
          this.arrayIndex.push(i);
          this.setValueToForm(this.listDataItems[this.arrayIndex[0]]);
          if (this.arrayIndex.length > 1) {
            this.listDataItems[this.arrayIndex[0]].checked = false;
            this.arrayIndex.splice(0, 1);
            this.setValueToForm(this.listDataItems[this.arrayIndex[0]]);
          }
        } else { this.arrayIndex = []; }
      }
    }
  }

  /**
   * Hàm khi click vào thẻ div body img ảnh file
   * @param i
   */
  clickDivFile(i) {
    this.listDataItems[i].checked = !this.listDataItems[i].checked;
    if (this.loaiGiaoDien === 0) {
      if (this.listDataItems[i].checked === true) {
        this.arrayIndex.push(i);
        this.setValueToForm(this.listDataItems[this.arrayIndex[0]]);
      } else {
        this.arrayIndex.splice(this.arrayIndex.indexOf(i), 1);
        if (this.arrayIndex.length === 0) {
        } else { this.setValueToForm(this.listDataItems[this.arrayIndex[0]]); }
      }
    } else {
      if ((this.loaiGiaoDien === 1) || (this.loaiGiaoDien === 2)) {
        if (this.listDataItems[i].checked === true) {
          this.arrayIndex.push(i);
          this.setValueToForm(this.listDataItems[this.arrayIndex[0]]);
          if (this.arrayIndex.length > 1) {
            this.listDataItems[this.arrayIndex[0]].checked = false;
            this.arrayIndex.splice(0, 1);
            this.setValueToForm(this.listDataItems[this.arrayIndex[0]]);
          }
        } else { this.arrayIndex = []; }
      }
    }
  }
  /**
   * Hàm lăn thanh cuộn đến cuối
   * @param event
   */
  async onScroll(event: any) {
    if ((event.target.offsetHeight + event.target.scrollTop + 2) >= event.target.scrollHeight) {
      this.pageNumber++;
      if (!this.setSearch === true) {
        this.cmFacadeService
          .getGmediaService()
          .getAll({ PageNumber: this.pageNumber, PageSize: this.pageSize, Type: this.type }).subscribe(data => {
            this.listData = data;
            this.listDataItems.push.apply(this.listDataItems, this.listData.items);
            this.setIconShow();
          });
      } else {
        this.cmFacadeService
          .getGmediaService()
          .searchItem({ Tieude: this.setSearch, PageNumber: this.pageNumber, PageSize: this.pageSize, Type: this.type }).subscribe(data => {
            this.listData = data;
            this.listDataItems.push.apply(this.listDataItems, this.listData.items);
            this.setIconShow();
          });
      }
    }
  }

  /**
   * Sự kiện thay đổi tab
   * @param event
   */
  selectTabChange(event) {
    this.activeTabCheck = 0;
    if (event.index === 1) {
      this.activeTabCheck = 1;
    }
  }

  /**
   * Sự kiện lấy dữ liệu từ tab upload chuyền sang
   * @param event
   */
  getDataUpload(event) {
    this.activeTabCheck = 0;
    this.arrayIndex = [];
    this.pageNumber = 1;
    for (let i = 0; i < event.length; i++) {
      this.arrayIndex.push(i);
    }
    // Load lại data và select file vừa upload
    this.cmFacadeService
      .getGmediaService()
      .getAll({ PageNumber: 1, PageSize: this.pageSize, Type: this.type }).subscribe(data => {
        this.listData = data;
        this.listDataItems = this.listData.items;
        this.setValueToForm(this.listDataItems[this.arrayIndex[0]]);
        for (let i = 0; i < event.length; i++) {
          this.listDataItems[i].checked = true;
        }
        this.setIconShow();
      });
  }

  /**
   * Hàm search enter key
   * @param data
   */
  searchEnter(data) {
    if (data) {
      this.setSearch = data;
      this.arrayIndex = [];
      this.cmFacadeService
        .getGmediaService()
        .searchItem({ Tieude: data, PageNumber: 1, PageSize: this.pageSize, Type: this.type }).subscribe(obj => {
          this.listData = obj;
          this.listDataItems = this.listData.items;
          this.setIconShow();
        });
    } else { this.getPageMedia(); this.setSearch = ''; }
  }

  /**
   * show icon docx, xlsx, pdf,...
   */
  setIconShow() {
    this.listDataItems.map((value, index) => {
      if (value.type === '.docx') {
        this.listDataItems[index].iconFile = "fas fa-file-word";
      }
      if (value.type === '.pdf') {
        this.listDataItems[index].iconFile = "fas fa-file-pdf";
      }
      if (value.type === '.xls') {
        this.listDataItems[index].iconFile = "fas fa-file-excel";
      }
      if (value.type === '.pptx') {
        this.listDataItems[index].iconFile = "fas fa-file-powerpoint";
      }
    });
  }
}
