import { Component, OnInit } from "@angular/core";
import { AdminRoutingName } from "src/app/routes/routes-name";

@Component({
  selector: "app-sidenav",
  templateUrl: "./sidenav.component.html",
  styleUrls: ["./sidenav.component.scss"],
})
export class SidenavComponent implements OnInit {
  QuantriList = [
    {
      label: "DANH MỤC",
      link: "/admin/" + AdminRoutingName.danhmucUri,
      faIcon: "far fa-clipboard",
      items: [
        {
          label: "Cá nhân",
          link: AdminRoutingName.canhanUri,
          faIcon: "far fa-user",
        },
        { label: "Công ty", 
          link: AdminRoutingName.congtyUri, 
          faIcon: "far fa-building" 
        },
        {
          label: "Cơ quan/Tổ chức",
          link: AdminRoutingName.coquanUri,
          faIcon: "far fa-building",
        },
        {
          label: "Đơn vị hành chính",
          link: AdminRoutingName.dvhcUri,
          faIcon: "far fa-map-marked-alt",
        },
        {
          label: "Đơn vị đo",
          link: AdminRoutingName.donvidoUri,
          faIcon: "far fa-sort-amount-up-alt",
        },
        { label: "Tiêu chuẩn", 
          link: AdminRoutingName.tieuchuanUri, 
          faIcon: "far fa-books" 
        },
        {
          label: "Nhóm tham số",
          link: AdminRoutingName.nhomthamsoUri,
          faIcon: "far fa-object-group",
        },
        { label: "Tham số", 
          link: AdminRoutingName.thamsoUri, 
          faIcon: "fab fa-elementor" 
        },
        {
          label: "Tiêu chuẩn chất lượng",
          link: AdminRoutingName.tcclUri,
          faIcon: "fab fa-elementor",
        },
        {
          label: "Dự án",
          link: AdminRoutingName.duanUri,
          faIcon: "fab fa-elementor",
        },
        {
          label: "Loại số liệu",
          link: AdminRoutingName.loaisolieuUri,
          faIcon: "fab fa-elementor",
        },
      ],
    },
    {
      label: "TÀI NGUYÊN NƯỚC MẶT",
      faIcon: "far fa-dewpoint",
      link: "/admin/" + AdminRoutingName.wrUri,
      items: [
        {
          label: "Khai thác nước mặt",
          faIcon: "far fa-monitor-heart-rate",
          link: AdminRoutingName.swexUri,
          items: [
            {
              label: "Công trình khai thác nước mặt",
              link: AdminRoutingName.swDiemkhaithac,
              faIcon: "far fa-building",
            },
          ],
        },
        {
          label: "Trạm bơm",
          faIcon: "far fa-monitor-heart-rate",
          link: AdminRoutingName.swTramBomUri,
          items: [
            {
              label: "Công trình trạm bơm",
              link: AdminRoutingName.swTrambom,
              faIcon: "far fa-building",
            },
          ],
        },
        {
          label: "Xả thải vào nguồn nước",
          faIcon: "far fa-monitor-heart-rate",
          link: AdminRoutingName.diUri,
          items: [
            {
              label: "Điểm xả thải",
              faIcon: "far fa-monitor-heart-rate",
              link: AdminRoutingName.pdiUri,
            },
          ],
        },
      ],
    },
    {
      label: "THƯ VIỆN",
      faIcon: "fas fa-photo-video",
      link: "/admin/" + AdminRoutingName.thuvienUri,
    },
    {
      label: "BÁO CÁO",
      faIcon: "fal fa-file-chart-pie",
      link: "/admin/" + AdminRoutingName.baocaoUri,
      items: [],
    },
    {
      label: "HỆ THỐNG",
      faIcon: "fab fa-accusoft",
      link: "/admin/" + AdminRoutingName.hethongUri,
      items: [
        {
          label: "Quản lý tài khoản",
          link: AdminRoutingName.quanlyUser,
          faIcon: "fas fa-users",
        },
        {
          label: "Quản lý quyền",
          link: AdminRoutingName.quanlyRole,
          faIcon: "far fa-address-book",
        },
      ],
    },
  ];

  constructor() {}

  ngOnInit() {}
}
