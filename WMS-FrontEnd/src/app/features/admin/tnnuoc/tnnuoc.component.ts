import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import { TnnuocFacadeService } from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
import { AdminRoutingName } from "src/app/routes/routes-name";
import { MapService } from "src/app/core/services/utilities/map.service";

@Component({
  selector: "app-tnnuoc",
  templateUrl: "./tnnuoc.component.html",
  styleUrls: ["./tnnuoc.component.scss"],
})
export class TnnuocComponent implements OnInit {
  listData: any;
  countDiemXT: number;
  listDataTramBom: any;
  countTramBom: number;
  listDataDiemKhaiThac: any;
  countDiemKhaiThacNM: number;

  navArray = [
    { title: "Quản trị", url: "/" },
    {
      title: "Quan trắc tài nguyên nước mặt",
      url: "/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri,
    },
  ];

  buttonArray = [];

  constructor(
    private subHeaderService: SubHeaderService,
    private router: Router,
    private tnnuocFacadeService: TnnuocFacadeService,
    public mapService: MapService
  ) {}

  ngOnInit() {
    this.subHeaderService.setParentComp(this);
    this.dataSubHeader();
    this.mapInit();
    this.getAllDiemXaThai();
    this.getAllKhaiThacNuocMat();
    this.getAllTramBom();
  }

  openXaNuocThai() {
    this.router.navigateByUrl(
      "/" +
        AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.diUri
    );
  }

  openTramBom() {
    this.router.navigateByUrl(
      "/" +
        AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.swTramBomUri
    );
  }

  // Chuyển hướng sang trang ds điểm khai thác nước mặt
  openDiemKhaiThacNuocMat() {
    this.router.navigateByUrl(
      "/" +
        AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.swexUri
    );
  }

  // Khởi tạo bản đồ
  mapInit() {
    this.mapService.mapInit("viewMap");
  }

  // Gán dữ liệu vào sub-header service
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }

  // Lấy về tất cả trạm bơm
  async getAllTramBom() {
    this.listDataTramBom = await this.tnnuocFacadeService
      .getSwTrambomService()
      .getFetchAll({ PageNumber: 1, PageSize: -1 });
    this.countTramBom = this.listDataTramBom.paging.totalItems;
  }

  // Lấy về tất cả điểm khai thác
  async getAllKhaiThacNuocMat() {
    this.listDataDiemKhaiThac = await this.tnnuocFacadeService
      .getSwKhaithacnuocmatService()
      .getFetchAll({ PageNumber: 1, PageSize: -1 });
    this.countDiemKhaiThacNM = this.listDataDiemKhaiThac.paging.totalItems;
  }

  async getAllDiemXaThai() {
    this.listData = await this.tnnuocFacadeService
      .getSwXaNuocThaiService()
      .getFetchAll({ PageNumber: 1, PageSize: -1 });
    this.countDiemXT = this.listData.paging.totalItems;
  }
}
