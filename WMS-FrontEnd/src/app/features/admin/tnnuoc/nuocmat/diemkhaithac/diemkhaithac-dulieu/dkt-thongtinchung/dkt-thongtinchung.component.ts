import {
  Component,
  ComponentFactoryResolver,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {MatSidenav} from "@angular/material/sidenav";
import {MatsidenavService} from "src/app/core/services/utilities/matsidenav.service";
import {TnnuocFacadeService} from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
import {ActivatedRoute} from "@angular/router";
import {DiemkhaithacIoComponent} from "src/app/features/admin/tnnuoc/nuocmat/diemkhaithac/diemkhaithac-io/diemkhaithac-io.component";
import {DmFacadeService} from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import {VanHanh} from "src/app/shared/constants/vanhanh-constants";
import {STATUS} from "src/app/shared/constants/status-constants";
import { Srid } from 'src/app/shared/constants/srid-constant';
import {CommonFacadeService} from "src/app/service/admin/common/common-facade.service";
import {MapService} from "src/app/core/services/utilities/map.service";

@Component({
  selector: 'app-dkt-thongtinchung',
  templateUrl: './dkt-thongtinchung.component.html',
  styleUrls: ['./dkt-thongtinchung.component.scss']
})
export class DktThongtinchungComponent implements OnInit {

  public obj: any = {};
  public vanHanh = VanHanh;
  public status = STATUS;
  @Output("getInformation") getInformation: EventEmitter<any> = new EventEmitter();
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compDiemKhaiThacNMIo", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  public dvqlData: any;
  public caNhanData: any;
  public dvql: any;
  public caNhan: any;
  public vanhanh: any;
  public trangThai: any;
  public hetoado: any;
  public dataTinh: any;
  public dataHuyen: any;
  public dataXa: any;
  public tinh: any;
  public huyen: any;
  public xa: any;

  constructor(
    public matsidenavService: MatsidenavService,
    public cfr: ComponentFactoryResolver,
    public wrFacadeService: TnnuocFacadeService,
    private route: ActivatedRoute,
    public dmFacadeService: DmFacadeService,
    public mapService: MapService,
    public commonFacadeService: CommonFacadeService
  ) {}

  ngOnInit() {
    this.getDiemKhaiThacNuocMat();
  }

  async showMap() {
    await this.mapInit();
    await this.zoomToObj();
  }

  /**
   * Hàm get 1 item bằng id trên url
   */
  async getDiemKhaiThacNuocMat() {
    this.getInformation.emit();
    const idurl = this.route.snapshot.paramMap.get("id");
    const id = +idurl;
    await this.wrFacadeService
      .getSwKhaithacnuocmatService()
      .getByid(id)
      .subscribe(diemkt => {
        this.obj = diemkt;
        this.showMap();
        this.showDVQL(this.obj.idDonviquanly);
        this.showCaNhan(this.obj.idChusudung);
        this.showTenVanHanh(this.obj.idVanhanh);
        this.showHeToaDo(this.obj.srid);
        this.showTinh(this.obj.matinh);
        this.showHuyen(this.obj.matinh, this.obj.mahuyen);
        this.showXa(this.obj.mahuyen, this.obj.maxa);
      });
    console.log(this.obj)
  }


  /**
   * hàm chuyển đến chế độ sửa
   */
  toEditMode() {
    this.matsidenavService.setSidenav(this.matSidenav, this, this.content, this.cfr);
    this.matsidenavService.setTitle("Sửa thông tin điểm khai thác nước mặt");
    this.matsidenavService.setContentComp(DiemkhaithacIoComponent, "edit", this.obj);
    this.matsidenavService.open();
  }

  /**
   * Hàm close SideNav Điểm khai thác nước mặt
   */
  public closeDiemKhaiThacIOSidebar() {
    this.matSidenav.close();
  }
  // Khởi tạo bản đồ
  async mapInit() {
    this.mapService.mapInit('mapViewDiv');
  }

  /**@function zoomToObj
   * Hàm zoom tới đối tượng trên bản đồ, bật popup tên công trình
   */
  public zoomToObj() {
    console.log(this.obj);
    this.mapService.zoomToPoint(this.obj, this.obj.tendiem, this.obj.vitri);
  }

  /**
   * Hàm get tên Đơn vị quản lý bằng id
   * @param id
   */
  async showDVQL(id: number) {
    if (id) {
      this.dvqlData = await this.dmFacadeService
        .getDmCoquantochucService()
        .getByid(id).subscribe(res => {
          res ? this.dvql = res.tencoquan : this.dvql = '';
        });
    }
  }

  /**
   * Hàm get tên Cá nhân theo id
   * @param id
   */
  async showCaNhan(id: number) {
    if (id) {
      this.caNhanData = await this.dmFacadeService
        .getDmCanhanService()
        .getByid(id).subscribe(res => {
          res ? this.caNhan = res.hovaten : this.caNhan = '';
        });
    }
  }


  async showTinh(id: number) {
    if (id) {
      this.dataTinh = await this.dmFacadeService
        .getProvinceService()
        .getFetchAll();
      if (this.dataTinh) {
        this.dataTinh.map(res => {
          if (res.matinh === id) {
            this.tinh = res.tendvhc;
          }
        });
      }
    }
  }

  async showHuyen(idtinh: number, idhuyen: number) {
    if (idtinh) {
      this.dataHuyen = await this.dmFacadeService
        .getDistrictService()
        .getByid(idtinh).subscribe( res => {
          if (res) {
            res.map(res1 => {
              if (res1.mahuyen === idhuyen) {
                this.huyen = res1.tendvhc;
              }
            });
        }
      });
    }
  }

  async showXa(idhuyen: number, idxa: number) {
    if (idxa) {
      await this.dmFacadeService
        .getWardService()
        .getByid(idhuyen).subscribe( res => {
          if (res) {
            res.map(res1 => {
              if (res1.maxa === idxa) {
                this.xa = res1.tendvhc;
              }
            });
          }
        });
    }
  }

  /**
   * Hàm get tên Vận hành bằng id
   * @param id
   */
  showTenVanHanh(id: number) {
    this.vanHanh.forEach(v => {
      if(v.id == id){
        this.vanhanh = v.ten;
      }
    });
  }

  /**
   * Hàm get tên he toa do bằng id
   * @param id
   */
  showHeToaDo(id: number) {
    Srid.forEach(v => {
      if(v.id == id){
        this.hetoado = v.ten;
      }
    });
  }

  /**@function showTrangThai
   * Hàm show trạng thái của điểm khai thác theo id truyền vào
   */
  showTrangThai(id: number) {
    for (const s of this.status) {
      if (s.id === id) {
        this.trangThai = s.name;
      }
      this.trangThai = "";
    }
  }

  /**
   * Call method
   * @param methodName
   */
  doFunction(methodName) {
    this[methodName]();
  }

}
