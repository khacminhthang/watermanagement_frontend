import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiemkhaithacComponent } from './diemkhaithac.component';

describe('DiemkhaithacComponent', () => {
  let component: DiemkhaithacComponent;
  let fixture: ComponentFixture<DiemkhaithacComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiemkhaithacComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiemkhaithacComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
