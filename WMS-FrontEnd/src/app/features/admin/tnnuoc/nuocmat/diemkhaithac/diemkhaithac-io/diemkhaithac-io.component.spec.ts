import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiemkhaithacIoComponent } from './diemkhaithac-io.component';

describe('DiemkhaithacIoComponent', () => {
  let component: DiemkhaithacIoComponent;
  let fixture: ComponentFixture<DiemkhaithacIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiemkhaithacIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiemkhaithacIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
