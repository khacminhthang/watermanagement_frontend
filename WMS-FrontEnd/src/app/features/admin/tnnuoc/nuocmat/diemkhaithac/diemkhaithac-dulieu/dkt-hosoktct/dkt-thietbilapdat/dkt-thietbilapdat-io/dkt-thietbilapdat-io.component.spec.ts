import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DktThietbilapdatIoComponent } from './dkt-thietbilapdat-io.component';

describe('DktThietbilapdatIoComponent', () => {
  let component: DktThietbilapdatIoComponent;
  let fixture: ComponentFixture<DktThietbilapdatIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DktThietbilapdatIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DktThietbilapdatIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
