import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiemkhaithacListComponent } from './diemkhaithac-list.component';

describe('DiemkhaithacListComponent', () => {
  let component: DiemkhaithacListComponent;
  let fixture: ComponentFixture<DiemkhaithacListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiemkhaithacListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiemkhaithacListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
