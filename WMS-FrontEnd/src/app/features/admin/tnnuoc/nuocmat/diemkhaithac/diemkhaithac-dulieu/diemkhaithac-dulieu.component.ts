import { Component, OnInit } from '@angular/core';
import { ObjKey } from "src/app/shared/constants/objkey-constants";
import { AdminRoutingName } from "src/app/routes/routes-name";
import { ActivatedRoute, Router } from "@angular/router";
import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import { TnnuocFacadeService } from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";


@Component({
  selector: 'app-diemkhaithac-dulieu',
  templateUrl: './diemkhaithac-dulieu.component.html',
  styleUrls: ['./diemkhaithac-dulieu.component.scss']
})
export class DiemkhaithacDulieuComponent implements OnInit {

  public objKey = ObjKey.KhaiThacNuocMat;

  activeTab: number;
  public objDiemKt: any;
  public navArray = [
    { title: "Quản trị", url: "/" },
    {
      title: "Quan trắc tài nguyên nước",
      url: "/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri
    },
    {
      title: "Khai thác nước mặt",
      url:
        "/" +
        AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.swexUri
    },
    {
      title: "Công trình khai thác nước mặt ",
      url:
        "/" +
        AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.swexUri +
        "/" +
        AdminRoutingName.swDiemkhaithac
    },
    {
      title: "Chi tiết",
      url: ""
    }
  ];
  public buttonArray = [
    {
      title: "Công trình khai thác nước mặt",
      icon: "fad fa-chevron-double-left",
      color: "btn-primary",
      url: "/" +
        AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.swexUri +
        "/" +
        AdminRoutingName.swDiemkhaithac
    }
  ];

  constructor(
    public router: Router,
    private subHeaderService: SubHeaderService,
    public wrFacadeService: TnnuocFacadeService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.subHeaderService.setParentComp(this);
    this.dataSubHeader();
    this.getInformation();
    this.activeTabs();
  }

  /**
   * Lấy data tham số active để set active tab
   */
  activeTabs() {
    const active = this.route.snapshot.paramMap.get('active');
    this.activeTab = +active;
  }

  /**
   * hàm lấy thông tin 1 item Điểm khai thác nước mặt bằng id lấy từ url
   */
  async getInformation() {
    const idurl = this.route.snapshot.paramMap.get('id');
    const id = +idurl;
    await this.wrFacadeService
      .getSwKhaithacnuocmatService()
      .getByid(id)
      .subscribe(diem => {
        this.objDiemKt = diem;
      });
  }

  /**
   * Hàm truyền data vào SubHeader
   */
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }

}
