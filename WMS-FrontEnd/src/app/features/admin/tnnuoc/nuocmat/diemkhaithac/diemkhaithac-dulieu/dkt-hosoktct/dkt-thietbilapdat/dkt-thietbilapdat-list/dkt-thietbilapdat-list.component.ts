import { Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { MatSidenav } from "@angular/material/sidenav";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { TnnuocFacadeService } from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
import { ActivatedRoute } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import { QueryCellInfoEventArgs } from "@syncfusion/ej2-grids";
import { MyAlertDialogComponent } from "src/app/shared/components/my-alert-dialog/my-alert-dialog.component";
import { OutputMayBomTramBom } from "src/app/models/admin/tnnuoc/maybomtrambom.model";
import { DktThietbilapdatIoComponent } from "src/app/features/admin/tnnuoc/nuocmat/diemkhaithac/diemkhaithac-dulieu/dkt-hosoktct/dkt-thietbilapdat/dkt-thietbilapdat-io/dkt-thietbilapdat-io.component";

@Component({
  selector: 'app-dkt-thietbilapdat-list',
  templateUrl: './dkt-thietbilapdat-list.component.html',
  styleUrls: ['./dkt-thietbilapdat-list.component.scss']
})
export class DktThietbilapdatListComponent implements OnInit {
  settingsCommon = new SettingsCommon();
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compThietBiLapDatio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  listData: any;
  listThietBiLapDat: OutputMayBomTramBom[];
  selectedItem: OutputMayBomTramBom;
  constructor(
    public matSidenavService: MatsidenavService,
    public cfr: ComponentFactoryResolver,
    public commonService: CommonServiceShared,
    private swFacadeService: TnnuocFacadeService,
    private route: ActivatedRoute,
    private modalDialog: MatDialog
  ) { }

  ngOnInit() {
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 10 };
    this.getAllMayBomDiemKhaiThac();
  }

  // get all data may bom diem khai thac
  async getAllMayBomDiemKhaiThac() {
    const idUrl = this.route.snapshot.paramMap.get("id");
    const id = +idUrl;
    this.listData = await this.swFacadeService
      .getSwMayBomDiemKhaiThacService()
      .getListMayBomDiemKhaiThac(id);
    if (this.listData) {
      this.listData.map((maybom, index) => {
        maybom.sNo = index + 1;
      });
    }
    this.listThietBiLapDat = this.listData;
  }

  // open sidebar execute insert
  public openThietBiLapDatIOSidebar() {
    this.matSidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.matSidenavService.setTitle("Thêm mới dữ máy bơm");
    this.matSidenavService.setContentComp(DktThietbilapdatIoComponent, "new");
    this.matSidenavService.open();
  }

  // close sidebar
  public closeThietBiLapDatIOSidebar() {
    this.matSidenav.close();
  }

  // Hàm xóa một bản ghi, được gọi khi nhấn nút xóa trên giao diện list
  async deleteThietBiLapDat(data) {
    const canDelete: string = this.swFacadeService
      .getSwMayBomDiemKhaiThacService()
      .checkBeDeleted(data.id);
    this.canBeDeletedCheck(canDelete, data);
  }

  // check deleted
  public canBeDeletedCheck(sMsg: string, data) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog(data);
    } else {
      this.cantDeleteDialog(sMsg);
    }
  }

  // dialog thông báo có muốn xóa bản ghi không và hàm deleted
  confirmDeleteDiaLog(data) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Chắc chắn xóa?";
    dialogRef.componentInstance.content =
      "Loại máy bơm: <b>" + data.loaimaybom + "</b>";
    dialogRef.componentInstance.okeButton = "Đồng ý";
    dialogRef.componentInstance.cancelButton = "Hủy bỏ";
    dialogRef.componentInstance.visibleOkButton = true;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.afterClosed().subscribe(async result => {
      if (result === "confirm") {
        await this.swFacadeService
          .getSwMayBomDiemKhaiThacService()
          .deleteItem(data.id)
          .subscribe(res => this.getAllMayBomDiemKhaiThac());
      }
    });
  }

  // dialog thông báo không thể xóa
  cantDeleteDialog(sMsg: string) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Không thể xóa!";
    dialogRef.componentInstance.content = "<b>" + sMsg + "</b>";
    dialogRef.componentInstance.cancelButton = "Đóng";
    dialogRef.componentInstance.visibleOkButton = false;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.componentInstance.headerColor = "red";
  }

  // Hàm sửa thông tin chi tiết một bản ghi, được gọi khi nhấn nút xem chi tiết trên giao diện list
  public editItemThietBiLapDat(data) {
    this.matSidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.matSidenavService.setTitle("Sửa dữ liệu máy bơm");
    this.matSidenavService.setContentComp(
      DktThietbilapdatIoComponent,
      "edit",
      data
    );
    this.matSidenavService.open();
  }

  // Convert string thành dd/mm/yyyy
  public convertDateTimeString(data) {
    const [date, time]: string[] = data.thoigianlapdat.split(' ');
    const [year, month, day]: string[] = date.split('-');
    if (year.length === 4) {
      const awesomeDateTime = `${day}-${month}-${year}`;
      return awesomeDateTime;
    } else {
      const awesomeDateTime1 = `${year}-${month}-${day}`;
      return awesomeDateTime1;
    }
  }
  customiseCell(args: QueryCellInfoEventArgs) {
    if (args.column.field === "id") {
      args.cell.id = args.data[args.column.field];
    }
  }
  // Hàm dùng để gọi các hàm khác, truyền vào tên hàm cần thực thi
  doFunction(methodName) {
    this[methodName]();
  }
}
