import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {OutputDvhcModel} from "src/app/models/admin/danhmuc/dvhc.model";
import {OutputCoquantochucModel} from "src/app/models/admin/danhmuc/coquantochuc.model";
import {MatsidenavService} from "src/app/core/services/utilities/matsidenav.service";
import {ProgressService} from "src/app/core/services/utilities/progress.service";
import {CommonServiceShared} from "src/app/core/services/utilities/common-service";
import {DmFacadeService} from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import {TnnuocFacadeService} from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {ImageuploadService} from "src/app/service/admin/common/imageupload.service";
import {DatePipe} from "@angular/common";
import {Router} from "@angular/router";
import {
  displayFieldCssService,
  validationAllErrorMessagesService
} from "src/app/core/services/utilities/validatorService";
import {InputKhaithacnuocmatModel} from "src/app/models/admin/tnnuoc/khaithacnuocmat.model";
import {OutputCanhanModel} from "src/app/models/admin/danhmuc/canhan.model";
import {VanHanh} from "src/app/shared/constants/vanhanh-constants";
import {STATUS} from "src/app/shared/constants/status-constants";
import {CommonFacadeService} from "src/app/service/admin/common/common-facade.service";
import {ThuvienComponent} from "src/app/features/admin/thuvien/thuvien.component";
import {MatDialog} from "@angular/material";
import {MatdialogService} from "src/app/core/services/utilities/matdialog.service";
import {ObjKey} from "src/app/shared/constants/objkey-constants";

@Component({
  selector: 'app-diemkhaithac-io',
  templateUrl: './diemkhaithac-io.component.html',
  styleUrls: ['./diemkhaithac-io.component.scss']
})
export class DiemkhaithacIoComponent implements OnInit {

  diemKhaiThacIOForm: FormGroup;
  IOCompact: boolean;
  public vanHanh = VanHanh;
  public status = STATUS;
  public inputModel: InputKhaithacnuocmatModel;
  public editMode: boolean;
  public purpose: string;
  public obj: any;
  public allTinh: any;
  public allHuyen: any;
  public allXa: any;
  public dvql: any;
  public caNhan: any;
  public allTinhData: any;
  public allDvqlData: any;
  public allCaNhanData: any;
  public projection: any;
  public projectionFilters = [];
  public canhanFilters: OutputCanhanModel[];
  public dvhcProvinceFilters: OutputDvhcModel[];
  public dvhcDistrictFilters: OutputDvhcModel[];
  public dvhcWardFilters: OutputDvhcModel[];
  public dvqlFilters: OutputCoquantochucModel[];

  // Những biến dành cho phần file
  mDialog: any;
  srcanh = '';
  public fileArray: any;
  errorToadox = '';
  errorToadoy = '';
  errorSrid = '';
  // Error message
  validationErrorMessages = {
    tendiem: { required: "Tên điểm khai thác không được để trống!" },
    matinh: { required: "Hãy chọn tỉnh!" },
    mahuyen: { required: "Hãy chọn huyện!" },
    luuluongchopheplonnhat: { min: "Lưu lượng cho phép lớn nhất phải lớn hơn 0", pattern: "Lưu lượng cho phép lớn nhất phải là kiểu số" },
    toadox: {pattern: "Tọa độ x phải là kiểu số"},
    toadoy: {pattern: "Tọa độ y phải là kiểu số"},
    caodoz: {pattern: "Cao độ z phải là kiểu số"},
  };

  // Form errors
  formErrors = {
    tendiem: "",
    matinh: "",
    mahuyen: "",
    luuluongchopheplonnhat: "",
    toadox: "",
    toadoy: "",
    caodoz: ""
  };


  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    public dmFacadeService: DmFacadeService,
    public wrFacadeService: TnnuocFacadeService,
    private http: HttpClient,
    private imgUpload: ImageuploadService,
    private datePipe: DatePipe,
    public router: Router,
    public commonFacadeService: CommonFacadeService,
    private imDialog: MatDialog, imDialogService: MatdialogService
  ) {
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
  }

  ngOnInit() {
    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
    this.showDVQL();
    this.showCaNhan();
    // this.showObjKey();
  }

  /**
   * Hàm khởi tạo Form
   */
  bindingConfigValidation() {
    this.diemKhaiThacIOForm = this.formBuilder.group({
      sohieudiem: [""],
      tendiem: ["", Validators.required],
      toadox: ["", Validators.pattern(/^[-+]?\d*\.?\d*$/)],
      toadoy: ["", Validators.pattern(/^[-+]?\d*\.?\d*$/)],
      caodoz: ["", Validators.pattern(/^[-+]?\d*\.?\d*$/)],
      srid: [""],
      vitri: [""],
      matinh: ["", Validators.required],
      mahuyen: ["", Validators.required],
      maxa: [""],
      thoigianxaydung: [""],
      thoigianvanhanh: [""],
      idDonviquanly: [""],
      idChusudung: [""],
      // idBody: [""],
      // bodyKey: [""],
      idMucdichsudungchinh: [""],
      luuluongchopheplonnhat: ["", [Validators.min(0), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      cothietbigiamsat: [""],
      idVanhanh: [""],
      thoiluongkhaithac: [""],
      nhamay: [""],
      dacapphep: [""],
      note: [""],
      imgLink: [""],
      // status: [""]
    });
  }

  /**
   * Hàm cấu hình form sử dụng Add or Update
   */
  bindingConfigAddOrUpdate() {
    this.showDvhcTinh();
    this.IOCompact = true;
    this.inputModel = new InputKhaithacnuocmatModel();
    if ( this.purpose === "edit") {
      this.IOCompact = false;
    }
    if (this.purpose === "edit" && this.obj) {
      this.diemKhaiThacIOForm.setValue({
        sohieudiem: this.obj.sohieudiem,
        tendiem: this.obj.tendiem,
        toadox: this.obj.toadox,
        toadoy: this.obj.toadoy,
        caodoz: this.obj.caodoz,
        srid: this.obj.srid,
        vitri: this.obj.vitri,
        matinh: this.obj.matinh,
        mahuyen: this.obj.mahuyen,
        maxa: this.obj.maxa,
        thoigianxaydung: this.obj.thoigianxaydung,
        thoigianvanhanh: this.obj.thoigianvanhanh,
        // idBody: this.obj.idBody,
        // bodyKey: this.obj.bodyKey,
        idDonviquanly: this.obj.idDonviquanly,
        idChusudung: this.obj.idChusudung,
        idMucdichsudungchinh: this.obj.idMucdichsudungchinh,
        luuluongchopheplonnhat: this.obj.luuluongchopheplonnhat,
        cothietbigiamsat: this.obj.cothietbigiamsat,
        idVanhanh: this.obj.idVanhanh,
        thoiluongkhaithac: this.obj.thoiluongkhaithac,
        nhamay: this.obj.nhamay,
        dacapphep: this.obj.dacapphep,
        note: this.obj.note,
        imgLink: this.obj.imgLink,
        // status: this.obj.status
      });
      this.srcanh = this.obj.imgLink;
      this.showDvhcHuyen();
      this.showDvhcXa();
      // this.showIdBody();
    }
    this.editMode = true;
  }


  /**
   * on Submit
   * @param operMode
   */
  public onSubmit(operMode: string) {
    this.errorToadox = "";
    this.errorToadoy = "";
    this.errorSrid = "";
    if (this.diemKhaiThacIOForm.valid === false) { this.logAllValidationErrorMessages(); }
    if ((!this.diemKhaiThacIOForm.value.toadox === true) &&
      (!this.diemKhaiThacIOForm.value.toadoy === true) &&
      (!this.diemKhaiThacIOForm.value.srid === true) ) {
      if (this.diemKhaiThacIOForm.valid === true) {
        this.addOrUpdate(operMode);
      }
    } else {
      if ( (!this.diemKhaiThacIOForm.value.toadox === true) ||
        (!this.diemKhaiThacIOForm.value.toadoy === true) ||
        (!this.diemKhaiThacIOForm.value.srid === true)) {
        (!this.diemKhaiThacIOForm.value.toadox === true) ? this.errorToadox = "Bạn phải nhập tọa độ x" : this.errorToadox = "";
        (!this.diemKhaiThacIOForm.value.toadoy === true) ? this.errorToadoy = "Bạn phải nhập tọa độ y" : this.errorToadoy = "";
        (!this.diemKhaiThacIOForm.value.srid === true) ? this.errorSrid = "Bạn phải chọn srid" : this.errorSrid = "";
      } else {
        if (this.diemKhaiThacIOForm.valid === true) {
          this.addOrUpdate(operMode);
        }
      }
    }
  }

  /**
   * Hàm Add or Update và reset form
   * @param operMode
   */
  public addOrUpdateContinue(operMode: string) {
    this.errorToadox = "";
    this.errorToadoy = "";
    this.errorSrid = "";
    if (this.diemKhaiThacIOForm.valid === false) { this.logAllValidationErrorMessages(); }
    if ((!this.diemKhaiThacIOForm.value.toadox === true) &&
      (!this.diemKhaiThacIOForm.value.toadoy === true) &&
      (!this.diemKhaiThacIOForm.value.srid === true) ) {
      if (this.diemKhaiThacIOForm.valid === true) {
        this.addOrUpdate(operMode);
        this.onFormReset();
        this.purpose = "new";
      }
    } else {
      if ( (!this.diemKhaiThacIOForm.value.toadox === true) ||
        (!this.diemKhaiThacIOForm.value.toadoy === true) ||
        (!this.diemKhaiThacIOForm.value.srid === true)) {
        (!this.diemKhaiThacIOForm.value.toadox === true) ? this.errorToadox = "Bạn phải nhập tọa độ x" : this.errorToadox = "";
        (!this.diemKhaiThacIOForm.value.toadoy === true) ? this.errorToadoy = "Bạn phải nhập tọa độ y" : this.errorToadoy = "";
        (!this.diemKhaiThacIOForm.value.srid === true) ? this.errorSrid = "Bạn phải chọn srid" : this.errorSrid = "";
      } else {
        if (this.diemKhaiThacIOForm.valid === true) {
          this.addOrUpdate(operMode);
          this.onFormReset();
          this.purpose = "new";
        }
      }
    }
  }

  /**
   * Hàm thực hiện Add or Update
   * @param operMode
   */
  private addOrUpdate(operMode: string) {
    this.inputModel = this.diemKhaiThacIOForm.value;
    this.inputModel.objKey = ObjKey.KhaiThacNuocMat;
    this.inputModel.thoigianvanhanh = this.datePipe.transform(this.diemKhaiThacIOForm.value.thoigianvanhanh, "yyyy-MM-dd");
    this.inputModel.thoigianxaydung = this.datePipe.transform(this.diemKhaiThacIOForm.value.thoigianxaydung, "yyyy-MM-dd");
    this.inputModel.imgLink = this.srcanh;
    if (operMode === "new") {
      this.wrFacadeService
        .getSwKhaithacnuocmatService()
        .addItem(this.inputModel)
        .subscribe(
          res =>  this.matSidenavService.doParentFunction("getDiemKhaiThacNuocMat"),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Thêm mới điểm khai thác nước mặt thành công!",
              2000
            )
        );
    } else if (operMode === "edit") {
      const id: number = this.obj.id;
      this.wrFacadeService
        .getSwKhaithacnuocmatService()
        .updateItem(id, this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("getDiemKhaiThacNuocMat"),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Cập nhật điểm khai thác nước mặt thành công",
              2000
            )
        );
    }
    // Close
    this.matSidenavService.close();
  }

  /**
   * Hàm get Đơn vị quản lý
   */
  async showDVQL() {
    this.allDvqlData = await this.dmFacadeService
      .getDmCoquantochucService()
      .getFetchAll();
    this.dvql = this.allDvqlData;
    this.dvqlFilters = this.allDvqlData;
  }

  /**
   * Hàm get Cá nhân
   */
  async showCaNhan() {
    this.allCaNhanData = await this.dmFacadeService
      .getDmCanhanService()
      .getFetchAll();
    this.caNhan = this.allCaNhanData;
    this.canhanFilters = this.allCaNhanData;
  }


  /**
   * Hàm get Province
   */
  async showDvhcTinh() {
    this.allTinhData = await this.dmFacadeService
      .getProvinceService()
      .getFetchAll();
    this.allTinh = this.allTinhData;
    this.dvhcProvinceFilters = this.allTinhData;
  }

  /**
   * Hàm get District
   */
  async showDvhcHuyen() {
    if (!this.diemKhaiThacIOForm.value.matinh === true) {
      this.allHuyen = [];
      this.dvhcDistrictFilters = [];
      this.allXa = [];
      this.dvhcWardFilters = [];
      if (this.editMode === true) { this.diemKhaiThacIOForm.controls.mahuyen.setValue(""); }
    }
    if (!this.diemKhaiThacIOForm.value.matinh === false) {
      if (this.editMode === true) { this.diemKhaiThacIOForm.controls.mahuyen.setValue(""); }
      this.allXa = [];
      this.dvhcWardFilters = [];
      this.allHuyen = await this.dmFacadeService
        .getDistrictService()
        .getDistrictByIdProvince(this.diemKhaiThacIOForm.value.matinh);
      this.dvhcDistrictFilters = this.allHuyen;
    }
  }

  /**
   * Hàm get Ward
   */
  async showDvhcXa() {
    if (!this.diemKhaiThacIOForm.value.mahuyen === true) {
      this.allXa = [];
      this.dvhcWardFilters = [];
      if (this.editMode === true) { this.diemKhaiThacIOForm.controls.maxa.setValue(""); }
    }
    if (!this.diemKhaiThacIOForm.value.matinh === false && !this.diemKhaiThacIOForm.value.mahuyen === false) {
      if (this.editMode === true) { this.diemKhaiThacIOForm.controls.maxa.setValue(""); }
      this.allXa = await this.dmFacadeService
        .getWardService()
        .getWardByIdDistrict(this.diemKhaiThacIOForm.value.mahuyen);
      this.dvhcWardFilters = this.allXa;
    }
  }

  /**
   * Hàm reset form
   */
  public onFormReset() {
    this.diemKhaiThacIOForm.reset();
  }

  /**
   * Hàm check validation khi submit
   */
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.diemKhaiThacIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  /**
   * Display fields css
   * @param field
   */
  public displayFieldCss(field: string) {
    displayFieldCssService(field);
  }

  /**
   * Hàm close SideNav Điểm khai thác nước mặt
   */
  public closeDiemKhaiThacIOSidebar() {
    this.matSidenavService.close();
  }

  /**
   * Hàm đóng mat dialog
   */
  closeMatDialog() {
    this.imDialog.closeAll();
  }

  /**
   * Hàm mở thư viện dialog
   */
  public showMatDialog() {
    this.mDialog.setDialog(this, ThuvienComponent, 'showFileSelect', 'closeMatDialog', 'simpleFileV1', '75%', '85vh');
    this.mDialog.open();
  }

  /**
   * Hiển thị những file select trong thư viện
   */
  showFileSelect() {
    this.fileArray = this.mDialog.dataResult;
    this.srcanh = this.fileArray[0].link;
  }

  /**
   * Xóa ảnh hiện có
   */
  deleteAnh() {
    this.srcanh = '';
  }

  /**
   *  Hàm gọi từ function con gọi vào chạy function cha
   * @param methodName
   */
  doFunction(methodName) {
    this[methodName]();
  }

}
