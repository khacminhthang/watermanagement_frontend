import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DktHosoktctComponent } from './dkt-hosoktct.component';

describe('DktHosoktctComponent', () => {
  let component: DktHosoktctComponent;
  let fixture: ComponentFixture<DktHosoktctComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DktHosoktctComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DktHosoktctComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
