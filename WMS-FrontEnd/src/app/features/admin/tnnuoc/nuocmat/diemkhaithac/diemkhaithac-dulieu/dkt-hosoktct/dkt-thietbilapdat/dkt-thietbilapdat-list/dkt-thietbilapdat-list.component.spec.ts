import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DktThietbilapdatListComponent } from './dkt-thietbilapdat-list.component';

describe('DktThietbilapdatListComponent', () => {
  let component: DktThietbilapdatListComponent;
  let fixture: ComponentFixture<DktThietbilapdatListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DktThietbilapdatListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DktThietbilapdatListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
