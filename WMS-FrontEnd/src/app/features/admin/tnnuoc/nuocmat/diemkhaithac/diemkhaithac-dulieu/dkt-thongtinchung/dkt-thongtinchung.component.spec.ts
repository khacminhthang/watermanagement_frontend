import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DktThongtinchungComponent } from './dkt-thongtinchung.component';

describe('DktThongtinchungComponent', () => {
  let component: DktThongtinchungComponent;
  let fixture: ComponentFixture<DktThongtinchungComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DktThongtinchungComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DktThongtinchungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
