import { Component, OnInit } from '@angular/core';
import {DataStateChangeEventArgs} from "@syncfusion/ej2-angular-grids";
import {SettingsCommon} from "src/app/shared/constants/setting-common";
import {Observable} from "rxjs";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AdminRoutingName} from "src/app/routes/routes-name";
import {ItemModel} from "@syncfusion/ej2-angular-splitbuttons";
import {HighchartService} from "src/app/service/admin/common/highchart.service";
import {SubHeaderService} from "src/app/core/services/utilities/subheader.service";
import {Router} from "@angular/router";
import {TnnuocFacadeService} from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
import {MapService} from "src/app/core/services/utilities/map.service";
import {CommonFacadeService} from "src/app/service/admin/common/common-facade.service";
import {ObjKey} from "src/app/shared/constants/objkey-constants";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-diemkhaithac',
  templateUrl: './diemkhaithac.component.html',
  styleUrls: ['./diemkhaithac.component.scss']
})
export class DiemkhaithacComponent implements OnInit {

  public swKhaiThacNuocMatService: any;
  public state: DataStateChangeEventArgs;
  public settingsCommon = new SettingsCommon();
  public listDiemKhaiThacNuocMat: Observable<DataStateChangeEventArgs>;
  thongsoForm: FormGroup;
  listData: any;
  listDataDiemKhaiThac: any;
  public listDataThongSo: any;
  public thongsoFilters: any;
  public idTram: number;
  public idThamso: any;
  public firstdate: any;
  public lastdate: any;

  public navArray = [
    { title: "Quản trị", url: "/" },
    {
      title: "Quan trắc tài nguyên nước",
      url: "/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri
    },
    {
      title: "Khai thác nước mặt",
      url: "/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri + "/" + AdminRoutingName.swexUri
    }
  ];
  public buttonArray = [];
  public items: ItemModel[] = [
    {
      id: "1",
      text: "Chi tiết",
      iconCss: ""
    },
    {
      id: "2",
      text: "Hồ sơ kỹ thuật",
      iconCss: ""
    },
    {
      id: "3",
      text: "Thông số quan trắc",
      iconCss: ""
    },
    {
      id: "4",
      text: "Kết quả quan trắc",
      iconCss: ""
    }
  ];

  public errorForm = "";

  constructor(
    private chartSv: HighchartService,
    private subHeaderService: SubHeaderService,
    private router: Router,
    public WrFacadeSv: TnnuocFacadeService,
    private formBuilder: FormBuilder,
    public mapService: MapService,
    public commonFacadeService: CommonFacadeService,
    private datePipe: DatePipe,
  ) {
    // Get new service
   this.swKhaiThacNuocMatService = this.WrFacadeSv.getSwKhaithacnuocmatService();
  }

  ngOnInit() {
    this.subHeaderService.setParentComp(this);
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 12 };
    this.mapInit();
    this.dataSubHeader();
    this.getDiemKhaiThacNuocMat();
    this.bindingConfigValidation();
  }

  async getDiemKhaiThacNuocMat() {
    this.listData = await this.WrFacadeSv.getSwKhaithacnuocmatService().getFetchAll({pageNumber: 1, pageSize: -1});
    if (this.listData.items) {
      this.listData.items.map((item, index) => {
        item.serialNumber = index + 1;
      });
    }
    this.listDataDiemKhaiThac = this.listData.items;
  }


  /**
   * Khởi tạo input form
   */
  bindingConfigValidation() {
    this.thongsoForm = this.formBuilder.group({
      idthamso: ["", Validators.required],
      firstdate: ["", Validators.required],
      lastdate: ["", Validators.required]
    });
  }

  /**
   * Hàm điều hướng đến trang thông tin chung khi click vào menu trên grid
   * @param event
   * @param data
   */
  selectMenu(event, data) {
    const buttonId = event.item.id;
    switch (buttonId) {
      case "1":
        this.router.navigateByUrl("/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri + "/" + AdminRoutingName.swexUri + "/" + AdminRoutingName.swDiemkhaithac + "/" + data.id );
        break;
      case "2":
        this.router.navigateByUrl("/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri + "/" + AdminRoutingName.swexUri + "/" + AdminRoutingName.swDiemkhaithac + "/" + data.id + "/2");
        break;
      case "3":
        this.router.navigateByUrl("/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri + "/" + AdminRoutingName.swexUri + "/" + AdminRoutingName.swDiemkhaithac + "/" + data.id + "/3");
        break;
      case "4":
        this.router.navigateByUrl("/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri + "/" + AdminRoutingName.swexUri + "/" + AdminRoutingName.swDiemkhaithac + "/" + data.id + "/4");
        break;
    }
  }

  // khởi tạo bản đồ
  async mapInit() {
    this.mapService.mapInit('mapViewDiv');
  }


  /**
   * Truyền data vào SubHeader
   */
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }

  /**
   * Call function
   * @param methodName
   */
  doFunction(methodName) {
    this[methodName]();
  }

  // khởi tạo biểu đồ
  chartInit(data) {
    if (data) {
      this.chartSv.setSource(data);
      this.chartSv.preDataChart(data, "gw");
      this.chartSv.setChartName("kết quả đo gần nhất");
    } else {
      this.chartSv.setSource(data);
      this.chartSv.preDataChart(data, "gw");
      this.chartSv.setChartName("chưa có kết quả đo");
    }
    this.chartSv.setXTittle("Thời gian đo");
    this.chartSv.setYTittle("Kết quả đo");
    this.chartSv.chartInit1m("chart", "line");
  }

  /**
   * Hàm hiển thị vị trí điểm khi click vào 1 row trên grid
   * @param event
   */
  async rowSelected(event) {
    this.errorForm = "";
    this.thongsoForm.controls.firstdate.setValue("");
    this.thongsoForm.controls.lastdate.setValue("");
    if (event.data.toadox && event.data.toadoy) {
      this.mapService.zoomToPoint(event.data , event.data.tendiem, event.data.vitri);
    } else {
      this.mapInit();
    }
    this.listDataThongSo = await this.commonFacadeService.getObjthamsoService()
      .getFetchAll({ idObj: event.data.id, objKey: 5 });
    this.thongsoFilters = this.listDataThongSo;
    this.idTram = event.data.id;
    this.idThamso = null;
    if (this.listDataThongSo && this.listDataThongSo[0]) {
      this.idThamso = this.listDataThongSo[0].idthamso;
      const data: any = await this.commonFacadeService.getDlquantractsService()
        .getAllKetQuaFirst({
          Take: 100,
          IdTramquantrac: this.idTram,
          IdThamsodo: this.listDataThongSo[0].idthamso,
          ObjKey: ObjKey.KhaiThacNuocMat,
          PageSize: -1,
        });
      this.chartInit(data.items);
    } else {
      this.idThamso = "";
      this.chartInit([]);
    }
  }

  async showThongSo() {
    this.errorForm = "";
    this.thongsoForm.controls.firstdate.setValue("");
    this.thongsoForm.controls.lastdate.setValue("");
    this.chartSv.changColor('#5f8dd9');
    const data: any = await this.commonFacadeService.getDlquantractsService()
      .getAllKetQuaFirst({
        Take: 100,
        IdTramquantrac: this.idTram,
        IdThamsodo: this.thongsoForm.value.idthamso,
        ObjKey: ObjKey.KhaiThacNuocMat,
        PageSize: -1,
      });
    this.chartInit(data.items);
  }

  // Show kết quả quan trắc
  showKetQua() {
    if (this.thongsoForm.valid) {
      // this.checkget = false;
      this.firstdate = this.datePipe.transform(this.thongsoForm.value.firstdate, "dd/MM/yyyy hh:mm:ss");
      this.lastdate = this.datePipe.transform(this.thongsoForm.value.lastdate, "dd/MM/yyyy hh:mm:ss");
      this.getKetQuaTimeFromTo(this.firstdate, this.lastdate);
    } else {
      this.errorForm = "Bạn phải nhập đầy đủ thông tin";
    }
  }
  async getKetQuaTimeFromTo(datefirst, datelast) {
    this.errorForm = "";
    // const datefirst: any = this.datePipe.transform(this.thongsoForm.value.firstdate, "dd/MM/yyyy hh:mm:ss");
    // const datelast: any = this.datePipe.transform(this.thongsoForm.value.lastdate, "dd/MM/yyyy hh:mm:ss");
    const data: any = await this.commonFacadeService.getDlquantractsService().getAllKetQuaTimeFromTo({
        IdTramquantrac: this.idTram,
        ObjKey: ObjKey.KhaiThacNuocMat,
        IdThamsodo: this.thongsoForm.value.idthamso,
        timeFrom: datefirst,
        timeTo: datelast,
        PageSize: -1,
      });
    this.chartInit(data.items);
  }
}
