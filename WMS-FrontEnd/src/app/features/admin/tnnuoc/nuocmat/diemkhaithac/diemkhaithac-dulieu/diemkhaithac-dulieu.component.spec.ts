import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiemkhaithacDulieuComponent } from './diemkhaithac-dulieu.component';

describe('DiemkhaithacDulieuComponent', () => {
  let component: DiemkhaithacDulieuComponent;
  let fixture: ComponentFixture<DiemkhaithacDulieuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiemkhaithacDulieuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiemkhaithacDulieuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
