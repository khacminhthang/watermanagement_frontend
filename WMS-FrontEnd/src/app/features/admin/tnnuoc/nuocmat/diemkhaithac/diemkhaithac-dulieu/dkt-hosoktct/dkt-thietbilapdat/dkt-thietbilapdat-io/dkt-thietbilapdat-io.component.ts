import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { HttpErrorResponse } from "@angular/common/http";
import {
  displayFieldCssService, validationAllErrorMessagesService,
} from "src/app/core/services/utilities/validatorService";
import { TnnuocFacadeService } from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
import { InputMayBomDiemKhaiThac } from "src/app/models/admin/tnnuoc/maybomdiemkhaithac.model";
import { ActivatedRoute } from "@angular/router";
import { DatePipe } from "@angular/common";

@Component({
  selector: 'app-dkt-thietbilapdat-io',
  templateUrl: './dkt-thietbilapdat-io.component.html',
  styleUrls: ['./dkt-thietbilapdat-io.component.scss']
})
export class DktThietbilapdatIoComponent implements OnInit {
  thietBiLapDatIOForm: FormGroup;
  public inputModel: InputMayBomDiemKhaiThac;
  public editMode: boolean;
  public purpose: string;
  public obj: any;

  // error message
  validationErrorMessages = {
    loaimaybom: { required: "Tên loại máy bơm không được để trống!" },
    dosaulapdat: { pattern: "Độ sâu lắp đặt phải là kiểu số", min: "Độ sâu lắp đặt phải lớn hơn 0" },
    congsuat: { pattern: "Công suất phải là kiểu số", min: "Công suất phải lớn hơn 0" },
  };

  // form errors
  formErrors = {
    loaimaybom: "",
    dosaulapdat: "",
    thoigianlapdat: "",
    congsuat: ""
  };

  // ctor
  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    public swFacadeService: TnnuocFacadeService,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
  ) { }

  // onInit
  ngOnInit() {
    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
  }

  // config Form use add or update
  bindingConfigAddOrUpdate() {
    this.editMode = false;
    this.inputModel = new InputMayBomDiemKhaiThac();
    if (this.purpose === "new" || this.purpose === "edit") {
      this.editMode = true;
    }
    // check edit
    if (this.editMode === true && this.obj) {
      this.thietBiLapDatIOForm.setValue({
        loaimaybom: this.obj.loaimaybom,
        dosaulapdat: this.obj.dosaulapdat,
        thoigianlapdat: this.obj.thoigianlapdat,
        congsuat: this.obj.congsuat,
        note: this.obj.note
      });
    }
  }

  // config input validation form
  bindingConfigValidation() {
    this.thietBiLapDatIOForm = this.formBuilder.group({
      loaimaybom: ["", Validators.required],
      dosaulapdat: ["", [Validators.min(0), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      thoigianlapdat: [""],
      congsuat: ["", [Validators.min(0), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      note: [""]
    });
  }

  // on Submit
  public onSubmit(operMode: string) {
    if (this.thietBiLapDatIOForm.valid === false) { this.logAllValidationErrorMessages(); } else {
      this.addOrUpdate(operMode);
      // close
      this.matSidenavService.close();
    }
  }

  // add or update continue
  public addOrUpdateContinue(operMode: string) {
    if (this.thietBiLapDatIOForm.valid === false) { this.logAllValidationErrorMessages(); } else {
      this.addOrUpdate(operMode);
      this.onFormReset();
      this.purpose = "new";
    }
  }

  // add or update
  private addOrUpdate(operMode: string) {
    this.inputModel = this.thietBiLapDatIOForm.value;
    const idUrl = this.route.snapshot.paramMap.get("id");
    this.inputModel.idDiemkhaithac = +idUrl;
    this.inputModel.thoigianlapdat = this.datePipe.transform(this.thietBiLapDatIOForm.value.thoigianlapdat, "yyyy-MM-dd");
    if (operMode === "new") {
      this.swFacadeService
        .getSwMayBomDiemKhaiThacService()
        .addItem(this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("getAllMayBomDiemKhaiThac"),
          (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Thêm mới máy bơm thành công!",
              2000
            )
        );
    } else if (operMode === "edit") {
      const id: number = this.obj.id;
      this.swFacadeService
        .getSwMayBomDiemKhaiThacService()
        .updateItem(id, this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("getAllMayBomDiemKhaiThac"),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Cập nhập dữ liệu máy bơm thành công",
              2000
            )
        );
    }
  }

  // on form reset
  public onFormReset() {
    this.thietBiLapDatIOForm.reset();
  }
  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.thietBiLapDatIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  // display fields css
  public displayFieldCss(field: string) {
    displayFieldCssService(field);
  }

  // close sidebar
  public closeThietBiLapDatIOSidebar() {
    this.matSidenavService.close();
  }
}
