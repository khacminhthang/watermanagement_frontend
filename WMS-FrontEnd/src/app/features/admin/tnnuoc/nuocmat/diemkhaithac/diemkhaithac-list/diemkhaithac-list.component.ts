import {Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {MatDialog, MatSidenav} from "@angular/material";
import {DataStateChangeEventArgs} from "@syncfusion/ej2-angular-grids";
import {SettingsCommon} from "src/app/shared/constants/setting-common";
import {Observable} from "rxjs";
import {AdminRoutingName} from "src/app/routes/routes-name";
import {Router} from "@angular/router";
import {TnnuocFacadeService} from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
import {ProgressService} from "src/app/core/services/utilities/progress.service";
import {CommonServiceShared} from "src/app/core/services/utilities/common-service";
import {MatsidenavService} from "src/app/core/services/utilities/matsidenav.service";
import {SubHeaderService} from "src/app/core/services/utilities/subheader.service";
import {MyAlertDialogComponent} from "src/app/shared/components/my-alert-dialog/my-alert-dialog.component";
import {OutputKhaithacnuocmatModel} from "src/app/models/admin/tnnuoc/khaithacnuocmat.model";
import {DiemkhaithacIoComponent} from "src/app/features/admin/tnnuoc/nuocmat/diemkhaithac/diemkhaithac-io/diemkhaithac-io.component";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-diemkhaithac-list',
  templateUrl: './diemkhaithac-list.component.html',
  styleUrls: ['./diemkhaithac-list.component.scss']
})
export class DiemkhaithacListComponent implements OnInit {

  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compDiemKhaiThacNuocMatio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  listData: any;
  listDataDiemKhaiThac: any;

  // Paging
  public swKhaiThacNuocMatService: any;
  public state: DataStateChangeEventArgs;
  public settingsCommon = new SettingsCommon();
  public listDiemKhaiThacNuocMat: Observable<DataStateChangeEventArgs>;

  // Add or edit item
  public selectedItem: OutputKhaithacnuocmatModel;

  navArray = [
    { title: "Quản trị", url: "/" },
    {
      title: "Quan trắc tài nguyên nước",
      url: "/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri
    },
    {
      title: "Khai thác nước mặt",
      url:
        "/" +
        AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.swexUri
    },
    {
      title: "Công trình khai thác nước mặt ",
      url:
        "/" +
        AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.swexUri +
        "/" +
        AdminRoutingName.swDiemkhaithac
    }
  ];

  buttonArray = [];

  constructor(
    public router: Router,
    public modalDialog: MatDialog,
    public cfr: ComponentFactoryResolver,
    public wrFacadeService: TnnuocFacadeService,
    public progressService: ProgressService,
    public commonService: CommonServiceShared,
    public matSidenavService: MatsidenavService,
    private subHeaderService: SubHeaderService
  ) {
    // Get new service
    this.swKhaiThacNuocMatService = this.wrFacadeService.getSwKhaithacnuocmatService();
  }

  ngOnInit() {
    this.subHeaderService.setParentComp(this);
    this.matSidenavService.setSidenav(this.matSidenav, this, this.content, this.cfr);
    // Get items
    this.getDiemKhaiThacNuocMat();
    this.dataSubHeader();
  }

  async getDiemKhaiThacNuocMat() {
    this.listData = await this.wrFacadeService.getSwKhaithacnuocmatService().getFetchAll({pageNumber: 1, pageSize: -1});
    if (this.listData.items) {
      this.listData.items.map((item, index) => {
        item.serialNumber = index + 1;
      });
    }
    this.listDataDiemKhaiThac = this.listData.items;
  }
  /**
   * Truyền data vào SubHeader
   */
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }
  //
  // /**
  //  * Hàm lấy về danh sách Điểm khai thác nước mặt ( phân trang bên server )
  //  */
  // async getDiemKhaiThacNuocMat() {
  //   this.listDiemKhaiThacNuocMat = this.swKhaiThacNuocMatService;
  //   const pageSize = this.settingsCommon.pageSettings.pageSize;
  //   this.swKhaiThacNuocMatService.getDataFromServer({ skip: 0, take: pageSize });
  // }
  //
  // /**
  //  * Hàm lấy về danh sách Điểm khai thác nước mặt (phân trang bên server) khi click vào pagination trên grid
  //  * @param state
  //  */
  // public dataStateChange(state: DataStateChangeEventArgs): void {
  //   this.swKhaiThacNuocMatService.getDataFromServer(state);
  // }

  /**
   * Mở SideNav thêm mới Điểm khai thác nước mặt
   */
  public openDiemKhaiThacIOSidebar() {
    this.matSidenavService.setTitle("Thêm mới điểm khai thác nước mặt");
    this.matSidenavService.setContentComp(DiemkhaithacIoComponent, "new");
    this.matSidenavService.open();
  }

  /**
   * Hàm getById Điểm khai thác, mở SideNav Sửa Item và truyền dữ liệu vừa get vào Form
   * @param data
   */
  async editItemDiemKhaiThacNM(data) {
    await this.wrFacadeService
      .getSwKhaithacnuocmatService()
      .getByid(data.id)
      .subscribe(diem => {
        this.matSidenavService.setTitle("Sửa điểm khai thác nước mặt");
        this.matSidenavService.setContentComp(DiemkhaithacIoComponent, "edit", diem);
        this.matSidenavService.open();
      });
  }

  /**
   * Hàm điều hướng đến trang thông tin chung của Điểm khai thác
   * @param id
   */
  public detailItem(id) {
    this.router.navigate([
      AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri + "/" + AdminRoutingName.swexUri + "/"
      + AdminRoutingName.swDiemkhaithac + "/", id
    ]);
  }

  /**
   * hàm xóa Điểm khai thác nước mặt
   * @param data
   */
  public deleteItem(data) {
    this.selectedItem = data;
    const canDelete: string = this.wrFacadeService.getSwKhaithacnuocmatService().checkBeDeleted(this.selectedItem.id);
    this.canBeDeletedCheck(canDelete);
  }

  /**
   * Hàm check delete
   * @param sMsg
   */
  public canBeDeletedCheck(sMsg: string) {
    if (sMsg === 'ok') {
      this.confirmDeleteDiaLog();
    } else {
      this.canDeleteDialog(sMsg);
    }
  }

  /**
   * Hàm mở dialog thông báo có chắc chắn xóa không
   */
  confirmDeleteDiaLog() {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = 'Chắc chắn xóa?';
    dialogRef.componentInstance.content = 'Tên điểm khai thác nước mặt: <b>' + this.selectedItem.tendiem + '</b>';
    dialogRef.componentInstance.okeButton = 'Đồng ý';
    dialogRef.componentInstance.cancelButton = 'Hủy bỏ';
    dialogRef.componentInstance.visibleOkButton = true;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'confirm') {
        this.wrFacadeService.getSwKhaithacnuocmatService().deleteItem(this.selectedItem.id)
          .subscribe(res => this.getDiemKhaiThacNuocMat(), (error: HttpErrorResponse) => {
              this.commonService.showError(error);
            },
            () =>
              this.commonService.showeNotiResult(
                "Đã xóa điểm khai thác nước mặt thành công!",
                2000
              ));
      }
    });
  }

  /**
   * Hàm thông báo không thể xóa
   * @param sMsg
   */
  canDeleteDialog(sMsg: string) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = 'Không thể xóa!';
    dialogRef.componentInstance.content = '<b>' + sMsg + '</b>';
    dialogRef.componentInstance.cancelButton = 'Đóng';
    dialogRef.componentInstance.visibleOkButton = false;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.componentInstance.headerColor = 'red';
  }

  /**
   * hàm đóng SideNav Điểm khai thác nước mặt
   */
  public closeDiemKhaiThacIOSidebar() {
    this.matSidenav.close();
  }

  /**
   * Call method
   * @param methodName
   */
  doFunction(methodName) {
    this[methodName]();
  }
}
