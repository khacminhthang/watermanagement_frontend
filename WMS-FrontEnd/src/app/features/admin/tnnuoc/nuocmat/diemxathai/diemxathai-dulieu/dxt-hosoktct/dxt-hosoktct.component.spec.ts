import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DxtHosoktctComponent } from './dxt-hosoktct.component';

describe('DxtHosoktctComponent', () => {
  let component: DxtHosoktctComponent;
  let fixture: ComponentFixture<DxtHosoktctComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DxtHosoktctComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DxtHosoktctComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
