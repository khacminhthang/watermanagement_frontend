import {
  Component,
  ComponentFactoryResolver,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {MatsidenavService} from "src/app/core/services/utilities/matsidenav.service";
import {ActivatedRoute} from "@angular/router";
import {TnnuocFacadeService} from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
import {MatSidenav} from "@angular/material/sidenav";
import {DmFacadeService} from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import {DiemxathaiIoComponent} from "src/app/features/admin/tnnuoc/nuocmat/diemxathai/diemxathai-io/diemxathai-io.component";
import {STATUS} from "src/app/shared/constants/status-constants";
import {CommonFacadeService} from "src/app/service/admin/common/common-facade.service";
import {MapService} from "src/app/core/services/utilities/map.service";

@Component({
  selector: 'app-dxt-thongtinchung',
  templateUrl: './dxt-thongtinchung.component.html',
  styleUrls: ['./dxt-thongtinchung.component.scss']
})
export class DxtThongtinchungComponent implements OnInit {
  public obj: any = {};
  @Output("getInformation") getInformation: EventEmitter<any> = new EventEmitter();
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compDxtio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  public itemCaNhan: any;
  public itemDvql: any;
  public itemProjection: any;
  public status = STATUS;
  public caNhanData: any;
  public dvql: any;
  public caNhan: any;
  public dataTinh: any;
  public dataHuyen: any;
  public dataXa: any;
  public tinh: any;
  public huyen: any;
  public xa: any;

  constructor(
    public matsidenavService: MatsidenavService,
    public cfr: ComponentFactoryResolver,
    public wrFacadeService: TnnuocFacadeService,
    public dmFacadeService: DmFacadeService,
    private route: ActivatedRoute,
    public commonFacadeService: CommonFacadeService,
    public mapService: MapService
  ) { }

  async ngOnInit() {
    await this.getDiemXaThai();
  }

  async showMap() {
    await this.mapInit();
    await this.zoomToObj();
  }
  // lấy thông tin điểm bằng id trên url
  async getDiemXaThai() {
    this.getInformation.emit();
    const idUrl = this.route.snapshot.paramMap.get("id");
    const id = +idUrl;
    await this.wrFacadeService
      .getSwXaNuocThaiService()
      .getByid(id)
      .subscribe(diem => {
          this.obj = diem;
          this.showMap();
          this.showTinh(this.obj.matinh);
          this.showHuyen(this.obj.matinh, this.obj.mahuyen);
          this.showXa(this.obj.mahuyen, this.obj.maxa);
        },
        error => { },
        () => {
          this.getInformationCaNhan();
          this.getInformationDvql();
        });
  }

  // lấy tên cá nhân theo idChusudung
  async getInformationCaNhan() {
    if (this.obj.idChusudung) {
      await this.dmFacadeService.getDmCanhanService()
        .getByid(this.obj.idChusudung)
        .subscribe(canhan => {
          this.itemCaNhan = canhan;
        });
    }
  }

  // Lấy tên đơn vị hành chính theo idDonviquanly
  async getInformationDvql() {
    if (this.obj.idDonviquanly) {
      await this.dmFacadeService.getDmCoquantochucService()
        .getByid(this.obj.idDonviquanly)
        .subscribe(dvql => this.itemDvql = dvql);
    }
  }
  toEditMode() {
    this.matsidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.matsidenavService.setTitle("Sửa dữ liệu thông tin điểm xả thải");
    this.matsidenavService.setContentComp(
      DiemxathaiIoComponent,
      "edit",
      this.obj
    );
    this.matsidenavService.open();
  }
  // close
  public closeDiemXaThaiIOSidebar() {
    this.matSidenav.close();
  }
  // call method
  doFunction(methodName) {
    this[methodName]();
  }

  // khởi tạo bản đồ
  async mapInit() {
    this.mapService.mapInit('mapViewDiv');
  }
  // show trạng thái
  showTrangThai(id: number) {
    for (const s of this.status) {
      if (s.id === id) {
        return s.name;
      }
      return "";
    }
  }

  public zoomToObj() {
    this.mapService.zoomToPoint(this.obj,  this.obj.tendiem, this.obj.vitri);
  }

  async showTinh(id: number) {
    if (id) {
      this.dataTinh = await this.dmFacadeService
        .getProvinceService()
        .getFetchAll();
      if (this.dataTinh) {
        this.dataTinh.map(res => {
          if (res.matinh === id) {
            this.tinh = res.tendvhc;
          }
        });
      }
    }
  }

  async showHuyen(idtinh: number, idhuyen: number) {
    if (idtinh) {
      this.dataHuyen = await this.dmFacadeService
        .getDistrictService()
        .getByid(idtinh).subscribe( res => {
          if (res) {
            res.map(res1 => {
              if (res1.mahuyen === idhuyen) {
                this.huyen = res1.tendvhc;
              }
            });
          }
        });
    }
  }

  async showXa(idhuyen: number, idxa: number) {
    if (idxa) {
      await this.dmFacadeService
        .getWardService()
        .getByid(idhuyen).subscribe( res => {
          if (res) {
            res.map(res1 => {
              if (res1.maxa === idxa) {
                this.xa = res1.tendvhc;
              }
            });
          }
        });
    }
  }
}
