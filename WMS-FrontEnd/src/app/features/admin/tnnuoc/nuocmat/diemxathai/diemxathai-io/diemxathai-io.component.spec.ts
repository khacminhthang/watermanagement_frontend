import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiemxathaiIoComponent } from './diemxathai-io.component';

describe('DiemxathaiIoComponent', () => {
  let component: DiemxathaiIoComponent;
  let fixture: ComponentFixture<DiemxathaiIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiemxathaiIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiemxathaiIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
