import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiemxathaiDulieuComponent } from './diemxathai-dulieu.component';

describe('DiemxathaiDulieuComponent', () => {
  let component: DiemxathaiDulieuComponent;
  let fixture: ComponentFixture<DiemxathaiDulieuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiemxathaiDulieuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiemxathaiDulieuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
