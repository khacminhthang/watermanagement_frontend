import {
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from "@angular/core";
import { MatSidenav } from "@angular/material/sidenav";
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { Observable } from "rxjs";
import { AdminRoutingName } from "src/app/routes/routes-name";
import { Router } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import { TnnuocFacadeService } from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import { MyAlertDialogComponent } from "src/app/shared/components/my-alert-dialog/my-alert-dialog.component";
import { HttpErrorResponse } from "@angular/common/http";
import { DataStateChangeEventArgs } from "@syncfusion/ej2-angular-grids";
import { DiemxathaiIoComponent } from "src/app/features/admin/tnnuoc/nuocmat/diemxathai/diemxathai-io/diemxathai-io.component";
import { OutputXanuocthaiModel } from "src/app/models/admin/tnnuoc/xanuocthai.model";

@Component({
  selector: "app-diemxathai-list",
  templateUrl: "./diemxathai-list.component.html",
  styleUrls: ["./diemxathai-list.component.scss"],
})
export class DiemxathaiListComponent implements OnInit {
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compDiemXaThaiio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  listData: any;
  listDataDiemXaThai: any;

  // Paging
  public swDiemXaThaiService: any;
  public state: DataStateChangeEventArgs;
  public settingsCommon = new SettingsCommon();
  public listDiemXaThai: Observable<DataStateChangeEventArgs>;
  public selectedItem: OutputXanuocthaiModel;

  navArray = [
    { title: "Quản trị", url: "/" },
    {
      title: "Quan trắc tài nguyên nước",
      url: "/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri,
    },
    {
      title: "Xả nước thải",
      url:
        "/" +
        AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.diUri,
    },
    {
      title: "Điểm xả thải",
      url:
        "/" +
        AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.diUri +
        "/" +
        AdminRoutingName.pdiUri,
    },
  ];

  buttonArray = [];

  constructor(
    public router: Router,
    public modalDialog: MatDialog,
    public cfr: ComponentFactoryResolver,
    public wrFacadeService: TnnuocFacadeService,
    public progressService: ProgressService,
    public commonService: CommonServiceShared,
    public matsidenavService: MatsidenavService,
    private subHeaderService: SubHeaderService
  ) {
    // Get new service
    this.swDiemXaThaiService = this.wrFacadeService.getSwXaNuocThaiService();
  }

  ngOnInit() {
    this.subHeaderService.setParentComp(this);
    this.matsidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    // Get items
    this.getDiemXaThai();
    this.dataSubHeader();
  }

  // Truyền data vào header
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }

  async getDiemXaThai() {
    this.listData = await this.wrFacadeService
      .getSwXaNuocThaiService()
      .getFetchAll({pageNumber: 1, pageSize: -1});
    if (this.listData.items) {
      this.listData.items.map((item, index) => {
        item.serialNumber = index + 1;
      });
    }
    this.listDataDiemXaThai = this.listData.items;
  }

  // // Get điểm xả thải
  // async getDiemXaThai() {
  //   this.listDiemXaThai = this.swDiemXaThaiService;
  //   const pageSize = this.settingsCommon.pageSettings.pageSize;
  //   this.swDiemXaThaiService.getDataFromServer({ skip: 0, take: pageSize });
  // }
  //
  // // When page item clicked
  // public dataStateChange(state: DataStateChangeEventArgs): void {
  //   this.swDiemXaThaiService.getDataFromServer(state);
  // }

  // open sidebar execute insert
  public openDiemXaThaiIOSidebar() {
    this.matsidenavService.setTitle("Thêm mới dữ liệu điểm xả thải");
    this.matsidenavService.setContentComp(DiemxathaiIoComponent, "new");
    this.matsidenavService.open();
  }

  // edit open sidebar
  async editItemDiemXaThai(data) {
    await this.wrFacadeService
      .getSwXaNuocThaiService()
      .getByid(data.id)
      .subscribe((alldata) => {
        this.matsidenavService.setTitle("Sửa dữ liệu thông tin điểm xả thải");
        this.matsidenavService.setContentComp(
          DiemxathaiIoComponent,
          "edit",
          alldata
        );
        this.matsidenavService.open();
      });
  }

  // detail component
  public detailItem(id) {
    this.router.navigate([
      AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.diUri +
        "/" +
        AdminRoutingName.pdiUri +
        "/",
      id,
    ]);
  }

  // delete
  public deleteItem(data) {
    this.selectedItem = data;
    const canDelete: string = this.wrFacadeService
      .getSwXaNuocThaiService()
      .checkBeDeleted(this.selectedItem.id);
    this.canBeDeletedCheck(canDelete);
  }

  public canBeDeletedCheck(sMsg: string) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog();
    } else {
      this.canDeleteDialog(sMsg);
    }
  }

  confirmDeleteDiaLog() {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Chắc chắn xóa?";
    dialogRef.componentInstance.content =
      "Tên điểm xả thải: <b>" + this.selectedItem.tendiem + "</b>";
    dialogRef.componentInstance.okeButton = "Đồng ý";
    dialogRef.componentInstance.cancelButton = "Hủy bỏ";
    dialogRef.componentInstance.visibleOkButton = true;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.afterClosed().subscribe((result) => {
      if (result === "confirm") {
        this.wrFacadeService
          .getSwXaNuocThaiService()
          .deleteItem(this.selectedItem.id)
          .subscribe(
            (res) => this.getDiemXaThai(),
            (error: HttpErrorResponse) => {
              this.commonService.showeNotiResult(error.message, 2000);
            },
            () =>
              this.commonService.showeNotiResult(
                "Đã xóa thành công: " + this.selectedItem.tendiem,
                2000
              )
          );
      }
    });
  }

  canDeleteDialog(sMsg: string) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Không thể xóa!";
    dialogRef.componentInstance.content = "<b>" + sMsg + "</b>";
    dialogRef.componentInstance.cancelButton = "Đóng";
    dialogRef.componentInstance.visibleOkButton = false;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.componentInstance.headerColor = "red";
  }

  // close
  public closeDiemXaThaiIOSidebar() {
    this.matSidenav.close();
  }

  // call method
  doFunction(methodName) {
    this[methodName]();
  }
}
