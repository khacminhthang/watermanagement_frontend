import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiemxathaiListComponent } from './diemxathai-list.component';

describe('DiemxathaiListComponent', () => {
  let component: DiemxathaiListComponent;
  let fixture: ComponentFixture<DiemxathaiListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiemxathaiListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiemxathaiListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
