import { Component, OnInit } from '@angular/core';
import { ObjKey } from "src/app/shared/constants/objkey-constants";
import { AdminRoutingName } from "src/app/routes/routes-name";
import { ActivatedRoute, Router } from "@angular/router";
import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import { TnnuocFacadeService } from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";

@Component({
  selector: 'app-diemxathai-dulieu',
  templateUrl: './diemxathai-dulieu.component.html',
  styleUrls: ['./diemxathai-dulieu.component.scss']
})
export class DiemxathaiDulieuComponent implements OnInit {
  public objKey = ObjKey.DiemXaThai;
  activeTabCheck: number;
  public objId: any;
  public navArray = [
    { title: "Quản trị", url: "/" },
    {
      title: "Quan trắc tài nguyên nước",
      url: "/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri
    },
    {
      title: "Xả nước thải",
      url: "/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri + "/" + AdminRoutingName.diUri
    },
    {
      title: "Điểm xả thải",
      url:
        "/" +
        AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.diUri +
        "/" +
        AdminRoutingName.pdiUri
    },
    {
      title: "Chi tiết",
      url: ""
    }
  ];
  public buttonArray = [
    {
      title: "Danh sách điểm xả thải",
      icon: "fad fa-chevron-double-left",
      color: "btn-primary",
      url: "/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri + "/" + AdminRoutingName.diUri + "/" + AdminRoutingName.pdiUri
    }
  ];
  constructor(
    public router: Router,
    private subHeaderService: SubHeaderService,
    public swFacadeService: TnnuocFacadeService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.subHeaderService.setParentComp(this);
    this.dataSubHeader();
    this.getInformation();
    this.activeTab();
  }
  // lấy dữ liệu từ router để set active tab
  activeTab() {
    const active = this.route.snapshot.paramMap.get('active');
    this.activeTabCheck = +active;
  }
  // lấy thông tin điểm bằng id trên url
  async getInformation() {
    const idurl = this.route.snapshot.paramMap.get('id');
    const id = +idurl;
    await this.swFacadeService
      .getSwXaNuocThaiService()
      .getByid(id)
      .subscribe(diemxathai => {
        this.objId = diemxathai;
      });
  }
  // Truyền data vào header
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }
}
