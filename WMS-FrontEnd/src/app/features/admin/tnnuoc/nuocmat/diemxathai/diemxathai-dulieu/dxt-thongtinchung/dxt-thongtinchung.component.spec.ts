import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DxtThongtinchungComponent } from './dxt-thongtinchung.component';

describe('DxtThongtinchungComponent', () => {
  let component: DxtThongtinchungComponent;
  let fixture: ComponentFixture<DxtThongtinchungComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DxtThongtinchungComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DxtThongtinchungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
