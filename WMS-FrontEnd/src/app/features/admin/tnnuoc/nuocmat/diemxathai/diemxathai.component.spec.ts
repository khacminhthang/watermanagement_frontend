import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiemxathaiComponent } from './diemxathai.component';

describe('DiemxathaiComponent', () => {
  let component: DiemxathaiComponent;
  let fixture: ComponentFixture<DiemxathaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiemxathaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiemxathaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
