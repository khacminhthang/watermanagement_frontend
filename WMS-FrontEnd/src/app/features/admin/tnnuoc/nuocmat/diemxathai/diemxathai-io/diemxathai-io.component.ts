import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { OutputCanhanModel } from "src/app/models/admin/danhmuc/canhan.model";
import { OutputDvhcModel } from "src/app/models/admin/danhmuc/dvhc.model";
import { OutputCoquantochucModel } from "src/app/models/admin/danhmuc/coquantochuc.model";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { TnnuocFacadeService } from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";
import {
  displayFieldCssService,
  validationAllErrorMessagesService
} from "src/app/core/services/utilities/validatorService";
import { InputXanuocthaiModel } from "src/app/models/admin/tnnuoc/xanuocthai.model";
import { ObjKey } from "src/app/shared/constants/objkey-constants";
import { STATUS } from "src/app/shared/constants/status-constants";
import { CommonFacadeService } from "src/app/service/admin/common/common-facade.service";
import { MatDialog } from "@angular/material";
import { MatdialogService } from "src/app/core/services/utilities/matdialog.service";
import { ThuvienComponent } from "src/app/features/admin/thuvien/thuvien.component";

@Component({
  selector: 'app-diemxathai-io',
  templateUrl: './diemxathai-io.component.html',
  styleUrls: ['./diemxathai-io.component.scss']
})
export class DiemxathaiIoComponent implements OnInit {
  diemXaThaiIOForm: FormGroup;
  IOCompact: boolean;
  public inputModel: InputXanuocthaiModel;
  public editMode: boolean;
  public purpose: string;
  public obj: any;
  public allTinh: any;
  public allHuyen: any;
  public allXa: any;
  public dvql: any;
  public canhan: any;
  public projection: any;
  public allTinhData: any;
  public allDvqlData: any;
  public allCaNhanData: any;
  public canhanFilters: OutputCanhanModel[];
  public dvhcProvinceFilters: OutputDvhcModel[];
  public dvhcDistrictFilters: OutputDvhcModel[];
  public dvhcWardFilters: OutputDvhcModel[];
  public dvqlFilters: OutputCoquantochucModel[];

  // Những biến dành cho phần file
  mDialog: any;
  srcanh = '';
  public fileArray: any;
  public status = STATUS;
  // error message
  errorLuuLuongChoPhepLonNhat = '';
  errorX = '';
  errorY = '';
  errorSrid = '';
  validationErrorMessages = {
    ten: { required: "Tên điểm không được để trống!" },
    matinh: { required: "Hãy chọn tỉnh!" },
    mahuyen: { required: "Hãy chọn huyện!" },
    luuluongxatrungbinh: { min: "Lưu lượng xả trung bình phải lớn hơn 0", pattern: "Lưu lượng xả trung bình phải là kiểu số" },
    luuluonglonnhatchophep: { min: "Lưu lượng lớn nhất cho phép phải lớn hơn 0", pattern: "Lưu lượng lớn nhất cho phép phải là kiểu số" },
    toadox: { pattern: "Tọa độ x phải là kiểu số" },
    toadoy: { pattern: "Tọa độ y phải là kiểu số" },
    caodoz: { pattern: "Cao độ z phải là kiểu số" },
  };

  // form errors
  formErrors = {
    sohieu: "",
    ten: "",
    toadox: "",
    toadoy: "",
    caodoz: "",
    srid: "",
    vitri: "",
    matinh: "",
    mahuyen: "",
    maxa: "",
    idDonviquanly: "",
    idChusudung: "",
    idBody: "",
    thuyvuctiepnhan: "",
    phuongthucxa: "",
    hinhthucdan: "",
    luuluongxatrungbinh: "",
    luuluonglonnhatchophep: "",
    quychuanapdung: "",
    cothietbigiamsat: "",
    dacapphep: "",
    note: "",
    imgLink: "",
  };

  // ctor
  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    public dmFacadeService: DmFacadeService,
    public wrFacadeService: TnnuocFacadeService,
    private http: HttpClient,
    private datePipe: DatePipe,
    public router: Router,
    public commonFacadeService: CommonFacadeService,
    private imDialog: MatDialog, imDialogService: MatdialogService
  ) {
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
  }

  // onInit
  ngOnInit() {
    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
    this.showDVQL();
    this.showCaNhan();
    // this.showObjKey();
  }

  // config Form use add or update
  bindingConfigAddOrUpdate() {
    this.showDvhcTinh();
    this.IOCompact = false;
    this.inputModel = new InputXanuocthaiModel();
    if (this.purpose === "edit") {
      this.IOCompact = false;
    }
    if (this.purpose === "edit" && this.obj) {
      this.diemXaThaiIOForm.setValue({
        sohieudiem: this.obj.sohieudiem,
        tendiem: this.obj.tendiem,
        toadox: this.obj.toadox,
        toadoy: this.obj.toadoy,
        caodoz: this.obj.caodoz,
        srid: this.obj.srid,
        vitri: this.obj.vitri,
        matinh: this.obj.matinh,
        mahuyen: this.obj.mahuyen,
        maxa: this.obj.maxa,
        thoigianxaydung: this.obj.thoigianxaydung,
        thoigianvanhanh: this.obj.thoigianvanhanh,
        idDonviquanly: this.obj.idDonviquanly,
        idChusudung: this.obj.idChusudung,
        // idBody: this.obj.idBody,
        // bodyKey: this.obj.bodyKey,
        thuyvuctiepnhan: this.obj.thuyvuctiepnhan,
        phuongthucxa: this.obj.phuongthucxa,
        hinhthucdan: this.obj.hinhthucdan,
        luuluongxatrungbinh: this.obj.luuluongxatrungbinh,
        luuluonglonnhatchophep: this.obj.luuluonglonnhatchophep,
        quychuanapdung: this.obj.quychuanapdung,
        cothietbigiamsat: this.obj.cothietbigiamsat,
        dacapphep: this.obj.dacapphep,
        note: this.obj.note,
        // status: this.obj.status,
        imgLink: this.obj.imgLink,
      });
      this.srcanh = this.obj.imgLink;
      this.showDvhcHuyen();
      this.showDvhcXa();
      // this.showIdBody();

    }
    this.editMode = true;
  }

  // config input validation form
  bindingConfigValidation() {
    this.diemXaThaiIOForm = this.formBuilder.group({
      sohieudiem: [""],
      tendiem: ["", Validators.required],
      toadox: ["", Validators.pattern(/^[-+]?\d*\.?\d*$/)],
      toadoy: ["", Validators.pattern(/^[-+]?\d*\.?\d*$/)],
      caodoz: ["", Validators.pattern(/^[-+]?\d*\.?\d*$/)],
      srid: [""],
      vitri: [""],
      matinh: ["", Validators.required],
      mahuyen: ["", Validators.required],
      maxa: [""],
      thoigianxaydung: [""],
      thoigianvanhanh: [""],
      idDonviquanly: [""],
      idChusudung: [""],
      // idBody: [""],
      thuyvuctiepnhan: [""],
      phuongthucxa: [""],
      hinhthucdan: [""],
      luuluongxatrungbinh: ["", [Validators.min(0), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      luuluonglonnhatchophep: ["", [Validators.min(0), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      quychuanapdung: [""],
      cothietbigiamsat: [""],
      dacapphep: [""],
      // status: [""],
      note: [""],
      imgLink: [""],
      // bodyKey: [""]
    });
  }

  // on Submit
  public onSubmit(operMode: string) {
    this.errorLuuLuongChoPhepLonNhat = '';
    this.errorX = '';
    this.errorY = '';
    this.errorSrid = '';
    if (this.diemXaThaiIOForm.valid === false) this.logAllValidationErrorMessages();
    if (((this.diemXaThaiIOForm.value.luuluonglonnhatchophep <= this.diemXaThaiIOForm.value.luuluongxatrungbinh)
      && ((!this.diemXaThaiIOForm.value.luuluonglonnhatchophep === false) && (!this.diemXaThaiIOForm.value.luuluongxatrungbinh === false)))
      || ((!this.diemXaThaiIOForm.value.luuluonglonnhatchophep === true) && (!this.diemXaThaiIOForm.value.luuluongxatrungbinh === false))) {
      this.errorLuuLuongChoPhepLonNhat = 'Lưu lượng cho phép lớn nhất phải lớn hơn lưu lượng xả trung bình';
    } else {
      if (((!this.diemXaThaiIOForm.value.toadox === true) && (!this.diemXaThaiIOForm.value.toadoy === true)
        && (!this.diemXaThaiIOForm.value.srid === true))) {
        if (this.diemXaThaiIOForm.valid === true) this.addOrUpdate(operMode);
      } else {
        if ((!this.diemXaThaiIOForm.value.toadox === true) || (!this.diemXaThaiIOForm.value.toadoy === true)
          || (!this.diemXaThaiIOForm.value.srid === true)) {
          (!this.diemXaThaiIOForm.value.toadox === true) ? this.errorX = "Bạn phải nhập tọa độ x" : this.errorX = "";
          (!this.diemXaThaiIOForm.value.toadoy === true) ? this.errorY = "Bạn phải nhập tọa độ y" : this.errorY = "";
          (!this.diemXaThaiIOForm.value.srid === true) ? this.errorSrid = "Bạn phải chọn srid" : this.errorSrid = "";
        } else { if (this.diemXaThaiIOForm.valid === true) this.addOrUpdate(operMode); }
      }
    }
  }

  // add or update continue
  public addOrUpdateContinue(operMode: string) {
    this.errorLuuLuongChoPhepLonNhat = '';
    this.errorX = '';
    this.errorY = '';
    this.errorSrid = '';
    if (this.diemXaThaiIOForm.valid === false) this.logAllValidationErrorMessages();
    if (((this.diemXaThaiIOForm.value.luuluonglonnhatchophep <= this.diemXaThaiIOForm.value.luuluongxatrungbinh)
      && ((!this.diemXaThaiIOForm.value.luuluonglonnhatchophep === false) && (!this.diemXaThaiIOForm.value.luuluongxatrungbinh === false)))
      || ((!this.diemXaThaiIOForm.value.luuluonglonnhatchophep === true) && (!this.diemXaThaiIOForm.value.luuluongxatrungbinh === false))) {
      this.errorLuuLuongChoPhepLonNhat = 'Lưu lượng cho phép lớn nhất phải lớn hơn lưu lượng xả trung bình';
    } else {
      if (((!this.diemXaThaiIOForm.value.toadox === true) && (!this.diemXaThaiIOForm.value.toadoy === true)
        && (!this.diemXaThaiIOForm.value.srid === true))) {
        if (this.diemXaThaiIOForm.valid === true) {
          this.addOrUpdate(operMode);
          this.onFormReset();
          this.purpose = "new";
        }
      } else {
        if ((!this.diemXaThaiIOForm.value.toadox === true) || (!this.diemXaThaiIOForm.value.toadoy === true)
          || (!this.diemXaThaiIOForm.value.srid === true)) {
          (!this.diemXaThaiIOForm.value.toadox === true) ? this.errorX = "Bạn phải nhập tọa độ x" : this.errorX = "";
          (!this.diemXaThaiIOForm.value.toadoy === true) ? this.errorY = "Bạn phải nhập tọa độ y" : this.errorY = "";
          (!this.diemXaThaiIOForm.value.srid === true) ? this.errorSrid = "Bạn phải chọn srid" : this.errorSrid = "";
        } else {
          if (this.diemXaThaiIOForm.valid === true) {
            this.addOrUpdate(operMode);
            this.onFormReset();
            this.purpose = "new";
          }
        }
      }
    }
  }

  // add or update
  private addOrUpdate(operMode: string) {
    this.inputModel = this.diemXaThaiIOForm.value;
    this.inputModel.objKey = ObjKey.DiemXaThai;
    this.inputModel.thoigianvanhanh = this.datePipe.transform(this.diemXaThaiIOForm.value.thoigianvanhanh, "yyyy-MM-dd");
    this.inputModel.thoigianxaydung = this.datePipe.transform(this.diemXaThaiIOForm.value.thoigianxaydung, "yyyy-MM-dd");
    this.inputModel.imgLink = this.srcanh;
    if (operMode === "new") {
      if (!this.diemXaThaiIOForm.value.cothietbigiamsat === true) this.inputModel.cothietbigiamsat = false;
      if (!this.diemXaThaiIOForm.value.dacapphep === true) this.inputModel.dacapphep = false;
      this.wrFacadeService
        .getSwXaNuocThaiService()
        .addItem(this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("getDiemXaThai"),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Thêm mới điểm xả thải nước mặt thành công!",
              2000
            )
        );
      this.matSidenavService.close();
    } else if (operMode === "edit") {
      const id: number = this.obj.id;
      this.wrFacadeService
        .getSwXaNuocThaiService()
        .updateItem(id, this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("getDiemXaThai"),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Cập nhập dữ liệu điểm xả thải thành công",
              2000
            )
        );
      // close
      this.matSidenavService.close();
    }
  }


  // Get đơn vị quản lý
  async showDVQL() {
    this.allDvqlData = await this.dmFacadeService
      .getDmCoquantochucService()
      .getFetchAll();
    this.dvql = this.allDvqlData;
    this.dvqlFilters = this.allDvqlData;
  }

  // Get cá nhân
  async showCaNhan() {
    this.allCaNhanData = await this.dmFacadeService
      .getDmCanhanService()
      .getFetchAll();
    this.canhan = this.allCaNhanData;
    this.canhanFilters = this.allCaNhanData;
  }
  /**
   * Hàm get Province
   */
  async showDvhcTinh() {
    this.allTinhData = await this.dmFacadeService
      .getProvinceService()
      .getFetchAll();
    this.allTinh = this.allTinhData;
    this.dvhcProvinceFilters = this.allTinhData;
  }

  /**
   * Hàm get District
   */
  async showDvhcHuyen() {
    if (!this.diemXaThaiIOForm.value.matinh === true) {
      this.allHuyen = [];
      this.dvhcDistrictFilters = [];
      this.allXa = [];
      this.dvhcWardFilters = [];
      if (this.editMode === true) { this.diemXaThaiIOForm.controls.mahuyen.setValue(""); }
    }
    if (!this.diemXaThaiIOForm.value.matinh === false) {
      if (this.editMode === true) { this.diemXaThaiIOForm.controls.mahuyen.setValue(""); }
      this.allXa = [];
      this.dvhcWardFilters = [];
      this.allHuyen = await this.dmFacadeService
        .getDistrictService()
        .getDistrictByIdProvince(this.diemXaThaiIOForm.value.matinh);
      this.dvhcDistrictFilters = this.allHuyen;
    }
  }

  /**
   * Hàm get Ward
   */
  async showDvhcXa() {
    if (!this.diemXaThaiIOForm.value.mahuyen === true) {
      this.allXa = [];
      this.dvhcWardFilters = [];
      if (this.editMode === true) { this.diemXaThaiIOForm.controls.maxa.setValue(""); }
    }
    if (!this.diemXaThaiIOForm.value.matinh === false && !this.diemXaThaiIOForm.value.mahuyen === false) {
      if (this.editMode === true) { this.diemXaThaiIOForm.controls.maxa.setValue(""); }
      this.allXa = await this.dmFacadeService
        .getWardService()
        .getWardByIdDistrict(this.diemXaThaiIOForm.value.mahuyen);
      this.dvhcWardFilters = this.allXa;
    }
  }

  // on form reset
  public onFormReset() {
    this.diemXaThaiIOForm.reset();
  }

  // Validation click submit
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.diemXaThaiIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }
  // display fields css
  public displayFieldCss(field: string) {
    displayFieldCssService(field);
  }

  // close sidebar
  public closeDiemXaThaiIOSidebar() {
    this.matSidenavService.confirmStatus = "doCancelFunction";
    this.matSidenavService.close();
  }

  /**
   * Hàm đóng mat dialog
   */
  closeMatDialog() {
    this.imDialog.closeAll();
  }

  /**
   * Hàm mở thư viện dialog
   */
  public showMatDialog() {
    this.mDialog.setDialog(this, ThuvienComponent, 'showFileSelect', 'closeMatDialog', 'simpleFileV1', '75%', '85vh');
    this.mDialog.open();
  }

  /**
   * Hiển thị những file select trong thư viện
   */
  showFileSelect() {
    this.fileArray = this.mDialog.dataResult;
    this.srcanh = this.fileArray[0].link;
  }

  /**
   * Xóa ảnh hiện có
   */
  deleteAnh() {
    this.srcanh = '';
  }

  /**
   *  Hàm gọi từ function con gọi vào chạy function cha
   * @param methodName
   */
  doFunction(methodName) {
    this[methodName]();
  }
}
