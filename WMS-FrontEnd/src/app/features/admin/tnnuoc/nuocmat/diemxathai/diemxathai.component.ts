import { Component, OnInit } from '@angular/core';
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { Observable } from "rxjs";
import { FormBuilder, FormGroup } from "@angular/forms";
import { AdminRoutingName } from "src/app/routes/routes-name";
import { ItemModel } from "@syncfusion/ej2-splitbuttons";
import { HighchartService } from "src/app/service/admin/common/highchart.service";
import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import { Router } from "@angular/router";
import { TnnuocFacadeService } from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
import { DataStateChangeEventArgs } from "@syncfusion/ej2-angular-grids";
import {MapService} from "src/app/core/services/utilities/map.service";
import {CommonFacadeService} from "src/app/service/admin/common/common-facade.service";
import {DatePipe} from "@angular/common";
import {ObjKey} from "src/app/shared/constants/objkey-constants";

@Component({
  selector: 'app-diemxathai',
  templateUrl: './diemxathai.component.html',
  styleUrls: ['./diemxathai.component.scss']
})
export class DiemxathaiComponent implements OnInit {
  public swXaNuocThaiService: any;
  public state: DataStateChangeEventArgs;
  public settingsCommon = new SettingsCommon();
  public listDiemXaThai: Observable<DataStateChangeEventArgs>;
  thongsoForm: FormGroup;
  listData: any;
  listDataDiemXaThai: any;

  public listDataThongSo: any;
  public thongsoFilters: any;
  public idTram: number;
  public idThamso: any;

  public navArray = [
    { title: "Quản trị", url: "/" },
    {
      title: "Quan trắc tài nguyên nước",
      url: "/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri
    },
    {
      title: "Xả nước thải",
      url: "/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri + "/" + AdminRoutingName.diUri
    }
  ];
  public buttonArray = [];
  public items: ItemModel[] = [
    {
      id: "1",
      text: "Chi tiết",
      iconCss: ""
    },
    {
      id: "2",
      text: "Hồ sơ kỹ thuật",
      iconCss: ""
    },
    {
      id: "3",
      text: "Thông số quan trắc",
      iconCss: ""
    },
    {
      id: "4",
      text: "Kết quả quan trắc",
      iconCss: ""
    }
  ];
  public errorForm = "";
  constructor(
    private chartSv: HighchartService,
    private subHeaderService: SubHeaderService,
    private router: Router,
    public WrFacadeSv: TnnuocFacadeService,
    private formBuilder: FormBuilder,
    public mapService: MapService,
    public commonFacadeService: CommonFacadeService,
    private datePipe: DatePipe,
  ) {
    // Get new service
    this.swXaNuocThaiService = this.WrFacadeSv.getSwXaNuocThaiService();
  }

  ngOnInit() {
    this.subHeaderService.setParentComp(this);
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 12 };
    this.mapInit();
    this.chartInit([]);
    this.dataSubHeader();
    this.getDiemXaThai();
    this.bindingConfigValidation();
  }

  async getDiemXaThai() {
    this.listData = await this.WrFacadeSv.getSwXaNuocThaiService().getFetchAll({pageNumber: 1, pageSize: -1});
    if (this.listData.items) {
      this.listData.items.map((item, index) => {
        item.serialNumber = index + 1;
      });
    }
    this.listDataDiemXaThai = this.listData.items;
  }

  // // Get gieng quan trac
  // async getDiemXaThai() {
  //   this.listDiemXaThai = this.swXaNuocThaiService;
  //   const pageSize = this.settingsCommon.pageSettings.pageSize;
  //   this.swXaNuocThaiService.getDataFromServer({ skip: 0, take: pageSize });
  // }
  //
  // // When page item clicked
  // public dataStateChange(state: DataStateChangeEventArgs): void {
  //   this.swXaNuocThaiService.getDataFromServer(state);
  // }

  // config input validation form
  bindingConfigValidation() {
    this.thongsoForm = this.formBuilder.group({
      idthamso: [""],
      firstdate: [""],
      lastdate: [""]
    });
  }
  // hàm khi chọn menu ngữ cảnh
  selectMenu(event, data) {
    const buttonId = event.item.id;
    switch (buttonId) {
      case "1":
        this.router.navigateByUrl("/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri + "/" + AdminRoutingName.diUri + "/" + AdminRoutingName.pdiUri + "/" + data.id);
        break;
      case "2":
        this.router.navigateByUrl("/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri + "/" + AdminRoutingName.diUri + "/" + AdminRoutingName.pdiUri + "/" + data.id + "/2");
        break;
      case "3":
        this.router.navigateByUrl("/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri + "/" + AdminRoutingName.diUri + "/" + AdminRoutingName.pdiUri + "/" + data.id + "/3");
        break;
      case "4":
        this.router.navigateByUrl("/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri + "/" + AdminRoutingName.diUri + "/" + AdminRoutingName.pdiUri + "/" + data.id + "/4");
        break;
    }
  }

  // khởi tạo bản đồ
  mapInit() {
    this.mapService.mapInit('viewDiv');
  }

  // khởi tạo biểu đồ
  chartInit(data) {
    if (data) {
      this.chartSv.setSource(data);
      this.chartSv.preDataChart(data, "gw");
      this.chartSv.setChartName("kết quả đo gần nhất");
    } else {
      this.chartSv.setSource(data);
      this.chartSv.preDataChart(data, "gw");
      this.chartSv.setChartName("chưa có kết quả đo");
    }
    this.chartSv.setXTittle("Thời gian đo");
    this.chartSv.setYTittle("Kết quả đo");
    this.chartSv.chartInit1m("chart", "line");
  }

  // Truyền data vào header
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }

  // call method
  doFunction(methodName) {
    this[methodName]();
  }

  /**
   * Hàm hiển thị vị trí điểm khi click vào 1 row trên grid
   * @param event
   */
  async rowSelected(event) {
    this.errorForm = "";
    if (event.data.toadox && event.data.toadoy) {
      this.mapService.zoomToPoint(event.data , event.data.tendiem, event.data.vitri);
    } else {
      this.mapInit();
    }
    this.listDataThongSo = await this.commonFacadeService.getObjthamsoService()
      .getFetchAll({ idObj: event.data.id, objKey: ObjKey.DiemXaThai });
    this.thongsoFilters = this.listDataThongSo;
    this.idTram = event.data.id;
    this.idThamso = null;
    if (this.listDataThongSo && this.listDataThongSo[0]) {
      this.idThamso = this.listDataThongSo[0].idthamso;
      const data: any = await this.commonFacadeService.getDlquantractsService()
        .getAllKetQuaFirst({
          Take: 100,
          IdTramquantrac: this.idTram,
          ObjKey: ObjKey.DiemXaThai,
          IdThamsodo: this.listDataThongSo[0].idthamso,
          PageSize: -1,
        });
      this.chartInit(data.items);
      console.log("DiemxathaiComponent -> rowSelected -> data", data)
    } else {
      this.idThamso = "";
      this.chartInit([]);
    }
  }

  async showThongSo() {
    this.errorForm = "";
    this.chartSv.changColor('#5f8dd9');
    const data: any = await this.commonFacadeService.getDlquantractsService()
      .getAllKetQuaFirst({
        Take: 100,
        IdTramquantrac: this.idTram,
        ObjKey:  ObjKey.DiemXaThai,
        IdThamsodo: this.thongsoForm.value.idthamso,
        PageSize: -1,
      });
    this.chartInit(data);
  }

  async getKetQuaTimeFromTo() {
    this.errorForm = "";
    if (this.thongsoForm.value.idthamso && this.thongsoForm.value.firstdate && this.thongsoForm.value.lastdate) {
      const datefirst: any = this.datePipe.transform(this.thongsoForm.value.firstdate, "dd/MM/yyyy hh:mm:ss");
      const datelast: any = this.datePipe.transform(this.thongsoForm.value.lastdate, "dd/MM/yyyy hh:mm:ss");
      const data: any = await this.commonFacadeService.getDlquantractsService().getAllKetQuaTimeFromTo({
        IdTramquantrac: this.idTram,
        ObjKey: ObjKey.DiemXaThai,
        IdThamsodo: this.thongsoForm.value.idthamso,
        timeFrom: datefirst,
        timeTo: datelast,
        PageSize: -1,
      });
      this.chartInit(data.items);
    } else {
      this.errorForm = "Bạn phải nhập đầy đủ thông tin";
    }
  }
}
