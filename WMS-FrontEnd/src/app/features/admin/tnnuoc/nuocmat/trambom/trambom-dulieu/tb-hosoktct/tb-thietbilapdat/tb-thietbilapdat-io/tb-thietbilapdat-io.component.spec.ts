import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TbThietbilapdatIoComponent } from './tb-thietbilapdat-io.component';

describe('TbThietbilapdatIoComponent', () => {
  let component: TbThietbilapdatIoComponent;
  let fixture: ComponentFixture<TbThietbilapdatIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TbThietbilapdatIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TbThietbilapdatIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
