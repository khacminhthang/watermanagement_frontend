import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { VanHanh } from "src/app/shared/constants/vanhanh-constants";
import { STATUS } from "src/app/shared/constants/status-constants";
import { OutputCanhanModel } from "src/app/models/admin/danhmuc/canhan.model";
import { OutputDvhcModel } from "src/app/models/admin/danhmuc/dvhc.model";
import { OutputCoquantochucModel } from "src/app/models/admin/danhmuc/coquantochuc.model";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { TnnuocFacadeService } from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { ImageuploadService } from "src/app/service/admin/common/imageupload.service";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";
import {
  displayFieldCssService,
  validationAllErrorMessagesService
} from "src/app/core/services/utilities/validatorService";
import { InputTrambomModel } from "src/app/models/admin/tnnuoc/trambom.model";
import { CommonFacadeService } from "src/app/service/admin/common/common-facade.service";
import { MatDialog } from "@angular/material";
import { MatdialogService } from "src/app/core/services/utilities/matdialog.service";
import { ObjKey } from "src/app/shared/constants/objkey-constants";
import { ThuvienComponent } from "src/app/features/admin/thuvien/thuvien.component";

@Component({
  selector: 'app-trambom-io',
  templateUrl: './trambom-io.component.html',
  styleUrls: ['./trambom-io.component.scss']
})
export class TrambomIoComponent implements OnInit {

  tramBomIOForm: FormGroup;
  IOCompact: boolean;
  public vanHanh = VanHanh;
  public status = STATUS;
  public inputModel: InputTrambomModel;
  public editMode: boolean;
  public purpose: string;
  public obj: any;
  public allTinh: any;
  public allHuyen: any;
  public allXa: any;
  public dvql: any;
  public caNhan: any;
  public allTinhData: any;
  public allDvqlData: any;
  public allCaNhanData: any;
  public projection: any;
  public canhanFilters: OutputCanhanModel[];
  public dvhcProvinceFilters: OutputDvhcModel[];
  public dvhcDistrictFilters: OutputDvhcModel[];
  public dvhcWardFilters: OutputDvhcModel[];
  public dvqlFilters: OutputCoquantochucModel[];

  // Những biến dành cho phần file
  mDialog: any;
  srcanh = '';
  public fileArray: any;
  errorToadox = '';
  errorToadoy = '';
  errorSrid = '';
  // error message
  validationErrorMessages = {
    ten: { required: "Tên trạm bơm không được để trống!" },
    matinh: { required: "Hãy chọn tỉnh!" },
    mahuyen: { required: "Hãy chọn huyện!" },
    somaybomtuoinuoc: { pattern: "Số máy bơm tưới nước phải là kiểu số" },
    somaybomtieunuoc: { pattern: "Số máy bơm tiêu nước phải là kiểu số" },
    khanangbomtuoi: { min: "Khả năng bơm tưới phải lớn hơn 0", pattern: "Khả năng bơm tưới phải là kiểu số" },
    luuluongtuoithietke: { min: "Lưu lượng tưới thiết kế lớn hơn 0", pattern: "Lưu lượng tưới thiết kế phải là kiểu số" },
    luuluongtuoithucte: { min: "Lưu lượng tưới thực tế lớn hơn 0", pattern: "Lưu lượng tưới thực tế phải là kiểu số" },
    dientichtuoithietke: { min: "Diện tích tưới thiết kế lớn hơn 0", pattern: "Diện tích tưới thiết kế phải là kiểu số" },
    dientichtuoithucte: { min: "Diện tích tưới thưc tế lớn hơn 0", pattern: "Diện tích tưới thưc tế phải là kiểu số" },
    khanangbomtieu: { min: "Khả năng bơm tiêu lớn hơn 0", pattern: "Khả năng bơm tiêu phải là kiểu số" },
    luuluongtieuthietke: { min: "Lưu lượng tiêu thiết kế lớn hơn 0", pattern: "Lưu lượng tiêu thiết kế phải là kiểu số" },
    luuluongtieuthucte: { min: "Lưu lượng tiêu thực tế lớn hơn 0", pattern: "Lưu lượng tiêu thực tế phải là kiểu số" },
    dientichtieuthietke: { min: "Diện tích tiêu thiết kế lớn hơn 0", pattern: "Diện tích tiêu thiết kế phải là kiểu số" },
    dientichtieuthucte: { min: "Diện tích tiêu thực tế lớn hơn 0", pattern: "Diện tích tiêu thực tế phải là kiểu số" },
    sohothietke: { min: "Số hồ thiết kế lớn hơn 0", pattern: "Số hồ thiết kế phải là kiểu số" },
    sohothucte: { min: "Số máy bơm tưới nước lớn hơn 0", pattern: "Số máy bơm tưới nước phải là kiểu số" },
    phantramcongsuathuuich: { min: "Phần trăm công suất hữu ích lớn hơn 0", pattern: "Phần trăm công suất hữu ích phải là kiểu số" },
    toadox: { pattern: "Tọa độ x phải là kiểu số" },
    toadoy: { pattern: "Tọa độ y phải là kiểu số" },
    caodoz: { pattern: "Cao độ z phải là kiểu số" },
  };

  // form errors
  formErrors = {
    ten: "",
    matinh: "",
    mahuyen: "",
    somaybomtuoinuoc: "",
    somaybomtieunuoc: "",
    khanangbomtuoi: "",
    luuluongtuoithietke: "",
    luuluongtuoithucte: "",
    dientichtuoithietke: "",
    dientichtuoithucte: "",
    khanangbomtieu: "",
    luuluongtieuthietke: "",
    luuluongtieuthucte: "",
    dientichtieuthietke: "",
    dientichtieuthucte: "",
    sohothietke: "",
    sohothucte: "",
    phantramcongsuathuuich: "",
    toadox: "",
    toadoy: "",
    caodoz: ""
  };

  // ctor
  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    public dmFacadeService: DmFacadeService,
    public wrFacadeService: TnnuocFacadeService,
    private http: HttpClient,
    private imgUpload: ImageuploadService,
    private datePipe: DatePipe,
    public router: Router,
    public commonFacadeService: CommonFacadeService,
    private imDialog: MatDialog, imDialogService: MatdialogService
  ) {
    this.mDialog = imDialogService;
    this.mDialog.initDialg(imDialog);
  }

  // onInit
  ngOnInit() {
    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
    this.showDVQL();
    this.showCaNhan();
    // this.showObjKey();
    console.log(this.obj)

  }

  /**
   * Hàm khởi tạo form
   */
  bindingConfigValidation() {
    this.tramBomIOForm = this.formBuilder.group({
      sohieutrambom: [""],
      tentrambom: ["", Validators.required],
      toadox: ["", Validators.pattern(/^[-+]?\d*\.?\d*$/)],
      toadoy: ["", Validators.pattern(/^[-+]?\d*\.?\d*$/)],
      caodoz: ["", Validators.pattern(/^[-+]?\d*\.?\d*$/)],
      srid: [""],
      vitri: [""],
      matinh: ["", Validators.required],
      mahuyen: ["", Validators.required],
      maxa: [""],
      thoigianxaydung: [""],
      thoigianvanhanh: [""],
      idDonviquanly: [""],
      somaybomtuoinuoc: ["", Validators.pattern(/^[-+]?\d*\.?\d*$/)],
      somaybomtieunuoc: ["", Validators.pattern(/^[-+]?\d*\.?\d*$/)],
      khanangbomtuoi: ["", [Validators.min(1), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      luuluongtuoithietke: ["", [Validators.min(1), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      luuluongtuoithucte: ["", [Validators.min(1), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      dientichtuoithietke: ["", [Validators.min(1), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      dientichtuoithucte: ["", [Validators.min(1), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      khanangbomtieu: ["", [Validators.min(1), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      luuluongtieuthietke: ["", [Validators.min(1), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      luuluongtieuthucte: ["", [Validators.min(1), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      dientichtieuthietke: ["", [Validators.min(1), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      dientichtieuthucte: ["", [Validators.min(1), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      sohothietke: ["", [Validators.min(1), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      sohothucte: ["", [Validators.min(1), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      phantramcongsuathuuich: ["", [Validators.min(1), Validators.pattern(/^[-+]?\d*\.?\d*$/)]],
      hientrang: [""],
      idVanhanh: [""],
      // idBody: [""],
      // bodyKey: [""],
      note: [""],
      imgLink: [""],
      // status: [""]
    });
  }

  /**
   *  Config Form use add or update
   */
  bindingConfigAddOrUpdate() {
    this.showDvhcTinh();
    this.IOCompact = true;
    this.inputModel = new InputTrambomModel();
    if (this.purpose === "edit") {
      this.IOCompact = false;
    }
    if (this.purpose === "edit" && this.obj) {
      this.tramBomIOForm.setValue({
        sohieutrambom: this.obj.sohieudiem,
        tentrambom: this.obj.tendiem,
        toadox: this.obj.toadox,
        toadoy: this.obj.toadoy,
        caodoz: this.obj.caodoz,
        srid: this.obj.srid,
        vitri: this.obj.vitri,
        matinh: this.obj.matinh,
        mahuyen: this.obj.mahuyen,
        maxa: this.obj.maxa,
        thoigianxaydung: this.obj.thoigianxaydung,
        thoigianvanhanh: this.obj.thoigianvanhanh,
        idDonviquanly: this.obj.idDonviquanly,
        somaybomtieunuoc: this.obj.somaybomtieunuoc,
        somaybomtuoinuoc: this.obj.somaybomtuoinuoc,
        khanangbomtuoi: this.obj.khanangbomtuoi,
        luuluongtuoithietke: this.obj.luuluongtuoithietke,
        luuluongtuoithucte: this.obj.luuluongtuoithucte,
        dientichtuoithietke: this.obj.dientichtuoithietke,
        dientichtuoithucte: this.obj.dientichtuoithucte,
        khanangbomtieu: this.obj.khanangbomtieu,
        luuluongtieuthietke: this.obj.luuluongtieuthietke,
        luuluongtieuthucte: this.obj.luuluongtieuthucte,
        dientichtieuthietke: this.obj.dientichtieuthietke,
        dientichtieuthucte: this.obj.dientichtieuthucte,
        sohothietke: this.obj.sohothietke,
        sohothucte: this.obj.sohothucte,
        phantramcongsuathuuich: this.obj.phantramcongsuathuuich,
        hientrang: this.obj.hientrang,
        idVanhanh: this.obj.idVanhanh,
        // idBody: this.obj.idBody,
        // bodyKey: this.obj.bodyKey,
        note: this.obj.note,
        imgLink: this.obj.imgLink,
        // status: this.obj.status
      });
      this.srcanh = this.obj.imgLink;
      this.showDvhcHuyen();
      this.showDvhcXa();
      // this.showIdBody();
    }
    this.editMode = true;
  }


  /**
   * Hàm on submit - check validation
   * @param operMode
   */
  public onSubmit(operMode: string) {
    this.errorToadox = "";
    this.errorToadoy = "";
    this.errorSrid = "";
    if (this.tramBomIOForm.valid === false) { this.logAllValidationErrorMessages(); }
    if ((!this.tramBomIOForm.value.toadox === true) &&
      (!this.tramBomIOForm.value.toadoy === true) &&
      (!this.tramBomIOForm.value.srid === true)) {
      if (this.tramBomIOForm.valid === true) {
        this.addOrUpdate(operMode);
      }
    } else {
      if ((!this.tramBomIOForm.value.toadox === true) ||
        (!this.tramBomIOForm.value.toadoy === true) ||
        (!this.tramBomIOForm.value.srid === true)) {
        (!this.tramBomIOForm.value.toadox === true) ? this.errorToadox = "Bạn phải nhập tọa độ x" : this.errorToadox = "";
        (!this.tramBomIOForm.value.toadoy === true) ? this.errorToadoy = "Bạn phải nhập tọa độ y" : this.errorToadoy = "";
        (!this.tramBomIOForm.value.srid === true) ? this.errorSrid = "Bạn phải chọn srid" : this.errorSrid = "";
      } else {
        if (this.tramBomIOForm.valid === true) {
          this.addOrUpdate(operMode);
        }
      }
    }
  }

  /**
   * Add or update continue
   * @param operMode
   */
  public addOrUpdateContinue(operMode: string) {
    this.errorToadox = "";
    this.errorToadoy = "";
    this.errorSrid = "";
    if (this.tramBomIOForm.valid === false) { this.logAllValidationErrorMessages(); }
    if ((!this.tramBomIOForm.value.toadox === true) &&
      (!this.tramBomIOForm.value.toadoy === true) &&
      (!this.tramBomIOForm.value.srid === true)) {
      if (this.tramBomIOForm.valid === true) {
        this.addOrUpdate(operMode);
        this.onFormReset();
        this.purpose = "new";
      }
    } else {
      if ((!this.tramBomIOForm.value.toadox === true) ||
        (!this.tramBomIOForm.value.toadoy === true) ||
        (!this.tramBomIOForm.value.srid === true)) {
        (!this.tramBomIOForm.value.toadox === true) ? this.errorToadox = "Bạn phải nhập tọa độ x" : this.errorToadox = "";
        (!this.tramBomIOForm.value.toadoy === true) ? this.errorToadoy = "Bạn phải nhập tọa độ y" : this.errorToadoy = "";
        (!this.tramBomIOForm.value.srid === true) ? this.errorSrid = "Bạn phải chọn srid" : this.errorSrid = "";
      } else {
        if (this.tramBomIOForm.valid === true) {
          this.addOrUpdate(operMode);
          this.onFormReset();
          this.purpose = "new";
        }
      }
    }
  }

  /**
   * Hàm Add or Update
   * @param operMode
   */
  private addOrUpdate(operMode: string) {
    this.inputModel = this.tramBomIOForm.value;
    this.inputModel.objKey = ObjKey.TramBom;
    this.inputModel.thoigianvanhanh = this.datePipe.transform(this.tramBomIOForm.value.thoigianvanhanh, "yyyy-MM-dd");
    this.inputModel.thoigianxaydung = this.datePipe.transform(this.tramBomIOForm.value.thoigianxaydung, "yyyy-MM-dd");
    this.inputModel.imgLink = this.srcanh;
    if (operMode === "new") {
      this.wrFacadeService
        .getSwTrambomService()
        .addItem(this.inputModel)
        .subscribe(
          res => { this.matSidenavService.doParentFunction("getTramBom"); },
          (error: HttpErrorResponse) => {
            if (error.status === 403) {
              this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
            } else {
              this.commonService.showError(error);
            }
            console.log(error, 'error')
          },
          () =>
            this.commonService.showeNotiResult(
              "Thêm mới trạm bơm thành công!",
              2000
            )
        );
    } else if (operMode === "edit") {
      const id: number = this.obj.id;
      this.wrFacadeService
        .getSwTrambomService()
        .updateItem(id, this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("getTramBom"),
          (error: HttpErrorResponse) => {
            if (error.status === 403) {
              this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
            } else {
              this.commonService.showError(error);
            }
            console.log(error, 'error')
          },
          () =>
            this.commonService.showeNotiResult(
              "Cập nhật trạm bơm thành công",
              2000
            )
        );
    }
    // Close
    this.matSidenavService.close();
  }

  /**
   * Hàm get data Đơn vị quản lý
   */
  async showDVQL() {
    this.allDvqlData = await this.dmFacadeService
      .getDmCoquantochucService()
      .getFetchAll();
    this.dvql = this.allDvqlData;
    this.dvqlFilters = this.allDvqlData;
  }

  /**
   * Hàm get data Dự án
   */
  async showCaNhan() {
    this.allCaNhanData = await this.dmFacadeService
      .getDmCanhanService()
      .getFetchAll();
    this.caNhan = this.allCaNhanData;
    this.canhanFilters = this.allCaNhanData.items;
  }


  /**
   * Hàm get Province
   */
  async showDvhcTinh() {
    this.allTinhData = await this.dmFacadeService
      .getProvinceService()
      .getFetchAll();
    this.allTinh = this.allTinhData;
    this.dvhcProvinceFilters = this.allTinhData;
  }

  /**
   * Hàm get District
   */
  async showDvhcHuyen() {
    if (!this.tramBomIOForm.value.matinh === true) {
      this.allHuyen = [];
      this.dvhcDistrictFilters = [];
      this.allXa = [];
      this.dvhcWardFilters = [];
      if (this.editMode === true) { this.tramBomIOForm.controls.mahuyen.setValue(""); }
    }
    if (!this.tramBomIOForm.value.matinh === false) {
      if (this.editMode === true) { this.tramBomIOForm.controls.mahuyen.setValue(""); }
      this.allXa = [];
      this.dvhcWardFilters = [];
      this.allHuyen = await this.dmFacadeService
        .getDistrictService()
        .getDistrictByIdProvince(this.tramBomIOForm.value.matinh);
      this.dvhcDistrictFilters = this.allHuyen;
    }
  }

  /**
   * Hàm get Ward
   */
  async showDvhcXa() {
    if (!this.tramBomIOForm.value.mahuyen === true) {
      this.allXa = [];
      this.dvhcWardFilters = [];
      if (this.editMode === true) { this.tramBomIOForm.controls.maxa.setValue(""); }
    }
    if (!this.tramBomIOForm.value.matinh === false && !this.tramBomIOForm.value.mahuyen === false) {
      if (this.editMode === true) { this.tramBomIOForm.controls.maxa.setValue(""); }
      this.allXa = await this.dmFacadeService
        .getWardService()
        .getWardByIdDistrict(this.tramBomIOForm.value.mahuyen);
      this.dvhcWardFilters = this.allXa;
    }
  }

  // async showObjKey() {
  //   this.allObjKey = await this.commonFacadeService.getObjKeyService().getFetchAll();
  //   this.objKeyFilters = this.allObjKey;
  // }

  /**
   * Hàm get all IdBody theo objKey
   */
  // async showIdBody() {
  //   this.commonFacadeService.getObjKeyService()
  //     .getByid(this.tramBomIOForm.value.bodyKey)
  //     .subscribe(res => {this.allIdBody = res; this.idBodyFilters = this.allIdBody; });
  // }

  /**
   * Hàm reset form
   */
  public onFormReset() {
    this.tramBomIOForm.reset();
  }

  /**
   * Hàm check validation
   */
  public logAllValidationErrorMessages() {
    validationAllErrorMessagesService(
      this.tramBomIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  /**
   * display fields css
   * @param field
   */
  public displayFieldCss(field: string) {
    displayFieldCssService(field);
  }

  /**
   * Hàm close Sidenav Trạm bơm
   */
  public closeTramBomIOSidebar() {
    this.matSidenavService.close();
  }

  /**
   * Hàm đóng mat dialog
   */
  closeMatDialog() {
    this.imDialog.closeAll();
  }

  /**
   * Hàm mở thư viện dialog
   */
  public showMatDialog() {
    this.mDialog.setDialog(this, ThuvienComponent, 'showFileSelect', 'closeMatDialog', 'simpleFileV1', '75%', '85vh');
    this.mDialog.open();
  }

  /**
   * Hiển thị những file select trong thư viện
   */
  showFileSelect() {
    this.fileArray = this.mDialog.dataResult;
    this.srcanh = this.fileArray[0].link;
  }

  /**
   * Xóa ảnh hiện có
   */
  deleteAnh() {
    this.srcanh = '';
  }

  /**
   *  Hàm gọi từ function con gọi vào chạy function cha
   * @param methodName
   */
  doFunction(methodName) {
    this[methodName]();
  }
}
