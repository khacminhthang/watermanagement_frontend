import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TbThietbilapdatListComponent } from './tb-thietbilapdat-list.component';

describe('TbThietbilapdatListComponent', () => {
  let component: TbThietbilapdatListComponent;
  let fixture: ComponentFixture<TbThietbilapdatListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TbThietbilapdatListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TbThietbilapdatListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
