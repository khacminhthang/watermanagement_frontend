import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrambomDulieuComponent } from './trambom-dulieu.component';

describe('TrambomDulieuComponent', () => {
  let component: TrambomDulieuComponent;
  let fixture: ComponentFixture<TrambomDulieuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrambomDulieuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrambomDulieuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
