import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TbThongtinchungComponent } from './tb-thongtinchung.component';

describe('TbThongtinchungComponent', () => {
  let component: TbThongtinchungComponent;
  let fixture: ComponentFixture<TbThongtinchungComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TbThongtinchungComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TbThongtinchungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
