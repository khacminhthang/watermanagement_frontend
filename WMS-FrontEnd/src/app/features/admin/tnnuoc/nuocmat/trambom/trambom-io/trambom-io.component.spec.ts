import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrambomIoComponent } from './trambom-io.component';

describe('TrambomIoComponent', () => {
  let component: TrambomIoComponent;
  let fixture: ComponentFixture<TrambomIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrambomIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrambomIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
