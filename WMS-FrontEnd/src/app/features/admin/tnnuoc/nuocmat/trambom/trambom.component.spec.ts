import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrambomComponent } from './trambom.component';

describe('TrambomComponent', () => {
  let component: TrambomComponent;
  let fixture: ComponentFixture<TrambomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrambomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrambomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
