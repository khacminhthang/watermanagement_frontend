import { Component, OnInit } from '@angular/core';
import {ObjKey} from "src/app/shared/constants/objkey-constants";
import {AdminRoutingName} from "src/app/routes/routes-name";
import {ActivatedRoute, Router} from "@angular/router";
import {SubHeaderService} from "src/app/core/services/utilities/subheader.service";
import {TnnuocFacadeService} from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";

@Component({
  selector: 'app-trambom-dulieu',
  templateUrl: './trambom-dulieu.component.html',
  styleUrls: ['./trambom-dulieu.component.scss']
})
export class TrambomDulieuComponent implements OnInit {
  public objKey = ObjKey.TramBom;

  activeTab: number;
  public objId: any;
  public navArray = [
    { title: "Quản trị", url: "/" },
    {
      title: "Quan trắc tài nguyên nước",
      url: "/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri
    },
    {
      title: "Trạm bơm",
      url:
        "/" +
        AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.swTramBomUri
    },
    {
      title: "Danh sách trạm bơm",
      url:
        "/" +
        AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.swTramBomUri +
        "/" +
        AdminRoutingName.swTrambom
    },
    {
      title: "Chi tiết",
      url: ""
    }
  ];
  public buttonArray = [
    {
      title: "Danh sách trạm bơm",
      icon: "fad fa-chevron-double-left",
      color: "btn-primary",
      url: "/" +
        AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.swTramBomUri +
        "/" +
        AdminRoutingName.swTrambom}
  ];
  constructor(
    public router: Router,
    private subHeaderService: SubHeaderService,
    public wrFacadeService: TnnuocFacadeService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.subHeaderService.setParentComp(this);
    this.dataSubHeader();
    this.getInformation();
    this.activeTabs();
  }
  // lấy dữ liệu từ router để set active tab
  activeTabs() {
    const active = this.route.snapshot.paramMap.get('active');
    this.activeTab = +active;
  }
  // lấy thông tin bằng id trên url
  async getInformation() {
    const idurl = this.route.snapshot.paramMap.get('id');
    const id = +idurl;
    await this.wrFacadeService
      .getSwTrambomService()
      .getByid(id)
      .subscribe(trambom => {
        this.objId = trambom;
      });
  }

  // Truyền data vào header
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }

}
