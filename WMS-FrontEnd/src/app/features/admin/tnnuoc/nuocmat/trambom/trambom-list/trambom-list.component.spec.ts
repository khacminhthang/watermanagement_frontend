import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrambomListComponent } from './trambom-list.component';

describe('TrambomListComponent', () => {
  let component: TrambomListComponent;
  let fixture: ComponentFixture<TrambomListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrambomListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrambomListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
