import { Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { MatSidenav } from "@angular/material/sidenav";
import { OutputMayBomDiemKhaiThac } from "src/app/models/admin/tnnuoc/maybomdiemkhaithac.model";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { TnnuocFacadeService } from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
import { ActivatedRoute } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import { MyAlertDialogComponent } from "src/app/shared/components/my-alert-dialog/my-alert-dialog.component";
import { QueryCellInfoEventArgs } from "@syncfusion/ej2-grids";
import { TbThietbilapdatIoComponent } from "src/app/features/admin/tnnuoc/nuocmat/trambom/trambom-dulieu/tb-hosoktct/tb-thietbilapdat/tb-thietbilapdat-io/tb-thietbilapdat-io.component";

@Component({
  selector: 'app-tb-thietbilapdat-list',
  templateUrl: './tb-thietbilapdat-list.component.html',
  styleUrls: ['./tb-thietbilapdat-list.component.scss']
})
export class TbThietbilapdatListComponent implements OnInit {
  settingsCommon = new SettingsCommon();
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compThietBiLapDatio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  listData: any;
  listThietBiLapDat: OutputMayBomDiemKhaiThac[];
  selectedItem: OutputMayBomDiemKhaiThac;
  constructor(
    public matSidenavService: MatsidenavService,
    public cfr: ComponentFactoryResolver,
    public commonService: CommonServiceShared,
    private swFacadeService: TnnuocFacadeService,
    private route: ActivatedRoute,
    private modalDialog: MatDialog
  ) { }

  ngOnInit() {
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 10 };
    this.getAllMayBomTramBom();
  }

  // get all data của bảng máy bơm trạm bơm
  async getAllMayBomTramBom() {
    const idUrl = this.route.snapshot.paramMap.get("id");
    const id = +idUrl;
    this.listData = await this.swFacadeService
      .getSwMayBomTramBomService()
      .getListMayBomTramBom(id);
    if (this.listData) {
      this.listData.map((maybom, index) => {
        maybom.sNo = index + 1;
      });
    }
    this.listThietBiLapDat = this.listData;
  }

  // open sidebar execute insert
  public openThietBiLapDatIOSidebar() {
    this.matSidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.matSidenavService.setTitle("Thêm mới dữ liệu máy bơm");
    this.matSidenavService.setContentComp(TbThietbilapdatIoComponent, "new");
    this.matSidenavService.open();
  }

  // Hàm đóng Sidebar
  public closeThietBiLapDatIOSidebar() {
    this.matSidenav.close();
  }

  // Hàm xóa một bản ghi, được gọi khi nhấn nút xóa trên giao diện list
  async deleteThietBiLapDat(data) {
    const canDelete: string = this.swFacadeService
      .getSwMayBomTramBomService()
      .checkBeDeleted(data.id);
    this.canBeDeletedCheck(canDelete, data);
  }

  // check deleted
  public canBeDeletedCheck(sMsg: string, data) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog(data);
    } else {
      this.cantDeleteDialog(sMsg);
    }
  }

  // dialog thông báo có muốn xóa bản ghi không và hàm deleted
  confirmDeleteDiaLog(data) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Chắc chắn xóa?";
    dialogRef.componentInstance.content =
      "Loại máy bơm: <b>" + data.loaimaybom + "</b>";
    dialogRef.componentInstance.okeButton = "Đồng ý";
    dialogRef.componentInstance.cancelButton = "Hủy bỏ";
    dialogRef.componentInstance.visibleOkButton = true;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.afterClosed().subscribe(async result => {
      if (result === "confirm") {
        await this.swFacadeService
          .getSwMayBomTramBomService()
          .deleteItem(data.id)
          .subscribe(res => this.getAllMayBomTramBom());
      }
    });
  }

  // dialog thông báo không thể xóa
  cantDeleteDialog(sMsg: string) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Không thể xóa!";
    dialogRef.componentInstance.content = "<b>" + sMsg + "</b>";
    dialogRef.componentInstance.cancelButton = "Đóng";
    dialogRef.componentInstance.visibleOkButton = false;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.componentInstance.headerColor = "red";
  }

  // Hàm sửa thông tin chi tiết một bản ghi, được gọi khi nhấn nút xem chi tiết trên giao diện list
  public editItemThietBiLapDat(data) {
    this.matSidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.matSidenavService.setTitle("Sửa dữ liệu máy bơm");
    this.matSidenavService.setContentComp(
      TbThietbilapdatIoComponent,
      "edit",
      data
    );
    this.matSidenavService.open();
  }

  // Convert string thành dd/mm/yyyy
  public convertDateTimeString(data) {
    const [date, time]: string[] = data.thoigianlapdat.split(' ');
    const [year, month, day]: string[] = date.split('-');
    if (year.length === 4) {
      const awesomeDateTime = `${day}-${month}-${year}`;
      return awesomeDateTime;
    } else {
      const awesomeDateTime1 = `${year}-${month}-${day}`;
      return awesomeDateTime1;
    }
  }
  customiseCell(args: QueryCellInfoEventArgs) {
    if (args.column.field === "id") {
      args.cell.id = args.data[args.column.field];
    }
  }
  // Hàm dùng để gọi các hàm khác, truyền vào tên hàm cần thực thi
  doFunction(methodName) {
    this[methodName]();
  }
}
