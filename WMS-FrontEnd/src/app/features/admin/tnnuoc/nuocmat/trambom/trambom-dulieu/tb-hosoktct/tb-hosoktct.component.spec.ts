import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TbHosoktctComponent } from './tb-hosoktct.component';

describe('TbHosoktctComponent', () => {
  let component: TbHosoktctComponent;
  let fixture: ComponentFixture<TbHosoktctComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TbHosoktctComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TbHosoktctComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
