import {Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {MatDialog, MatSidenav} from "@angular/material";
import {DataStateChangeEventArgs} from "@syncfusion/ej2-angular-grids";
import {SettingsCommon} from "src/app/shared/constants/setting-common";
import {Observable} from "rxjs";
import {AdminRoutingName} from "src/app/routes/routes-name";
import {Router} from "@angular/router";
import {TnnuocFacadeService} from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
import {ProgressService} from "src/app/core/services/utilities/progress.service";
import {CommonServiceShared} from "src/app/core/services/utilities/common-service";
import {MatsidenavService} from "src/app/core/services/utilities/matsidenav.service";
import {SubHeaderService} from "src/app/core/services/utilities/subheader.service";
import {MyAlertDialogComponent} from "src/app/shared/components/my-alert-dialog/my-alert-dialog.component";
import {HttpErrorResponse} from "@angular/common/http";
import {OutputTrambomModel} from "src/app/models/admin/tnnuoc/trambom.model";
import {TrambomIoComponent} from "src/app/features/admin/tnnuoc/nuocmat/trambom/trambom-io/trambom-io.component";

@Component({
  selector: 'app-trambom-list',
  templateUrl: './trambom-list.component.html',
  styleUrls: ['./trambom-list.component.scss']
})
export class TrambomListComponent implements OnInit {

  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compTrambomio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;

  // Paging
  public swTramBomService: any;
  public state: DataStateChangeEventArgs;
  public settingsCommon = new SettingsCommon();
  public listTramBom: Observable<DataStateChangeEventArgs>;
  listData: any;
  listDataTramBom: any;

  // Add or edit item
  public selectedItem: OutputTrambomModel;

  navArray = [
    { title: "Quản trị", url: "/" },
    {
      title: "Quan trắc tài nguyên nước",
      url: "/" + AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri
    },
    {
      title: "Trạm bơm",
      url:
        "/" +
        AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.swTramBomUri
    },
    {
      title: "Danh sách trạm bơm ",
      url:
        "/" +
        AdminRoutingName.adminUri +
        "/" +
        AdminRoutingName.wrUri +
        "/" +
        AdminRoutingName.swTramBomUri +
        "/" +
        AdminRoutingName.swTrambom
    }
  ];

  buttonArray = [];

  constructor(
    public router: Router,
    public modalDialog: MatDialog,
    public cfr: ComponentFactoryResolver,
    public wrFacadeService: TnnuocFacadeService,
    public progressService: ProgressService,
    public commonService: CommonServiceShared,
    public matsidenavService: MatsidenavService,
    private subHeaderService: SubHeaderService
  ) {
    // Get new service
    this.swTramBomService = this.wrFacadeService.getSwTrambomService();
  }

  ngOnInit() {
    this.subHeaderService.setParentComp(this);
    this.matsidenavService.setSidenav(this.matSidenav, this, this.content, this.cfr);
    this.getTramBom();
    this.dataSubHeader();
  }

  /**
   * Hàm truyền data vào SubHeader
   */
  dataSubHeader() {
    this.subHeaderService.navArray = this.navArray;
    this.subHeaderService.buttonArray = this.buttonArray;
  }

  async getTramBom() {
    this.listData = await this.wrFacadeService.getSwTrambomService().getFetchAll({pageNumber: 1, pageSize: -1});
    if (this.listData.items) {
      this.listData.items.map((item, index) => {
        item.serialNumber = index + 1;
      });
    }
    this.listDataTramBom = this.listData.items;
  }
  // /**
  //  * Hàm get data Trạm bơm (phân trang bên server)
  //  */
  // async getTrambom() {
  //   this.listTramBom = this.swTramBomService;
  //   const pageSize = this.settingsCommon.pageSettings.pageSize;
  //   this.swTramBomService.getDataFromServer({ skip: 0, take: pageSize });
  //   console.log(this.listTramBom);
  // }
  //
  // /**
  //  * Hàm get data Trạm bơm (phân trang bên server) khi click pagination
  //  */
  // public dataStateChange(state: DataStateChangeEventArgs): void {
  //   this.swTramBomService.getDataFromServer(state);
  // }

  /**
   * Hàm open Sidenav thêm mới trạm bơm và khởi tạo form
   */
  public openTramBomIOSidebar() {
    this.matsidenavService.setTitle("Thêm mới trạm bơm");
    this.matsidenavService.setContentComp(TrambomIoComponent, "new");
    this.matsidenavService.open();
  }

  /**
   * Hàm open Sidenav sửa trạm bơm và truyền data vào form
   * @param data
   */
  async editItemTramBom(data) {
        this.matsidenavService.setTitle("Sửa trạm bơm");
        this.matsidenavService.setContentComp(TrambomIoComponent, "edit", data);
        this.matsidenavService.open();
  }

  /**
   * Hàm điều hướng đến trang thông tin chung của trạm bơm đó
   * @param id
   */
  public detailItem(id) {
    this.router.navigate([
      AdminRoutingName.adminUri + "/" + AdminRoutingName.wrUri + "/" + AdminRoutingName.swTramBomUri + "/"
      + AdminRoutingName.swTrambom + "/", id
    ]);
  }

  /**
   * Hàm delete
   * @param data
   */
  public deleteItem(data) {
    this.selectedItem = data;
    const canDelete: string = this.wrFacadeService.getSwTrambomService().checkBeDeleted(this.selectedItem.id);
    this.canBeDeletedCheck(canDelete);
  }

  /**
   * Hàm check delete
   * @param sMsg
   */
  public canBeDeletedCheck(sMsg: string) {
    if (sMsg === 'ok') {
      this.confirmDeleteDiaLog();
    } else {
      this.canDeleteDialog(sMsg);
    }
  }

  /**
   * Hàm open dialog thông báo có chắc chắn xóa không
   */
  confirmDeleteDiaLog() {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = 'Chắc chắn xóa?';
    dialogRef.componentInstance.content = 'Tên trạm bơm: <b>' + this.selectedItem.tendiem + '</b>';
    dialogRef.componentInstance.okeButton = 'Đồng ý';
    dialogRef.componentInstance.cancelButton = 'Hủy bỏ';
    dialogRef.componentInstance.visibleOkButton = true;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'confirm') {
        this.wrFacadeService.getSwTrambomService().deleteItem(this.selectedItem.id)
          .subscribe(res => this.getTramBom(), (error: HttpErrorResponse) => {
              this.commonService.showError(error);
            },
            () =>
              this.commonService.showeNotiResult(
                "Đã xóa trạm bơm thành công!",
                2000
              ));
      }
    });
  }

  /**
   * Hàm open dialog thông báo không thể xóa
   * @param sMsg
   */
  canDeleteDialog(sMsg: string) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = 'Không thể xóa!';
    dialogRef.componentInstance.content = '<b>' + sMsg + '</b>';
    dialogRef.componentInstance.cancelButton = 'Đóng';
    dialogRef.componentInstance.visibleOkButton = false;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.componentInstance.headerColor = 'red';
  }

  /**
   * Hàm close Sidenav Trạm bơm
   */
  public closeTramBomIOSidebar() {
    this.matSidenav.close();
  }

  /**
   * Call method
   * @param methodName
   */
  doFunction(methodName) {
    console.log("TrambomListComponent -> doFunction -> methodName", methodName)
    this[methodName]();
  }

}
