import {
  Component,
  ComponentFactoryResolver,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {VanHanh} from "src/app/shared/constants/vanhanh-constants";
import {STATUS} from "src/app/shared/constants/status-constants";
import {MatSidenav} from "@angular/material/sidenav";
import {MatsidenavService} from "src/app/core/services/utilities/matsidenav.service";
import {TnnuocFacadeService} from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
import {ActivatedRoute} from "@angular/router";
import {DmFacadeService} from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import {TrambomIoComponent} from "src/app/features/admin/tnnuoc/nuocmat/trambom/trambom-io/trambom-io.component";
import {CommonFacadeService} from "src/app/service/admin/common/common-facade.service";
import {MapService} from "src/app/core/services/utilities/map.service";

@Component({
  selector: 'app-tb-thongtinchung',
  templateUrl: './tb-thongtinchung.component.html',
  styleUrls: ['./tb-thongtinchung.component.scss']
})
export class TbThongtinchungComponent implements OnInit {

  public obj: any = {};
  public vanHanh = VanHanh;
  public status = STATUS;
  @Output("getInformation") getInformation: EventEmitter<any> = new EventEmitter();
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compTramBomIo", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  public dvqlData: any;
  public dvql: any;
  public caNhanData: any;
  public caNhan: any;
  public dataTinh: any;
  public dataHuyen: any;
  public dataXa: any;
  public tinh: any;
  public huyen: any;
  public xa: any;

  constructor(
    public matsidenavService: MatsidenavService,
    public cfr: ComponentFactoryResolver,
    public wrFacadeService: TnnuocFacadeService,
    private route: ActivatedRoute,
    public dmFacadeService: DmFacadeService,
    public commonFacadeService: CommonFacadeService,
    public mapService: MapService
  ) {}

  ngOnInit() {
    this.getTrambom();
  }

  async showMap() {
    await this.mapInit();
    await this.zoomToObj();
  }

  /**
   * hàm get data Trạm bơm bằng id lấy từ url
   */
  async getTrambom() {
    this.getInformation.emit();
    const idurl = this.route.snapshot.paramMap.get("id");
    const id = +idurl;
    await this.wrFacadeService
      .getSwTrambomService()
      .getByid(id)
      .subscribe(trambom => {
        this.obj = trambom;
        this.showMap();
        this.showDVQL(this.obj.IdDonvivanhanh);
        this.showCaNhan(this.obj.idChusudung);
        this.showTinh(this.obj.matinh);
        this.showHuyen(this.obj.matinh, this.obj.mahuyen);
        this.showXa(this.obj.mahuyen, this.obj.maxa);
      });
  }

  /**
   * Get name Nguồn cấp nước
   * @param bodyKey
   */
  // async getNameNguonCapNuoc(bodyKey) {
  //   this.allNguonCapNuoc = await this.commonFacadeService.getObjKeyService().getFetchAll();
  //   for (let i of this.allNguonCapNuoc) {
  //     if (i.objKey === bodyKey) {
  //       this.nameNguonCapNuoc = i.objName;
  //     }
  //   }
  // }

  /**
   * Lấy về tên nơi cấp nước
   * @param objKey
   * @param id
   */
  // async getNameNoiCapNuoc(objKey, id) {
  //   this.nameNoiCapNuoc = await this.commonFacadeService.getObjKeyService().getDataByIdObjKey(objKey, id);
  //   this.noiCapNuoc = this.nameNoiCapNuoc.name;
  // }

  /**
   * Hàm open Sidenav sửa trạm bơm
   */
  toEditMode() {
    this.matsidenavService.setSidenav(this.matSidenav, this, this.content, this.cfr);
    this.matsidenavService.setTitle("Sửa dữ liệu thông tin trạm bơm");
    this.matsidenavService.setContentComp(TrambomIoComponent, "edit", this.obj);
    this.matsidenavService.open();
  }

  /**
   * Hàm close Sidenav Trạm bơm
   */
  public closeTramBomIOSidebar() {
    this.matSidenav.close();
  }

  // Khởi tạo bản đồ
  async mapInit() {
    this.mapService.mapInit('viewTram');
  }

  /**@function zoomToObj
   * Hàm zoom tới đối tượng trên bản đồ, bật popup tên công trình
   */
  public zoomToObj() {
    this.mapService.zoomToPoint(this.obj, this.obj.tendiem, this.obj.vitri);
  }
  /**
   * Get đơn vị quản lý theo id
   * @param id
   */
  async showDVQL(id: number) {
    if (id) {
      this.dvqlData = await this.dmFacadeService
        .getDmCoquantochucService()
        .getByid(id).subscribe(res => {
          res ? this.dvql = res.tencoquan : this.dvql = '';
        });
    }
  }

  /**
   * Hàm get tên Cá nhân theo id
   * @param id
   */
  async showCaNhan(id: number) {
    if (id) {
      this.caNhanData = await this.dmFacadeService
        .getDmCanhanService()
        .getByid(id).subscribe(res => {
          res ? this.caNhan = res.hovaten : this.caNhan = '';
        });
    }
  }


  async showTinh(id: number) {
    if (id) {
      this.dataTinh = await this.dmFacadeService
        .getProvinceService()
        .getFetchAll();
      if (this.dataTinh) {
        this.dataTinh.map(res => {
          if (res.matinh === id) {
            this.tinh = res.tendvhc;
          }
        });
      }
    }
  }

  async showHuyen(idtinh: number, idhuyen: number) {
    if (idtinh) {
      this.dataHuyen = await this.dmFacadeService
        .getDistrictService()
        .getByid(idtinh).subscribe( res => {
          if (res) {
            res.map(res1 => {
              if (res1.mahuyen === idhuyen) {
                this.huyen = res1.tendvhc;
              }
            });
          }
        });
    }
  }

  async showXa(idhuyen: number, idxa: number) {
    if (idxa) {
      await this.dmFacadeService
        .getWardService()
        .getByid(idhuyen).subscribe( res => {
          if (res) {
            res.map(res1 => {
              if (res1.maxa === idxa) {
                this.xa = res1.tendvhc;
              }
            });
          }
        });
    }
  }

  /**
   * Get tên vận hành theo id
   * @param id
   */
  showTenVanHanh(id: number) {
    for (let v of this.vanHanh) {
      if (v.id === id) {
        return v.ten;
      }
    }
  }

  /**
   * Get tên trạng thái theo id
   * @param id
   */
  showTrangThai(id: number) {
    for (let s of this.status) {
      if (s.id === id) {
        return s.name;
      }
    }
  }

  /**
   * call method
   * @param methodName
   */
  doFunction(methodName) {
    this[methodName]();
  }

}
