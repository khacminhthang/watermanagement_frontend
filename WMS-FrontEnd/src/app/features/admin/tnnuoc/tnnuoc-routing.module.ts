import { TnnuocComponent } from "src/app/features/admin/tnnuoc/tnnuoc.component";
import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { AdminRoutingName } from "src/app/routes/routes-name";
import { DiemxathaiComponent } from "./nuocmat/diemxathai/diemxathai.component";
import { DiemxathaiListComponent } from "./nuocmat/diemxathai/diemxathai-list/diemxathai-list.component";
import { DiemxathaiDulieuComponent } from "./nuocmat/diemxathai/diemxathai-dulieu/diemxathai-dulieu.component";
import { DiemkhaithacComponent } from "./nuocmat/diemkhaithac/diemkhaithac.component";
import { DiemkhaithacListComponent } from "./nuocmat/diemkhaithac/diemkhaithac-list/diemkhaithac-list.component";
import { DiemkhaithacDulieuComponent } from "./nuocmat/diemkhaithac/diemkhaithac-dulieu/diemkhaithac-dulieu.component";
import { TrambomComponent } from "./nuocmat/trambom/trambom.component";
import { TrambomListComponent } from "./nuocmat/trambom/trambom-list/trambom-list.component";
import { TrambomDulieuComponent } from "./nuocmat/trambom/trambom-dulieu/trambom-dulieu.component";

const wrRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "",
        children: [
          { path: "", component: TnnuocComponent },
          {
            path: AdminRoutingName.diUri,
            children: [
              { path: "", component: DiemxathaiComponent },
              {
                path: AdminRoutingName.pdiUri,
                component: DiemxathaiListComponent,
              },
              {
                path: AdminRoutingName.pdiUri + "/:id",
                component: DiemxathaiDulieuComponent,
              },
              {
                path: AdminRoutingName.pdiUri + "/:id/:active",
                component: DiemxathaiDulieuComponent,
              },
            ],
          },
          {
            path: AdminRoutingName.swexUri,
            children: [
              { path: "", component: DiemkhaithacComponent },
              {
                path: AdminRoutingName.swDiemkhaithac,
                component: DiemkhaithacListComponent,
              },
              {
                path: AdminRoutingName.swDiemkhaithac + "/:id",
                component: DiemkhaithacDulieuComponent,
              },
              {
                path: AdminRoutingName.swDiemkhaithac + "/:id/:active",
                component: DiemkhaithacDulieuComponent,
              },
            ],
          },
          {
            path: AdminRoutingName.swTramBomUri,
            children: [
              { path: "", component: TrambomComponent },
              {
                path: AdminRoutingName.swTrambom,
                component: TrambomListComponent,
              },
              {
                path: AdminRoutingName.swTrambom + "/:id",
                component: TrambomDulieuComponent,
              },
              {
                path: AdminRoutingName.swTrambom + "/:id/:active",
                component: TrambomDulieuComponent,
              },
            ],
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(wrRoutes)],
  exports: [RouterModule],
  declarations: [],
})
export class TnnuocRoutingModule {}
