import { TnnuocRoutingModule } from "src/app/features/admin/tnnuoc/tnnuoc-routing.module";
import { TnnuocComponent } from "src/app/features/admin/tnnuoc/tnnuoc.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { GridModule } from "@syncfusion/ej2-angular-grids";
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { MatSelectFilterModule } from "mat-select-filter";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "src/app/shared/material.module";
import { RouterModule } from "@angular/router";
import { SharedModule } from "src/app/shared/shared.module";
import { TnnuocFacadeService } from "src/app/service/admin/tnnuoc/tnnuoc-facade.service";
import { DropDownButtonModule } from "@syncfusion/ej2-angular-splitbuttons";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AdminSharedModule } from "src/app/features/admin/admin-shared.module";
import { DatePipe } from "@angular/common";
import {
  OwlDateTimeModule,
  OwlNativeDateTimeModule,
  DateTimeAdapter,
  OWL_DATE_TIME_LOCALE,
  OWL_DATE_TIME_FORMATS,
} from "ng-pick-datetime";
import { MomentDateTimeAdapter } from "ng-pick-datetime-moment";
import { DiemkhaithacListComponent } from "src/app/features/admin/tnnuoc/nuocmat/diemkhaithac/diemkhaithac-list/diemkhaithac-list.component";
import { DiemkhaithacIoComponent } from "src/app/features/admin/tnnuoc/nuocmat/diemkhaithac/diemkhaithac-io/diemkhaithac-io.component";
import { DiemkhaithacComponent } from "src/app/features/admin/tnnuoc/nuocmat/diemkhaithac/diemkhaithac.component";
import { DiemkhaithacDulieuComponent } from "src/app/features/admin/tnnuoc/nuocmat/diemkhaithac/diemkhaithac-dulieu/diemkhaithac-dulieu.component";
import { TrambomComponent } from "src/app/features/admin/tnnuoc/nuocmat/trambom/trambom.component";
import { TrambomListComponent } from "src/app/features/admin/tnnuoc/nuocmat/trambom/trambom-list/trambom-list.component";
import { TrambomIoComponent } from "src/app/features/admin/tnnuoc/nuocmat/trambom/trambom-io/trambom-io.component";
import { TrambomDulieuComponent } from "src/app/features/admin/tnnuoc/nuocmat/trambom/trambom-dulieu/trambom-dulieu.component";
import { DiemxathaiComponent } from "src/app/features/admin/tnnuoc/nuocmat/diemxathai/diemxathai.component";
import { DiemxathaiListComponent } from "src/app/features/admin/tnnuoc/nuocmat/diemxathai/diemxathai-list/diemxathai-list.component";
import { DiemxathaiIoComponent } from "src/app/features/admin/tnnuoc/nuocmat/diemxathai/diemxathai-io/diemxathai-io.component";
import { DiemxathaiDulieuComponent } from "src/app/features/admin/tnnuoc/nuocmat/diemxathai/diemxathai-dulieu/diemxathai-dulieu.component";
import { DktThongtinchungComponent } from "src/app/features/admin/tnnuoc/nuocmat/diemkhaithac/diemkhaithac-dulieu/dkt-thongtinchung/dkt-thongtinchung.component";
import { TbThongtinchungComponent } from "src/app/features/admin/tnnuoc/nuocmat/trambom/trambom-dulieu/tb-thongtinchung/tb-thongtinchung.component";
import { TbHosoktctComponent } from "src/app/features/admin/tnnuoc/nuocmat/trambom/trambom-dulieu/tb-hosoktct/tb-hosoktct.component";
import { DktHosoktctComponent } from "src/app/features/admin/tnnuoc/nuocmat/diemkhaithac/diemkhaithac-dulieu/dkt-hosoktct/dkt-hosoktct.component";
import { DktThietbilapdatListComponent } from "src/app/features/admin/tnnuoc/nuocmat/diemkhaithac/diemkhaithac-dulieu/dkt-hosoktct/dkt-thietbilapdat/dkt-thietbilapdat-list/dkt-thietbilapdat-list.component";
import { DktThietbilapdatIoComponent } from "src/app/features/admin/tnnuoc/nuocmat/diemkhaithac/diemkhaithac-dulieu/dkt-hosoktct/dkt-thietbilapdat/dkt-thietbilapdat-io/dkt-thietbilapdat-io.component";
import { TbThietbilapdatListComponent } from "src/app/features/admin/tnnuoc/nuocmat/trambom/trambom-dulieu/tb-hosoktct/tb-thietbilapdat/tb-thietbilapdat-list/tb-thietbilapdat-list.component";
import { TbThietbilapdatIoComponent } from "src/app/features/admin/tnnuoc/nuocmat/trambom/trambom-dulieu/tb-hosoktct/tb-thietbilapdat/tb-thietbilapdat-io/tb-thietbilapdat-io.component";
import { DxtThongtinchungComponent } from "src/app/features/admin/tnnuoc/nuocmat/diemxathai/diemxathai-dulieu/dxt-thongtinchung/dxt-thongtinchung.component";
import { DxtHosoktctComponent } from "src/app/features/admin/tnnuoc/nuocmat/diemxathai/diemxathai-dulieu/dxt-hosoktct/dxt-hosoktct.component";
// Format custom
export const MY_CUSTOM_FORMATS = {
  parseInput: "DD-MM-YYYY HH:mm:ss",
  fullPickerInput: "DD-MM-YYYY HH:mm:ss",
  datePickerInput: "DD-MM-YYYY",
  timePickerInput: " HH:mm:ss",
  monthYearLabel: "MMM YYYY",
  dateA11yLabel: "LL",
  monthYearA11yLabel: "MMMM YYYY",
};
@NgModule({
  declarations: [
    DiemkhaithacComponent,
    DiemkhaithacListComponent,
    DiemkhaithacIoComponent,
    DiemkhaithacDulieuComponent,
    DktThongtinchungComponent,
    TrambomComponent,
    TrambomListComponent,
    TrambomIoComponent,
    TrambomDulieuComponent,
    DiemxathaiComponent,
    DiemxathaiListComponent,
    DiemxathaiIoComponent,
    DiemxathaiDulieuComponent,
    TbThongtinchungComponent,
    TbHosoktctComponent,
    DktHosoktctComponent,
    DktThietbilapdatListComponent,
    DktThietbilapdatIoComponent,
    TbThietbilapdatListComponent,
    TbThietbilapdatIoComponent,
    DxtThongtinchungComponent,
    DxtHosoktctComponent,
    TnnuocComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    GridModule,
    ReactiveFormsModule,
    MatSelectFilterModule,
    CKEditorModule,
    RouterModule,
    SharedModule,
    FormsModule,
    DropDownButtonModule,
    NgbModule,
    AdminSharedModule,
    TnnuocRoutingModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
  ],
  providers: [
    TnnuocFacadeService,
    { provide: OWL_DATE_TIME_LOCALE, useValue: "vi" },
    {
      provide: DateTimeAdapter,
      useClass: MomentDateTimeAdapter,
      deps: [OWL_DATE_TIME_LOCALE],
    },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS },
    DatePipe,
  ],
  entryComponents: [
    DiemkhaithacIoComponent,
    TrambomIoComponent,
    DiemxathaiIoComponent,
    DktThietbilapdatIoComponent,
    TbThietbilapdatIoComponent
  ],
})
export class TnnuocModule {}
