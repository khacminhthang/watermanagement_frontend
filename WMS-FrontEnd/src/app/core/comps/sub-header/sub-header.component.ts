
import { Component, OnInit } from '@angular/core';
import { SubHeaderService } from 'src/app/core/services/utilities/subheader.service';

@Component({
  selector: 'app-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.scss']
})
export class SubHeaderComponent implements OnInit {
  constructor(private subHeaderService: SubHeaderService) {
      }

  subHeadernav: any;
  subHeaderbutton: any;
  ngOnInit() {
    this.getDataSubHeader();
  }
  public callActionButton(action: string) {
    this.subHeaderService.doParentFunction(action);
  }
  async getDataSubHeader() {
    this.subHeadernav = await this.subHeaderService.navArray ;
    this.subHeaderbutton = await this.subHeaderService.buttonArray ;
  }
}
