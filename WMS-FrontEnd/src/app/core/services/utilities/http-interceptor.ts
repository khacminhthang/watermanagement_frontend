import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {ProgressService} from './progress.service';
import {Router} from "@angular/router";

@Injectable()
export class ProgressHttpInterceptor implements HttpInterceptor {

  constructor(
    private progressService: ProgressService,
    private router: Router
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    this.progressService.show();
    return next
      .handle(req)
      .pipe(
        tap((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            setTimeout(() => {
              this.progressService.hide();
            }, 500);
          }
        }, (error) => {
          if (error.status === 401) {
            localStorage.removeItem('token');
            this.router.navigateByUrl('/login');
          }
          setTimeout(() => {
            this.progressService.hide();
          }, 500);
        })
      );
  }
}
