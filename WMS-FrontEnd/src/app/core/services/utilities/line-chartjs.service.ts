import {Injectable} from "@angular/core";
import {Chart} from "chart.js";
import "chartjs-plugin-zoom";
import "chartjs-plugin-annotation";
import "hammerjs";

@Injectable({
  providedIn: "root"
})
export class LineChartjsService {
  public chartElementId: string;
  public myChart: any;
  public chartColors = {
    red: "rgb(255, 99, 132)",
    orange: "rgb(255, 159, 64)",
    yellow: "rgb(255, 205, 86)",
    green: "rgb(75, 192, 192)",
    blue: "rgb(54, 162, 235)",
    purple: "rgb(153, 102, 255)",
    grey: "rgb(231,233,237)"
  };
  public dataset1 = {
    label: "Độ mặn",
    borderColor: this.chartColors.blue,
    borderWidth: 1,
    fill: false,
    data: [2, 10, 3, 6, 1, 10, 3, 6, 1, 10, 3, 6, 1, 10, 3, 6, 1, 10, 3, 6, 1],
    pointRadius: 3,
    pointBackgroundColor: "#fff"
  };
  public labels = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21];
  public opt = {
    responsive: true,
    title: {
      display: true,
      text: "Chart.js Draw Line on Chart"
    },
    tooltips: {
      mode: "index",
      intersect: true
    },
    scales: {
      xAxes: [{
        ticks: {
          max: 25,
          min: 0,
          stepSize: 1
        }
      }]
    },
    plugins: {
      zoom: {
        zoom: {
          sensitivity:0,
          // drag: true,
          enabled: true,
          mode: 'x',
          drag: {
            	 borderColor: 'rgb(54, 162, 235)',
            	 borderWidth: 1,
            	 backgroundColor: 'rgba(54, 162, 235, 0.3)',
            	 animationDuration: 0
            },
        }
      }
    },
    annotation: {
      annotations: [{
        type: "line",
        mode: "horizontal",
        scaleID: "y-axis-0",
        value: 5,
        borderColor: "rgb(255, 0, 0)",
        borderWidth: 2,
        borderDash: [5, 5],
        label: {
          backgroundColor: "rgba(255, 255, 255, 0.6)",
          fontColor: "rgb(0, 255, 0)",
          position: "right",
          yAdjust: -15,
          enabled: true,
          content: "Giới hạn trên (Tiêu chuẩn nước uống - Bộ Y Tế)"
        }
      },
        {
          type: "line",
          mode: "horizontal",
          scaleID: "y-axis-0",
          value: 2.3,
          borderColor: "rgb(0, 0, 255)",
          borderWidth: 2,
          borderDash: [5, 5],
          label: {
            backgroundColor: "rgba(255, 255, 255, 0.6)",
            fontColor: "rgb(0, 255, 0)",
            position: "right",
            yAdjust: 15,
            enabled: true,
            content: "Giới hạn dưới (Tiêu chuẩn nước uống - Bộ Y Tế)"
          }
        }]
    }
  };

  public setElement(id: string) {
    this.chartElementId = id;
  }
  public resetZoom(){
    this.myChart.resetZoom();
  }

  public initChart() {
    const canvas = <HTMLCanvasElement>document.getElementById(this.chartElementId);
    const ctx = canvas.getContext("2d");
    const conf = {
      type: "line",
      data: {
        labels: this.labels,
        datasets: [this.dataset1]
      },
      credits: {
        enabled: false
      },
      options: this.opt
    };
    // @ts-ignore
    this.myChart = new Chart(ctx, conf);
  }

  constructor() {
  }
}
