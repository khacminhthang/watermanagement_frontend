import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  Input,
  ComponentRef,
  Compiler,
  OnDestroy,
  ComponentFactoryResolver,
  Injectable
} from '@angular/core';

import {MatSidenav} from '@angular/material';


@Injectable()
export class SidenavService {
  private sidenav: MatSidenav;
  public sidenavTitle: string;
  public contentComp: any;
  public contentObj: any;
  private compRef:ComponentRef<any>
  private vcf: ViewContainerRef;
  private cfr: ComponentFactoryResolver;

  public setSidenav(sidenav: MatSidenav, vcf: ViewContainerRef, cfr: ComponentFactoryResolver) {
    this.sidenav = sidenav;
    this.vcf = vcf;
    this.cfr = cfr;
  }

  public open(): void {
    this.sidenav.open();
  }


  public close(): void {
    this.sidenav.close();
  }

  public toggle(): void {
    this.sidenav.toggle();
  }

  public setContentComp(comp: any, obj?: any) {
    this.contentComp = comp;
    const factory = this.cfr.resolveComponentFactory(this.contentComp);
    this.vcf.clear();
    this.compRef = this.vcf.createComponent(factory, 0);
    if (obj) {
      this.compRef.instance.obj = obj;
    }
    return this.compRef;
  }
  public setTitle(title: string) {
    this.sidenavTitle = title;
  }



}
