import { Injectable } from "@angular/core";
import { HighchartService } from "src/app/service/admin/common/highchart.service";
declare const L: any;

@Injectable({
  providedIn: "root",
})
export class MapService {
  public map;
  marker: any;
  markerGroup;
  iconDefault;
  public mapComp: any;
  constructor(private highchartSv: HighchartService) {
    const iconRetinaUrl = "assets/libs/leaflet/images/marker-icon-2x.png";
    const iconUrl = "assets/libs/leaflet/images/marker-icon.png";
    const shadowUrl = "assets/libs/leaflet/images/marker-shadow.png";
    this.iconDefault = L.icon({
      iconRetinaUrl,
      iconUrl,
      shadowUrl,
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      tooltipAnchor: [16, -28],
      shadowSize: [41, 41],
    });
  }

  /* Hàm hiển thị marker tương ứng tất cả các điểm
  / Truyền vào: tất cả đối tượng, icon điểm
  */
  changeMarker(list: any[], icon: any) {
    this.markerGroup = L.layerGroup().addTo(this.map);
    for (let i in list) {
      this.marker = L.marker([list[i].toadox, list[i].toadoy], {
        icon: icon,
      }).addTo(this.markerGroup);
      this.showPopup(list);
      // this.chartInit(this.highchartSv.dataChartTest);
    }
  }

  /* Hàm hiển thị popup thông tin chi tiết và chart
  / Truyền vào đối tượng cần hiển thị
  */
  showPopup(list: any[]) {
    for (let i in list) {
      let popupContent =
        '<div class="tabs">' +
        '<div class="tab" id="thong-tin">' +
        '<div class="content">' +
        "<b>Thông tin chi tiết</b>" +
        "<p>Tên trạm : " +
        list[i].tentram +
        "</p>" +
        "<p>Tên : " +
        list[i].ten +
        "</p>" +
        "</div>" +
        "</div>" +
        '<div class="tab" id="bieu-do">' +
        '<div class="content">' +
        "<b>Biểu đồ</b>" +
        '<div id = "myChart" style = "width: 350px; height: 300px; margin: 0 auto"></div>' +
        "</div>" +
        "</div>" +
        '<ul class="tabs-link">' +
        '<li class="tab-link"> <a style="color : red" href=' +
        location.href +
        "#thong-tin><span>Thông tin</span></a></li>" +
        '<li class="tab-link"> <a style="color : red" href=' +
        location.href +
        "#bieu-do><span>Biểu đồ</span></a></li>" +
        "</ul>" +
        "</div>";
      this.marker.bindPopup(popupContent);
    }
  }

  showPopupOneItem(ten, vitri) {
    let popupContent =
      '<div class="tabs">' +
      '<div class="tab" id="thong-tin">' +
      '<div class="content">' +
      "<b>Thông tin trạm</b>" +
      "<p>Tên : " +
      ten +
      "</p>" +
      "<p>Vị trí : " +
      vitri +
      "</p>" +
      "</div>" +
      "</div>";
    this.marker.bindPopup(popupContent).openPopup();
  }

  // showPopupOneItem(data) {
  //     let popupContent =
  //       '<div class="tabs">' +
  //       '<div class="tab" id="thong-tin">' +
  //       '<div class="content">' +
  //       '<b>Thông tin chi tiết</b>' +
  //       '<p>Tên điểm : ' + data.tendiem + '</p>' +
  //       '<p>Tên : ' + data.tendiem + '</p>' +
  //       '</div>' +
  //       '</div>';
  //     this.marker.bindPopup(popupContent).openPopup();
  //   }

  /* Hàm tạo biểu đồ
  / Truyền vào: data biểu đồ
  
  */
  chartInit(dataChart) {
    this.marker.on(
      "popupopen",
      function (e) {
        this.highchartSv.setSource(dataChart);
        this.highchartSv.preDataChart(dataChart, "gw");
        this.highchartSv.setChartName("Biểu đồ nước");
        this.highchartSv.setXTittle("Thời gian đo");
        this.highchartSv.setYTittle("Giá trị đo");
        this.highchartSv.chartInit1m("myChart", "line");
      },
      this
    );
  }

  /* Hàm zoom tới vị trí đối tượng đã chọn
  / Truyền vào:  đối tượng
  
  */
  zoomToPoint(object: any, ten, vitri) {
    this.removeMarker();
    this.markerGroup = L.layerGroup().addTo(this.map);
    this.map.setView([object.toadox, object.toadoy], 15);
    this.marker = L.marker([object.toadox, object.toadoy], {
      icon: this.iconDefault,
    }).addTo(this.markerGroup);
    this.showPopupOneItem(ten, vitri);
  }

  /* Hàm change View
  / Truyền vào tọa độ x ,y và zoom z
  
  */
  changeView(x: number, y: number, z: number) {
    this.map.setView([x, y], z);
  }

  /* Hàm change Zoom
  / Truyền vào level Zoom
  
  */
  changeZoom(z: number) {
    this.map.setZoom(z);
  }

  /* Hàm thay đổi data source với url dạng tileLayer
  / Truyền vào data source
  
  */
  changeDataSource(url: any) {
    let dataSource = L.tileLayer(url, {
      //subdomains:['mt0']
    });
    dataSource.addTo(this.map);
  }

  /* Hàm thay đổi data source với url dạng wms
  / Truyền vào data source , tên layer
  
  */
  changeDataWms(url: any, layer: string, maxZoom: number) {
    let dataSource = L.tileLayer.wms(url, {
      layers: layer,
      format: "image/png",
      transparent: true,

      maxZoom: maxZoom,
      opacity: 1,
    });
    dataSource.addTo(this.map);
  }

  /* Hàm xóa tất cả các marker trên map
  /
  
  */
  removeMarker() {
    if (this.markerGroup) {
      this.map.removeLayer(this.markerGroup);
    } else {
      return;
    }
  }

  // hàm khởi tạo bản đồ, truyền vào id element
  mapInit(id: string) {
    // street
    let google = L.tileLayer(
      "http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}",
      {
        maxZoom: 21,
        subdomains: ["mt0", "mt1", "mt2", "mt3"],
      }

    );
    // Hybrid
    // let google = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', {
    //   maxZoom: 20,
    //   subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    // });
    this.map = L.map(id, {
      layers: [google],
      zoomControl: false,
    }).setView([17, 105], 7);

    L.control
      .zoom({
        position: "topright",
      })
      .addTo(this.map);

    // L.easyButton({
    //   position: 'topright',
    //   states: [{
    //     icon:      'fa-refresh',
    //     title:     'Reset zoom',
    //     onClick: function(btn, map) {
    //       map.setView([17,105],7);
    //     },
    //
    //   }]
    // }).addTo(this.map);
  }

  // hàm get feature info
  public getFeatureInfo() {
    var MySource = L.WMS.Source.extend({
      showFeatureInfo: function (latlng, info) {
        var obj = JSON.parse(info);
        this.mapComp.openSidenav(obj);
      }.bind(this),
    });
    var getInfo = new MySource(
      "http://localhost:8080/geoserver/hanhchinhvn/wms",
      {
        format: "image/png",
        transparent: true,
        info_format: "application/json",
        feature_count: 100,
      }
    );

    getInfo.getLayer("hanhchinhvn:diemkhaithac").addTo(this.map);
    // getInfo.getLayer("hanhchinhvn:diemxathai").addTo(this.map);
  }

  public openSidenav(obj) { }
}
