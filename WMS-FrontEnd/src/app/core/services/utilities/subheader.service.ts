import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SubHeaderService {

  parentComp: any;
  navArray: any;
  buttonArray: any;
constructor() {
}

// hàm xác định component cha
public setParentComp(parentComp: any) {
  this.parentComp = parentComp;
}

// hàm gọi các function ở component cha
public doParentFunction(fname: string) {
  this.parentComp.doFunction(fname);
}
}
