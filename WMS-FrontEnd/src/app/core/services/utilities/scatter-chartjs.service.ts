import { Injectable } from "@angular/core";
import { Chart } from "chart.js";
import "chartjs-plugin-zoom";
import "chartjs-plugin-annotation";
import "hammerjs";

@Injectable({
  providedIn: "root"
})
export class ScatterChartjsService {
  public chartElementId: string;
  public myChart: any;
  public chartColors = {
    red: "rgb(255, 99, 132)",
    orange: "rgb(255, 159, 64)",
    yellow: "rgb(255, 205, 86)",
    green: "rgb(75, 192, 192)",
    blue: "rgb(54, 162, 235)",
    purple: "rgb(153, 102, 255)",
    grey: "rgb(231,233,237)"
  };

  public dataset1 = {
    label: "Scatter Dataset",
    data: [
      { x: -3, y: 0 },
      { x: 0, y: 10 },
      { x: 1, y: 2 },
      { x: 2, y: 10 },
      { x: 3, y: 3 },
      { x: 4, y: 6 },
      { x: 10, y: 5 }
    ],
    borderColor: this.chartColors.blue,
    borderWidth: 1,
    showLine: true,
    fill: false
  };
  public opt = {
    responsive: true,
    title: {
      display: true,
      text: "Chart.js Draw Line on Chart"
    },
    tooltips: {
      mode: "index",
      intersect: true
    },
    scales: {
      xAxes: [{
        ticks: {
          max: 10,
          min: -5,
          stepSize: 1
        }
      }]
    },
    plugins: {
      zoom: {
        zoom: {
          sensitivity: 0,
          // drag: true,
          enabled: true,
          mode: "x",
          drag: {
            borderColor: "rgb(54, 162, 235)",
            borderWidth: 1,
            backgroundColor: "rgba(54, 162, 235, 0.3)",
            animationDuration: 0
          },
        }
      }
    },
    annotation: {
      annotations: [
        {
          drawTime: "afterDraw",
          id: "hline",
          type: "line",
          mode: "horizontal",
          scaleID: "y-axis-0",
          value: 5,
          borderColor: this.chartColors.red,
          borderWidth: 2,
          label: {
            backgroundColor: "rgba(255, 255, 255, 0.6)",
            fontColor: this.chartColors.red,
            position: "right",
            yAdjust: -12,
            enabled: true,
            content: "Giới hạn trên (Tiêu chuẩn nước uống - Bộ Y Tế)"
          }
        }
      ]
    }
  };

  public setElement(id: string) {
    this.chartElementId = id;
  }

  resetZoom() {
    this.myChart.resetZoom();
  }

  public initChart() {
    const canvas = <HTMLCanvasElement>document.getElementById(this.chartElementId);
    const ctx = canvas.getContext("2d");
    const conf = {
      type: "scatter",
      data: {
        datasets: [this.dataset1]
      },
      credits: {
        enabled: false
      },
      options: this.opt
    };
    // @ts-ignore
    this.myChart = new Chart(ctx, conf);
  }

  constructor() {
  }
}
