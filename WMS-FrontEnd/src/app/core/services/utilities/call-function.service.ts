import { Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CallFunctionService {
  public parentComp: any;
  constructor() { }

  public setComp(parentComp: any) {
    this.parentComp = parentComp;
  }

  public callFunctionComp(fname: string) {
    this.parentComp.doFunction(fname);
  }

}
