import { Injectable } from "@angular/core";
import { catchError, map } from "rxjs/operators";
import { Observable, throwError, Subject } from "rxjs";
import {
  HttpErrorResponse,
  HttpClient,
  HttpXhrBackend,
  HttpHeaders,
} from "@angular/common/http";
import {
  DataStateChangeEventArgs,
  DataResult,
} from "@syncfusion/ej2-angular-grids";

/**
 * Api url
 * Input model name
 * Output model name
 */
let apiUrl;
let inputModelName;
let outputModelName;

@Injectable({
  providedIn: "root",
})
export class RepositoryEloquentService extends Subject<
  DataStateChangeEventArgs
> {
  /**
   * @type {HttpClient} httpClient
   */
  public httpClient = new HttpClient(
    new HttpXhrBackend({
      build: () => new XMLHttpRequest(),
    })
  );

  public headers = new HttpHeaders({
    Accept: "multipart/form-data",
    "Content-Type": "application/json; charset=utf-8",
    Authorization: `Bearer ${localStorage.getItem("token")}`,
  });

  /**
   * Return query string from object
   * @param obj Object
   * @returns String
   */
  public convertObjectToQueryString(obj: Object) {
    return Object.keys(obj)
      .map((key) => key + "=" + encodeURIComponent(obj[key]))
      .join("&");
  }

  /**
   * Function set model from child class
   */
  public setServiceInfo(serviceInfo) {
    apiUrl =
      typeof serviceInfo.apiUrl !== undefined ? serviceInfo.apiUrl : apiUrl;
    inputModelName =
      typeof serviceInfo.inputModelName !== undefined
        ? serviceInfo.inputModelName
        : inputModelName;
    outputModelName =
      typeof serviceInfo.outputModelName !== undefined
        ? serviceInfo.outputModelName
        : outputModelName;
  }

  /**
   * Get data from server
   * @param state DataStateChangeEventArgs
   */
  public getDataFromServer(
    state: DataStateChangeEventArgs,
    params: object = {}
  ): void {
    this.getData(state, params).subscribe((x) => {
      super.next(x);
    });
  }

  /**
   * Get data from server
   * @param state DataStateChangeEventArgs
   * @param params Search data
   */
  public getData(state, params): Observable<DataStateChangeEventArgs> {
    // Get query string
    let queryString = this.convertObjectToQueryString({
      ...params,
      PageSize: state.take,
      PageNumber: state.skip / state.take + 1,
    });

    // Get data
    return this.httpClient
      .get(`${apiUrl}?${queryString}`)
      .pipe(
        map((response: any) => {
          if (response) {
            // Set serial number
            response.items.map((item, index) => {
              item.serialNumber =
                index +
                1 +
                (response.paging.pageNumber - 1) * response.paging.pageSize;
            });

            // Format data result
            let dataresult = {
              result: response.items,
              count: response.paging.totalItems,
            } as DataResult;

            return dataresult;
          }
        })
      )
      .pipe((data: any) => {
        return data;
      });
  }

  /**
   * Get all data
   * @returns {Observable}
   */
  public getAll(params = {}): Observable<typeof outputModelName[]> {
    let queryString = this.convertObjectToQueryString(params);
    return this.httpClient
      .get<typeof outputModelName[]>(`${apiUrl}?${queryString}`, {
        headers: this.headers,
      })
      .pipe(catchError(this.errorHandler));
  }
  /**
   * Fetch all data
   * @returns {Promise}
   */
  public getByid(id: number): Observable<typeof inputModelName> {
    return this.httpClient.get<typeof inputModelName>(`${apiUrl}\\${id}`, {
      headers: this.headers,
    });
  }
  /**
   * Add new item into list data
   * @param {Object} body The data input.
   * @returns {Observable}
   */
  public addItemFile(body: any): Observable<typeof inputModelName> {
    const localHeader = new HttpHeaders();
    localHeader.append('Content-Type', 'application/json');
    localHeader.append('Accept', 'multipart/form-data');
    localHeader.append('Authorization',  'Bearer' + localStorage.getItem("token"));
    const options = {headers: localHeader};
    return this.httpClient.post<typeof inputModelName>(apiUrl, body, options);
  }

  /**
   * Add new item into list data
   * @param {Object} body The data input.
   * @returns {Observable}
   */
  public addItem(body: any): Observable<typeof inputModelName> {
    console.log(body, 'body')
    return this.httpClient.post<typeof inputModelName>(apiUrl, body, {
      headers: this.headers
    });
  }


  /**
   * Add new item into list data by form request
   * @param params
   */
  public addRequestItem(params = {}): Observable<typeof inputModelName> {
    let queryString = this.convertObjectToQueryString(params);
    return this.httpClient.post<typeof inputModelName>(
      `${apiUrl}?${queryString}`,
      {},
      {
        headers: this.headers,
      }
    );
  }
  /**
   * Update item
   * @param {Number} id Item id.
   * @param {Any} body Data input.
   * @returns {Observable}
   */
  public updateItem(id: number, body: any): Observable<typeof inputModelName> {
    return this.httpClient.put<typeof inputModelName>(`${apiUrl}/${id}`, body, {
      headers: this.headers,
    });
  }

  /**
   * Update item không cần id
   * @param {Number} id Item id.
   * @param {Any} body Data input.
   * @returns {Observable}
   */
  public updateBodyItem(body: any): Observable<typeof inputModelName> {
    return this.httpClient.put<typeof inputModelName>(`${apiUrl}`, body, {
      headers: this.headers,
    });
  }
  /**
   * Delete item
   * @param {Number} id Item id.
   * @returns {Observable}
   */
  public deleteItem(id: number): Observable<any> {
    return this.httpClient.delete<any>(`${apiUrl}/${id}`, {
      headers: this.headers,
    });
  }

  /**
   * Delete item
   * @param {Number} id Item id.
   * @returns {Observable}
   */
  public deleteRequestItem(params = {}): Observable<any> {
    let queryString = this.convertObjectToQueryString(params);
    return this.httpClient.delete<any>(`${apiUrl}?${queryString}`, {
      headers: this.headers,
    });
  }

  /**
   * Fetch all data
   * @returns {Promise}
   */
  public getFetchAll(params = {}) {
    let queryString = this.convertObjectToQueryString(params);
    if (!params === false) {
      return this.httpClient
        .get(`${apiUrl}?${queryString}`, {
          headers: this.headers,
        })
        .toPromise();
    } else {
      return this.httpClient
        .get(`${apiUrl}`, {
          headers: this.headers,
        })
        .toPromise();
    }
  }

  /**
   * get fetch all by id
   * @param id
   */
  public getFetchAllId(id: any) {
    return this.httpClient.get(`${apiUrl}/${id}`).toPromise();
  }
  /**
   * Catch error from Observable
   * @param {HttpErrorResponse} error Error.
   */
  errorHandler(error: HttpErrorResponse) {
    return throwError(error.message || "Server error");
  }
}
