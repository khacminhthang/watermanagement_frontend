import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {HethongFacadeService} from "src/app/service/admin/hethong/hethong-facade.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  userName = "";
  img: "";
  user: any;

  constructor(
    public hethongFacadeService: HethongFacadeService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.getInformationUser();
  }

  // Lấy thông tin người dùng
  async getInformationUser() {
    this.user = await this.hethongFacadeService.getUserService().getProfile();
    this.userName = this.user.fullName;
    console.log(this.user, 'user')
  }
  // Logout khỏi hệ thống
  public logoutAdmin() {
    localStorage.removeItem('token');
    window.location.href = 'http://localhost:4200';
  }
}
