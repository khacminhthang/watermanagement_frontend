import { Component, OnInit } from '@angular/core';
import { SubsidenavService } from "src/app/core/services/utilities/subsidenav.service";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";

@Component({
  selector: 'app-sub-sidenav',
  templateUrl: './sub-sidenav.component.html',
  styleUrls: ['./sub-sidenav.component.scss']
})
export class SubSidenavComponent implements OnInit {
  obj: any;
  status = true;

  activeIndex: number = 0;

  constructor(public subNav: SubsidenavService,
    public dmFacadeService: DmFacadeService) { }

  ngOnInit() {
  }

  // Get item tham so
  public getItem(item) {
    this.status = !this.status;
    this.subNav.item = item;
    this.subNav.doParentFunction('getKetQuaQuanTrac');
  }
}
