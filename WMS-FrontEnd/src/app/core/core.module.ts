import { NgModule, ViewContainerRef } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HeaderComponent } from "src/app/core/layout/header/header.component";
import { BrandComponent } from "src/app/core/layout/brand/brand.component";
import { HttpClientModule } from "@angular/common/http";
import { Router, RouterModule } from "@angular/router";
import { MaterialModule } from "src/app/shared/material.module";
import { ExComponentFactoryResolverService } from "src/app/core/services/utilities/ex-component-factory-resolver.service";

@NgModule({
  declarations: [
    HeaderComponent,
    BrandComponent,
  ],
  exports: [
    HeaderComponent,
  ],
  imports: [
    MaterialModule,
    CommonModule,
    HttpClientModule,
    RouterModule,

  ],
  entryComponents: [],
  providers: []
})
export class CoreModule {
  constructor(router: Router, exResolver: ExComponentFactoryResolverService) {
    exResolver.init();
  }
}
