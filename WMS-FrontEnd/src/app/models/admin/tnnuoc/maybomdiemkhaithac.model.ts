export class OutputMayBomDiemKhaiThac {
  id: number;
  idDiemkhaithac: number;
  loaimaybom: number;
  dosaulapdat: number;
  thoigianlapdat: string;
  congsuat: string;
  note: string;
}

export class InputMayBomDiemKhaiThac {
  id: number;
  idDiemkhaithac: number;
  loaimaybom: number;
  dosaulapdat: number;
  thoigianlapdat: string;
  congsuat: string;
  note: string;
}
