export class OutputMayBomTramBom {
  id: number;
  idTrambom: number;
  loaimaybom: number;
  thoigianlapdat: string;
  congsuat: string;
  note: string;
}

export class InputMayBomTramBom {
  id: number;
  idTrambom: number;
  loaimaybom: number;
  thoigianlapdat: string;
  congsuat: string;
  note: string;
}
