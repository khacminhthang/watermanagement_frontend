import { DeserializableModel } from "src/app/models/admin/deserializable.model";

export class OutputKhaithacnuocmatModel implements DeserializableModel {

  id: number;
  tendiem: string;
  sohieudiem: string;
  objKey: number;
  geox: number;
  geoy: number;
  toadox: number;
  toadoy: number;
  caodoz: number;
  srid: number;
  vitri: string;
  matinh: number;
  mahuyen: number;
  maxa: number;
  tentinh: string;
  tenhuyen: string;
  tenxa: string;
  idDonviquanly: number;
  donviquanly: string;
  idChusudung: number;
  tenchusudung: string;
  idBody: number;
  bodyKey: number;
  thoigianxaydung: string;
  thoigianvanhanh: string;
  idVanhanh: number;
  tendonvivanhanh: string;
  idMucdichsudungchinh: number;
  nhamay: string;
  luuluongchopheplonnhat: number;
  cothietbigiamsat: boolean;
  thoiluongkhaithac: number;
  note: string;
  dacapphep: boolean;
  imgLink: string;
  status: number;
  deserialize(input: any): this {
    return undefined;
  }
}

export class InputKhaithacnuocmatModel {
  id: number;
  objKey: number;
  tendiem: string;
  sohieudiem: string;
  toadox: number;
  toadoy: number;
  caodoz: number;
  srid: number;
  vitri: string;
  matinh: number;
  mahuyen: number;
  maxa: number;
  thoigianxaydung: string;
  thoigianvanhanh: string;
  idDonviquanly: number;
  idChusudung: number;
  idMucdichsudungchinh: number;
  luuluongchopheplonnhat: number;
  cothietbigiamsat: boolean;
  idVanhanh: number;
  thoiluongkhaithac: string;
  nhamay: string;
  idBody: number;
  bodyKey: number;
  dacapphep: boolean;
  note: string
  imgLink: string;
}
