import { DeserializableModel } from "src/app/models/admin/deserializable.model";

export class OutputXanuocthaiModel implements DeserializableModel {
  id: number;
  guid: string;
  objKey: number;
  geom: number;
  sohieudiem: string;
  tendiem: string;
  geox: number;
  geoy: number;
  toadox: number;
  toadoy: number;
  caodoz: number;
  srid: number;
  vitri: string;
  matinh: string;
  mahuyen: string;
  maxa: string;
  thoigianxaydung: string;
  thoigianvanhanh: string;
  idDonviquanly: number;
  idChusudung: number;
  idBody: number;
  thuyvuctiepnhan: string;
  phuongthucxa: string;
  hinhthucdan: string;
  luuluongxatrungbinh: number;
  luuluonglonnhatchophep: number;
  quychuanapdung: string;
  cothietbigiamsat: boolean;
  dacapphep: boolean;
  note: string;
  imgLink: string;
  status: number;
  createdAt: string;
  updatedAt: string;
  deletedAt: number;
  bodyKey: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class InputXanuocthaiModel {
  id: number;
  objKey: number;
  sohieudiem: string;
  tendiem: string;
  toadox: number;
  toadoy: number;
  caodoz: number;
  srid: number;
  vitri: string;
  matinh: string;
  mahuyen: string;
  maxa: string;
  thoigianxaydung: string;
  thoigianvanhanh: string;
  idDonviquanly: number;
  idChusudung: number;
  idBody: number;
  thuyvuctiepnhan: string;
  phuongthucxa: string;
  hinhthucdan: string;
  luuluongxatrungbinh: number;
  luuluonglonnhatchophep: number;
  quychuanapdung: string;
  cothietbigiamsat: boolean;
  dacapphep: boolean;
  note: string;
  imgLink: string;
  status: number;
  bodyKey: number;
}
