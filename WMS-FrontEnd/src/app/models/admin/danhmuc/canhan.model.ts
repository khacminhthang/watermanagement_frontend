import { DeserializableModel } from "../deserializable.model";

export class OutputCanhanModel implements DeserializableModel {
  id: number;
  hovaten: string;
  diachi: string;
  matinh: string;
  mahuyen: string;
  maxa: string;
  sodienthoai: string;
  email: string;
  loaigiayto: string;
  socmthochieu: string;
  note: string;
  imgLink: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class InputCanhanModel {
  id: number;
  hovaten: string;
  diachi: string;
  matinh: string;
  mahuyen: string;
  maxa: string;
  sodienthoai: string;
  email: string;
  loaigiayto: string;
  socmthochieu: string;
  note: string;
  imgLink: string;
}
