import { DeserializableModel } from "../deserializable.model";

export class OutputUserModel implements DeserializableModel {
  id: string;
  userName: string;
  normalizedUserName: string;
  email: string;
  normalizedEmail: string;
  emailConfirmed: boolean;
  passwordHash: string;
  securityStamp: string;
  concurrencyStamp: string;
  phoneNumber: string;
  phoneNumberConfirmed: boolean;
  twoFactorEnabled: boolean;
  lockoutEnd: string;
  lockoutEnabled: boolean;
  accessFailedCount: number;
  fullName: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class InputAddUserModel {
  userName: string;
  email: string;
  password: string;
  fullName: string;
  role: string;
}

export class InputUpdateUserModel {
  id: string;
  userName: string;
  email: string;
  fullName: string;
}
