import { DeserializableModel } from "../deserializable.model";

export class OutputRoleModel implements DeserializableModel {
  id: string;
  name: string;
  normalizedName: string;
  concurrencyStamp: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

