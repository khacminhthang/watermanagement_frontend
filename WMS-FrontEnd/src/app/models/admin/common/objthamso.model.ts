import { DeserializableModel } from '../deserializable.model';

export class OutputObjthamsoModel implements DeserializableModel {
  id: number;
  idthamso: number;
  tenthamso: string;
  tennhom: string;
  kyhieuthamso: string;
  tendovido: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class InputObjthamsoModel {
  idObj: number;
  objKey: number;
  idThamso: number;
}

export class InputArrayObjthamsoModel {
  jsonParameter: InputObjthamsoModel[];
}
