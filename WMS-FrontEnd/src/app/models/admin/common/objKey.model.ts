import { DeserializableModel } from '../deserializable.model';

export class OutputObjKeyModel implements DeserializableModel {
  id: number;
  objTable: string;
  note: string;
  objKey: number;
  objName: string

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class InputObjKeyModel {
  id: number;
  objTable: string;
  note: string;
  objKey: number;
  objName: string
}
