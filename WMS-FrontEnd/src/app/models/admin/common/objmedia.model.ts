export class InputObjGmediaModel {
  listIdMedia: number[];
  idObj: number;
  objKey: number;
  mediaType: string;
  idObjmediaType: number;
}

export class OutputObjGmediaModel {
  id: number;
  idObj: number;
  idMedia: number;
  objKey: number;
  mediaType: string;
  idObjmediaType: number;
  media: {
    id: number;
    tenFile: string;
    type: string;
    toadox: number;
    toadoy: number;
    caodoz: number;
    checked: boolean;
    srid: number;
    objKey: number;
    vitri: string;
    link: string;
    tieude: string;
    thoigiankhoitao: string;
    idLoaitailieu: number;
    iconFile: string;
    tacgia: string;
    nguoiky: string;
    kieutoado: string;
    note: string;
  };
  createdAt: string;
  updatedAt: string;
  deletedAt: number;
}
