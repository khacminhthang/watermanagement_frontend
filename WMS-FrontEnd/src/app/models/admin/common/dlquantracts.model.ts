import { DeserializableModel } from '../deserializable.model';

export class OutputDlquantractsModel implements DeserializableModel {
  id: number;
  fullTime: string;
  ketquado: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}

export class InputDlquantractsModel {
  id: number;
  idTramquantrac: number;
  objKey: number;
  idThamsodo: number;
  phuongphapdo: number;
  loaisolieu: string;
  fullTime: string;
  giatrido: number;
  note: string;
}
