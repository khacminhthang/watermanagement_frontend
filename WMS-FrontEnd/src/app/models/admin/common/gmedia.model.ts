export class GmediaModel {
  base64: string;
  type: string;
}

export  class GmediaArrayModel {
  jsonParameter: GmediaModel[];
}

export class OutputGmediaModel {
  id: number;
  tenFile: string;
  type: string;
  checked: boolean;
  objKey: number;
  link: string;
  tieude: string;
  thoigiankhoitao: string;
  idLoaitailieu: number;
  iconFile: string;
  tacgia: string;
  nguoiky: string;
  note: string;
  createdAt: string;
  updatedAt: string;
  deletedAt: number;
}

export class InputGmediaModel {
  id: number;
  objKey: number;
  link: string;
  tieude: string;
  thoigiankhoitao: string;
  idLoaitailieu: number;
  tacgia: string;
  nguoiky: string;
  note: string;
}
