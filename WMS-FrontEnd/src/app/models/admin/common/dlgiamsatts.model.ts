import { DeserializableModel } from '../deserializable.model';

export class OutputDlgiamsattsModel implements DeserializableModel {
  id: number;
  fullTime: string;
  ketquado: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
