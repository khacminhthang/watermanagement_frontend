import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'enumToArrayObject'
})
export class EnumToArrayObjectPipe implements PipeTransform {
  transform(enumObject): any {
    let results = Object.keys(enumObject).map((key) => {
      return {
        index: key,
        name: enumObject[key]
      }
    });
    return results;
  }
}
