import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EnumToArrayObjectPipe } from "src/app/shared/pipes/enum-to-array-object.pipe";
import { GetFieldValueFromArrayObjectPipe } from "src/app/shared/pipes/get-field-value-from-array-object.pipe";

@NgModule({
  declarations: [EnumToArrayObjectPipe, GetFieldValueFromArrayObjectPipe],
  imports: [
    CommonModule
  ],
  exports: [EnumToArrayObjectPipe, GetFieldValueFromArrayObjectPipe]
})

export class CommonPipesModule { }
