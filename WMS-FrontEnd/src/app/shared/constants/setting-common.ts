import {
  PageSettingsModel,
  ToolbarItems,
  EditSettingsModel
} from "@syncfusion/ej2-angular-grids";

export class SettingsCommon {
  public editSettings: EditSettingsModel;
  public toolbar: ToolbarItems[] = ["Search"];
  public pageSettings: PageSettingsModel = { pageSize: 10, pageCount: 5 };
}
