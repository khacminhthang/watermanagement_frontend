export class ObjKey {
  public static DiemXaThai = 6;
  public static TramBom = 7;
  public static KhaiThacNuocMat = 5;
}
export class ObjKeyArray {
  public static ArrayObj = [
    { name: "Điểm xả thải", value: 6 },
    { name: "Điểm khai thác nước mặt", value: 5 },
    { name: "Trạm bơm", value: 7 },
  ];
}
