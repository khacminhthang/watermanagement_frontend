// List layer type
export const LayerTypes = [
  {
    index: 'wms',
    name: 'WMS'
  },
  {
    index: 'geojson',
    name: 'GEOJSON'
  },
  {
    index: 'postgis',
    name: 'POSTGIS'
  },
  {
    index: 'shapefile',
    name: 'SHAPEFILE'
  }
];

// List status type
export const LayerStatus = [
  {
    index: 0,
    name: 'Khóa'
  },
  {
    index: 1,
    name: 'Sẵn sàng'
  }
];

// List status type
export const LayerGroupStatus = [
  {
    index: 0,
    name: 'Khóa'
  },
  {
    index: 1,
    name: 'Sẵn sàng'
  }
];

// List status type
export const LayerGroupTypes = [
  {
    index: 1,
    name: 'Cho phép thay đổi lớp thành phần'
  },
  {
    index: 0,
    name: 'Không cho phép thay đổi tùy chọn lớp'
  }
];

// Projection status
export const ProjectionStatus = [
  {
    index: 0,
    name: 'Khóa'
  },
  {
    index: 1,
    name: 'Sẵn sàng'
  }
];

// Map status
export const MapStatus = [
  {
    index: 0,
    name: 'Khóa'
  },
  {
    index: 1,
    name: 'Sẵn sàng'
  }
];

// Map status
export const MapCloneTypes = [
  {
    index: 0,
    name: 'Chọn Lớp/Nhóm lớp'
  },
  {
    index: 1,
    name: 'Copy lớp/nhóm lớp từ bản đồ khác'
  }
];

// Map unit
export const MapUnits = [
  {
    index: 'm',
    name: 'm'
  },
  {
    index: 'lat_long',
    name: 'Lat/long'
  }
];

// Category type
export const CategoryTypes = [
  {
    index: 'map',
    name: 'MAP'
  }
];

// Picture symbol
export const PictureSymbol = {
  type: "circle",
  url: "/assets/media/maps/map-marker.svg",
  width: "25px",
  height: "30px"
};

// Simple symbol
export const SimpleSymbol = {
  type: "simple-marker",
  color: "red",
  outline: {
    color: [128, 128, 128, 0.5],
    width: "8px"
  }
};

export const BaseMap = {
  name: 'streets-navigation-vector',
  description: 'World Navigation Map [v2]'
}


