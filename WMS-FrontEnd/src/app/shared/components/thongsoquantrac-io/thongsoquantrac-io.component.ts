import {Component, OnInit, ViewChild, ViewEncapsulation} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  displayFieldCssService,
  validationErrorMessagesService
} from "src/app/core/services/utilities/validatorService";
import { OutputNhomthamsoModel } from "src/app/models/admin/danhmuc/nhomthamso.model";
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { QueryCellInfoEventArgs } from "@syncfusion/ej2-grids";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { DetailRowService, GridComponent } from "@syncfusion/ej2-angular-grids";
import { DmFacadeService } from "src/app/service/admin/danhmuc/danhmuc-facade.service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ActivatedRoute } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";
import { InputArrayObjthamsoModel, InputObjthamsoModel } from "src/app/models/admin/common/objthamso.model";
import { CommonFacadeService } from "src/app/service/admin/common/common-facade.service";

@Component({
  selector: "app-thongsoquantrac-io",
  templateUrl: "./thongsoquantrac-io.component.html",
  styleUrls: ["./thongsoquantrac-io.component.scss"],
  providers: [DetailRowService],
  encapsulation: ViewEncapsulation.None
})
export class ThongsoquantracIoComponent implements OnInit {
  // @ts-ignore
  @ViewChild('grid') public grid: GridComponent;
  settingsCommon = new SettingsCommon();
  ThongsoIOForm: FormGroup;
  selected = '';
  submited = true;
  // obj la danh sách id thông số của trạm
  obj: any;
  public NhomthamsoFilters: OutputNhomthamsoModel[] = [];
  public allNhomthamso: any;
  public checked = false;
  public listdsthongso: any;
  public ldsthongso: number[] = [];
  public objthongsoinput: InputObjthamsoModel;
  public listobjthongsoinput: InputObjthamsoModel[] = [];
  public listArray: InputArrayObjthamsoModel = new InputArrayObjthamsoModel();
  validationErrorMessages = {
    idNhomthamso: { required: "Hãy chọn nhóm tham số!" }
  };

  // form errors
  formErrors = {
    idNhomthamso: ""
  };

  constructor(
    private formBuilder: FormBuilder,
    public dmFacadeService: DmFacadeService,
    public commonFacadeService: CommonFacadeService,
    public commonService: CommonServiceShared,
    public matSidenavService: MatsidenavService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.bindingConfigValidation();
    this.showNhomthamso();
  }

  // config input validation form
  bindingConfigValidation() {
    this.ThongsoIOForm = this.formBuilder.group({
      idNhomthamso: ["", Validators.required]
    });
  }

  // show list select Nhóm tham số
  async showNhomthamso() {
    this.allNhomthamso = await this.dmFacadeService
      .getNhomthamsoService()
      .getFetchAll();
    this.NhomthamsoFilters = this.allNhomthamso;
    this.selected = this.NhomthamsoFilters[0].tennhom;
    this.listdsthongso = await this.dmFacadeService.getThamsoService().getThamSoByNhomThamSoId(this.NhomthamsoFilters[0].id);
  }

  async showThamso() {
    this.listdsthongso = await this.dmFacadeService.getThamsoService().getThamSoByNhomThamSoId( this.ThongsoIOForm.value.idNhomthamso);
  }

  // validation error message
  public logValidationErrorMessages() {
    validationErrorMessagesService(
      this.ThongsoIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  // display fields css
  public displayFieldCss(field: string) {
    displayFieldCssService(field);
  }

  // set id in coloumn
  customiseCell(args: QueryCellInfoEventArgs) {
    if (args.column.field === "id") {
      args.cell.id = args.data[args.column.field];
    }
  }

  // Xử lý sự thay đổi của sự kiện thay đổi checkbox
  checkboxchange(event, data) {
    this.getItemByEvent(event, data);
  }

  // getItemByEvent check
  public getItemByEvent(event, data) {
    const target = event;
    if (target.checked === true) {
      this.ldsthongso.push(data.id);
    }
    if (target.checked === false) {
      this.ldsthongso.splice(this.ldsthongso.indexOf(data.id), 1);
    }
    if (this.ldsthongso.length > 0) this.submited = false;
    else { this.submited = true; }
  }

  public onSubmit() {
    let id;
    if (this.route.snapshot.paramMap.get('id')) {
      const idurl = this.route.snapshot.paramMap.get('id');
      id = +idurl;
    } else {
      this.route.paramMap.subscribe(result => {  id = result['params']['objId']; });
    }
    // lấy danh sách tham số đã chọn và bỏ những tham số mà trạm đã quan sát
    for (let i = 0; i < this.ldsthongso.length; i++) {
      if (this.obj) {
        if (this.obj.idthongso.indexOf(this.ldsthongso[i]) === -1) {
          this.objthongsoinput = { idObj: id, objKey: this.obj.objKey, idThamso: 0 };
          this.objthongsoinput.idThamso = this.ldsthongso[i];
          this.listobjthongsoinput.push(this.objthongsoinput);
        }
      }
    }
    // gán dữ liệu vào input model theo dạng jsonParameter
    this.listArray.jsonParameter = this.listobjthongsoinput.slice();
    if (this.listArray.jsonParameter.length > 0) {
      this.commonFacadeService.getObjthamsoService().addItem(this.listArray).subscribe(
        res => this.matSidenavService.doParentFunction("getAllThongso"),
        (error: HttpErrorResponse) => {
          this.commonService.showError(error);
        },
        () =>
          this.commonService.showeNotiResult(
            "Thêm thông số nhân thành công!",
            2000
          )
      );
    }
    this.matSidenavService.close();
  }

  // Chọn check all
  public checkall() {
    this.checked = true;
    if (this.listdsthongso.length > 0) this.submited = false;
    if (this.ldsthongso.length < this.listdsthongso.length) {
      for (let i = 0; i < this.listdsthongso.length; i++) {
        this.ldsthongso.push(this.listdsthongso[i].id);
      }
    }
  }

  // Chọn check all
  public uncheckall() {
    this.checked = false;
    this.grid.refresh();
    this.ldsthongso = [];
    this.submited = true;
  }

  // Close thông số sidebar
  public closeThongsoIOSidebar() {
    this.matSidenavService.close();
  }
}
