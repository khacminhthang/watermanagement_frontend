import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThongsoquantracIoComponent } from './thongsoquantrac-io.component';

describe('ThongsoquantracIoComponent', () => {
  let component: ThongsoquantracIoComponent;
  let fixture: ComponentFixture<ThongsoquantracIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThongsoquantracIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThongsoquantracIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
