import {Component, Input, OnInit} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import {
  displayFieldCssService,
  validationErrorMessagesService
} from "src/app/core/services/utilities/validatorService";
import { InputDlquantractsModel } from "src/app/models/admin/common/dlquantracts.model";
import {HttpErrorResponse} from "@angular/common/http";
import {CommonFacadeService} from "src/app/service/admin/common/common-facade.service";
import {DatePipe} from "@angular/common";

@Component({
  selector: "app-ketquaquantrac-io",
  templateUrl: "./ketquaquantrac-io.component.html",
  styleUrls: ["./ketquaquantrac-io.component.scss"]
})
export class KetquaquantracIoComponent implements OnInit {
  KetquaIOForm: FormGroup;
  public inputModel: InputDlquantractsModel;
  public editMode: boolean;
  public purpose: string;
  public obj: any;

  // error message
  validationErrorMessages = {
    fullTime: { required: "Thời gian không được để trống!" },
    ketquado: { required: "Kết quả đo không được để trống!" }
  };

  // form errors
  formErrors = {
    fullTime: "",
    ketquado: ""
  };

  // ctor
  constructor(
    public matSidenavService: MatsidenavService,
    public progressService: ProgressService,
    private formBuilder: FormBuilder,
    public commonService: CommonServiceShared,
    public cmFacadeService: CommonFacadeService,
    private datePipe: DatePipe,
  ) { }

  // onInit
  ngOnInit() {
    this.bindingConfigValidation();
    this.bindingConfigAddOrUpdate();
  }

  // config Form use add or update
  bindingConfigAddOrUpdate() {
    this.editMode = false;
    this.inputModel = new InputDlquantractsModel();
    if (this.purpose === "new" || this.purpose === "edit") {
      this.editMode = true;
    }
    // check edit
    if (this.purpose === "edit" && this.editMode === true && this.obj) {
      this.KetquaIOForm.setValue({
        fullTime: this.obj.fullTime,
        ketquado: this.obj.ketquado,
        phuongphapdo: this.obj.phuongphapdo,
        note: this.obj.note
      });
    }
  }

  // config input validation form
  bindingConfigValidation() {
    this.KetquaIOForm = this.formBuilder.group({
      fullTime: ["", Validators.required],
      ketquado: ["", Validators.required],
      phuongphapdo: [""],
      note: [""]
    });
  }

  // on Submit
  public onSubmit(operMode: string) {
    this.addOrUpdate(operMode);
    // close
    this.matSidenavService.close();
  }

  // add or update continue
  public addOrUpdateContinue(operMode: string) {
    this.addOrUpdate(operMode);
    this.onFormReset();
    this.purpose = "new";
  }

  // add or update
  private addOrUpdate(operMode: string) {
    this.inputModel = this.KetquaIOForm.value;
    this.inputModel.idTramquantrac = this.obj.idTramquantrac;
    this.inputModel.idThamsodo = this.obj.idThamsodo;
    this.inputModel.objKey = this.obj.objKey;
    this.inputModel.fullTime = this.datePipe.transform(this.KetquaIOForm.value.fullTime, "yyyy-MM-dd HH:mm:ss");
    if (operMode === "new") {
      this.cmFacadeService
        .getDlquantractsService()
        .addItem(this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("getKetQuaQuanTrac"),
          (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Thêm mới kết quả thành công!",
              2000
            )
        );
    } else if (operMode === "edit") {
      const id: number = this.obj.id;
      this.cmFacadeService
        .getDlquantractsService()
        .updateItem(id, this.inputModel)
        .subscribe(
          res => this.matSidenavService.doParentFunction("getKetQuaQuanTrac"),
            (error: HttpErrorResponse) => {
          if(error.status === 403){
            this.commonService.showeNotiResult("Tài khoản không có quyền thực hiện chức năng này!", 2000)
          } else{
            this.commonService.showError(error);
          }
          console.log(error, 'error')
        },
          () =>
            this.commonService.showeNotiResult(
              "Cập nhập dữ liệu thành công",
              2000
            )
        );
    }
  }

  // on form reset
  public onFormReset() {
    this.KetquaIOForm.reset();
  }

  // validation error message
  public logValidationErrorMessages() {
    validationErrorMessagesService(
      this.KetquaIOForm,
      this.validationErrorMessages,
      this.formErrors
    );
  }

  // display fields css
  public displayFieldCss(field: string) {
    displayFieldCssService(field);
  }

  // close sidebar
  public closeKetquaIOSidebar() {
    this.matSidenavService.close();
  }
}
