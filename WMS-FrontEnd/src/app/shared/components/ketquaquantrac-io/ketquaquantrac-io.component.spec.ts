import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KetquaquantracIoComponent } from './ketquaquantrac-io.component';

describe('KetquaquantracIoComponent', () => {
  let component: KetquaquantracIoComponent;
  let fixture: ComponentFixture<KetquaquantracIoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KetquaquantracIoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KetquaquantracIoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
