import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThongsoquantracListComponent } from './thongsoquantrac-list.component';

describe('ThongsoquantracListComponent', () => {
  let component: ThongsoquantracListComponent;
  let fixture: ComponentFixture<ThongsoquantracListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThongsoquantracListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThongsoquantracListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
