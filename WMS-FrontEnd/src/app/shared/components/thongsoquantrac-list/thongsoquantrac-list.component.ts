import {
  Component,
  ComponentFactoryResolver, Input,
  OnInit,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { MatSidenav } from "@angular/material/sidenav";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { MatDialog } from "@angular/material/dialog";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { SubHeaderService } from "src/app/core/services/utilities/subheader.service";
import { ActivatedRoute, Router } from "@angular/router";
import { QueryCellInfoEventArgs } from "@syncfusion/ej2-grids";
import { OutputThamsoModel } from "src/app/models/admin/danhmuc/thamso.model";
import { ThongsoquantracIoComponent } from "src/app/shared/components/thongsoquantrac-io/thongsoquantrac-io.component";
import { MyAlertDialogComponent } from "src/app/shared/components/my-alert-dialog/my-alert-dialog.component";
import { SubsidenavService } from "src/app/core/services/utilities/subsidenav.service";
import { CommonFacadeService } from "src/app/service/admin/common/common-facade.service";

@Component({
  selector: "app-thongsoquantrac-list",
  templateUrl: "./thongsoquantrac-list.component.html",
  styleUrls: ["./thongsoquantrac-list.component.scss"]
})
export class ThongsoquantracListComponent implements OnInit {
  @Input() objKey: number;
  listThamso: any;
  selectedItem: OutputThamsoModel;
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compThongsoio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  // id của danh sách thông số trạm
  idthongso: any[] = [];
  objToIO: any;
  listData: any;
  settingsCommon = new SettingsCommon();
  constructor(
    public matsidenavService: MatsidenavService,
    public progressService: ProgressService,
    public cfr: ComponentFactoryResolver,
    public commonFacadeService: CommonFacadeService,
    public modalDialog: MatDialog,
    public commonService: CommonServiceShared,
    private subHeaderService: SubHeaderService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.subHeaderService.setParentComp(this);
    this.settingsCommon.toolbar = ["Search"];
    this.settingsCommon.pageSettings = { pageSize: 10 };
    this.getAllThongso();
  }
  // get all
  async getAllThongso() {
    let id;
    if (this.route.snapshot.paramMap.get('id')) {
      const idurl = this.route.snapshot.paramMap.get('id');
      id = +idurl;
    } else {
      this.route.paramMap.subscribe(result => { id = result['params']['objId']; });
    }
    this.listData = await this.commonFacadeService.getObjthamsoService().getFetchAll({ idObj: id, objKey: this.objKey });
    if (this.listData) {
      this.listData.map((thamso, index) => {
        thamso.serialNumber = index + 1;
      });
    }
    this.listThamso = this.listData;
  }

  // open sidebar execute insert
  public openThongsoIOSidebar() {
    this.idthongso = [];
    this.matsidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.matsidenavService.setTitle("Thêm mới dữ liệu thông số");
    if (this.listThamso) {
      for (let i = 0; i < this.listThamso.length; i++) {
        this.idthongso.push(this.listThamso[i].idthamso);
      }
    }
    this.objToIO = {
      objKey: this.objKey,
      idthongso: this.idthongso
    }
    this.matsidenavService.setContentComp(ThongsoquantracIoComponent, "new", this.objToIO);
    this.matsidenavService.open();
  }
  // check delete
  public deleteThongso(event) {
    this.getItemByEvent(event);
    const canDelete: string = this.commonFacadeService.getObjthamsoService().checkBeDeleted(this.selectedItem.id);
    this.canBeDeletedCheck(canDelete);
  }

  public canBeDeletedCheck(sMsg: string) {
    if (sMsg === 'ok') {
      this.confirmDeleteDiaLog();
    } else {
      this.canDeleteDialog(sMsg);
    }
  }

  // Chấp nhận xóa
  confirmDeleteDiaLog() {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = 'Chắc chắn xóa?';
    dialogRef.componentInstance.content = 'Tham số: <b>' + this.selectedItem.tenthamso + '</b>';
    dialogRef.componentInstance.okeButton = 'Đồng ý';
    dialogRef.componentInstance.cancelButton = 'Hủy bỏ';
    dialogRef.componentInstance.visibleOkButton = true;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'confirm') {
        this.commonFacadeService.getObjthamsoService().deleteItem(this.selectedItem.id).subscribe(res => this.getAllThongso());
      }
    });
  }

  canDeleteDialog(sMsg: string) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = 'Không thể xóa!';
    dialogRef.componentInstance.content = '<b>' + sMsg + '</b>';
    dialogRef.componentInstance.cancelButton = 'Đóng';
    dialogRef.componentInstance.visibleOkButton = false;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.componentInstance.headerColor = 'red';
  }

  // close
  public closeThongsoIOSidebar() {
    this.matSidenav.close();
  }

  // set id in coloumn
  customiseCell(args: QueryCellInfoEventArgs) {
    if (args.column.field === "id") {
      args.cell.id = args.data[args.column.field];
    }
  }
  // getItemByEvent
  public getItemByEvent(event) {
    const target = event.currentTarget;
    const item = this.commonService.getByEvent(event, this.listThamso);
    this.selectedItem = item;
  }

  // call method
  doFunction(methodName) {
    this[methodName]();
  }
}
