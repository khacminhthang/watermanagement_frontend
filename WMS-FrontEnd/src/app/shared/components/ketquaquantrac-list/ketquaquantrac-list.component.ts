import {
  Component,
  ComponentFactoryResolver,
  Input,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from "@angular/core";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import { SubsidenavService } from "src/app/core/services/utilities/subsidenav.service";
import { SubSidenavComponent } from "src/app/core/layout/sub-sidenav/sub-sidenav.component";
import { HighchartService } from "src/app/service/admin/common/highchart.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { SettingsCommon } from "src/app/shared/constants/setting-common";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatSidenav } from "@angular/material/sidenav";
import { KetquaquantracIoComponent } from "src/app/shared/components/ketquaquantrac-io/ketquaquantrac-io.component";
import { ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { L10n, setCulture } from "@syncfusion/ej2-base";
import {
  DataStateChangeEventArgs,
  GridComponent,
} from "@syncfusion/ej2-angular-grids";
import { CommonFacadeService } from "src/app/service/admin/common/common-facade.service";
import { DatePipe } from "@angular/common";
import { MyAlertDialogComponent } from "src/app/shared/components/my-alert-dialog/my-alert-dialog.component";
import { HttpErrorResponse } from "@angular/common/http";
import { MatDialog } from "@angular/material/dialog";

setCulture("en-US");
L10n.load({
  "en-US": {
    grid: {
      EmptyRecord: "Chưa có bản ghi nào",
    },
    pager: {
      currentPageInfo: "{0} trên {1} trang",
      totalItemsInfo: "({0} Bản ghi)",
      firstPageTooltip: "Đi đến trang đầu",
      lastPageTooltip: "Đi đến trang cuối",
      nextPageTooltip: "Tiếp theo",
      previousPageTooltip: "Quay lại",
      nextPagerTooltip: "Trang tiếp",
      previousPagerTooltip: "Trang sau",
    },
  },
});

@Component({
  selector: "app-ketquaquantrac-list",
  templateUrl: "./ketquaquantrac-list.component.html",
  styleUrls: ["./ketquaquantrac-list.component.scss"],
})
export class KetquaquantracListComponent implements OnInit {
  @Input() objKey: number;
  // @ts-ignore
  @ViewChild("grid") public grid: GridComponent;
  DateForm: FormGroup;
  public id: number;
  public checkget = true;
  firstdate: string;
  lastdate: string;
  public dataBieuDo: any;
  public listThamso: any;
  settingsCommon = new SettingsCommon();
  @ViewChild("aside", { static: true }) public matSidenav: MatSidenav;
  @ViewChild("compketquaio", { read: ViewContainerRef, static: true })
  public content: ViewContainerRef;
  @ViewChild("subsidebar", { read: ViewContainerRef, static: true })
  public subsidebar: ViewContainerRef;
  // khai báo cho list phân trang
  public state: DataStateChangeEventArgs;
  public listdatathongso: Observable<DataStateChangeEventArgs>;
  public dlquantractsService: any;
  public listData: any;
  public errorForm = "";
  fileData: File = null;

  constructor(
    public progressService: ProgressService,
    public cfr: ComponentFactoryResolver,
    public subNav: SubsidenavService,
    private chartSv: HighchartService,
    public commonService: CommonServiceShared,
    public matsidenavService: MatsidenavService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public commonFacadeService: CommonFacadeService,
    private datePipe: DatePipe,
    public modalDialog: MatDialog
  ) {
    // Get new service
    this.dlquantractsService = this.commonFacadeService.getDlquantractsService();
    this.listdatathongso = this.dlquantractsService;
  }

  ngOnInit() {
    this.subNav.setSubnav(this, this.subsidebar, this.cfr);
    this.bindingConfigValidation();
    this.getAllThamSo();
  }

  // hien thi Sidebar
  public showSidebar() {
    this.subNav.setContentComp(SubSidenavComponent, this.listThamso);
  }

  // config input validation form
  bindingConfigValidation() {
    this.DateForm = this.formBuilder.group({
      firstdate: ["", Validators.required],
      lastdate: ["", Validators.required],
    });
  }

  // get id in url
  public getId() {
    // const idurl = this.route.snapshot.paramMap.get('id'); // get id in url
    // this.id = +idurl;
    // let id;
    if (this.route.snapshot.paramMap.get("id")) {
      const idurl = this.route.snapshot.paramMap.get("id");
      this.id = +idurl;
    } else {
      this.route.paramMap.subscribe((result) => {
        this.id = result["params"]["objId"];
      });
    }
  }

  // get all danh sach tham so và truyền kết quả quan trắc của tham số thứ nhất
  async getAllThamSo() {
    this.errorForm = "";
    await this.getId();
    this.listThamso = await this.commonFacadeService
      .getObjthamsoService()
      .getFetchAll({
        idObj: this.id,
        objKey: this.objKey,
      });
    if (this.listThamso && this.listThamso[0]) {
      this.showSidebar();
      this.subNav.item = this.listThamso[0];
      let pageSize = 10;
      const data: any = await this.commonFacadeService
        .getDlquantractsService()
        .getAllKetQuaFirst({
          Take: 100,
          IdTramquantrac: this.id,
          IdThamsodo: this.listThamso[0].idthamso,
          ObjKey: this.objKey,
          PageSize: -1,
        });
      if (data) {
        data.items.map((item, index) => {
          item.serialNumber = index + 1;
        });
        this.listData = data.items;
        this.dataBieuDo = data.items;
        this.chartInit(this.dataBieuDo);
      } else {
        this.dataBieuDo = [];
        this.chartInit(this.dataBieuDo);
      }
    }
  }
  // Convert string thành dd/mm/yyyy
  public convertDateTimeString(data) {
    const [date, time]: string[] = data.fullTime.split(" ");
    const [year, month, day]: string[] = date.split("-");
    if (year.length === 4) {
      const awesomeDateTime = `${day}-${month}-${year}`;
      return awesomeDateTime;
    } else {
      const awesomeDateTime1 = `${year}-${month}-${day}`;
      return awesomeDateTime1;
    }
  }

  // Convert string thành dd/mm/yyyy
  public convertTimeString(data) {
    const [date, time]: string[] = data.fullTime.split(" ");
    if (time) {
      return time;
    } else {
      return null;
    }
  }

  async uploadFile(dataFile: any) {
    let formData: FormData = new FormData();
    this.fileData = dataFile.target.files[0];
    formData.append("File", this.fileData);
    formData.append("IdThamsodo", this.subNav.item.idthamso.toString());
    formData.append("IdTramquantrac", this.id.toString());
    formData.append("ObjKey", this.objKey.toString());
    await this.commonFacadeService
      .getDlquantractsService()
      .uploadFileExcel(formData)
      .subscribe(
        (res) => {
          this.commonService.showeNotiResult("Import file thành công !", 2000);
          this.getKetQuaQuanTrac();
        },
        (errResponse: HttpErrorResponse) => {
          if (errResponse.error instanceof Error) {
            this.commonService.showeNotiResult(errResponse.error.message, 2000);
          } else {
            this.commonService.showeNotiResult(errResponse.error, 2000);
          }
        },
        () => {}
      );
  }

  // get all phân trang kết quả quan trắc  100 bản gần nhất
  async getKetQuaQuanTrac() {
    this.errorForm = "";
    this.listdatathongso = this.dlquantractsService;
    let pageSize = 10;
    this.checkget = true;
    const data: any = await this.commonFacadeService
      .getDlquantractsService()
      .getAllKetQuaFirst({
        Take: 100,
        IdTramquantrac: this.id,
        IdThamsodo: this.subNav.item.idthamso,
        ObjKey: this.objKey,
        PageSize: -1,
      });
    if (data) {
      data.items.map((item, index) => {
        item.serialNumber = index + 1;
      });
      this.listData = data.items;
      this.dataBieuDo = data.items;
      this.chartInit(this.dataBieuDo);
    } else {
      this.listData = [];
      this.dataBieuDo = [];
      this.chartInit(this.dataBieuDo);
    }
  }

  // khởi tạo biểu đồ
  chartInit(data) {
    this.chartSv.setSource(data);
    this.chartSv.preDataChart(data, "gw");
    if (data) {
      this.chartSv.setChartName("kết quả");
    } else {
      this.chartSv.setChartName("chưa có kết quả đo");
    }
    this.chartSv.setXTittle("Thời gian đo");
    this.chartSv.setYTittle("Kết quả đo");
    this.chartSv.chartInit1m("chartkq", "line");
  }

  // Show kết quả quan trắc
  showKetQua() {
    if (this.DateForm.value.firstdate && this.DateForm.value.lastdate) {
      this.checkget = false;
      this.firstdate = this.datePipe.transform(
        this.DateForm.value.firstdate,
        "dd/MM/yyyy hh:mm:ss"
      );
      this.lastdate = this.datePipe.transform(
        this.DateForm.value.lastdate,
        "dd/MM/yyyy hh:mm:ss"
      );
      this.getAllTimeFromTo(this.firstdate, this.lastdate);
    } else {
      this.errorForm = "Bạn phải nhập đầy đủ thông tin";
    }
  }

  // Get all kết quả quan trắc phân trang time from to
  async getAllTimeFromTo(firstDate, lastDate) {
    this.listdatathongso = this.dlquantractsService;
    const data: any = await this.commonFacadeService
      .getDlquantractsService()
      .getAllKetQuaTimeFromTo({
        timeFrom: firstDate,
        timeTo: lastDate,
        IdTramquantrac: this.id,
        IdThamsodo: this.subNav.item.idthamso,
        ObjKey: this.objKey,
        PageSize: -1,
      });
    if (data) {
      data.items.map((item, index) => {
        item.serialNumber = index + 1;
      });
      this.listData = data.items;
      this.dataBieuDo = data.items;
      this.chartInit(this.dataBieuDo);
    } else {
      this.dataBieuDo = [];
      this.chartInit(this.dataBieuDo);
      this.chartSv.setChartName("chưa có kết quả đo");
    }
  }

  // open sidenar du lieu thong so
  openKetquaIOSidebar() {
    this.matsidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    const obj = {
      idTramquantrac: this.id,
      idThamsodo: this.subNav.item.idthamso,
      objKey: this.objKey,
    };
    this.matsidenavService.setTitle("Thêm dữ liệu thông số quan trắc");
    this.matsidenavService.setContentComp(
      KetquaquantracIoComponent,
      "new",
      obj
    );
    this.matsidenavService.open();
  }

  // delete
  public deleteItem(data) {
    const canDelete: string = this.commonFacadeService
      .getDlquantractsService()
      .checkBeDeleted(data.id);
    this.canBeDeletedCheck(canDelete, data);
  }

  public canBeDeletedCheck(sMsg: string, data) {
    if (sMsg === "ok") {
      this.confirmDeleteDiaLog(data);
    } else {
      this.canDeleteDialog(sMsg);
    }
  }

  canDeleteDialog(sMsg: string) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Không thể xóa!";
    dialogRef.componentInstance.content = "<b>" + sMsg + "</b>";
    dialogRef.componentInstance.cancelButton = "Đóng";
    dialogRef.componentInstance.visibleOkButton = false;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.componentInstance.headerColor = "red";
  }

  confirmDeleteDiaLog(data) {
    const dialogRef = this.modalDialog.open(MyAlertDialogComponent);
    dialogRef.componentInstance.header = "Chắc chắn xóa?";
    dialogRef.componentInstance.content =
      "Thời gian: <b>" + data.fullTime + "</b>";
    dialogRef.componentInstance.okeButton = "Đồng ý";
    dialogRef.componentInstance.cancelButton = "Hủy bỏ";
    dialogRef.componentInstance.visibleOkButton = true;
    dialogRef.componentInstance.visibleCancelButton = true;
    dialogRef.afterClosed().subscribe((result) => {
      if (result === "confirm") {
        this.commonFacadeService
          .getDlquantractsService()
          .deleteItem(data.id)
          .subscribe(
            () => {
              if (this.checkget === true) {
                this.getKetQuaQuanTrac();
              } else {
                this.showKetQua();
              }
            },
            (error: HttpErrorResponse) => {
              this.commonService.showeNotiResult(error.message, 2000);
            },
            () =>
              this.commonService.showeNotiResult(
                "Đã xóa thành công kết quả lúc: " + data.fullTime,
                2000
              )
          );
      }
    });
  }

  // close
  public closeKetquaIOSidebar() {
    this.matSidenav.close();
  }

  // Sửa dữ liệu quan trắc
  public openEditIOSidebar(data) {
    this.matsidenavService.setSidenav(
      this.matSidenav,
      this,
      this.content,
      this.cfr
    );
    this.matsidenavService.setTitle("Sửa dữ liệu thông số quan trắc");
    this.matsidenavService.setContentComp(
      KetquaquantracIoComponent,
      "edit",
      data
    );
    this.matsidenavService.open();
  }

  // call method
  doFunction(methodName) {
    this[methodName]();
  }
  refreshGrid() {
    this.grid.refresh();
  }
}
