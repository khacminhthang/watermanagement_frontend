import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KetquaquantracListComponent } from './ketquaquantrac-list.component';

describe('KetquaquantracListComponent', () => {
  let component: KetquaquantracListComponent;
  let fixture: ComponentFixture<KetquaquantracListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KetquaquantracListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KetquaquantracListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
