import { NgModule } from "@angular/core";
import { CommonModule, TitleCasePipe } from "@angular/common";
import { MatDialogModule } from "@angular/material/dialog";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { MatSnackBarModule } from "@angular/material/snack-bar";

import { MyAlertDialogComponent } from "./components/my-alert-dialog/my-alert-dialog.component";
import { ImageuploadService } from "src/app/service/admin/common/imageupload.service";
import { SharedFacadeService } from "src/app/shared/shared-facade.service";
import { HighchartService } from "src/app/service/admin/common/highchart.service";
import { CommonServiceShared } from "src/app/core/services/utilities/common-service";
import { ProgressHttpInterceptor } from "src/app/core/services/utilities/http-interceptor";
import { MatdialogService } from "src/app/core/services/utilities/matdialog.service";
import { MatsidenavService } from "src/app/core/services/utilities/matsidenav.service";
import { ProgressService } from "src/app/core/services/utilities/progress.service";
import {ValidatorToaDoService} from "../core/services/utilities/validatorService";

// @ts-ignore
@NgModule({
  declarations: [MyAlertDialogComponent],
  imports: [CommonModule, MatSnackBarModule, MatDialogModule],
  entryComponents: [MyAlertDialogComponent],
  providers: [
    ValidatorToaDoService,
    ImageuploadService,
    MatsidenavService,
    MatdialogService,
    CommonServiceShared,
    TitleCasePipe,
    SharedFacadeService,
    ProgressService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ProgressHttpInterceptor,
      multi: true
    },
    HighchartService
  ]
})
export class SharedModule { }
