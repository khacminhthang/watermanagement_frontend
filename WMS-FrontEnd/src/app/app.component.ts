import {
  Component,
  ViewChild,
  OnInit,
  ViewContainerRef,
  Injector,
} from "@angular/core";
import { Router } from "@angular/router";
import { ProgressService } from "src/app/core/services/utilities/progress.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'WMS-FrontEnd';

  // Sử dụng biến roleAdmin để xác định người dùng có vai trò gì trong hệ thống và load giao diện tương ứng
  // Mặc định roleAdmin = false, Nếu người dùng đăng nhập có role = Admin thì roleAdmin = true;
  roleAdmin = true;
  constructor(
    private router: Router,
    public progressService: ProgressService
  ) {}
  ngOnInit() {
    // this.checkLogin();
  }

}
