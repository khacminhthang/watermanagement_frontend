import { SharedModule } from 'src/app/shared/shared.module';
import { CoreModule } from 'src/app/core/core.module';
import { PublicModule } from 'src/app/features/public/public.module';
import { AdminModule } from 'src/app/features/admin/admin.module';
import {
  BrowserModule,
  BrowserTransferStateModule
} from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "src/app/app.component";
import { AppRoutingModule } from "src/app/app-routing.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {MatProgressBarModule} from "@angular/material";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({appId: "serverApp"}),
    BrowserTransferStateModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AdminModule,
    CoreModule,
    SharedModule,
    PublicModule,
    MatProgressBarModule
  ],
  providers: [],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
