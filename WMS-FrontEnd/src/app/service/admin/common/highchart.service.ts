import { Injectable } from '@angular/core';
import * as Highcharts from 'highcharts';
import * as moment from 'moment-mini';

declare var require: any;


@Injectable({
  providedIn: 'root'
})
export class HighchartService {

  constructor() {
  }

  /* Các hàm cần sử dụng để lập biểu đồ
    // setSource()
    // preDataChart()
    // setChartName()
    // setYTittle()
    // setXTittle()
    // chartInit()
  */
  public options: any = {
    chart: {
      type: 'line',
      zoomType: 'x',
      credits: {
        enabled: false
      }
    },
  };
  public chart;
  public dataChart;
  public name: string;
  public titleY: string;
  public titleX: string;
  public fullTimeInput = [];
  public valueInput = [];
  public color = 'blue';
  public bgColor = '#ffffff'

  /* Các hàm chuẩn hóa data biểu đồ
  / Truyền vào: data biểu đồ
  */

  preDataChart(dataChart, para) {
    // dữ liệu nước
    this.valueInput = [];
    this.fullTimeInput = [];
    if (para === 'gw' && !dataChart === false) {
      for (let i = 0; i < dataChart.length; i++) {
        this.fullTimeInput.push(moment(dataChart[i].fullTime).format('DD/MM/YYYY h:mm:ss'));
      }
      for (let i = 0; i < dataChart.length; i++) {
        const n = (dataChart[i].ketquado).toFixed(2);
        this.valueInput.push(+n);
      }
    } else if (para === 'wt') {

    }
  }


  /* Hàm tạo biểu đồ 1 tháng hiện tại
  / Truyền vào: element id, kiểu biểu đồ
  */
  chartInit1m(id: string, type: string) {
    const options: any = {
      chart: {
        backgroundColor: this.bgColor,
        type: type,
        zoomType: 'x'
      },
      credits: {
        enabled: false
      },
      title: {
        text: 'Biểu đồ đo ' + this.name
      },
      xAxis: {
        title: {
          text: this.titleX
        },
        categories: this.fullTimeInput,
        labels: {
          formatter() {
            return this.value.toString().substring(0, 10);
          }
        },
        tickInterval: 24
      },
      yAxis: {
        title: {
          text: this.titleY
        },
        gridLineWidth: 0
      },

      series: [
        {
          color: this.color,
          name: 'Kết quả đo',
          data: this.valueInput
        }
      ]
    };

    // Load module after Highcharts is loaded
    require('highcharts/modules/exporting')(Highcharts);

    Highcharts.chart(id, options);
  }

  /* Hàm tạo biểu đồ 3 tháng
  / Truyền vào: element id, kiểu biểu đồ
  */
  chartInit3m(id: string, type: string) {
    const options: any = {
      chart: {
        backgroundColor: this.bgColor,
        type: type,
        zoomType: 'x'
      },
      credits: {
        enabled: false
      },
      title: {
        text: this.name
      },
      xAxis: {
        title: {
          text: this.titleX
        },
        categories: this.fullTimeInput,
        labels: {
          formatter() {
            return this.value.toString().substring(0, 10);
          }
        },
        tickInterval: 24
      },
      yAxis: {
        title: {
          text: this.titleY
        }
      },

      series: [
        {
          color: '#fc0000',
          name: 'Tháng hiện tại',
          data: this.valueInput
        },
        {
          color: '#f6f7b7',
          name: 'Tháng sau',
        },
        {
          color: '#dfedf5',
          name: 'Tháng trước',
        }
      ],
    };

    // Load module after Highcharts is loaded
    require('highcharts/modules/exporting')(Highcharts);

    this.chart = Highcharts.chart(id, options);
  }

  /* Các hàm đặt tên biểu đồ
  / Truyền vào: tên biểu đồ
  */
  setChartName(name: string) {
    this.name = name;
  }

  /* Các hàm đặt data biểu đồ
  / Truyền vào: data biểu đồ
  */
  setSource(dataChart: any) {
    this.dataChart = dataChart;
  }

  // set trục x title
  setXTittle(tittle: string) {
    this.titleX = tittle;
  }

  // set trục y title
  setYTittle(tittle: string) {
    this.titleY = tittle;
  }

  // thay đổi màu line hiện tại
  changColor(color: string) {
    this.color = color;
  }

  // Thay đổi màu back ground hiện tại
  changeBgColor(bgColor: string) {
    this.bgColor = bgColor;
  }


  // chuẩn hóa data
  preData(dataChart, time, value) {
    // dữ liệu nước
    this.valueInput = [];
    this.fullTimeInput = [];
    for (let i = 0; i < dataChart.monthInput.items.length; i++) {
      this.fullTimeInput.push(moment(dataChart.monthInput.items[i].fullTime).format('DD/MM/YYYY h:mm:ss'));
    }
    for (let i = 0; i < dataChart.monthInput.items.length; i++) {
      const n = (dataChart.monthInput.items[i].giatrido).toFixed(2);
      this.valueInput.push(+n);
    }
  }
}
