import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { ServiceName } from "src/app/shared/constants/service-name";
import { RepositoryEloquentService } from "src/app/core/services/data/baserepository.service";
import {InputGmediaModel, OutputGmediaModel} from "src/app/models/admin/common/gmedia.model";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class GmediaService extends RepositoryEloquentService {

  constructor(public httpClient: HttpClient) {
    super();
    this.setServiceInfo({
      httpClient,
      inputModelName: InputGmediaModel,
      outputModelName: OutputGmediaModel,
      apiUrl: environment.apiCommonURL + ServiceName.GMEDIA
    });
  }

  public checkBeDeleted(id: number) {
    return "ok";
  }

  /**
   * Hàm post dùng để xóa mảng id
   * @param body
   */
  public deleteArrayItem(body) {
    this.setServiceInfo({
      apiUrl: environment.apiCommonURL + ServiceName.GMEDIA
    });
    console.log("GmediaService -> deleteArrayItem -> body", body[0])
    return  this.deleteItem(body[0]);
  }

  /**
   * Hàm search item
   * @param params
   */
  public searchItem(params) {
    this.setServiceInfo({
      apiUrl: environment.apiCommonURL + ServiceName.GMEDIA + '/search'
    });
    return  this.getAll(params);
  }
}
