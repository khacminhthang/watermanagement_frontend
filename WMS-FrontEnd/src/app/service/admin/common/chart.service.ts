import { Injectable } from '@angular/core';
import { Chart } from 'chart.js';
import 'chartjs-plugin-zoom';
import 'hammerjs';
@Injectable({
  providedIn: 'root'
})
export class ChartService {
  dataChart : any = {
    "monthInput": {
      "month": 7,
      "items": [
        {
          "fullTime": "2019-07-01T00:44:00",
          "giatrido": -9.664999000000002
        },
        {
          "fullTime": "2019-07-01T01:44:00",
          "giatrido": -9.653599
        },
        {
          "fullTime": "2019-07-01T02:44:00",
          "giatrido": -9.643298999999999
        }
      ]
    },
    "monthBefore": {
      "month": 6,
      "items": [
        {
          "fullTime": "2019-06-01T00:44:00",
          "giatrido": -9.697499
        },
        {
          "fullTime": "2019-06-01T01:44:00",
          "giatrido": -9.689399000000002
        },
        {
          "fullTime": "2019-06-01T02:44:00",
          "giatrido": -9.672499000000002
        },
        {
          "fullTime": "2019-06-01T03:44:00",
          "giatrido": -9.657699000000001
        },
        {
          "fullTime": "2019-06-01T04:44:00",
          "giatrido": -9.640599000000002
        }
      ]
    },
    "monthAfter": {
      "month": 8,
      "items": [
        {
          "fullTime": "2019-08-01T02:44:00",
          "giatrido": -9.6512
        },
        {
          "fullTime": "2019-08-01T03:44:00",
          "giatrido": -9.641100000000002
        },
        {
          "fullTime": "2019-08-01T04:44:00",
          "giatrido": -9.625199000000002
        },
        {
          "fullTime": "2019-08-01T05:44:00",
          "giatrido": -9.607199000000001
        },
        {
          "fullTime": "2019-08-01T06:44:00",
          "giatrido": -9.5943
        }
      ]
    }
  }
    public myChart : any = 1;
    /* Hàm tạo biểu đồ
    / Truyền vào: data biểu đồ, element id, kiểu biểu đồ
    
   */
    chartInit(dataChart, id: string, type? : string)
    {
        var ctx = <HTMLCanvasElement>document.getElementById(id);
        //console.log(dataChart.length);
        var time = [];
        for( let i =0 ; i< dataChart.length; i++)
        {
          time.push(dataChart[i].time);
        }
        var data = [];
        for( let i =0 ; i< dataChart.length; i++)
        {
          data.push(dataChart[i].value);
        }

         this.myChart = new Chart(ctx, {
          type: type,
          data: {
            labels: time,
            datasets: [{
                label: 'Giá trị',
                data: data,
                borderColor: '#311f99',
                borderWidth: 2,
                pointBorderColor: 'rgba(0, 0, 0, 0)',
                pointBackgroundColor: 'rgba(0, 0, 0, 0)',
                pointHoverBackgroundColor: 'rgb(255, 99, 132)',
                pointHoverBorderColor: 'rgb(255, 99, 132)',
            }]
        },

          options: {
            responsive: true,
            hover: {
              intersect: false
            },
            legend: {
              position: 'top',
            },
            scales: {
              xAxes: [{
                ticks: {
                  autoSkip: true,
                  maxRotation: 0,
                  minRotation: 0
                }
              }]
            },
            plugins: {

              zoom: {
                zoom: {
                  sensitivity:0.5,
                  drag: true,
                  enabled: true,
                  mode: 'x'
                }
              }
            }
        }



      });
    }
  /* Hàm thay đổi data source cho chart
  / Truyền vào data source
  
  */
 changeChartSource(newData)
 {
    this.dataChart = newData;
       return this.dataChart;
 }
 resetZoom()
 {
   this.myChart.resetZoom();
 }
  constructor() { }
}

