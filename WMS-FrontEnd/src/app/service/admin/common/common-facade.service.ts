import { Injectable } from "@angular/core";
import { GmediaService } from "src/app/service/admin/common/gmedia.service";
import { ObjthamsoService } from "src/app/service/admin/common/objthamso.service";
import { DlquantractsService } from "./dlquantracts.service";
import { ObjmediaService } from "./objmedia.service";
import {HttpClient} from "@angular/common/http";
import {SearchService} from "./search.service";

@Injectable({
  providedIn: "root"
})
export class CommonFacadeService {
  constructor(private httpClient: HttpClient) { }

  public getObjthamsoService() {
    return new ObjthamsoService(this.httpClient);
  }
  public getDlquantractsService() {
    return new DlquantractsService(this.httpClient);
  }
  public getObjmediaService() {
    return new ObjmediaService(this.httpClient);
  }

  // Gmedia service
  public getGmediaService() {
    return new GmediaService(this.httpClient);
  }

  public getSearchService() {
    return new SearchService(this.httpClient);
  }
}
