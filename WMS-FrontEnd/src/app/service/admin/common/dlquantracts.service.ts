import { Injectable } from "@angular/core";
import { RepositoryEloquentService } from "../../../core/services/data/baserepository.service";
import { environment } from "../../../../environments/environment";
import { ServiceName } from "../../../shared/constants/service-name";
import {
  InputDlquantractsModel,
  OutputDlquantractsModel,
} from "../../../models/admin/common/dlquantracts.model";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class DlquantractsService extends RepositoryEloquentService {
  constructor(public httpClient: HttpClient) {
    super();
    this.setServiceInfo({
      httpClient,
      inputModelName: new InputDlquantractsModel(),
      outputModelName: new OutputDlquantractsModel(),
      apiUrl: environment.apiCommonURL + ServiceName.DLQUANTRACTS,
    });
  }
  public getKetQuaTimeFromTo(state, param) {
    this.setServiceInfo({
      apiUrl:
        environment.apiCommonURL + ServiceName.DLQUANTRACTS + "/time-from-to",
    });
    return this.getDataFromServer(state, param);
  }

  // Lấy kết quả 100 bản ghi đầu tiên
  public getKetQuaFirst(state, param) {
    this.setServiceInfo({
      apiUrl: environment.apiCommonURL + ServiceName.DLQUANTRACTS + "/Take",
    });
    return this.getDataFromServer(state, param);
  }

  // upload file
  public uploadFileExcel(param) {
    this.setServiceInfo({
      apiUrl:
        environment.apiCommonURL + ServiceName.DLQUANTRACTS + "/upload-excel",
    });
    return this.addItemFile(param);
  }

  public getAllKetQuaFirst(param) {
    this.setServiceInfo({
      apiUrl: environment.apiCommonURL + ServiceName.DLQUANTRACTS + "/Take",
    });
    return this.getFetchAll(param);
  }
  public getAllKetQuaTimeFromTo(param) {
    this.setServiceInfo({
      apiUrl:
        environment.apiCommonURL + ServiceName.DLQUANTRACTS + "/time-from-to",
    });
    return this.getFetchAll(param);
  }

  public getAllKetQuaDayLast(param) {
    this.setServiceInfo({
      apiUrl: environment.apiCommonURL + ServiceName.DLQUANTRACTS + "/lastest",
    });
    return this.getFetchAll(param);
  }

  // Lấy dữ liệu kết quả theo năm
  public getKetquaYear(param) {
    this.setServiceInfo({
      apiUrl: environment.apiCommonURL + ServiceName.DLQUANTRACTS + "/year",
    });
    return this.getFetchAll(param);
  }
  public checkBeDeleted(id: number) {
    return "ok";
  }
}
