import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { ServiceName } from "src/app/shared/constants/service-name";
import { RepositoryEloquentService } from "src/app/core/services/data/baserepository.service";
import { InputObjthamsoModel, OutputObjthamsoModel } from "../../../models/admin/common/objthamso.model";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ObjthamsoService extends RepositoryEloquentService {
  constructor(public httpClient: HttpClient) {
    super();
    this.setServiceInfo({
      httpClient,
      inputModelName: new InputObjthamsoModel(),
      outputModelName: new OutputObjthamsoModel(),
      apiUrl: environment.apiCategoryURL + ServiceName.OBJTHAMSO
    });
  }

  // public getAllKetQuaGiamSat(params = {}) {
  //   let queryString = Object.keys(params)
  //     .map(key => key + "=" + params[key])
  //     .join("&");
  //   return this.httpClient.get(`${environment.apiResultURL + 'giamsatts/year-month'}?${queryString}`).toPromise();
  // }
  public checkBeDeleted(id: number) {
    return "ok";
  }
}
