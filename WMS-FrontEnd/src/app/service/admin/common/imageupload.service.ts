import { Injectable } from "@angular/core";

import { GmediaModel } from "src/app/models/admin/common/gmedia.model";
import { ServiceName } from "src/app/shared/constants/service-name";
import { RepositoryEloquentService } from "src/app/core/services/data/baserepository.service";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})

export class ImageuploadService extends RepositoryEloquentService {

  constructor() {
    super();
    this.setServiceInfo({
      inputModelName: new GmediaModel(),
      outputModelName: new GmediaModel(),
      apiUrl: environment.apiCommonURL + ServiceName.GMEDIA
    });
  }

  public checkBeDeleted(id: number) {
    return "ok";
  }
}

