import { Injectable } from '@angular/core';
import {RepositoryEloquentService} from "../../../core/services/data/baserepository.service";
import {HttpClient} from "@angular/common/http";
import {InputCongtyModel, OutputCongtyModel} from "../../../models/admin/danhmuc/congty.model";
import {environment} from "../../../../environments/environment";
import {ServiceName} from "../../../shared/constants/service-name";

@Injectable({
  providedIn: 'root'
})
export class SearchService extends RepositoryEloquentService{

  constructor(public httpClient: HttpClient) {
    super();
    this.setServiceInfo({
      httpClient,
      inputModelName: null,
      outputModelName: null,
      apiUrl: environment.apiCommonURL + ServiceName.SEARCH
    });
  }
}
