import {Injectable} from "@angular/core";
import {RepositoryEloquentService} from "../../../core/services/data/baserepository.service";
import {environment} from "../../../../environments/environment";
import {ServiceName} from "../../../shared/constants/service-name";
import {InputObjGmediaModel, OutputObjGmediaModel} from "../../../models/admin/common/objmedia.model";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ObjmediaService extends RepositoryEloquentService {

  constructor(public httpClient: HttpClient) {
    super();
    this.setServiceInfo({
      httpClient,
      inputModelName: InputObjGmediaModel,
      outputModelName: OutputObjGmediaModel,
      apiUrl: environment.apiCommonURL + ServiceName.OBJMEDIA
    });
  }

  public checkBeDeleted(id: number) {
    return "ok";
  }
}
