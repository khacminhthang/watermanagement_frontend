import { Injectable } from "@angular/core";

import { environment } from "src/environments/environment";
import { ServiceName } from "src/app/shared/constants/service-name";
import { RepositoryEloquentService } from "src/app/core/services/data/baserepository.service";
import {InputDvhcModel, OutputDvhcModel} from "src/app/models/admin/danhmuc/dvhc.model";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class WardService extends RepositoryEloquentService {

  constructor(public httpClient: HttpClient) {
    super();
    this.setServiceInfo({
      httpClient,
      inputModelName: new InputDvhcModel(),
      outputModelName: new OutputDvhcModel(),
      apiUrl: environment.apiCategoryURL + ServiceName.WARD,
    });
  }

  async getWardByIdDistrict(id) {
    this.setServiceInfo({
      apiUrl: `${environment.apiCategoryURL}${ServiceName.WARD}/${id}`
    });
    const listData = await this.getFetchAll();
    return listData;
  }

  public checkBeDeleted(id: number) {
    return "ok";
  }
}
