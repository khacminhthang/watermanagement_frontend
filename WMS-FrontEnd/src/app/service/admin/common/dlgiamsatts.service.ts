import {Injectable} from "@angular/core";
import {RepositoryEloquentService} from "../../../core/services/data/baserepository.service";
import {environment} from "../../../../environments/environment";
import {OutputDlgiamsattsModel} from "../../../models/admin/common/dlgiamsatts.model";
import {ServiceName} from "../../../shared/constants/service-name";


@Injectable({
  providedIn: "root"
})
export class DlgiamsattsService extends RepositoryEloquentService {
  constructor() {
    super();
    this.setServiceInfo({
      inputModelName: new OutputDlgiamsattsModel(),
      outputModelName: new OutputDlgiamsattsModel(),
      apiUrl: environment.apiCommonURL + ServiceName.DLGIAMSATTS
    });
  }
  public test(state, param) {
    this.setServiceInfo({
      apiUrl: environment.apiCommonURL + ServiceName.DLGIAMSATTS + '/time-from-to'
    });
    return this.getDataFromServer(state, param);
  }
  public checkBeDeleted(id: number) {
    return "ok";
  }
}
