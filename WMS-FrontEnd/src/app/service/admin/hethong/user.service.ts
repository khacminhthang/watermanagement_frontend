import {Injectable} from "@angular/core";
import {RepositoryEloquentService} from "src/app/core/services/data/baserepository.service";
import {environment} from "src/environments/environment";
import {ServiceName} from "src/app/shared/constants/service-name";
import {OutputUserModel} from "src/app/models/admin/hethong/user.model";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class UserService extends RepositoryEloquentService {
  constructor(public httpClient: HttpClient) {
    super();
    this.setServiceInfo({
      httpClient,
      outputModelName: new OutputUserModel(),
      apiUrl: environment.apiCategoryURL + ServiceName.USER
    });
  }

  // Thêm mới một user
  public addUser(body) {
    this.setServiceInfo({
      apiUrl: environment.apiCategoryURL + ServiceName.USER + '/create-user'
    });
    return this.addItem(body);
  }

  // Đăng nhập lấy token
  public loginAdmin(body) {
    this.setServiceInfo({
      apiUrl: environment.apiCategoryURL + ServiceName.USER + '/login'
    });
    return this.addItem(body);
  }

  // Lấy thông tin user
  public getProfile() {
    this.setServiceInfo({
      apiUrl: environment.apiCategoryURL + ServiceName.USER + '/profile'
    });
    return this.getFetchAll();
  }

  // Lấy thông tin user
  public changePass(id, newpass) {
    this.setServiceInfo({
      apiUrl: environment.apiCategoryURL + ServiceName.USER + '/change-password'
    });
    return this.addItem({id: id, newpassword: newpass});

  }

  public checkBeDeleted(id: number) {
    return "ok";
  }
}
