import {Injectable} from "@angular/core";
import {RoleService} from "src/app/service/admin/hethong/role.service";
import {UserService} from "src/app/service/admin/hethong/user.service";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class HethongFacadeService {
  constructor(public httpClient: HttpClient) {
  }
  // Get role service
  public getRoleService() {
    return new RoleService(this.httpClient);
  }
  // Get user service
  public getUserService() {
    return new UserService(this.httpClient);
  }
}
