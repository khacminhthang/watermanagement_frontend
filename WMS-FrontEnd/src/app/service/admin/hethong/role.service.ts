import {Injectable} from "@angular/core";
import {RepositoryEloquentService} from "src/app/core/services/data/baserepository.service";
import {environment} from "src//environments/environment";
import {ServiceName} from "src/app/shared/constants/service-name";
import {OutputRoleModel} from "src/app/models/admin/hethong/role.model";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class RoleService extends RepositoryEloquentService {
  constructor(public httpClient: HttpClient) {
    super();
    this.setServiceInfo({
      httpClient,
      outputModelName: new OutputRoleModel(),
      apiUrl: environment.apiCategoryURL + ServiceName.ROLE
    });
  }

  public checkBeDeleted(id: number) {
    return "ok";
  }
}
