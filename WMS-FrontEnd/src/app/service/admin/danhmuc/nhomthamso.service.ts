import { Injectable } from "@angular/core";

import { environment } from "src/environments/environment";
import { ServiceName } from "src/app/shared/constants/service-name";
import { RepositoryEloquentService } from "src/app/core/services/data/baserepository.service";
import {
  InputNhomthamsoModel,
  OutputNhomthamsoModel
} from "src/app/models/admin/danhmuc/nhomthamso.model";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class NhomthamsoService extends RepositoryEloquentService {
  constructor(public httpClient: HttpClient) {
    super();
    this.setServiceInfo({
      httpClient,
      inputModelName: new InputNhomthamsoModel(),
      outputModelName: new OutputNhomthamsoModel(),
      apiUrl: environment.apiCategoryURL + ServiceName.NHOMTHAMSO
    });
  }

  public checkBeDeleted(id: number) {
    return "ok";
  }
}
