import { Injectable } from "@angular/core";

import {
  InputDuanModel,
  OutputDuanModel
} from "src/app/models/admin/danhmuc/duan.model";
import { environment } from "src/environments/environment";
import { RepositoryEloquentService } from "src/app/core/services/data/baserepository.service";
import { ServiceName } from "src/app/shared/constants/service-name";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class DuanService extends RepositoryEloquentService {
  constructor(public httpClient: HttpClient) {
    super();
    this.setServiceInfo({
      httpClient,
      inputModelName: new InputDuanModel(),
      outputModelName: new OutputDuanModel(),
      apiUrl: environment.apiCategoryURL + ServiceName.DUAN
    });
  }

  public checkBeDeleted(id: number) {
    return "ok";
  }
}
