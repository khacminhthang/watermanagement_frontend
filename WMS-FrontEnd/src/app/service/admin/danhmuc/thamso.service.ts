import { Injectable } from "@angular/core";
import {
  InputThamsoModel,
  OutputThamsoModel
} from "src/app/models/admin/danhmuc/thamso.model";
import { environment } from "src/environments/environment";
import { ServiceName } from "src/app/shared/constants/service-name";
import { RepositoryEloquentService } from "src/app/core/services/data/baserepository.service";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ThamsoService extends RepositoryEloquentService {
  constructor(public httpClient: HttpClient) {
    super();
    this.setServiceInfo({
      httpClient,
      inputModelName: new InputThamsoModel(),
      outputModelName: new OutputThamsoModel(),
      apiUrl: environment.apiCategoryURL + ServiceName.THAMSO
    });
  }

  public getThamSoByNhomThamSoId(id) {
    return this.httpClient.get(`${environment.apiCategoryURL + ServiceName.THAMSO + '/idnhomthamso/' + id}`).toPromise();
  }
  public checkBeDeleted(id: number) {
    return "ok";
  }
}
