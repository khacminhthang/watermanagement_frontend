import { Injectable } from "@angular/core";

import {
  InputCoquantochucModel,
  OutputCoquantochucModel
} from "src/app/models/admin/danhmuc/coquantochuc.model";
import { environment } from "src/environments/environment";
import { ServiceName } from "src/app/shared/constants/service-name";
import { RepositoryEloquentService } from "src/app/core/services/data/baserepository.service";
import { Observable } from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class DmCoquantochucService extends RepositoryEloquentService {
  constructor(public httpClient: HttpClient) {
    super();
    this.setServiceInfo({
      httpClient,
      inputModelName: new InputCoquantochucModel(),
      outputModelName: new OutputCoquantochucModel(),
      apiUrl: environment.apiCategoryURL + ServiceName.COQUANTOCHUC
    });
  }
  public checkBeDeleted(id: number) {
    return "ok";
  }
}
