import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { ServiceName } from "src/app/shared/constants/service-name";
import { RepositoryEloquentService } from "src/app/core/services/data/baserepository.service";
import {InputDonvidoModel, OutputDonvidoModel} from "../../../models/admin/danhmuc/donvido.model";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class DonvidoService extends RepositoryEloquentService {
  constructor(public httpClient: HttpClient) {
    super();
    this.setServiceInfo({
      httpClient,
      inputModelName: new InputDonvidoModel(),
      outputModelName: new OutputDonvidoModel(),
      apiUrl: environment.apiCategoryURL + ServiceName.DONVIDO
    });
  }

  public checkBeDeleted(id: number) {
    return "ok";
  }
}
