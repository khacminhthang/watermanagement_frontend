import { Injectable } from "@angular/core";

import { DmCanhanService } from "src/app/service/admin/danhmuc/canhan.service";
import { TieuchuanService } from "src/app/service/admin/danhmuc/tieuchuan.service";
import { ThamsoService } from "src/app/service/admin/danhmuc/thamso.service";
import { TcclService } from "src/app/service/admin/danhmuc/tccl.service";
import { LoaisolieuService } from "src/app/service/admin/danhmuc/loaisolieu.service";
import { ProvinceService } from "src/app/service/admin/common/province.service";
import { DistrictService } from "src/app/service/admin/common/district.service";
import { WardService } from "src/app/service/admin/common/ward.service";
import { NhomthamsoService } from "src/app/service/admin/danhmuc/nhomthamso.service";
import { DuanService } from "src/app/service/admin/danhmuc/duan.service";
import { DmCongtyService } from "src/app/service/admin/danhmuc/congty.service";
import { DmCoquantochucService } from "src/app/service/admin/danhmuc/coquantochuc.service";
import { GmediaService } from "src/app/service/admin/common/gmedia.service";
import { DonvidoService } from "./donvido.service";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class DmFacadeService {
  constructor(private httpClient: HttpClient) {}
  // Công ty service
  public getDmCongtyService() {
    return new DmCongtyService(this.httpClient);
  }

  // Cơ quan tổ chức service
  public getDmCoquantochucService() {
    return new DmCoquantochucService(this.httpClient);
  }
  // cá nhân service
  public getDmCanhanService() {
    return new DmCanhanService(this.httpClient);
  }

  // nhóm tham số service
  public getNhomthamsoService() {
    return new NhomthamsoService(this.httpClient);
  }

  // tiêu chuẩn chất lượng service
  public getTieuchuanchatluongService() {
    return new TcclService(this.httpClient);
  }

  // tham số service
  public getThamsoService() {
    return new ThamsoService(this.httpClient);
  }

  // tiêu chuẩn service
  public getTieuchuanService() {
    return new TieuchuanService(this.httpClient);
  }

  // loại số liệu service
  public getLoaisolieuService() {
    return new LoaisolieuService(this.httpClient);
  }

  // dự án service
  public getDuanService() {
    return new DuanService(this.httpClient);
  }

  // province service
  public getProvinceService() {
    return new ProvinceService(this.httpClient);
  }

  // district service
  public getDistrictService() {
    return new DistrictService(this.httpClient);
  }

  // ward service
  public getWardService() {
    return new WardService(this.httpClient);
  }
  // Gmedia service
  public getGmediaService() {
    return new GmediaService(this.httpClient);
  }
  // Donvido service
  public getDonvidoService() {
    return new DonvidoService(this.httpClient);
  }
}
