import { Injectable } from '@angular/core';
import {RepositoryEloquentService} from "src/app/core/services/data/baserepository.service";
import {environment} from "src/environments/environment";
import {ServiceName} from "src/app/shared/constants/service-name";
import {InputTrambomModel, OutputTrambomModel} from "src/app/models/admin/tnnuoc/trambom.model";

@Injectable({
  providedIn: 'root'
})
export class TrambomService extends RepositoryEloquentService {

  constructor() {
    super();
    this.setServiceInfo({
      inputModelName: new InputTrambomModel(),
      outputModelName: new OutputTrambomModel(),
      apiUrl: environment.apiSwUrl + ServiceName.TRAMBOM
    });
  }
  public getDataTramBom(state, param) {
    this.setServiceInfo({
      apiUrl: environment.apiSwUrl + ServiceName.TRAMBOM
    });
    return this.getDataFromServer(state, param);
  }
  public checkBeDeleted(id: number) {
    return "ok";
  }
}
