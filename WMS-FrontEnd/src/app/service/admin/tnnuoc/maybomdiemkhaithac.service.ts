import { Injectable } from "@angular/core";
import { RepositoryEloquentService } from "src/app/core/services/data/baserepository.service";
import { environment } from "src/environments/environment";
import { ServiceName } from "src/app/shared/constants/service-name";
import {InputMayBomDiemKhaiThac, OutputMayBomDiemKhaiThac} from "../../../models/admin/tnnuoc/maybomdiemkhaithac.model";

@Injectable({
  providedIn: "root"
})
export class MaybomdiemkhaithacService extends RepositoryEloquentService {
  constructor() {
    super();
    this.setServiceInfo({
      inputModelName: new InputMayBomDiemKhaiThac(),
      outputModelName: new OutputMayBomDiemKhaiThac(),
      apiUrl: environment.apiSwUrl + ServiceName.MAYBOMDIEMKHAITHAC
    });
  }

  async getListMayBomDiemKhaiThac(id) {
    this.setServiceInfo({
      apiUrl: `${environment.apiSwUrl}${ServiceName.MAYBOMDIEMKHAITHAC}/get-list/${id}`
    });
    const listData = await this.getFetchAll();
    return listData;
  }

  public checkBeDeleted(id: number) {
    return "ok";
  }
}
