import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { KhaithacnuocmatService } from "./khaithacnuocmat.service";
import { TrambomService } from "./trambom.service";
import { XanuocthaiService } from "./xanuocthai.service";
import { MaybomdiemkhaithacService } from "./maybomdiemkhaithac.service";
import { MaybomtrambomService } from "./maybomtrambom.service";
@Injectable({
  providedIn: "root",
})
export class TnnuocFacadeService {
  constructor(public httpClient: HttpClient) {}

  // Khai thác nước mặt service
  public getSwKhaithacnuocmatService() {
    return new KhaithacnuocmatService();
  }

  // Trạm bơm service
  public getSwTrambomService() {
    return new TrambomService();
  }
  // Điểm xả thải service
  public getSwXaNuocThaiService() {
    return new XanuocthaiService();
  }

  // Máy bơm điểm khai thác service
  public getSwMayBomDiemKhaiThacService() {
    return new MaybomdiemkhaithacService();
  }

  // Máy bơm trạm bơm service
  public getSwMayBomTramBomService() {
    return new MaybomtrambomService();
  }
}
