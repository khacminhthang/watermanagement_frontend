import { Injectable } from '@angular/core';
import {environment} from "src/environments/environment";
import {ServiceName} from "src/app/shared/constants/service-name";
import {RepositoryEloquentService} from "src/app/core/services/data/baserepository.service";
import {
  InputKhaithacnuocmatModel,
  OutputKhaithacnuocmatModel
} from "src/app/models/admin/tnnuoc/khaithacnuocmat.model";

@Injectable({
  providedIn: 'root'
})
export class KhaithacnuocmatService extends RepositoryEloquentService {

  constructor() {
    super();
    this.setServiceInfo({
      inputModelName: new InputKhaithacnuocmatModel(),
      outputModelName: new OutputKhaithacnuocmatModel(),
      apiUrl: environment.apiSwUrl + ServiceName.KHAITHACNUOCMAT
    });
  }
  public getDataKhaithacnuocmat(state, param) {
    this.setServiceInfo({
      apiUrl: environment.apiSwUrl + ServiceName.KHAITHACNUOCMAT
    });
    return this.getDataFromServer(state, param);
  }
  public checkBeDeleted(id: number) {
    return "ok";
  }
}
