import { Injectable } from "@angular/core";
import { RepositoryEloquentService } from "src/app/core/services/data/baserepository.service";
import { environment } from "src/environments/environment";
import { ServiceName } from "src/app/shared/constants/service-name";
import {InputMayBomTramBom, OutputMayBomTramBom} from "../../../models/admin/tnnuoc/maybomtrambom.model";

@Injectable({
  providedIn: "root"
})
export class MaybomtrambomService extends RepositoryEloquentService {
  constructor() {
    super();
    this.setServiceInfo({
      inputModelName: new InputMayBomTramBom(),
      outputModelName: new OutputMayBomTramBom(),
      apiUrl: environment.apiSwUrl + ServiceName.MAYBOMTRAMBOM
    });
  }

  async getListMayBomTramBom(id) {
    this.setServiceInfo({
      apiUrl: `${environment.apiSwUrl}${ServiceName.MAYBOMTRAMBOM}/get-list/${id}`
    });
    const listData = await this.getFetchAll();
    return listData;
  }

  public checkBeDeleted(id: number) {
    return "ok";
  }
}
