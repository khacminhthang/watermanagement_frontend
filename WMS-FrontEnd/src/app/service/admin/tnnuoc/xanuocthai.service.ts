import {Injectable} from "@angular/core";
import {RepositoryEloquentService} from "../../../core/services/data/baserepository.service";
import {environment} from "../../../../environments/environment";
import {ServiceName} from "../../../shared/constants/service-name";
import {InputXanuocthaiModel, OutputXanuocthaiModel} from "../../../models/admin/tnnuoc/xanuocthai.model";

@Injectable({
  providedIn: "root"
})

export class XanuocthaiService extends RepositoryEloquentService {
  constructor() {
    super();
    this.setServiceInfo({
      inputModelName: new InputXanuocthaiModel(),
      outputModelName: new OutputXanuocthaiModel(),
      apiUrl: environment.apiSwUrl + ServiceName.XANUOCTHAI
    });
  }
  public checkBeDeleted(id: number) {
    return "ok";
  }
}
